(function() {
  var Browse, name;

  name = require("../package.json").name;

  module.exports = Browse = {
    config: {
      fileManager: {
        title: "File manager",
        description: "Specify the full path to a custom file manager",
        type: "string",
        "default": ""
      },
      notify: {
        title: "Verbose Mode",
        description: "Show info notifications for all actions",
        type: "boolean",
        "default": false
      }
    },
    subscriptions: null,
    activate: function() {
      var CompositeDisposable, obj, obj1, obj2, obj3, obj4, obj5, obj6, obj7;
      CompositeDisposable = require("atom").CompositeDisposable;
      this.subscriptions = new CompositeDisposable;
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj = {},
        obj[name + ":.apm-folder"] = (function(_this) {
          return function() {
            return _this.apmFolder();
          };
        })(this),
        obj
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj1 = {},
        obj1[name + ":application-folder"] = (function(_this) {
          return function() {
            return _this.appFolder();
          };
        })(this),
        obj1
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj2 = {},
        obj2[name + ":configuration-folder"] = (function(_this) {
          return function() {
            return _this.browseConfig();
          };
        })(this),
        obj2
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj3 = {},
        obj3[name + ":packages-folder"] = (function(_this) {
          return function() {
            return _this.browsePackages();
          };
        })(this),
        obj3
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj4 = {},
        obj4[name + ":project-folders"] = (function(_this) {
          return function() {
            return _this.browseProjects();
          };
        })(this),
        obj4
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj5 = {},
        obj5[name + ":reveal-all-open-files"] = (function(_this) {
          return function() {
            return _this.revealFiles();
          };
        })(this),
        obj5
      )));
      this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj6 = {},
        obj6[name + ":reveal-file"] = (function(_this) {
          return function() {
            return _this.revealFile();
          };
        })(this),
        obj6
      )));
      return this.subscriptions.add(atom.commands.add("atom-workspace", (
        obj7 = {},
        obj7[name + ":reveal-file-from-tree-view"] = (function(_this) {
          return function() {
            return _this.revealFileFromTreeview();
          };
        })(this),
        obj7
      )));
    },
    deactivate: function() {
      var ref;
      if ((ref = this.subscriptions) != null) {
        ref.dispose();
      }
      return this.subscriptions = null;
    },
    apmFolder: function() {
      var apmPath, configFile, configPath, dirname, join, ref;
      require("./ga").sendEvent(name, "apm-folder");
      ref = require("path"), dirname = ref.dirname, join = ref.join;
      configFile = atom.config.getUserConfigPath();
      configPath = dirname(configFile);
      apmPath = join(configPath, '.apm');
      if (apmPath) {
        return this.openFolder(apmPath);
      }
    },
    appFolder: function() {
      var appFolder, dirname, join, platform, processBin, processPath, ref, resolve;
      require("./ga").sendEvent(name, "application-folder");
      platform = require("os").platform;
      ref = require("path"), dirname = ref.dirname, join = ref.join, resolve = ref.resolve;
      processBin = resolve(process.execPath);
      processPath = dirname(processBin);
      switch (platform()) {
        case "darwin":
          appFolder = join(processPath, "..", "..", "..", "..");
          break;
        default:
          appFolder = processPath;
      }
      if (appFolder) {
        return this.openFolder(appFolder);
      }
    },
    browsePackages: function() {
      var i, len, packageDir, packageDirs, results;
      require("./ga").sendEvent(name, "packages-folder");
      packageDirs = atom.packages.getPackageDirPaths();
      results = [];
      for (i = 0, len = packageDirs.length; i < len; i++) {
        packageDir = packageDirs[i];
        results.push(this.openFolder(packageDir));
      }
      return results;
    },
    revealFile: function() {
      var editor, file, ref;
      require("./ga").sendEvent(name, "reveal-file");
      editor = atom.workspace.getActivePaneItem();
      if ((editor != null ? editor.constructor.name : void 0) === "TextEditor" || (editor != null ? editor.constructor.name : void 0) === "ImageEditor") {
        file = (editor != null ? (ref = editor.buffer) != null ? ref.file : void 0 : void 0) ? editor.buffer.file : (editor != null ? editor.file : void 0) ? editor.file : void 0;
        if (file != null ? file.path : void 0) {
          this.selectFile(file.path);
          return;
        }
      }
      return atom.notifications.addWarning("**" + name + "**: No active file", {
        dismissable: false
      });
    },
    revealFiles: function() {
      var count, editor, editors, file, i, len, ref;
      require("./ga").sendEvent(name, "reveal-all-open-files");
      editors = atom.workspace.getPaneItems();
      if (editors.length > 0) {
        count = 0;
        for (i = 0, len = editors.length; i < len; i++) {
          editor = editors[i];
          if (!(editor.constructor.name === "TextEditor" || editor.constructor.name === "ImageEditor")) {
            continue;
          }
          file = (editor != null ? (ref = editor.buffer) != null ? ref.file : void 0 : void 0) ? editor.buffer.file : (editor != null ? editor.file : void 0) ? editor.file : void 0;
          if (file != null ? file.path : void 0) {
            this.selectFile(file.path);
            count++;
          }
        }
        if (count > 0) {
          return;
        }
      }
      return atom.notifications.addWarning("**" + name + "**: No open files", {
        dismissable: false
      });
    },
    revealFileFromTreeview: function() {
      var count, file, i, len, pane, panes;
      require("./ga").sendEvent(name, "reveal-file-from-tree-view");
      panes = atom.workspace.getPaneItems();
      if (panes.length > 0) {
        count = 0;
        for (i = 0, len = panes.length; i < len; i++) {
          pane = panes[i];
          if (pane.constructor.name !== "TreeView") {
            continue;
          }
          file = pane.selectedPath;
          if (file != null) {
            this.selectFile(file);
            return;
          }
        }
        if (count > 0) {
          return;
        }
      }
      return atom.notifications.addWarning("**" + name + "**: No selected files", {
        dismissable: false
      });
    },
    browseProjects: function() {
      var i, len, projectPath, projectPaths, results;
      require("./ga").sendEvent(name, "project-folders");
      projectPaths = atom.project.getPaths();
      if (!(projectPaths.length > 0)) {
        return atom.notifications.addWarning("**" + name + "**: No active project", {
          dismissable: false
        });
      }
      results = [];
      for (i = 0, len = projectPaths.length; i < len; i++) {
        projectPath = projectPaths[i];
        if (projectPath.startsWith('atom://')) {
          continue;
        }
        results.push(this.openFolder(projectPath));
      }
      return results;
    },
    browseConfig: function() {
      var configFile, configPath, dirname;
      require("./ga").sendEvent(name, "configuration-folder");
      dirname = require("path").dirname;
      configFile = atom.config.getUserConfigPath();
      configPath = dirname(configFile);
      if (configPath) {
        return this.openFolder(configPath);
      }
    },
    selectFile: function(path) {
      var basename, fileManager, shell;
      require("./ga").sendEvent(name, "configuration-folder");
      basename = require("path").basename;
      fileManager = atom.config.get(name + ".fileManager");
      if (fileManager) {
        return this.spawnCmd(fileManager, [path], basename(path), "file manager");
      }
      switch (process.platform) {
        case "darwin":
          return this.spawnCmd("open", ["-R", path], basename(path), "Finder");
        case "win32":
          return this.spawnCmd("explorer", ["/select," + path], basename(path), "Explorer");
        case "linux":
          shell = require("electron").shell;
          shell.showItemInFolder(path);
          if (atom.config.get(name + ".notify")) {
            return atom.notifications.addInfo("**" + name + "**: Opening `" + (basename(path)) + "` in file manager", {
              dismissable: false
            });
          }
      }
    },
    openFolder: function(path) {
      var F_OK, access, basename, ref;
      ref = require("fs"), access = ref.access, F_OK = ref.F_OK;
      basename = require("path").basename;
      return access(path, F_OK, function(err) {
        var fileManager, shell;
        if (err) {
          return atom.notifications.addError(name, {
            detail: error,
            dismissable: true
          });
        }
        fileManager = atom.config.get(name + ".fileManager");
        if (fileManager) {
          return Browse.spawnCmd(fileManager, [path], basename(path), "file manager");
        }
        switch (process.platform) {
          case "darwin":
            return Browse.spawnCmd("open", [path], basename(path), "Finder");
          case "win32":
            return Browse.spawnCmd("explorer", [path], basename(path), "Explorer");
          case "linux":
            shell = require("electron").shell;
            shell.openItem(path);
            if (atom.config.get(name + ".notify")) {
              return atom.notifications.addInfo("**" + name + "**: Opening `" + (basename(path)) + "` in file manager", {
                dismissable: false
              });
            }
        }
      });
    },
    spawnCmd: function(cmd, args, baseName, fileManager) {
      var open, spawn;
      spawn = require("child_process").spawn;
      open = spawn(cmd, args);
      open.stderr.on("data", function(error) {
        return atom.notifications.addError("**" + name + "**: " + error, {
          dismissable: true
        });
      });
      return open.on("close", function(errorCode) {
        if (errorCode === 0 && atom.config.get(name + ".notify")) {
          return atom.notifications.addInfo("**" + name + "**: Opening `" + baseName + "` in " + fileManager, {
            dismissable: false
          });
        }
      });
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9icm93c2UvbGliL2Jyb3dzZS5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFFLE9BQVMsT0FBQSxDQUFRLGlCQUFSOztFQUVYLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLE1BQUEsR0FDZjtJQUFBLE1BQUEsRUFDRTtNQUFBLFdBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxjQUFQO1FBQ0EsV0FBQSxFQUFhLGdEQURiO1FBRUEsSUFBQSxFQUFNLFFBRk47UUFHQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLEVBSFQ7T0FERjtNQUtBLE1BQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxjQUFQO1FBQ0EsV0FBQSxFQUFhLHlDQURiO1FBRUEsSUFBQSxFQUFNLFNBRk47UUFHQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLEtBSFQ7T0FORjtLQURGO0lBV0EsYUFBQSxFQUFlLElBWGY7SUFhQSxRQUFBLEVBQVUsU0FBQTtBQUVSLFVBQUE7TUFBRSxzQkFBd0IsT0FBQSxDQUFRLE1BQVI7TUFDMUIsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtNQUdyQixJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQztjQUFBLEVBQUE7WUFBRyxJQUFELEdBQU0sa0JBQWUsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsU0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZCOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSx5QkFBc0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsU0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTlCOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSwyQkFBd0IsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsWUFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhDOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSxzQkFBbUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsY0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNCOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSxzQkFBbUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsY0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTNCOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSw0QkFBeUIsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsV0FBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDOztPQUFwQyxDQUFuQjtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DO2VBQUEsRUFBQTthQUFHLElBQUQsR0FBTSxrQkFBZSxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxVQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdkI7O09BQXBDLENBQW5CO2FBQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0M7ZUFBQSxFQUFBO2FBQUcsSUFBRCxHQUFNLGlDQUE4QixDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxzQkFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXRDOztPQUFwQyxDQUFuQjtJQWJRLENBYlY7SUE0QkEsVUFBQSxFQUFZLFNBQUE7QUFDVixVQUFBOztXQUFjLENBQUUsT0FBaEIsQ0FBQTs7YUFDQSxJQUFDLENBQUEsYUFBRCxHQUFpQjtJQUZQLENBNUJaO0lBZ0NBLFNBQUEsRUFBVyxTQUFBO0FBQ1QsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxZQUFoQztNQUVBLE1BQW9CLE9BQUEsQ0FBUSxNQUFSLENBQXBCLEVBQUUscUJBQUYsRUFBVztNQUVYLFVBQUEsR0FBYSxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFaLENBQUE7TUFDYixVQUFBLEdBQWEsT0FBQSxDQUFRLFVBQVI7TUFDYixPQUFBLEdBQVUsSUFBQSxDQUFLLFVBQUwsRUFBaUIsTUFBakI7TUFFVixJQUF3QixPQUF4QjtlQUFBLElBQUMsQ0FBQSxVQUFELENBQVksT0FBWixFQUFBOztJQVRTLENBaENYO0lBMkNBLFNBQUEsRUFBVyxTQUFBO0FBQ1QsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxvQkFBaEM7TUFFRSxXQUFhLE9BQUEsQ0FBUSxJQUFSO01BQ2YsTUFBNkIsT0FBQSxDQUFRLE1BQVIsQ0FBN0IsRUFBRSxxQkFBRixFQUFXLGVBQVgsRUFBaUI7TUFFakIsVUFBQSxHQUFhLE9BQUEsQ0FBUSxPQUFPLENBQUMsUUFBaEI7TUFDYixXQUFBLEdBQWMsT0FBQSxDQUFRLFVBQVI7QUFFZCxjQUFPLFFBQUEsQ0FBQSxDQUFQO0FBQUEsYUFDTyxRQURQO1VBRUksU0FBQSxHQUFZLElBQUEsQ0FBSyxXQUFMLEVBQWtCLElBQWxCLEVBQXdCLElBQXhCLEVBQThCLElBQTlCLEVBQW9DLElBQXBDO0FBRFQ7QUFEUDtVQUlJLFNBQUEsR0FBWTtBQUpoQjtNQU1BLElBQTBCLFNBQTFCO2VBQUEsSUFBQyxDQUFBLFVBQUQsQ0FBWSxTQUFaLEVBQUE7O0lBZlMsQ0EzQ1g7SUE0REEsY0FBQSxFQUFnQixTQUFBO0FBQ2QsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxpQkFBaEM7TUFHQSxXQUFBLEdBQWMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBZCxDQUFBO0FBRWQ7V0FBQSw2Q0FBQTs7cUJBQ0UsSUFBQyxDQUFBLFVBQUQsQ0FBWSxVQUFaO0FBREY7O0lBTmMsQ0E1RGhCO0lBcUVBLFVBQUEsRUFBWSxTQUFBO0FBQ1YsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxhQUFoQztNQUVBLE1BQUEsR0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFmLENBQUE7TUFFVCxzQkFBRyxNQUFNLENBQUUsV0FBVyxDQUFDLGNBQXBCLEtBQTRCLFlBQTVCLHNCQUE0QyxNQUFNLENBQUUsV0FBVyxDQUFDLGNBQXBCLEtBQTRCLGFBQTNFO1FBQ0UsSUFBQSx3REFBd0IsQ0FBRSx1QkFBbkIsR0FBNkIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUEzQyxxQkFBd0QsTUFBTSxDQUFFLGNBQVgsR0FBcUIsTUFBTSxDQUFDLElBQTVCLEdBQUE7UUFFNUQsbUJBQUcsSUFBSSxDQUFFLGFBQVQ7VUFDRSxJQUFDLENBQUEsVUFBRCxDQUFZLElBQUksQ0FBQyxJQUFqQjtBQUNBLGlCQUZGO1NBSEY7O2FBT0EsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixJQUFBLEdBQUssSUFBTCxHQUFVLG9CQUF4QyxFQUE2RDtRQUFBLFdBQUEsRUFBYSxLQUFiO09BQTdEO0lBWlUsQ0FyRVo7SUFtRkEsV0FBQSxFQUFhLFNBQUE7QUFDWCxVQUFBO01BQUEsT0FBQSxDQUFRLE1BQVIsQ0FBZSxDQUFDLFNBQWhCLENBQTBCLElBQTFCLEVBQWdDLHVCQUFoQztNQUVBLE9BQUEsR0FBVSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQWYsQ0FBQTtNQUVWLElBQUcsT0FBTyxDQUFDLE1BQVIsR0FBaUIsQ0FBcEI7UUFDRSxLQUFBLEdBQVE7QUFDUixhQUFBLHlDQUFBOztVQUNFLElBQUEsQ0FBQSxDQUFnQixNQUFNLENBQUMsV0FBVyxDQUFDLElBQW5CLEtBQTJCLFlBQTNCLElBQTJDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBbkIsS0FBMkIsYUFBdEYsQ0FBQTtBQUFBLHFCQUFBOztVQUVBLElBQUEsd0RBQXdCLENBQUUsdUJBQW5CLEdBQTZCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBM0MscUJBQXdELE1BQU0sQ0FBRSxjQUFYLEdBQXFCLE1BQU0sQ0FBQyxJQUE1QixHQUFBO1VBRTVELG1CQUFHLElBQUksQ0FBRSxhQUFUO1lBQ0UsSUFBQyxDQUFBLFVBQUQsQ0FBWSxJQUFJLENBQUMsSUFBakI7WUFDQSxLQUFBLEdBRkY7O0FBTEY7UUFTQSxJQUFVLEtBQUEsR0FBUSxDQUFsQjtBQUFBLGlCQUFBO1NBWEY7O2FBYUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixJQUFBLEdBQUssSUFBTCxHQUFVLG1CQUF4QyxFQUE0RDtRQUFBLFdBQUEsRUFBYSxLQUFiO09BQTVEO0lBbEJXLENBbkZiO0lBdUdBLHNCQUFBLEVBQXdCLFNBQUE7QUFDdEIsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyw0QkFBaEM7TUFFQSxLQUFBLEdBQVEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFmLENBQUE7TUFFUixJQUFHLEtBQUssQ0FBQyxNQUFOLEdBQWUsQ0FBbEI7UUFDRSxLQUFBLEdBQVE7QUFDUixhQUFBLHVDQUFBOztVQUNFLElBQWdCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBakIsS0FBeUIsVUFBekM7QUFBQSxxQkFBQTs7VUFFQSxJQUFBLEdBQU8sSUFBSSxDQUFDO1VBRVosSUFBRyxZQUFIO1lBQ0UsSUFBQyxDQUFBLFVBQUQsQ0FBWSxJQUFaO0FBQ0EsbUJBRkY7O0FBTEY7UUFTQSxJQUFVLEtBQUEsR0FBUSxDQUFsQjtBQUFBLGlCQUFBO1NBWEY7O2FBYUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixJQUFBLEdBQUssSUFBTCxHQUFVLHVCQUF4QyxFQUFnRTtRQUFBLFdBQUEsRUFBYSxLQUFiO09BQWhFO0lBbEJzQixDQXZHeEI7SUEySEEsY0FBQSxFQUFnQixTQUFBO0FBQ2QsVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxpQkFBaEM7TUFHQSxZQUFBLEdBQWUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFiLENBQUE7TUFDZixJQUFBLENBQUEsQ0FBa0csWUFBWSxDQUFDLE1BQWIsR0FBc0IsQ0FBeEgsQ0FBQTtBQUFBLGVBQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixJQUFBLEdBQUssSUFBTCxHQUFVLHVCQUF4QyxFQUFnRTtVQUFBLFdBQUEsRUFBYSxLQUFiO1NBQWhFLEVBQVA7O0FBRUE7V0FBQSw4Q0FBQTs7UUFFRSxJQUFZLFdBQVcsQ0FBQyxVQUFaLENBQXVCLFNBQXZCLENBQVo7QUFBQSxtQkFBQTs7cUJBRUEsSUFBQyxDQUFBLFVBQUQsQ0FBWSxXQUFaO0FBSkY7O0lBUGMsQ0EzSGhCO0lBd0lBLFlBQUEsRUFBYyxTQUFBO0FBQ1osVUFBQTtNQUFBLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixJQUExQixFQUFnQyxzQkFBaEM7TUFFRSxVQUFZLE9BQUEsQ0FBUSxNQUFSO01BRWQsVUFBQSxHQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQVosQ0FBQTtNQUNiLFVBQUEsR0FBYSxPQUFBLENBQVEsVUFBUjtNQUViLElBQTJCLFVBQTNCO2VBQUEsSUFBQyxDQUFBLFVBQUQsQ0FBWSxVQUFaLEVBQUE7O0lBUlksQ0F4SWQ7SUFrSkEsVUFBQSxFQUFZLFNBQUMsSUFBRDtBQUNWLFVBQUE7TUFBQSxPQUFBLENBQVEsTUFBUixDQUFlLENBQUMsU0FBaEIsQ0FBMEIsSUFBMUIsRUFBZ0Msc0JBQWhDO01BRUUsV0FBYSxPQUFBLENBQVEsTUFBUjtNQUdmLFdBQUEsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBbUIsSUFBRCxHQUFNLGNBQXhCO01BQ2QsSUFBMEUsV0FBMUU7QUFBQSxlQUFPLElBQUMsQ0FBQSxRQUFELENBQVUsV0FBVixFQUF1QixDQUFFLElBQUYsQ0FBdkIsRUFBaUMsUUFBQSxDQUFTLElBQVQsQ0FBakMsRUFBaUQsY0FBakQsRUFBUDs7QUFHQSxjQUFPLE9BQU8sQ0FBQyxRQUFmO0FBQUEsYUFDTyxRQURQO2lCQUVJLElBQUMsQ0FBQSxRQUFELENBQVUsTUFBVixFQUFrQixDQUFFLElBQUYsRUFBUSxJQUFSLENBQWxCLEVBQWtDLFFBQUEsQ0FBUyxJQUFULENBQWxDLEVBQWtELFFBQWxEO0FBRkosYUFHTyxPQUhQO2lCQUlJLElBQUMsQ0FBQSxRQUFELENBQVUsVUFBVixFQUFzQixDQUFFLFVBQUEsR0FBVyxJQUFiLENBQXRCLEVBQTZDLFFBQUEsQ0FBUyxJQUFULENBQTdDLEVBQTZELFVBQTdEO0FBSkosYUFLTyxPQUxQO1VBTU0sUUFBVSxPQUFBLENBQVEsVUFBUjtVQUNaLEtBQUssQ0FBQyxnQkFBTixDQUF1QixJQUF2QjtVQUNBLElBQThHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFELEdBQU0sU0FBeEIsQ0FBOUc7bUJBQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFuQixDQUEyQixJQUFBLEdBQUssSUFBTCxHQUFVLGVBQVYsR0FBd0IsQ0FBQyxRQUFBLENBQVMsSUFBVCxDQUFELENBQXhCLEdBQXdDLG1CQUFuRSxFQUF1RjtjQUFBLFdBQUEsRUFBYSxLQUFiO2FBQXZGLEVBQUE7O0FBUko7SUFWVSxDQWxKWjtJQXNLQSxVQUFBLEVBQVksU0FBQyxJQUFEO0FBQ1YsVUFBQTtNQUFBLE1BQW1CLE9BQUEsQ0FBUSxJQUFSLENBQW5CLEVBQUUsbUJBQUYsRUFBVTtNQUNSLFdBQWEsT0FBQSxDQUFRLE1BQVI7YUFFZixNQUFBLENBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUIsU0FBQyxHQUFEO0FBQ2pCLFlBQUE7UUFBQSxJQUE4RSxHQUE5RTtBQUFBLGlCQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBbkIsQ0FBNEIsSUFBNUIsRUFBa0M7WUFBQSxNQUFBLEVBQVEsS0FBUjtZQUFlLFdBQUEsRUFBYSxJQUE1QjtXQUFsQyxFQUFQOztRQUdBLFdBQUEsR0FBYyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBbUIsSUFBRCxHQUFNLGNBQXhCO1FBQ2QsSUFBZ0YsV0FBaEY7QUFBQSxpQkFBTyxNQUFNLENBQUMsUUFBUCxDQUFnQixXQUFoQixFQUE2QixDQUFFLElBQUYsQ0FBN0IsRUFBdUMsUUFBQSxDQUFTLElBQVQsQ0FBdkMsRUFBdUQsY0FBdkQsRUFBUDs7QUFHQSxnQkFBTyxPQUFPLENBQUMsUUFBZjtBQUFBLGVBQ08sUUFEUDttQkFFSSxNQUFNLENBQUMsUUFBUCxDQUFnQixNQUFoQixFQUF3QixDQUFFLElBQUYsQ0FBeEIsRUFBa0MsUUFBQSxDQUFTLElBQVQsQ0FBbEMsRUFBa0QsUUFBbEQ7QUFGSixlQUdPLE9BSFA7bUJBSUksTUFBTSxDQUFDLFFBQVAsQ0FBZ0IsVUFBaEIsRUFBNEIsQ0FBRSxJQUFGLENBQTVCLEVBQXNDLFFBQUEsQ0FBUyxJQUFULENBQXRDLEVBQXNELFVBQXREO0FBSkosZUFLTyxPQUxQO1lBTU0sUUFBVSxPQUFBLENBQVEsVUFBUjtZQUNaLEtBQUssQ0FBQyxRQUFOLENBQWUsSUFBZjtZQUNBLElBQThHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFELEdBQU0sU0FBeEIsQ0FBOUc7cUJBQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFuQixDQUEyQixJQUFBLEdBQUssSUFBTCxHQUFVLGVBQVYsR0FBd0IsQ0FBQyxRQUFBLENBQVMsSUFBVCxDQUFELENBQXhCLEdBQXdDLG1CQUFuRSxFQUF1RjtnQkFBQSxXQUFBLEVBQWEsS0FBYjtlQUF2RixFQUFBOztBQVJKO01BUmlCLENBQW5CO0lBSlUsQ0F0S1o7SUE0TEEsUUFBQSxFQUFVLFNBQUMsR0FBRCxFQUFNLElBQU4sRUFBWSxRQUFaLEVBQXNCLFdBQXRCO0FBQ1IsVUFBQTtNQUFFLFFBQVUsT0FBQSxDQUFRLGVBQVI7TUFFWixJQUFBLEdBQU8sS0FBQSxDQUFNLEdBQU4sRUFBVyxJQUFYO01BRVAsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFaLENBQWUsTUFBZixFQUF1QixTQUFDLEtBQUQ7ZUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFuQixDQUE0QixJQUFBLEdBQUssSUFBTCxHQUFVLE1BQVYsR0FBZ0IsS0FBNUMsRUFBcUQ7VUFBQSxXQUFBLEVBQWEsSUFBYjtTQUFyRDtNQURxQixDQUF2QjthQUdBLElBQUksQ0FBQyxFQUFMLENBQVEsT0FBUixFQUFpQixTQUFFLFNBQUY7UUFDZixJQUFHLFNBQUEsS0FBYSxDQUFiLElBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFELEdBQU0sU0FBeEIsQ0FBdEI7aUJBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFuQixDQUEyQixJQUFBLEdBQUssSUFBTCxHQUFVLGVBQVYsR0FBeUIsUUFBekIsR0FBa0MsT0FBbEMsR0FBeUMsV0FBcEUsRUFBbUY7WUFBQSxXQUFBLEVBQWEsS0FBYjtXQUFuRixFQURGOztNQURlLENBQWpCO0lBUlEsQ0E1TFY7O0FBSEYiLCJzb3VyY2VzQ29udGVudCI6WyJ7IG5hbWUgfSA9IHJlcXVpcmUgXCIuLi9wYWNrYWdlLmpzb25cIlxuXG5tb2R1bGUuZXhwb3J0cyA9IEJyb3dzZSA9XG4gIGNvbmZpZzpcbiAgICBmaWxlTWFuYWdlcjpcbiAgICAgIHRpdGxlOiBcIkZpbGUgbWFuYWdlclwiXG4gICAgICBkZXNjcmlwdGlvbjogXCJTcGVjaWZ5IHRoZSBmdWxsIHBhdGggdG8gYSBjdXN0b20gZmlsZSBtYW5hZ2VyXCJcbiAgICAgIHR5cGU6IFwic3RyaW5nXCJcbiAgICAgIGRlZmF1bHQ6IFwiXCJcbiAgICBub3RpZnk6XG4gICAgICB0aXRsZTogXCJWZXJib3NlIE1vZGVcIlxuICAgICAgZGVzY3JpcHRpb246IFwiU2hvdyBpbmZvIG5vdGlmaWNhdGlvbnMgZm9yIGFsbCBhY3Rpb25zXCJcbiAgICAgIHR5cGU6IFwiYm9vbGVhblwiXG4gICAgICBkZWZhdWx0OiBmYWxzZVxuICBzdWJzY3JpcHRpb25zOiBudWxsXG5cbiAgYWN0aXZhdGU6IC0+XG4gICAgIyBFdmVudHMgc3Vic2NyaWJlZCB0byBpbiBBdG9tJ3Mgc3lzdGVtIGNhbiBiZSBlYXNpbHkgY2xlYW5lZCB1cCB3aXRoIGEgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIHsgQ29tcG9zaXRlRGlzcG9zYWJsZSB9ID0gcmVxdWlyZSBcImF0b21cIlxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgICMgUmVnaXN0ZXIgY29tbWFuZCB0aGF0IHRvZ2dsZXMgdGhpcyB2aWV3XG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkIFwiYXRvbS13b3Jrc3BhY2VcIiwgXCIje25hbWV9Oi5hcG0tZm9sZGVyXCI6ID0+IEBhcG1Gb2xkZXIoKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCBcImF0b20td29ya3NwYWNlXCIsIFwiI3tuYW1lfTphcHBsaWNhdGlvbi1mb2xkZXJcIjogPT4gQGFwcEZvbGRlcigpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkIFwiYXRvbS13b3Jrc3BhY2VcIiwgXCIje25hbWV9OmNvbmZpZ3VyYXRpb24tZm9sZGVyXCI6ID0+IEBicm93c2VDb25maWcoKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCBcImF0b20td29ya3NwYWNlXCIsIFwiI3tuYW1lfTpwYWNrYWdlcy1mb2xkZXJcIjogPT4gQGJyb3dzZVBhY2thZ2VzKClcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgXCJhdG9tLXdvcmtzcGFjZVwiLCBcIiN7bmFtZX06cHJvamVjdC1mb2xkZXJzXCI6ID0+IEBicm93c2VQcm9qZWN0cygpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkIFwiYXRvbS13b3Jrc3BhY2VcIiwgXCIje25hbWV9OnJldmVhbC1hbGwtb3Blbi1maWxlc1wiOiA9PiBAcmV2ZWFsRmlsZXMoKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCBcImF0b20td29ya3NwYWNlXCIsIFwiI3tuYW1lfTpyZXZlYWwtZmlsZVwiOiA9PiBAcmV2ZWFsRmlsZSgpXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkIFwiYXRvbS13b3Jrc3BhY2VcIiwgXCIje25hbWV9OnJldmVhbC1maWxlLWZyb20tdHJlZS12aWV3XCI6ID0+IEByZXZlYWxGaWxlRnJvbVRyZWV2aWV3KClcblxuICBkZWFjdGl2YXRlOiAtPlxuICAgIEBzdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcbiAgICBAc3Vic2NyaXB0aW9ucyA9IG51bGxcblxuICBhcG1Gb2xkZXI6IC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwiYXBtLWZvbGRlclwiXG5cbiAgICB7IGRpcm5hbWUsIGpvaW4gfSA9IHJlcXVpcmUgXCJwYXRoXCJcblxuICAgIGNvbmZpZ0ZpbGUgPSBhdG9tLmNvbmZpZy5nZXRVc2VyQ29uZmlnUGF0aCgpXG4gICAgY29uZmlnUGF0aCA9IGRpcm5hbWUoY29uZmlnRmlsZSlcbiAgICBhcG1QYXRoID0gam9pbihjb25maWdQYXRoLCAnLmFwbScpXG5cbiAgICBAb3BlbkZvbGRlcihhcG1QYXRoKSBpZiBhcG1QYXRoXG5cbiAgYXBwRm9sZGVyOiAtPlxuICAgIHJlcXVpcmUoXCIuL2dhXCIpLnNlbmRFdmVudCBuYW1lLCBcImFwcGxpY2F0aW9uLWZvbGRlclwiXG5cbiAgICB7IHBsYXRmb3JtIH0gPSByZXF1aXJlIFwib3NcIlxuICAgIHsgZGlybmFtZSwgam9pbiwgcmVzb2x2ZSB9ID0gcmVxdWlyZSBcInBhdGhcIlxuXG4gICAgcHJvY2Vzc0JpbiA9IHJlc29sdmUgcHJvY2Vzcy5leGVjUGF0aFxuICAgIHByb2Nlc3NQYXRoID0gZGlybmFtZSBwcm9jZXNzQmluXG5cbiAgICBzd2l0Y2ggcGxhdGZvcm0oKVxuICAgICAgd2hlbiBcImRhcndpblwiXG4gICAgICAgIGFwcEZvbGRlciA9IGpvaW4ocHJvY2Vzc1BhdGgsIFwiLi5cIiwgXCIuLlwiLCBcIi4uXCIsIFwiLi5cIilcbiAgICAgIGVsc2VcbiAgICAgICAgYXBwRm9sZGVyID0gcHJvY2Vzc1BhdGhcblxuICAgIEBvcGVuRm9sZGVyKGFwcEZvbGRlcikgaWYgYXBwRm9sZGVyXG5cbiAgYnJvd3NlUGFja2FnZXM6IC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwicGFja2FnZXMtZm9sZGVyXCJcblxuXG4gICAgcGFja2FnZURpcnMgPSBhdG9tLnBhY2thZ2VzLmdldFBhY2thZ2VEaXJQYXRocygpXG5cbiAgICBmb3IgcGFja2FnZURpciBpbiBwYWNrYWdlRGlyc1xuICAgICAgQG9wZW5Gb2xkZXIocGFja2FnZURpcilcblxuICByZXZlYWxGaWxlOiAtPlxuICAgIHJlcXVpcmUoXCIuL2dhXCIpLnNlbmRFdmVudCBuYW1lLCBcInJldmVhbC1maWxlXCJcblxuICAgIGVkaXRvciA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVBhbmVJdGVtKClcblxuICAgIGlmIGVkaXRvcj8uY29uc3RydWN0b3IubmFtZSBpcyBcIlRleHRFZGl0b3JcIiBvciBlZGl0b3I/LmNvbnN0cnVjdG9yLm5hbWUgaXMgXCJJbWFnZUVkaXRvclwiXG4gICAgICBmaWxlID0gaWYgZWRpdG9yPy5idWZmZXI/LmZpbGUgdGhlbiBlZGl0b3IuYnVmZmVyLmZpbGUgZWxzZSBpZiBlZGl0b3I/LmZpbGUgdGhlbiBlZGl0b3IuZmlsZVxuXG4gICAgICBpZiBmaWxlPy5wYXRoXG4gICAgICAgIEBzZWxlY3RGaWxlKGZpbGUucGF0aClcbiAgICAgICAgcmV0dXJuXG5cbiAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhcIioqI3tuYW1lfSoqOiBObyBhY3RpdmUgZmlsZVwiLCBkaXNtaXNzYWJsZTogZmFsc2UpXG5cbiAgcmV2ZWFsRmlsZXM6IC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwicmV2ZWFsLWFsbC1vcGVuLWZpbGVzXCJcblxuICAgIGVkaXRvcnMgPSBhdG9tLndvcmtzcGFjZS5nZXRQYW5lSXRlbXMoKVxuXG4gICAgaWYgZWRpdG9ycy5sZW5ndGggPiAwXG4gICAgICBjb3VudCA9IDBcbiAgICAgIGZvciBlZGl0b3IgaW4gZWRpdG9yc1xuICAgICAgICBjb250aW51ZSB1bmxlc3MgZWRpdG9yLmNvbnN0cnVjdG9yLm5hbWUgaXMgXCJUZXh0RWRpdG9yXCIgb3IgZWRpdG9yLmNvbnN0cnVjdG9yLm5hbWUgaXMgXCJJbWFnZUVkaXRvclwiXG5cbiAgICAgICAgZmlsZSA9IGlmIGVkaXRvcj8uYnVmZmVyPy5maWxlIHRoZW4gZWRpdG9yLmJ1ZmZlci5maWxlIGVsc2UgaWYgZWRpdG9yPy5maWxlIHRoZW4gZWRpdG9yLmZpbGVcblxuICAgICAgICBpZiBmaWxlPy5wYXRoXG4gICAgICAgICAgQHNlbGVjdEZpbGUoZmlsZS5wYXRoKVxuICAgICAgICAgIGNvdW50KytcblxuICAgICAgcmV0dXJuIGlmIGNvdW50ID4gMFxuXG4gICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCIqKiN7bmFtZX0qKjogTm8gb3BlbiBmaWxlc1wiLCBkaXNtaXNzYWJsZTogZmFsc2UpXG5cbiAgcmV2ZWFsRmlsZUZyb21UcmVldmlldzogLT5cbiAgICByZXF1aXJlKFwiLi9nYVwiKS5zZW5kRXZlbnQgbmFtZSwgXCJyZXZlYWwtZmlsZS1mcm9tLXRyZWUtdmlld1wiXG5cbiAgICBwYW5lcyA9IGF0b20ud29ya3NwYWNlLmdldFBhbmVJdGVtcygpXG5cbiAgICBpZiBwYW5lcy5sZW5ndGggPiAwXG4gICAgICBjb3VudCA9IDBcbiAgICAgIGZvciBwYW5lIGluIHBhbmVzXG4gICAgICAgIGNvbnRpbnVlIHVubGVzcyBwYW5lLmNvbnN0cnVjdG9yLm5hbWUgaXMgXCJUcmVlVmlld1wiXG5cbiAgICAgICAgZmlsZSA9IHBhbmUuc2VsZWN0ZWRQYXRoXG5cbiAgICAgICAgaWYgZmlsZT9cbiAgICAgICAgICBAc2VsZWN0RmlsZShmaWxlKVxuICAgICAgICAgIHJldHVyblxuXG4gICAgICByZXR1cm4gaWYgY291bnQgPiAwXG5cbiAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhcIioqI3tuYW1lfSoqOiBObyBzZWxlY3RlZCBmaWxlc1wiLCBkaXNtaXNzYWJsZTogZmFsc2UpXG5cbiAgYnJvd3NlUHJvamVjdHM6IC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwicHJvamVjdC1mb2xkZXJzXCJcblxuXG4gICAgcHJvamVjdFBhdGhzID0gYXRvbS5wcm9qZWN0LmdldFBhdGhzKClcbiAgICByZXR1cm4gYXRvbS5ub3RpZmljYXRpb25zLmFkZFdhcm5pbmcoXCIqKiN7bmFtZX0qKjogTm8gYWN0aXZlIHByb2plY3RcIiwgZGlzbWlzc2FibGU6IGZhbHNlKSB1bmxlc3MgcHJvamVjdFBhdGhzLmxlbmd0aCA+IDBcblxuICAgIGZvciBwcm9qZWN0UGF0aCBpbiBwcm9qZWN0UGF0aHNcbiAgICAgICMgU2tpcCBBdG9tIGRpYWxvZ3NcbiAgICAgIGNvbnRpbnVlIGlmIHByb2plY3RQYXRoLnN0YXJ0c1dpdGgoJ2F0b206Ly8nKVxuXG4gICAgICBAb3BlbkZvbGRlcihwcm9qZWN0UGF0aClcblxuICBicm93c2VDb25maWc6IC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwiY29uZmlndXJhdGlvbi1mb2xkZXJcIlxuXG4gICAgeyBkaXJuYW1lIH0gPSByZXF1aXJlIFwicGF0aFwiXG5cbiAgICBjb25maWdGaWxlID0gYXRvbS5jb25maWcuZ2V0VXNlckNvbmZpZ1BhdGgoKVxuICAgIGNvbmZpZ1BhdGggPSBkaXJuYW1lKGNvbmZpZ0ZpbGUpXG5cbiAgICBAb3BlbkZvbGRlcihjb25maWdQYXRoKSBpZiBjb25maWdQYXRoXG5cbiAgc2VsZWN0RmlsZTogKHBhdGgpIC0+XG4gICAgcmVxdWlyZShcIi4vZ2FcIikuc2VuZEV2ZW50IG5hbWUsIFwiY29uZmlndXJhdGlvbi1mb2xkZXJcIlxuXG4gICAgeyBiYXNlbmFtZSB9ID0gcmVxdWlyZSBcInBhdGhcIlxuXG4gICAgIyBDdXN0b20gZmlsZSBtYW5hZ2VyXG4gICAgZmlsZU1hbmFnZXIgPSBhdG9tLmNvbmZpZy5nZXQoXCIje25hbWV9LmZpbGVNYW5hZ2VyXCIpXG4gICAgcmV0dXJuIEBzcGF3bkNtZCBmaWxlTWFuYWdlciwgWyBwYXRoIF0sIGJhc2VuYW1lKHBhdGgpLCBcImZpbGUgbWFuYWdlclwiIGlmIGZpbGVNYW5hZ2VyXG5cbiAgICAjIERlZmF1bHQgZmlsZSBtYW5hZ2VyXG4gICAgc3dpdGNoIHByb2Nlc3MucGxhdGZvcm1cbiAgICAgIHdoZW4gXCJkYXJ3aW5cIlxuICAgICAgICBAc3Bhd25DbWQgXCJvcGVuXCIsIFsgXCItUlwiLCBwYXRoIF0sIGJhc2VuYW1lKHBhdGgpLCBcIkZpbmRlclwiXG4gICAgICB3aGVuIFwid2luMzJcIlxuICAgICAgICBAc3Bhd25DbWQgXCJleHBsb3JlclwiLCBbIFwiL3NlbGVjdCwje3BhdGh9XCIgXSwgYmFzZW5hbWUocGF0aCksIFwiRXhwbG9yZXJcIlxuICAgICAgd2hlbiBcImxpbnV4XCJcbiAgICAgICAgeyBzaGVsbCB9ID0gcmVxdWlyZSBcImVsZWN0cm9uXCJcbiAgICAgICAgc2hlbGwuc2hvd0l0ZW1JbkZvbGRlcihwYXRoKVxuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkSW5mbyhcIioqI3tuYW1lfSoqOiBPcGVuaW5nIGAje2Jhc2VuYW1lKHBhdGgpfWAgaW4gZmlsZSBtYW5hZ2VyXCIsIGRpc21pc3NhYmxlOiBmYWxzZSkgaWYgYXRvbS5jb25maWcuZ2V0KFwiI3tuYW1lfS5ub3RpZnlcIilcblxuICBvcGVuRm9sZGVyOiAocGF0aCkgLT5cbiAgICB7IGFjY2VzcywgRl9PSyB9ID0gcmVxdWlyZSBcImZzXCJcbiAgICB7IGJhc2VuYW1lIH0gPSByZXF1aXJlIFwicGF0aFwiXG5cbiAgICBhY2Nlc3MgcGF0aCwgRl9PSywgKGVycikgLT5cbiAgICAgIHJldHVybiBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IobmFtZSwgZGV0YWlsOiBlcnJvciwgZGlzbWlzc2FibGU6IHRydWUpIGlmIGVyclxuXG4gICAgICAjIEN1c3RvbSBmaWxlIG1hbmFnZXJcbiAgICAgIGZpbGVNYW5hZ2VyID0gYXRvbS5jb25maWcuZ2V0KFwiI3tuYW1lfS5maWxlTWFuYWdlclwiKVxuICAgICAgcmV0dXJuIEJyb3dzZS5zcGF3bkNtZCBmaWxlTWFuYWdlciwgWyBwYXRoIF0sIGJhc2VuYW1lKHBhdGgpLCBcImZpbGUgbWFuYWdlclwiIGlmIGZpbGVNYW5hZ2VyXG5cbiAgICAgICMgRGVmYXVsdCBmaWxlIG1hbmFnZXJcbiAgICAgIHN3aXRjaCBwcm9jZXNzLnBsYXRmb3JtXG4gICAgICAgIHdoZW4gXCJkYXJ3aW5cIlxuICAgICAgICAgIEJyb3dzZS5zcGF3bkNtZCBcIm9wZW5cIiwgWyBwYXRoIF0sIGJhc2VuYW1lKHBhdGgpLCBcIkZpbmRlclwiXG4gICAgICAgIHdoZW4gXCJ3aW4zMlwiXG4gICAgICAgICAgQnJvd3NlLnNwYXduQ21kIFwiZXhwbG9yZXJcIiwgWyBwYXRoIF0sIGJhc2VuYW1lKHBhdGgpLCBcIkV4cGxvcmVyXCJcbiAgICAgICAgd2hlbiBcImxpbnV4XCJcbiAgICAgICAgICB7IHNoZWxsIH0gPSByZXF1aXJlIFwiZWxlY3Ryb25cIlxuICAgICAgICAgIHNoZWxsLm9wZW5JdGVtKHBhdGgpXG4gICAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZEluZm8oXCIqKiN7bmFtZX0qKjogT3BlbmluZyBgI3tiYXNlbmFtZShwYXRoKX1gIGluIGZpbGUgbWFuYWdlclwiLCBkaXNtaXNzYWJsZTogZmFsc2UpIGlmIGF0b20uY29uZmlnLmdldChcIiN7bmFtZX0ubm90aWZ5XCIpXG5cbiAgc3Bhd25DbWQ6IChjbWQsIGFyZ3MsIGJhc2VOYW1lLCBmaWxlTWFuYWdlcikgLT5cbiAgICB7IHNwYXduIH0gPSByZXF1aXJlKFwiY2hpbGRfcHJvY2Vzc1wiKVxuXG4gICAgb3BlbiA9IHNwYXduIGNtZCwgYXJnc1xuXG4gICAgb3Blbi5zdGRlcnIub24gXCJkYXRhXCIsIChlcnJvcikgLT5cbiAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihcIioqI3tuYW1lfSoqOiAje2Vycm9yfVwiLCBkaXNtaXNzYWJsZTogdHJ1ZSlcblxuICAgIG9wZW4ub24gXCJjbG9zZVwiLCAoIGVycm9yQ29kZSApIC0+XG4gICAgICBpZiBlcnJvckNvZGUgaXMgMCBhbmQgYXRvbS5jb25maWcuZ2V0KFwiI3tuYW1lfS5ub3RpZnlcIilcbiAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZEluZm8oXCIqKiN7bmFtZX0qKjogT3BlbmluZyBgI3tiYXNlTmFtZX1gIGluICN7ZmlsZU1hbmFnZXJ9XCIsIGRpc21pc3NhYmxlOiBmYWxzZSlcbiJdfQ==
