(function() {
  var CompositeDisposable, Point, RubyBlock, RubyBlockView, _, ref;

  ref = require('atom'), CompositeDisposable = ref.CompositeDisposable, Point = ref.Point;

  _ = null;

  RubyBlockView = null;

  module.exports = RubyBlock = {
    config: {
      showBottomPanel: {
        type: 'boolean',
        "default": true
      },
      highlightLine: {
        type: 'boolean',
        "default": true
      },
      highlightLineNumber: {
        type: 'boolean',
        "default": false
      }
    },
    rubyBlockView: null,
    modalPanel: null,
    rubyRootScope: 'source.ruby',
    rubyStartBlockNames: ['for', 'if', 'unless', 'until', 'while', 'class', 'module', 'case', 'def', 'begin', 'describe', 'context'],
    rubyStartBlockScopes: ['keyword.control.ruby', 'keyword.control.start-block.ruby', 'keyword.control.class.ruby', 'keyword.control.module.ruby', 'keyword.control.def.ruby', 'meta.rspec.behaviour'],
    rubyWhileBlockName: 'while',
    rubyDoBlockName: 'do',
    rubyEndBlockName: 'end',
    rubyKeywordControlScope: 'keyword.control.ruby',
    rubyKeywordControlNames: ['end', 'elsif', 'else', 'when', 'rescue', 'ensure'],
    rubyDoScope: 'keyword.control.start-block.ruby',
    endBlockStack: [],
    activate: function() {
      return this.activeItemSubscription = atom.workspace.observeActivePaneItem((function(_this) {
        return function() {
          return _this.subscribeToActiveTextEditor();
        };
      })(this));
    },
    deactivate: function() {
      var ref1, ref2, ref3, ref4, ref5;
      if ((ref1 = this.marker) != null) {
        ref1.destroy();
      }
      this.marker = null;
      if ((ref2 = this.modalPanel) != null) {
        ref2.destroy();
      }
      this.modalPanel = null;
      if ((ref3 = this.activeItemSubscription) != null) {
        ref3.dispose();
      }
      this.activeItemSubscription = null;
      if ((ref4 = this.editorSubscriptions) != null) {
        ref4.dispose();
      }
      this.editorSubscriptions = null;
      if ((ref5 = this.rubyBlockView) != null) {
        ref5.destroy();
      }
      return this.rubyBlockView = null;
    },
    init: function() {
      if (!(RubyBlockView && _)) {
        this.loadClasses();
      }
      this.rubyBlockView = new RubyBlockView;
      return this.modalPanel = atom.workspace.addBottomPanel({
        item: this.rubyBlockView.getElement(),
        visible: false,
        priority: 500
      });
    },
    getActiveTextEditor: function() {
      return atom.workspace.getActiveTextEditor();
    },
    goToMatchingLine: function() {
      var editor, firstCharPoint, row;
      if (this.blockStartedRowNumber == null) {
        return atom.beep();
      }
      editor = this.getActiveTextEditor();
      row = editor.lineTextForBufferRow(this.blockStartedRowNumber);
      firstCharPoint = row.search(/\S/);
      return editor.setCursorBufferPosition([this.blockStartedRowNumber, firstCharPoint]);
    },
    subscribeToActiveTextEditor: function() {
      var editor, editorElement, ref1, ref2, ref3;
      if ((ref1 = this.marker) != null) {
        ref1.destroy();
      }
      if ((ref2 = this.modalPanel) != null ? ref2.isVisible() : void 0) {
        this.modalPanel.hide();
      }
      if ((ref3 = this.editorSubscriptions) != null) {
        ref3.dispose();
      }
      editor = this.getActiveTextEditor();
      if (editor == null) {
        return;
      }
      if (editor.getRootScopeDescriptor().scopes[0].indexOf(this.rubyRootScope) === -1) {
        return;
      }
      if (this.rubyBlockView == null) {
        this.init();
      }
      editorElement = atom.views.getView(editor);
      this.editorSubscriptions = new CompositeDisposable;
      this.editorSubscriptions.add(atom.commands.add(editorElement, {
        'ruby-block:go-to-matching-line': (function(_this) {
          return function() {
            return _this.goToMatchingLine();
          };
        })(this)
      }));
      this.editorSubscriptions.add(editor.onDidChangeCursorPosition(_.debounce((function(_this) {
        return function() {
          var ref4;
          if (_this.getActiveTextEditor() !== editor) {
            return;
          }
          _this.blockStartedRowNumber = null;
          if (_this.modalPanel.isVisible()) {
            _this.modalPanel.hide();
          }
          if ((ref4 = _this.marker) != null) {
            ref4.destroy();
          }
          return _this.searchForBlock();
        };
      })(this), 100)));
      return this.searchForBlock();
    },
    searchForBlock: function() {
      var currentRowNumber, cursor, editor, filteredTokens, firstTokenScope, grammar, i, j, k, l, len, len1, m, prevWordBoundaryPos, ref1, ref2, ref3, row, rowNumber, scope, startBlock, token, tokens;
      editor = this.getActiveTextEditor();
      grammar = editor.getGrammar();
      cursor = editor.getLastCursor();
      currentRowNumber = cursor.getBufferRow();
      if (cursor.getScopeDescriptor().scopes.indexOf(this.rubyKeywordControlScope) === -1 || this.rubyKeywordControlNames.indexOf(editor.getWordUnderCursor()) === -1) {
        return;
      }
      this.endBlockStack.push(editor.getWordUnderCursor);
      for (rowNumber = j = ref1 = cursor.getBufferRow(); ref1 <= 0 ? j <= 0 : j >= 0; rowNumber = ref1 <= 0 ? ++j : --j) {
        if (editor.isBufferRowCommented(rowNumber)) {
          continue;
        }
        if (rowNumber === currentRowNumber) {
          prevWordBoundaryPos = cursor.getPreviousWordBoundaryBufferPosition();
          row = editor.getTextInBufferRange([[rowNumber, 0], prevWordBoundaryPos]);
        } else {
          row = editor.lineTextForBufferRow(rowNumber);
        }
        tokens = grammar.tokenizeLine(row).tokens;
        filteredTokens = (function() {
          var k, len, results;
          results = [];
          for (i = k = 0, len = tokens.length; k < len; i = ++k) {
            token = tokens[i];
            if (!token.value.match(/^\s*$/)) {
              results.push(token);
            }
          }
          return results;
        })();
        startBlock = (function() {
          var k, len, results;
          results = [];
          for (k = 0, len = filteredTokens.length; k < len; k++) {
            token = filteredTokens[k];
            if (token.scopes.indexOf(this.rubyDoScope) >= 0) {
              results.push(token);
            }
          }
          return results;
        }).call(this);
        if (startBlock.length > 0) {
          if (token.value !== this.rubyDoBlockName || filteredTokens[0].value !== this.rubyWhileBlockName) {
            this.endBlockStack.pop();
          }
          if (this.endBlockStack.length === 0) {
            return this.highlightBlock(rowNumber);
          }
        }
        for (k = filteredTokens.length - 1; k >= 0; k += -1) {
          token = filteredTokens[k];
          ref2 = token.scopes;
          for (l = 0, len = ref2.length; l < len; l++) {
            scope = ref2[l];
            if (scope === this.rubyKeywordControlScope && token.value === this.rubyEndBlockName) {
              this.endBlockStack.push(scope.value);
            } else if (this.rubyStartBlockScopes.indexOf(scope) >= 0 && this.rubyStartBlockNames.indexOf(token.value) >= 0) {
              if (token.value === 'case') {
                this.endBlockStack.pop();
              } else {
                ref3 = filteredTokens[0].scopes;
                for (m = 0, len1 = ref3.length; m < len1; m++) {
                  firstTokenScope = ref3[m];
                  if (this.rubyStartBlockScopes.indexOf(firstTokenScope) >= 0 && this.rubyStartBlockNames.indexOf(filteredTokens[0].value) >= 0) {
                    this.endBlockStack.pop();
                    break;
                  }
                }
              }
              if (this.endBlockStack.length === 0) {
                return this.highlightBlock(rowNumber);
              }
            }
          }
        }
      }
    },
    highlightBlock: function(rowNumber) {
      var editor, firstCharPoint, row;
      editor = this.getActiveTextEditor();
      row = editor.lineTextForBufferRow(rowNumber);
      firstCharPoint = row.search(/\S/);
      this.marker = editor.markBufferRange([[rowNumber, firstCharPoint], [rowNumber, row.length]]);
      this.blockStartedRowNumber = rowNumber;
      if (atom.config.get('ruby-block.highlightLine')) {
        editor.decorateMarker(this.marker, {
          type: 'highlight',
          "class": 'ruby-block-highlight'
        });
      }
      if (atom.config.get('ruby-block.highlightLineNumber')) {
        editor.decorateMarker(this.marker, {
          type: 'line-number',
          "class": 'ruby-block-highlight'
        });
      }
      if (atom.config.get('ruby-block.showBottomPanel')) {
        this.rubyBlockView.updateMessage(rowNumber);
        return this.modalPanel.show();
      }
    },
    loadClasses: function() {
      _ = require('underscore-plus');
      return RubyBlockView = require('./ruby-block-view');
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9ydWJ5LWJsb2NrL2xpYi9ydWJ5LWJsb2NrLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsTUFBK0IsT0FBQSxDQUFRLE1BQVIsQ0FBL0IsRUFBQyw2Q0FBRCxFQUFzQjs7RUFDdEIsQ0FBQSxHQUFJOztFQUNKLGFBQUEsR0FBZ0I7O0VBRWhCLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLFNBQUEsR0FDZjtJQUFBLE1BQUEsRUFDRTtNQUFBLGVBQUEsRUFDRTtRQUFBLElBQUEsRUFBTSxTQUFOO1FBQ0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxJQURUO09BREY7TUFHQSxhQUFBLEVBQ0U7UUFBQSxJQUFBLEVBQU0sU0FBTjtRQUNBLENBQUEsT0FBQSxDQUFBLEVBQVMsSUFEVDtPQUpGO01BTUEsbUJBQUEsRUFDRTtRQUFBLElBQUEsRUFBTSxTQUFOO1FBQ0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxLQURUO09BUEY7S0FERjtJQVlBLGFBQUEsRUFBZSxJQVpmO0lBYUEsVUFBQSxFQUFZLElBYlo7SUFjQSxhQUFBLEVBQWUsYUFkZjtJQWdCQSxtQkFBQSxFQUFxQixDQUNuQixLQURtQixFQUVuQixJQUZtQixFQUduQixRQUhtQixFQUluQixPQUptQixFQUtuQixPQUxtQixFQU1uQixPQU5tQixFQU9uQixRQVBtQixFQVFuQixNQVJtQixFQVNuQixLQVRtQixFQVVuQixPQVZtQixFQVduQixVQVhtQixFQVluQixTQVptQixDQWhCckI7SUE4QkEsb0JBQUEsRUFBc0IsQ0FDbkIsc0JBRG1CLEVBRW5CLGtDQUZtQixFQUduQiw0QkFIbUIsRUFJbkIsNkJBSm1CLEVBS25CLDBCQUxtQixFQU1uQixzQkFObUIsQ0E5QnRCO0lBdUNBLGtCQUFBLEVBQW9CLE9BdkNwQjtJQXdDQSxlQUFBLEVBQWlCLElBeENqQjtJQXlDQSxnQkFBQSxFQUFrQixLQXpDbEI7SUEyQ0EsdUJBQUEsRUFBeUIsc0JBM0N6QjtJQTRDQSx1QkFBQSxFQUF5QixDQUN2QixLQUR1QixFQUV2QixPQUZ1QixFQUd2QixNQUh1QixFQUl2QixNQUp1QixFQUt2QixRQUx1QixFQU12QixRQU51QixDQTVDekI7SUFxREEsV0FBQSxFQUFhLGtDQXJEYjtJQXVEQSxhQUFBLEVBQWUsRUF2RGY7SUF5REEsUUFBQSxFQUFVLFNBQUE7YUFFUixJQUFDLENBQUEsc0JBQUQsR0FBMEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBZixDQUFzQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLDJCQUFELENBQUE7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBdEM7SUFGbEIsQ0F6RFY7SUE2REEsVUFBQSxFQUFZLFNBQUE7QUFDVixVQUFBOztZQUFPLENBQUUsT0FBVCxDQUFBOztNQUNBLElBQUMsQ0FBQSxNQUFELEdBQVU7O1lBQ0MsQ0FBRSxPQUFiLENBQUE7O01BQ0EsSUFBQyxDQUFBLFVBQUQsR0FBYzs7WUFDUyxDQUFFLE9BQXpCLENBQUE7O01BQ0EsSUFBQyxDQUFBLHNCQUFELEdBQTBCOztZQUNOLENBQUUsT0FBdEIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsbUJBQUQsR0FBdUI7O1lBQ1QsQ0FBRSxPQUFoQixDQUFBOzthQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCO0lBVlAsQ0E3RFo7SUF5RUEsSUFBQSxFQUFNLFNBQUE7TUFDSixJQUFBLENBQUEsQ0FBc0IsYUFBQSxJQUFrQixDQUF4QyxDQUFBO1FBQUEsSUFBQyxDQUFBLFdBQUQsQ0FBQSxFQUFBOztNQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7YUFDckIsSUFBQyxDQUFBLFVBQUQsR0FBYyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWYsQ0FBOEI7UUFBQSxJQUFBLEVBQU0sSUFBQyxDQUFBLGFBQWEsQ0FBQyxVQUFmLENBQUEsQ0FBTjtRQUFtQyxPQUFBLEVBQVMsS0FBNUM7UUFBbUQsUUFBQSxFQUFVLEdBQTdEO09BQTlCO0lBSFYsQ0F6RU47SUE4RUEsbUJBQUEsRUFBcUIsU0FBQTthQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7SUFEbUIsQ0E5RXJCO0lBaUZBLGdCQUFBLEVBQWtCLFNBQUE7QUFDaEIsVUFBQTtNQUFBLElBQTBCLGtDQUExQjtBQUFBLGVBQU8sSUFBSSxDQUFDLElBQUwsQ0FBQSxFQUFQOztNQUNBLE1BQUEsR0FBUyxJQUFDLENBQUEsbUJBQUQsQ0FBQTtNQUNULEdBQUEsR0FBTSxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsSUFBQyxDQUFBLHFCQUE3QjtNQUNOLGNBQUEsR0FBaUIsR0FBRyxDQUFDLE1BQUosQ0FBVyxJQUFYO2FBQ2pCLE1BQU0sQ0FBQyx1QkFBUCxDQUErQixDQUFDLElBQUMsQ0FBQSxxQkFBRixFQUF5QixjQUF6QixDQUEvQjtJQUxnQixDQWpGbEI7SUF3RkEsMkJBQUEsRUFBNkIsU0FBQTtBQUMzQixVQUFBOztZQUFPLENBQUUsT0FBVCxDQUFBOztNQUNBLDJDQUFpQyxDQUFFLFNBQWIsQ0FBQSxVQUF0QjtRQUFBLElBQUMsQ0FBQSxVQUFVLENBQUMsSUFBWixDQUFBLEVBQUE7OztZQUVvQixDQUFFLE9BQXRCLENBQUE7O01BQ0EsTUFBQSxHQUFTLElBQUMsQ0FBQSxtQkFBRCxDQUFBO01BRVQsSUFBYyxjQUFkO0FBQUEsZUFBQTs7TUFDQSxJQUFVLE1BQU0sQ0FBQyxzQkFBUCxDQUFBLENBQStCLENBQUMsTUFBTyxDQUFBLENBQUEsQ0FBRSxDQUFDLE9BQTFDLENBQWtELElBQUMsQ0FBQSxhQUFuRCxDQUFBLEtBQXFFLENBQUMsQ0FBaEY7QUFBQSxlQUFBOztNQUVBLElBQWUsMEJBQWY7UUFBQSxJQUFDLENBQUEsSUFBRCxDQUFBLEVBQUE7O01BRUEsYUFBQSxHQUFnQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsTUFBbkI7TUFDaEIsSUFBQyxDQUFBLG1CQUFELEdBQXVCLElBQUk7TUFFM0IsSUFBQyxDQUFBLG1CQUFtQixDQUFDLEdBQXJCLENBQXlCLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixhQUFsQixFQUN2QjtRQUFBLGdDQUFBLEVBQWtDLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQ2hDLEtBQUMsQ0FBQSxnQkFBRCxDQUFBO1VBRGdDO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFsQztPQUR1QixDQUF6QjtNQU1BLElBQUMsQ0FBQSxtQkFBbUIsQ0FBQyxHQUFyQixDQUF5QixNQUFNLENBQUMseUJBQVAsQ0FBaUMsQ0FBQyxDQUFDLFFBQUYsQ0FBWSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7QUFDcEUsY0FBQTtVQUFBLElBQWMsS0FBQyxDQUFBLG1CQUFELENBQUEsQ0FBQSxLQUEwQixNQUF4QztBQUFBLG1CQUFBOztVQUNBLEtBQUMsQ0FBQSxxQkFBRCxHQUF5QjtVQUN6QixJQUFzQixLQUFDLENBQUEsVUFBVSxDQUFDLFNBQVosQ0FBQSxDQUF0QjtZQUFBLEtBQUMsQ0FBQSxVQUFVLENBQUMsSUFBWixDQUFBLEVBQUE7OztnQkFDTyxDQUFFLE9BQVQsQ0FBQTs7aUJBQ0EsS0FBQyxDQUFBLGNBQUQsQ0FBQTtRQUxvRTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWixFQU14RCxHQU53RCxDQUFqQyxDQUF6QjthQVFBLElBQUMsQ0FBQSxjQUFELENBQUE7SUE3QjJCLENBeEY3QjtJQXVIQSxjQUFBLEVBQWdCLFNBQUE7QUFDZCxVQUFBO01BQUEsTUFBQSxHQUFTLElBQUMsQ0FBQSxtQkFBRCxDQUFBO01BQ1QsT0FBQSxHQUFVLE1BQU0sQ0FBQyxVQUFQLENBQUE7TUFDVixNQUFBLEdBQVMsTUFBTSxDQUFDLGFBQVAsQ0FBQTtNQUNULGdCQUFBLEdBQW1CLE1BQU0sQ0FBQyxZQUFQLENBQUE7TUFHbkIsSUFBVSxNQUFNLENBQUMsa0JBQVAsQ0FBQSxDQUEyQixDQUFDLE1BQU0sQ0FBQyxPQUFuQyxDQUEyQyxJQUFDLENBQUEsdUJBQTVDLENBQUEsS0FBd0UsQ0FBQyxDQUF6RSxJQUNBLElBQUMsQ0FBQSx1QkFBdUIsQ0FBQyxPQUF6QixDQUFpQyxNQUFNLENBQUMsa0JBQVAsQ0FBQSxDQUFqQyxDQUFBLEtBQWlFLENBQUMsQ0FENUU7QUFBQSxlQUFBOztNQUdBLElBQUMsQ0FBQSxhQUFhLENBQUMsSUFBZixDQUFvQixNQUFNLENBQUMsa0JBQTNCO0FBR0EsV0FBaUIsNEdBQWpCO1FBQ0UsSUFBWSxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsU0FBNUIsQ0FBWjtBQUFBLG1CQUFBOztRQUVBLElBQUcsU0FBQSxLQUFhLGdCQUFoQjtVQUNFLG1CQUFBLEdBQXNCLE1BQU0sQ0FBQyxxQ0FBUCxDQUFBO1VBQ3RCLEdBQUEsR0FBTSxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsQ0FBQyxDQUFDLFNBQUQsRUFBWSxDQUFaLENBQUQsRUFBaUIsbUJBQWpCLENBQTVCLEVBRlI7U0FBQSxNQUFBO1VBSUUsR0FBQSxHQUFNLE1BQU0sQ0FBQyxvQkFBUCxDQUE0QixTQUE1QixFQUpSOztRQU1BLE1BQUEsR0FBUyxPQUFPLENBQUMsWUFBUixDQUFxQixHQUFyQixDQUF5QixDQUFDO1FBQ25DLGNBQUE7O0FBQWtCO2VBQUEsZ0RBQUE7O2dCQUFpQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBWixDQUFrQixPQUFsQjsyQkFBbEM7O0FBQUE7OztRQUVsQixVQUFBOztBQUFjO2VBQUEsZ0RBQUE7O2dCQUF1QyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQWIsQ0FBcUIsSUFBQyxDQUFBLFdBQXRCLENBQUEsSUFBc0M7MkJBQTdFOztBQUFBOzs7UUFDZCxJQUFHLFVBQVUsQ0FBQyxNQUFYLEdBQW9CLENBQXZCO1VBQ0UsSUFBRyxLQUFLLENBQUMsS0FBTixLQUFpQixJQUFDLENBQUEsZUFBbEIsSUFDQSxjQUFlLENBQUEsQ0FBQSxDQUFFLENBQUMsS0FBbEIsS0FBNkIsSUFBQyxDQUFBLGtCQURqQztZQUVFLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFBLEVBRkY7O1VBR0EsSUFBRyxJQUFDLENBQUEsYUFBYSxDQUFDLE1BQWYsS0FBeUIsQ0FBNUI7QUFDRSxtQkFBTyxJQUFDLENBQUEsY0FBRCxDQUFnQixTQUFoQixFQURUO1dBSkY7O0FBT0EsYUFBQSw4Q0FBQTs7QUFDRTtBQUFBLGVBQUEsc0NBQUE7O1lBQ0UsSUFBRyxLQUFBLEtBQVMsSUFBQyxDQUFBLHVCQUFWLElBQXNDLEtBQUssQ0FBQyxLQUFOLEtBQWUsSUFBQyxDQUFBLGdCQUF6RDtjQUNFLElBQUMsQ0FBQSxhQUFhLENBQUMsSUFBZixDQUFvQixLQUFLLENBQUMsS0FBMUIsRUFERjthQUFBLE1BRUssSUFBRyxJQUFDLENBQUEsb0JBQW9CLENBQUMsT0FBdEIsQ0FBOEIsS0FBOUIsQ0FBQSxJQUF3QyxDQUF4QyxJQUNBLElBQUMsQ0FBQSxtQkFBbUIsQ0FBQyxPQUFyQixDQUE2QixLQUFLLENBQUMsS0FBbkMsQ0FBQSxJQUE2QyxDQURoRDtjQU9ILElBQUcsS0FBSyxDQUFDLEtBQU4sS0FBZSxNQUFsQjtnQkFDRSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBQSxFQURGO2VBQUEsTUFBQTtBQUdFO0FBQUEscUJBQUEsd0NBQUE7O2tCQUNFLElBQUcsSUFBQyxDQUFBLG9CQUFvQixDQUFDLE9BQXRCLENBQThCLGVBQTlCLENBQUEsSUFBa0QsQ0FBbEQsSUFDQSxJQUFDLENBQUEsbUJBQW1CLENBQUMsT0FBckIsQ0FBNkIsY0FBZSxDQUFBLENBQUEsQ0FBRSxDQUFDLEtBQS9DLENBQUEsSUFBeUQsQ0FENUQ7b0JBRUUsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQUE7QUFDQSwwQkFIRjs7QUFERixpQkFIRjs7Y0FTQSxJQUFHLElBQUMsQ0FBQSxhQUFhLENBQUMsTUFBZixLQUF5QixDQUE1QjtBQUNFLHVCQUFPLElBQUMsQ0FBQSxjQUFELENBQWdCLFNBQWhCLEVBRFQ7ZUFoQkc7O0FBSFA7QUFERjtBQXBCRjtJQWJjLENBdkhoQjtJQStLQSxjQUFBLEVBQWdCLFNBQUMsU0FBRDtBQUNkLFVBQUE7TUFBQSxNQUFBLEdBQVMsSUFBQyxDQUFBLG1CQUFELENBQUE7TUFDVCxHQUFBLEdBQU0sTUFBTSxDQUFDLG9CQUFQLENBQTRCLFNBQTVCO01BQ04sY0FBQSxHQUFpQixHQUFHLENBQUMsTUFBSixDQUFXLElBQVg7TUFDakIsSUFBQyxDQUFBLE1BQUQsR0FBVSxNQUFNLENBQUMsZUFBUCxDQUF1QixDQUFDLENBQUMsU0FBRCxFQUFZLGNBQVosQ0FBRCxFQUE4QixDQUFDLFNBQUQsRUFBWSxHQUFHLENBQUMsTUFBaEIsQ0FBOUIsQ0FBdkI7TUFFVixJQUFDLENBQUEscUJBQUQsR0FBeUI7TUFDekIsSUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMEJBQWhCLENBQUg7UUFDRSxNQUFNLENBQUMsY0FBUCxDQUFzQixJQUFDLENBQUEsTUFBdkIsRUFBK0I7VUFBQyxJQUFBLEVBQU0sV0FBUDtVQUFvQixDQUFBLEtBQUEsQ0FBQSxFQUFPLHNCQUEzQjtTQUEvQixFQURGOztNQUVBLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLGdDQUFoQixDQUFIO1FBQ0UsTUFBTSxDQUFDLGNBQVAsQ0FBc0IsSUFBQyxDQUFBLE1BQXZCLEVBQStCO1VBQUMsSUFBQSxFQUFNLGFBQVA7VUFBc0IsQ0FBQSxLQUFBLENBQUEsRUFBTyxzQkFBN0I7U0FBL0IsRUFERjs7TUFFQSxJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQiw0QkFBaEIsQ0FBSDtRQUNFLElBQUMsQ0FBQSxhQUFhLENBQUMsYUFBZixDQUE2QixTQUE3QjtlQUNBLElBQUMsQ0FBQSxVQUFVLENBQUMsSUFBWixDQUFBLEVBRkY7O0lBWGMsQ0EvS2hCO0lBOExBLFdBQUEsRUFBYSxTQUFBO01BQ1gsQ0FBQSxHQUFJLE9BQUEsQ0FBUSxpQkFBUjthQUNKLGFBQUEsR0FBZ0IsT0FBQSxDQUFRLG1CQUFSO0lBRkwsQ0E5TGI7O0FBTEYiLCJzb3VyY2VzQ29udGVudCI6WyJ7Q29tcG9zaXRlRGlzcG9zYWJsZSwgUG9pbnR9ID0gcmVxdWlyZSAnYXRvbSdcbl8gPSBudWxsXG5SdWJ5QmxvY2tWaWV3ID0gbnVsbFxuXG5tb2R1bGUuZXhwb3J0cyA9IFJ1YnlCbG9jayA9XG4gIGNvbmZpZzpcbiAgICBzaG93Qm90dG9tUGFuZWw6XG4gICAgICB0eXBlOiAnYm9vbGVhbidcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICBoaWdobGlnaHRMaW5lOlxuICAgICAgdHlwZTogJ2Jvb2xlYW4nXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgaGlnaGxpZ2h0TGluZU51bWJlcjpcbiAgICAgIHR5cGU6ICdib29sZWFuJ1xuICAgICAgZGVmYXVsdDogZmFsc2VcblxuXG4gIHJ1YnlCbG9ja1ZpZXc6IG51bGxcbiAgbW9kYWxQYW5lbDogbnVsbFxuICBydWJ5Um9vdFNjb3BlOiAnc291cmNlLnJ1YnknXG5cbiAgcnVieVN0YXJ0QmxvY2tOYW1lczogW1xuICAgICdmb3InXG4gICAgJ2lmJ1xuICAgICd1bmxlc3MnXG4gICAgJ3VudGlsJ1xuICAgICd3aGlsZSdcbiAgICAnY2xhc3MnXG4gICAgJ21vZHVsZSdcbiAgICAnY2FzZSdcbiAgICAnZGVmJ1xuICAgICdiZWdpbidcbiAgICAnZGVzY3JpYmUnXG4gICAgJ2NvbnRleHQnXG4gIF1cbiAgcnVieVN0YXJ0QmxvY2tTY29wZXM6IFtcbiAgICAgJ2tleXdvcmQuY29udHJvbC5ydWJ5J1xuICAgICAna2V5d29yZC5jb250cm9sLnN0YXJ0LWJsb2NrLnJ1YnknXG4gICAgICdrZXl3b3JkLmNvbnRyb2wuY2xhc3MucnVieSdcbiAgICAgJ2tleXdvcmQuY29udHJvbC5tb2R1bGUucnVieSdcbiAgICAgJ2tleXdvcmQuY29udHJvbC5kZWYucnVieSdcbiAgICAgJ21ldGEucnNwZWMuYmVoYXZpb3VyJ1xuICBdXG5cbiAgcnVieVdoaWxlQmxvY2tOYW1lOiAnd2hpbGUnXG4gIHJ1YnlEb0Jsb2NrTmFtZTogJ2RvJ1xuICBydWJ5RW5kQmxvY2tOYW1lOiAnZW5kJ1xuXG4gIHJ1YnlLZXl3b3JkQ29udHJvbFNjb3BlOiAna2V5d29yZC5jb250cm9sLnJ1YnknXG4gIHJ1YnlLZXl3b3JkQ29udHJvbE5hbWVzOiBbXG4gICAgJ2VuZCdcbiAgICAnZWxzaWYnXG4gICAgJ2Vsc2UnXG4gICAgJ3doZW4nXG4gICAgJ3Jlc2N1ZSdcbiAgICAnZW5zdXJlJ1xuICBdXG5cbiAgcnVieURvU2NvcGU6ICdrZXl3b3JkLmNvbnRyb2wuc3RhcnQtYmxvY2sucnVieSdcblxuICBlbmRCbG9ja1N0YWNrOiBbXVxuXG4gIGFjdGl2YXRlOiAtPlxuICAgICMgRXZlbnRzIHN1YnNjcmliZWQgdG8gaW4gYXRvbSdzIHN5c3RlbSBjYW4gYmUgZWFzaWx5IGNsZWFuZWQgdXAgd2l0aCBhIENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBAYWN0aXZlSXRlbVN1YnNjcmlwdGlvbiA9IGF0b20ud29ya3NwYWNlLm9ic2VydmVBY3RpdmVQYW5lSXRlbSggPT4gQHN1YnNjcmliZVRvQWN0aXZlVGV4dEVkaXRvcigpKVxuXG4gIGRlYWN0aXZhdGU6IC0+XG4gICAgQG1hcmtlcj8uZGVzdHJveSgpXG4gICAgQG1hcmtlciA9IG51bGxcbiAgICBAbW9kYWxQYW5lbD8uZGVzdHJveSgpXG4gICAgQG1vZGFsUGFuZWwgPSBudWxsXG4gICAgQGFjdGl2ZUl0ZW1TdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEBhY3RpdmVJdGVtU3Vic2NyaXB0aW9uID0gbnVsbFxuICAgIEBlZGl0b3JTdWJzY3JpcHRpb25zPy5kaXNwb3NlKClcbiAgICBAZWRpdG9yU3Vic2NyaXB0aW9ucyA9IG51bGxcbiAgICBAcnVieUJsb2NrVmlldz8uZGVzdHJveSgpXG4gICAgQHJ1YnlCbG9ja1ZpZXcgPSBudWxsXG5cbiAgaW5pdDogLT5cbiAgICBAbG9hZENsYXNzZXMoKSB1bmxlc3MgUnVieUJsb2NrVmlldyBhbmQgX1xuICAgIEBydWJ5QmxvY2tWaWV3ID0gbmV3IFJ1YnlCbG9ja1ZpZXdcbiAgICBAbW9kYWxQYW5lbCA9IGF0b20ud29ya3NwYWNlLmFkZEJvdHRvbVBhbmVsKGl0ZW06IEBydWJ5QmxvY2tWaWV3LmdldEVsZW1lbnQoKSwgdmlzaWJsZTogZmFsc2UsIHByaW9yaXR5OiA1MDApXG5cbiAgZ2V0QWN0aXZlVGV4dEVkaXRvcjogLT5cbiAgICBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVUZXh0RWRpdG9yKClcblxuICBnb1RvTWF0Y2hpbmdMaW5lOiAtPlxuICAgIHJldHVybiBhdG9tLmJlZXAoKSB1bmxlc3MgQGJsb2NrU3RhcnRlZFJvd051bWJlcj9cbiAgICBlZGl0b3IgPSBAZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgcm93ID0gZWRpdG9yLmxpbmVUZXh0Rm9yQnVmZmVyUm93KEBibG9ja1N0YXJ0ZWRSb3dOdW1iZXIpXG4gICAgZmlyc3RDaGFyUG9pbnQgPSByb3cuc2VhcmNoKC9cXFMvKVxuICAgIGVkaXRvci5zZXRDdXJzb3JCdWZmZXJQb3NpdGlvbihbQGJsb2NrU3RhcnRlZFJvd051bWJlciwgZmlyc3RDaGFyUG9pbnRdKVxuXG4gIHN1YnNjcmliZVRvQWN0aXZlVGV4dEVkaXRvcjogLT5cbiAgICBAbWFya2VyPy5kZXN0cm95KClcbiAgICBAbW9kYWxQYW5lbC5oaWRlKCkgaWYgQG1vZGFsUGFuZWw/LmlzVmlzaWJsZSgpXG5cbiAgICBAZWRpdG9yU3Vic2NyaXB0aW9ucz8uZGlzcG9zZSgpXG4gICAgZWRpdG9yID0gQGdldEFjdGl2ZVRleHRFZGl0b3IoKVxuXG4gICAgcmV0dXJuIHVubGVzcyBlZGl0b3I/XG4gICAgcmV0dXJuIGlmIGVkaXRvci5nZXRSb290U2NvcGVEZXNjcmlwdG9yKCkuc2NvcGVzWzBdLmluZGV4T2YoQHJ1YnlSb290U2NvcGUpIGlzIC0xXG5cbiAgICBAaW5pdCgpIHVubGVzcyBAcnVieUJsb2NrVmlldz9cblxuICAgIGVkaXRvckVsZW1lbnQgPSBhdG9tLnZpZXdzLmdldFZpZXcoZWRpdG9yKVxuICAgIEBlZGl0b3JTdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgIEBlZGl0b3JTdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZChlZGl0b3JFbGVtZW50LFxuICAgICAgJ3J1YnktYmxvY2s6Z28tdG8tbWF0Y2hpbmctbGluZSc6ID0+XG4gICAgICAgIEBnb1RvTWF0Y2hpbmdMaW5lKClcbiAgICApXG5cbiAgICAjIEBlZGl0b3JTdWJzY3JpcHRpb25zLmFkZChlZGl0b3Iub25EaWRDaGFuZ2VDdXJzb3JQb3NpdGlvbihAZGVib3VuY2VkQ3Vyc29yQ2hhbmdlZENhbGxiYWNrKSlcbiAgICBAZWRpdG9yU3Vic2NyaXB0aW9ucy5hZGQoZWRpdG9yLm9uRGlkQ2hhbmdlQ3Vyc29yUG9zaXRpb24oXy5kZWJvdW5jZSggPT5cbiAgICAgIHJldHVybiB1bmxlc3MgQGdldEFjdGl2ZVRleHRFZGl0b3IoKSBpcyBlZGl0b3JcbiAgICAgIEBibG9ja1N0YXJ0ZWRSb3dOdW1iZXIgPSBudWxsXG4gICAgICBAbW9kYWxQYW5lbC5oaWRlKCkgaWYgQG1vZGFsUGFuZWwuaXNWaXNpYmxlKClcbiAgICAgIEBtYXJrZXI/LmRlc3Ryb3koKVxuICAgICAgQHNlYXJjaEZvckJsb2NrKClcbiAgICAsIDEwMCkpKVxuXG4gICAgQHNlYXJjaEZvckJsb2NrKClcblxuICBzZWFyY2hGb3JCbG9jazogLT5cbiAgICBlZGl0b3IgPSBAZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgZ3JhbW1hciA9IGVkaXRvci5nZXRHcmFtbWFyKClcbiAgICBjdXJzb3IgPSBlZGl0b3IuZ2V0TGFzdEN1cnNvcigpXG4gICAgY3VycmVudFJvd051bWJlciA9IGN1cnNvci5nZXRCdWZmZXJSb3coKVxuXG4gICAgIyBzY29wZSBhbmQgd29yZCBtYXRjaGVzICdlbmQnXG4gICAgcmV0dXJuIGlmIGN1cnNvci5nZXRTY29wZURlc2NyaXB0b3IoKS5zY29wZXMuaW5kZXhPZihAcnVieUtleXdvcmRDb250cm9sU2NvcGUpIGlzIC0xIG9yXG4gICAgICAgICAgICAgIEBydWJ5S2V5d29yZENvbnRyb2xOYW1lcy5pbmRleE9mKGVkaXRvci5nZXRXb3JkVW5kZXJDdXJzb3IoKSkgaXMgLTFcblxuICAgIEBlbmRCbG9ja1N0YWNrLnB1c2goZWRpdG9yLmdldFdvcmRVbmRlckN1cnNvcilcblxuICAgICMgaXRlcmF0ZSBsaW5lcyBhYm92ZSB0aGUgY3Vyc29yXG4gICAgZm9yIHJvd051bWJlciBpbiBbY3Vyc29yLmdldEJ1ZmZlclJvdygpLi4wXVxuICAgICAgY29udGludWUgaWYgZWRpdG9yLmlzQnVmZmVyUm93Q29tbWVudGVkKHJvd051bWJlcilcblxuICAgICAgaWYgcm93TnVtYmVyIGlzIGN1cnJlbnRSb3dOdW1iZXJcbiAgICAgICAgcHJldldvcmRCb3VuZGFyeVBvcyA9IGN1cnNvci5nZXRQcmV2aW91c1dvcmRCb3VuZGFyeUJ1ZmZlclBvc2l0aW9uKClcbiAgICAgICAgcm93ID0gZWRpdG9yLmdldFRleHRJbkJ1ZmZlclJhbmdlKFtbcm93TnVtYmVyLCAwXSwgcHJldldvcmRCb3VuZGFyeVBvc10pXG4gICAgICBlbHNlXG4gICAgICAgIHJvdyA9IGVkaXRvci5saW5lVGV4dEZvckJ1ZmZlclJvdyhyb3dOdW1iZXIpXG5cbiAgICAgIHRva2VucyA9IGdyYW1tYXIudG9rZW5pemVMaW5lKHJvdykudG9rZW5zXG4gICAgICBmaWx0ZXJlZFRva2VucyA9ICh0b2tlbiBmb3IgdG9rZW4saSBpbiB0b2tlbnMgd2hlbiAhdG9rZW4udmFsdWUubWF0Y2ggL15cXHMqJC8pXG5cbiAgICAgIHN0YXJ0QmxvY2sgPSAodG9rZW4gZm9yIHRva2VuIGluIGZpbHRlcmVkVG9rZW5zIHdoZW4gdG9rZW4uc2NvcGVzLmluZGV4T2YoQHJ1YnlEb1Njb3BlKSA+PSAwKVxuICAgICAgaWYgc3RhcnRCbG9jay5sZW5ndGggPiAwXG4gICAgICAgIGlmIHRva2VuLnZhbHVlIGlzbnQgQHJ1YnlEb0Jsb2NrTmFtZSBvclxuICAgICAgICAgICBmaWx0ZXJlZFRva2Vuc1swXS52YWx1ZSBpc250IEBydWJ5V2hpbGVCbG9ja05hbWVcbiAgICAgICAgICBAZW5kQmxvY2tTdGFjay5wb3AoKVxuICAgICAgICBpZiBAZW5kQmxvY2tTdGFjay5sZW5ndGggaXMgMFxuICAgICAgICAgIHJldHVybiBAaGlnaGxpZ2h0QmxvY2socm93TnVtYmVyKVxuXG4gICAgICBmb3IgdG9rZW4gaW4gZmlsdGVyZWRUb2tlbnMgYnkgLTFcbiAgICAgICAgZm9yIHNjb3BlIGluIHRva2VuLnNjb3Blc1xuICAgICAgICAgIGlmIHNjb3BlIGlzIEBydWJ5S2V5d29yZENvbnRyb2xTY29wZSBhbmQgdG9rZW4udmFsdWUgaXMgQHJ1YnlFbmRCbG9ja05hbWVcbiAgICAgICAgICAgIEBlbmRCbG9ja1N0YWNrLnB1c2goc2NvcGUudmFsdWUpXG4gICAgICAgICAgZWxzZSBpZiBAcnVieVN0YXJ0QmxvY2tTY29wZXMuaW5kZXhPZihzY29wZSkgPj0gMCBhbmRcbiAgICAgICAgICAgICAgICAgIEBydWJ5U3RhcnRCbG9ja05hbWVzLmluZGV4T2YodG9rZW4udmFsdWUpID49IDBcbiAgICAgICAgICAgICMgU3VwcG9ydCBhc3NpZ25pbmcgdmFyaWFibGUgd2l0aCBhIGNhc2Ugc3RhdGVtZW50XG4gICAgICAgICAgICAjIGUuZy5cbiAgICAgICAgICAgICMgdmFyID0gY2FzZSBjb25kXG4gICAgICAgICAgICAjICAgICAgIHdoZW4gMSB0aGVuIDEwXG4gICAgICAgICAgICAjICAgICAgIGVuZFxuICAgICAgICAgICAgaWYgdG9rZW4udmFsdWUgaXMgJ2Nhc2UnXG4gICAgICAgICAgICAgIEBlbmRCbG9ja1N0YWNrLnBvcCgpXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgIGZvciBmaXJzdFRva2VuU2NvcGUgaW4gZmlsdGVyZWRUb2tlbnNbMF0uc2NvcGVzXG4gICAgICAgICAgICAgICAgaWYgQHJ1YnlTdGFydEJsb2NrU2NvcGVzLmluZGV4T2YoZmlyc3RUb2tlblNjb3BlKSA+PSAwIGFuZFxuICAgICAgICAgICAgICAgICAgIEBydWJ5U3RhcnRCbG9ja05hbWVzLmluZGV4T2YoZmlsdGVyZWRUb2tlbnNbMF0udmFsdWUpID49IDBcbiAgICAgICAgICAgICAgICAgIEBlbmRCbG9ja1N0YWNrLnBvcCgpXG4gICAgICAgICAgICAgICAgICBicmVha1xuXG4gICAgICAgICAgICBpZiBAZW5kQmxvY2tTdGFjay5sZW5ndGggaXMgMFxuICAgICAgICAgICAgICByZXR1cm4gQGhpZ2hsaWdodEJsb2NrKHJvd051bWJlcilcblxuICBoaWdobGlnaHRCbG9jazogKHJvd051bWJlciktPlxuICAgIGVkaXRvciA9IEBnZXRBY3RpdmVUZXh0RWRpdG9yKClcbiAgICByb3cgPSBlZGl0b3IubGluZVRleHRGb3JCdWZmZXJSb3cocm93TnVtYmVyKVxuICAgIGZpcnN0Q2hhclBvaW50ID0gcm93LnNlYXJjaCgvXFxTLylcbiAgICBAbWFya2VyID0gZWRpdG9yLm1hcmtCdWZmZXJSYW5nZShbW3Jvd051bWJlciwgZmlyc3RDaGFyUG9pbnRdLCBbcm93TnVtYmVyLCByb3cubGVuZ3RoXV0pXG5cbiAgICBAYmxvY2tTdGFydGVkUm93TnVtYmVyID0gcm93TnVtYmVyXG4gICAgaWYgYXRvbS5jb25maWcuZ2V0KCdydWJ5LWJsb2NrLmhpZ2hsaWdodExpbmUnKVxuICAgICAgZWRpdG9yLmRlY29yYXRlTWFya2VyKEBtYXJrZXIsIHt0eXBlOiAnaGlnaGxpZ2h0JywgY2xhc3M6ICdydWJ5LWJsb2NrLWhpZ2hsaWdodCd9KVxuICAgIGlmIGF0b20uY29uZmlnLmdldCgncnVieS1ibG9jay5oaWdobGlnaHRMaW5lTnVtYmVyJylcbiAgICAgIGVkaXRvci5kZWNvcmF0ZU1hcmtlcihAbWFya2VyLCB7dHlwZTogJ2xpbmUtbnVtYmVyJywgY2xhc3M6ICdydWJ5LWJsb2NrLWhpZ2hsaWdodCd9KVxuICAgIGlmIGF0b20uY29uZmlnLmdldCgncnVieS1ibG9jay5zaG93Qm90dG9tUGFuZWwnKVxuICAgICAgQHJ1YnlCbG9ja1ZpZXcudXBkYXRlTWVzc2FnZShyb3dOdW1iZXIpXG4gICAgICBAbW9kYWxQYW5lbC5zaG93KClcblxuICBsb2FkQ2xhc3NlczogLT5cbiAgICBfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xuICAgIFJ1YnlCbG9ja1ZpZXcgPSByZXF1aXJlICcuL3J1YnktYmxvY2stdmlldydcbiJdfQ==
