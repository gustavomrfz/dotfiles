(function() {
  var provider;

  provider = require('./provider');

  module.exports = {
    config: provider.config,
    activate: function() {
      return console.log('activate aligner-puppet');
    },
    getProvider: function() {
      return provider;
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9hbGlnbmVyLXB1cHBldC9saWIvbWFpbi5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLFFBQUEsR0FBVyxPQUFBLENBQVEsWUFBUjs7RUFFWCxNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsTUFBQSxFQUFRLFFBQVEsQ0FBQyxNQUFqQjtJQUVBLFFBQUEsRUFBVSxTQUFBO2FBQ1IsT0FBTyxDQUFDLEdBQVIsQ0FBWSx5QkFBWjtJQURRLENBRlY7SUFLQSxXQUFBLEVBQWEsU0FBQTthQUFHO0lBQUgsQ0FMYjs7QUFIRiIsInNvdXJjZXNDb250ZW50IjpbInByb3ZpZGVyID0gcmVxdWlyZSAnLi9wcm92aWRlcidcblxubW9kdWxlLmV4cG9ydHMgPVxuICBjb25maWc6IHByb3ZpZGVyLmNvbmZpZ1xuXG4gIGFjdGl2YXRlOiAtPlxuICAgIGNvbnNvbGUubG9nICdhY3RpdmF0ZSBhbGlnbmVyLXB1cHBldCdcblxuICBnZXRQcm92aWRlcjogLT4gcHJvdmlkZXJcbiJdfQ==
