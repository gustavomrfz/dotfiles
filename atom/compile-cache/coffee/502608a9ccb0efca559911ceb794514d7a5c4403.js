(function() {
  var HtmlEntities, XmlEntities, countTabs, crypto, escape_sql, htmlEntities, string, tabify, unescape_sql, untabify, xmlEntities,
    modulo = function(a, b) { return (+a % (b = +b) + b) % b; };

  HtmlEntities = require('html-entities').AllHtmlEntities;

  htmlEntities = new HtmlEntities();

  XmlEntities = require('html-entities').XmlEntities;

  xmlEntities = new XmlEntities();

  crypto = require('crypto');

  string = require('string');

  module.exports = {
    activate: function() {
      atom.commands.add('atom-workspace', 'text-manipulation:base64-encode', (function(_this) {
        return function() {
          return _this.convert(_this.encodeBase64);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:base64-decode', (function(_this) {
        return function() {
          return _this.convert(_this.decodeBase64);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:html-encode', (function(_this) {
        return function() {
          return _this.convert(_this.encodeHtml);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:html-decode', (function(_this) {
        return function() {
          return _this.convert(_this.decodeHtml);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:xml-encode', (function(_this) {
        return function() {
          return _this.convert(_this.encodeXml);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:xml-decode', (function(_this) {
        return function() {
          return _this.convert(_this.decodeXml);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:sql-encode', (function(_this) {
        return function() {
          return _this.convert(_this.encodeSql);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:sql-decode', (function(_this) {
        return function() {
          return _this.convert(_this.decodeSql);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:url-encode', (function(_this) {
        return function() {
          return _this.convert(_this.encodeUrl);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:url-decode', (function(_this) {
        return function() {
          return _this.convert(_this.decodeUrl);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:hash-md5', (function(_this) {
        return function() {
          return _this.convert(_this.hashMD5);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:hash-sha1', (function(_this) {
        return function() {
          return _this.convert(_this.hashSHA1);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:hash-sha256', (function(_this) {
        return function() {
          return _this.convert(_this.hashSHA256);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:hash-sha512', (function(_this) {
        return function() {
          return _this.convert(_this.hashSHA512);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:format-camelize', (function(_this) {
        return function() {
          return _this.convert(_this.formatCamelize);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:format-dasherize', (function(_this) {
        return function() {
          return _this.convert(_this.formatDasherize);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:format-underscore', (function(_this) {
        return function() {
          return _this.convert(_this.formatUnderscore);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:format-slugify', (function(_this) {
        return function() {
          return _this.convert(_this.formatSlugify);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:format-humanize', (function(_this) {
        return function() {
          return _this.convert(_this.formatHumanize);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-trim', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceTrim);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-collapse', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceCollapse);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-remove', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceRemove);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-emptylines', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceEmptyLines);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-tabify', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceTabify);
        };
      })(this));
      atom.commands.add('atom-workspace', 'text-manipulation:whitespace-untabify', (function(_this) {
        return function() {
          return _this.convert(_this.whitespaceUntabify);
        };
      })(this));
      return atom.commands.add('atom-workspace', 'text-manipulation:strip-punctuation', (function(_this) {
        return function() {
          return _this.convert(_this.stripPunctuation);
        };
      })(this));
    },
    convert: function(converter) {
      var editor, i, len, results, selection, selections;
      editor = atom.workspace.getActiveTextEditor();
      selections = editor.getSelections();
      results = [];
      for (i = 0, len = selections.length; i < len; i++) {
        selection = selections[i];
        results.push(selection.insertText(converter(selection.getText()), {
          'select': true
        }));
      }
      return results;
    },
    encodeBase64: function(text) {
      return new Buffer(text).toString('base64');
    },
    decodeBase64: function(text) {
      if (/^[A-Za-z0-9+\/=]+$/.test(text)) {
        return new Buffer(text, 'base64').toString('utf8');
      } else {
        return text;
      }
    },
    encodeHtml: function(text) {
      return htmlEntities.encodeNonUTF(text);
    },
    decodeHtml: function(text) {
      return htmlEntities.decode(text);
    },
    encodeXml: function(text) {
      return xmlEntities.encodeNonUTF(text);
    },
    decodeXml: function(text) {
      return xmlEntities.decode(text);
    },
    encodeSql: function(text) {
      return escape_sql(text);
    },
    decodeSql: function(text) {
      return unescape_sql(text);
    },
    encodeUrl: function(text) {
      return encodeURIComponent(text);
    },
    decodeUrl: function(text) {
      return decodeURIComponent(text);
    },
    hashMD5: function(text) {
      var hash;
      hash = crypto.createHash('md5');
      hash.update(new Buffer(text));
      return hash.digest('hex');
    },
    hashSHA1: function(text) {
      var hash;
      hash = crypto.createHash('sha1');
      hash.update(new Buffer(text));
      return hash.digest('hex');
    },
    hashSHA256: function(text) {
      var hash;
      hash = crypto.createHash('sha256');
      hash.update(new Buffer(text));
      return hash.digest('hex');
    },
    hashSHA512: function(text) {
      var hash;
      hash = crypto.createHash('sha512');
      hash.update(new Buffer(text));
      return hash.digest('hex');
    },
    formatCamelize: function(text) {
      return string(text).camelize().s;
    },
    formatDasherize: function(text) {
      return string(text).dasherize().s;
    },
    formatUnderscore: function(text) {
      return string(text).underscore().s;
    },
    formatSlugify: function(text) {
      return string(text).slugify().s;
    },
    formatHumanize: function(text) {
      return string(text).humanize().s;
    },
    whitespaceTrim: function(text) {
      var line, lines;
      lines = (function() {
        var i, len, ref, results;
        ref = text.split('\n');
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          line = ref[i];
          results.push(string(line).replace(/\s+$/, "").s);
        }
        return results;
      })();
      return lines.join('\n');
    },
    whitespaceCollapse: function(text) {
      return string(text).collapseWhitespace().s;
    },
    whitespaceRemove: function(text) {
      return string(text).collapseWhitespace().s.replace(/\s+/g, '');
    },
    whitespaceEmptyLines: function(text) {
      var line, lines;
      lines = (function() {
        var i, len, ref, results;
        ref = text.split('\n');
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          line = ref[i];
          if (line.length > 0) {
            results.push(line);
          }
        }
        return results;
      })();
      return lines.join('\n');
    },
    whitespaceTabify: function(text) {
      var editor, line, lines, tabLength;
      editor = atom.workspace.getActiveTextEditor();
      tabLength = editor.getTabLength();
      lines = (function() {
        var i, len, ref, results;
        ref = text.split('\n');
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          line = ref[i];
          results.push(tabify(line, tabLength));
        }
        return results;
      })();
      return lines.join('\n');
    },
    whitespaceUntabify: function(text) {
      var editor, line, lines, tabLength;
      editor = atom.workspace.getActiveTextEditor();
      tabLength = editor.getTabLength();
      lines = (function() {
        var i, len, ref, results;
        ref = text.split('\n');
        results = [];
        for (i = 0, len = ref.length; i < len; i++) {
          line = ref[i];
          results.push(untabify(line, tabLength));
        }
        return results;
      })();
      return lines.join('\n');
    },
    stripPunctuation: function(text) {
      return string(text).stripPunctuation().s;
    }
  };

  tabify = function(str, tabLength) {
    var count, ref, spaces, start, tabs;
    ref = countTabs(str, tabLength), start = ref[0], count = ref[1];
    tabs = string('\t').repeat(Math.floor(count / tabLength)).s;
    spaces = string(' ').repeat(modulo(count, tabLength)).s;
    return tabs + spaces + str.substr(start);
  };

  untabify = function(str, tabLength) {
    var count, ref, spaces, start;
    ref = countTabs(str, tabLength), start = ref[0], count = ref[1];
    spaces = string(' ').repeat(count).s;
    return spaces + str.substr(start);
  };

  countTabs = function(str, tabLength) {
    var ch, count, i, len, ref, start;
    start = str.search(/[^\s]/);
    if (start < 0) {
      start = str.length;
    }
    count = 0;
    ref = str.substr(0, start);
    for (i = 0, len = ref.length; i < len; i++) {
      ch = ref[i];
      switch (ch) {
        case ' ':
          count += 1;
          break;
        case '\t':
          count = (Math.floor(count / tabLength) + 1) * tabLength;
      }
    }
    return [start, count];
  };

  escape_sql = function(str) {
    return str.replace(/[\0\b\t\n\r\\"'%\x1a]/g, function(char) {
      switch (char) {
        case "\0":
          return "\\0";
        case "\b":
          return "\\b";
        case "\t":
          return "\\t";
        case "\n":
          return "\\n";
        case "\r":
          return "\\r";
        case "\"":
          return "\\\"";
        case "'":
          return "\\'";
        case "\\":
          return "\\\\";
        case "%":
          return "\\%";
        case "\x1a":
          return "\\z";
      }
    });
  };

  unescape_sql = function(str) {
    return str.replace(/\\[0btnr"'\\%z]/g, function(char) {
      switch (char) {
        case "\\0":
          return "\0";
        case "\\b":
          return "\b";
        case "\\t":
          return "\t";
        case "\\n":
          return "\n";
        case "\\r":
          return "\r";
        case "\\\"":
          return "\"";
        case "\\'":
          return "'";
        case "\\\\":
          return "\\";
        case "\\%":
          return "%";
        case "\\z":
          return "\x1a";
      }
    });
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy90ZXh0LW1hbmlwdWxhdGlvbi9saWIvdGV4dC1tYW5pcHVsYXRpb24uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSwySEFBQTtJQUFBOztFQUFBLFlBQUEsR0FBZSxPQUFBLENBQVEsZUFBUixDQUF3QixDQUFDOztFQUN4QyxZQUFBLEdBQW1CLElBQUEsWUFBQSxDQUFBOztFQUNuQixXQUFBLEdBQWMsT0FBQSxDQUFRLGVBQVIsQ0FBd0IsQ0FBQzs7RUFDdkMsV0FBQSxHQUFrQixJQUFBLFdBQUEsQ0FBQTs7RUFDbEIsTUFBQSxHQUFTLE9BQUEsQ0FBUSxRQUFSOztFQUNULE1BQUEsR0FBUyxPQUFBLENBQVEsUUFBUjs7RUFFVCxNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsUUFBQSxFQUFVLFNBQUE7TUFDUixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLGlDQUFwQyxFQUF1RSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsWUFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF2RTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsaUNBQXBDLEVBQXVFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxZQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXZFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQywrQkFBcEMsRUFBcUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLFVBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBckU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLCtCQUFwQyxFQUFxRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsVUFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFyRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsOEJBQXBDLEVBQW9FLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxTQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXBFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyw4QkFBcEMsRUFBb0UsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLFNBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBcEU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLDhCQUFwQyxFQUFvRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsU0FBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFwRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsOEJBQXBDLEVBQW9FLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxTQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXBFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyw4QkFBcEMsRUFBb0UsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLFNBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBcEU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLDhCQUFwQyxFQUFvRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsU0FBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFwRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsNEJBQXBDLEVBQWtFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxPQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWxFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyw2QkFBcEMsRUFBbUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLFFBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBbkU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLCtCQUFwQyxFQUFxRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsVUFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFyRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsK0JBQXBDLEVBQXFFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxVQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXJFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyxtQ0FBcEMsRUFBeUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLGNBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBekU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLG9DQUFwQyxFQUEwRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsZUFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUExRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MscUNBQXBDLEVBQTJFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxnQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0Msa0NBQXBDLEVBQXdFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxhQUFWO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXhFO01BQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQyxtQ0FBcEMsRUFBeUUsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxPQUFELENBQVMsS0FBQyxDQUFBLGNBQVY7UUFBSDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBekU7TUFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLG1DQUFwQyxFQUF5RSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBUyxLQUFDLENBQUEsY0FBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF6RTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsdUNBQXBDLEVBQTZFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxrQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE3RTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MscUNBQXBDLEVBQTJFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxnQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MseUNBQXBDLEVBQStFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxvQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEvRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MscUNBQXBDLEVBQTJFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxnQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzRTtNQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MsdUNBQXBDLEVBQTZFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxrQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUE3RTthQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0MscUNBQXBDLEVBQTJFLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsT0FBRCxDQUFTLEtBQUMsQ0FBQSxnQkFBVjtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzRTtJQTFCUSxDQUFWO0lBNEJBLE9BQUEsRUFBUyxTQUFDLFNBQUQ7QUFDUCxVQUFBO01BQUEsTUFBQSxHQUFTLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQWYsQ0FBQTtNQUNULFVBQUEsR0FBYSxNQUFNLENBQUMsYUFBUCxDQUFBO0FBS2I7V0FBQSw0Q0FBQTs7cUJBQUEsU0FBUyxDQUFDLFVBQVYsQ0FBcUIsU0FBQSxDQUFVLFNBQVMsQ0FBQyxPQUFWLENBQUEsQ0FBVixDQUFyQixFQUFxRDtVQUFDLFFBQUEsRUFBVSxJQUFYO1NBQXJEO0FBQUE7O0lBUE8sQ0E1QlQ7SUFxQ0EsWUFBQSxFQUFjLFNBQUMsSUFBRDthQUNSLElBQUEsTUFBQSxDQUFPLElBQVAsQ0FBWSxDQUFDLFFBQWIsQ0FBc0IsUUFBdEI7SUFEUSxDQXJDZDtJQXdDQSxZQUFBLEVBQWMsU0FBQyxJQUFEO01BQ1osSUFBRyxvQkFBbUIsQ0FBQyxJQUFwQixDQUF5QixJQUF6QixDQUFIO2VBQ00sSUFBQSxNQUFBLENBQU8sSUFBUCxFQUFhLFFBQWIsQ0FBc0IsQ0FBQyxRQUF2QixDQUFnQyxNQUFoQyxFQUROO09BQUEsTUFBQTtlQUdFLEtBSEY7O0lBRFksQ0F4Q2Q7SUE4Q0EsVUFBQSxFQUFZLFNBQUMsSUFBRDthQUNWLFlBQVksQ0FBQyxZQUFiLENBQTBCLElBQTFCO0lBRFUsQ0E5Q1o7SUFpREEsVUFBQSxFQUFZLFNBQUMsSUFBRDthQUNWLFlBQVksQ0FBQyxNQUFiLENBQW9CLElBQXBCO0lBRFUsQ0FqRFo7SUFvREEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULFdBQVcsQ0FBQyxZQUFaLENBQXlCLElBQXpCO0lBRFMsQ0FwRFg7SUF1REEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULFdBQVcsQ0FBQyxNQUFaLENBQW1CLElBQW5CO0lBRFMsQ0F2RFg7SUEwREEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULFVBQUEsQ0FBVyxJQUFYO0lBRFMsQ0ExRFg7SUE2REEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULFlBQUEsQ0FBYSxJQUFiO0lBRFMsQ0E3RFg7SUFnRUEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULGtCQUFBLENBQW1CLElBQW5CO0lBRFMsQ0FoRVg7SUFtRUEsU0FBQSxFQUFXLFNBQUMsSUFBRDthQUNULGtCQUFBLENBQW1CLElBQW5CO0lBRFMsQ0FuRVg7SUFzRUEsT0FBQSxFQUFTLFNBQUMsSUFBRDtBQUNQLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLFVBQVAsQ0FBa0IsS0FBbEI7TUFDUCxJQUFJLENBQUMsTUFBTCxDQUFnQixJQUFBLE1BQUEsQ0FBTyxJQUFQLENBQWhCO2FBQ0EsSUFBSSxDQUFDLE1BQUwsQ0FBWSxLQUFaO0lBSE8sQ0F0RVQ7SUEyRUEsUUFBQSxFQUFVLFNBQUMsSUFBRDtBQUNSLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLFVBQVAsQ0FBa0IsTUFBbEI7TUFDUCxJQUFJLENBQUMsTUFBTCxDQUFnQixJQUFBLE1BQUEsQ0FBTyxJQUFQLENBQWhCO2FBQ0EsSUFBSSxDQUFDLE1BQUwsQ0FBWSxLQUFaO0lBSFEsQ0EzRVY7SUFnRkEsVUFBQSxFQUFZLFNBQUMsSUFBRDtBQUNWLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLFVBQVAsQ0FBa0IsUUFBbEI7TUFDUCxJQUFJLENBQUMsTUFBTCxDQUFnQixJQUFBLE1BQUEsQ0FBTyxJQUFQLENBQWhCO2FBQ0EsSUFBSSxDQUFDLE1BQUwsQ0FBWSxLQUFaO0lBSFUsQ0FoRlo7SUFxRkEsVUFBQSxFQUFZLFNBQUMsSUFBRDtBQUNWLFVBQUE7TUFBQSxJQUFBLEdBQU8sTUFBTSxDQUFDLFVBQVAsQ0FBa0IsUUFBbEI7TUFDUCxJQUFJLENBQUMsTUFBTCxDQUFnQixJQUFBLE1BQUEsQ0FBTyxJQUFQLENBQWhCO2FBQ0EsSUFBSSxDQUFDLE1BQUwsQ0FBWSxLQUFaO0lBSFUsQ0FyRlo7SUEwRkEsY0FBQSxFQUFnQixTQUFDLElBQUQ7YUFDZCxNQUFBLENBQU8sSUFBUCxDQUFZLENBQUMsUUFBYixDQUFBLENBQXVCLENBQUM7SUFEVixDQTFGaEI7SUE2RkEsZUFBQSxFQUFpQixTQUFDLElBQUQ7YUFDZixNQUFBLENBQU8sSUFBUCxDQUFZLENBQUMsU0FBYixDQUFBLENBQXdCLENBQUM7SUFEVixDQTdGakI7SUFnR0EsZ0JBQUEsRUFBa0IsU0FBQyxJQUFEO2FBQ2hCLE1BQUEsQ0FBTyxJQUFQLENBQVksQ0FBQyxVQUFiLENBQUEsQ0FBeUIsQ0FBQztJQURWLENBaEdsQjtJQW1HQSxhQUFBLEVBQWUsU0FBQyxJQUFEO2FBQ2IsTUFBQSxDQUFPLElBQVAsQ0FBWSxDQUFDLE9BQWIsQ0FBQSxDQUFzQixDQUFDO0lBRFYsQ0FuR2Y7SUFzR0EsY0FBQSxFQUFnQixTQUFDLElBQUQ7YUFDZCxNQUFBLENBQU8sSUFBUCxDQUFZLENBQUMsUUFBYixDQUFBLENBQXVCLENBQUM7SUFEVixDQXRHaEI7SUF5R0EsY0FBQSxFQUFnQixTQUFDLElBQUQ7QUFDZCxVQUFBO01BQUEsS0FBQTs7QUFBUztBQUFBO2FBQUEscUNBQUE7O3VCQUFBLE1BQUEsQ0FBTyxJQUFQLENBQVksQ0FBQyxPQUFiLENBQXFCLE1BQXJCLEVBQTZCLEVBQTdCLENBQWdDLENBQUM7QUFBakM7OzthQUNULEtBQUssQ0FBQyxJQUFOLENBQVcsSUFBWDtJQUZjLENBekdoQjtJQTZHQSxrQkFBQSxFQUFvQixTQUFDLElBQUQ7YUFDbEIsTUFBQSxDQUFPLElBQVAsQ0FBWSxDQUFDLGtCQUFiLENBQUEsQ0FBaUMsQ0FBQztJQURoQixDQTdHcEI7SUFnSEEsZ0JBQUEsRUFBa0IsU0FBQyxJQUFEO2FBQ2hCLE1BQUEsQ0FBTyxJQUFQLENBQVksQ0FBQyxrQkFBYixDQUFBLENBQWlDLENBQUMsQ0FBQyxDQUFDLE9BQXBDLENBQTRDLE1BQTVDLEVBQW9ELEVBQXBEO0lBRGdCLENBaEhsQjtJQW1IQSxvQkFBQSxFQUFzQixTQUFDLElBQUQ7QUFDcEIsVUFBQTtNQUFBLEtBQUE7O0FBQVM7QUFBQTthQUFBLHFDQUFBOztjQUF1QyxJQUFJLENBQUMsTUFBTCxHQUFjO3lCQUFyRDs7QUFBQTs7O2FBQ1QsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFYO0lBRm9CLENBbkh0QjtJQXVIQSxnQkFBQSxFQUFrQixTQUFDLElBQUQ7QUFDaEIsVUFBQTtNQUFBLE1BQUEsR0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7TUFDVCxTQUFBLEdBQVksTUFBTSxDQUFDLFlBQVAsQ0FBQTtNQUNaLEtBQUE7O0FBQVM7QUFBQTthQUFBLHFDQUFBOzt1QkFBQSxNQUFBLENBQU8sSUFBUCxFQUFhLFNBQWI7QUFBQTs7O2FBQ1QsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFYO0lBSmdCLENBdkhsQjtJQTZIQSxrQkFBQSxFQUFvQixTQUFDLElBQUQ7QUFDbEIsVUFBQTtNQUFBLE1BQUEsR0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7TUFDVCxTQUFBLEdBQVksTUFBTSxDQUFDLFlBQVAsQ0FBQTtNQUNaLEtBQUE7O0FBQVM7QUFBQTthQUFBLHFDQUFBOzt1QkFBQSxRQUFBLENBQVMsSUFBVCxFQUFlLFNBQWY7QUFBQTs7O2FBQ1QsS0FBSyxDQUFDLElBQU4sQ0FBVyxJQUFYO0lBSmtCLENBN0hwQjtJQW1JQSxnQkFBQSxFQUFrQixTQUFDLElBQUQ7YUFDaEIsTUFBQSxDQUFPLElBQVAsQ0FBWSxDQUFDLGdCQUFiLENBQUEsQ0FBK0IsQ0FBQztJQURoQixDQW5JbEI7OztFQXdJRixNQUFBLEdBQVMsU0FBQyxHQUFELEVBQU0sU0FBTjtBQUNQLFFBQUE7SUFBQSxNQUFpQixTQUFBLENBQVUsR0FBVixFQUFlLFNBQWYsQ0FBakIsRUFBQyxjQUFELEVBQVE7SUFDUixJQUFBLEdBQU8sTUFBQSxDQUFPLElBQVAsQ0FBWSxDQUFDLE1BQWIsWUFBb0IsUUFBUyxVQUE3QixDQUF1QyxDQUFDO0lBQy9DLE1BQUEsR0FBUyxNQUFBLENBQU8sR0FBUCxDQUFXLENBQUMsTUFBWixRQUFtQixPQUFTLFVBQTVCLENBQXNDLENBQUM7V0FDaEQsSUFBQSxHQUFPLE1BQVAsR0FBZ0IsR0FBRyxDQUFDLE1BQUosQ0FBVyxLQUFYO0VBSlQ7O0VBTVQsUUFBQSxHQUFXLFNBQUMsR0FBRCxFQUFNLFNBQU47QUFDVCxRQUFBO0lBQUEsTUFBaUIsU0FBQSxDQUFVLEdBQVYsRUFBZSxTQUFmLENBQWpCLEVBQUMsY0FBRCxFQUFRO0lBQ1IsTUFBQSxHQUFTLE1BQUEsQ0FBTyxHQUFQLENBQVcsQ0FBQyxNQUFaLENBQW1CLEtBQW5CLENBQXlCLENBQUM7V0FDbkMsTUFBQSxHQUFTLEdBQUcsQ0FBQyxNQUFKLENBQVcsS0FBWDtFQUhBOztFQUtYLFNBQUEsR0FBWSxTQUFDLEdBQUQsRUFBTSxTQUFOO0FBQ1YsUUFBQTtJQUFBLEtBQUEsR0FBUSxHQUFHLENBQUMsTUFBSixDQUFXLE9BQVg7SUFDUixJQUFHLEtBQUEsR0FBUSxDQUFYO01BQ0UsS0FBQSxHQUFRLEdBQUcsQ0FBQyxPQURkOztJQUVBLEtBQUEsR0FBUTtBQUNSO0FBQUEsU0FBQSxxQ0FBQTs7QUFDRSxjQUFPLEVBQVA7QUFBQSxhQUNPLEdBRFA7VUFDZ0IsS0FBQSxJQUFTO0FBQWxCO0FBRFAsYUFFTyxJQUZQO1VBRWlCLEtBQUEsR0FBUSxZQUFDLFFBQVMsVUFBVCxHQUFxQixDQUF0QixDQUFBLEdBQTJCO0FBRnBEO0FBREY7V0FJQSxDQUFDLEtBQUQsRUFBUSxLQUFSO0VBVFU7O0VBV1osVUFBQSxHQUFhLFNBQUMsR0FBRDtXQUNYLEdBQUcsQ0FBQyxPQUFKLENBQVksd0JBQVosRUFBc0MsU0FBQyxJQUFEO0FBQ3BDLGNBQU8sSUFBUDtBQUFBLGFBQ08sSUFEUDtpQkFDaUI7QUFEakIsYUFFTyxJQUZQO2lCQUVpQjtBQUZqQixhQUdPLElBSFA7aUJBR2lCO0FBSGpCLGFBSU8sSUFKUDtpQkFJaUI7QUFKakIsYUFLTyxJQUxQO2lCQUtpQjtBQUxqQixhQU1PLElBTlA7aUJBTWlCO0FBTmpCLGFBT08sR0FQUDtpQkFPZ0I7QUFQaEIsYUFRTyxJQVJQO2lCQVFpQjtBQVJqQixhQVNPLEdBVFA7aUJBU2dCO0FBVGhCLGFBVU8sTUFWUDtpQkFVbUI7QUFWbkI7SUFEb0MsQ0FBdEM7RUFEVzs7RUFlYixZQUFBLEdBQWUsU0FBQyxHQUFEO1dBQ2IsR0FBRyxDQUFDLE9BQUosQ0FBWSxrQkFBWixFQUFnQyxTQUFDLElBQUQ7QUFDOUIsY0FBTyxJQUFQO0FBQUEsYUFDTyxLQURQO2lCQUNrQjtBQURsQixhQUVPLEtBRlA7aUJBRWtCO0FBRmxCLGFBR08sS0FIUDtpQkFHa0I7QUFIbEIsYUFJTyxLQUpQO2lCQUlrQjtBQUpsQixhQUtPLEtBTFA7aUJBS2tCO0FBTGxCLGFBTU8sTUFOUDtpQkFNbUI7QUFObkIsYUFPTyxLQVBQO2lCQU9rQjtBQVBsQixhQVFPLE1BUlA7aUJBUW1CO0FBUm5CLGFBU08sS0FUUDtpQkFTa0I7QUFUbEIsYUFVTyxLQVZQO2lCQVVrQjtBQVZsQjtJQUQ4QixDQUFoQztFQURhO0FBckxmIiwic291cmNlc0NvbnRlbnQiOlsiSHRtbEVudGl0aWVzID0gcmVxdWlyZSgnaHRtbC1lbnRpdGllcycpLkFsbEh0bWxFbnRpdGllc1xuaHRtbEVudGl0aWVzID0gbmV3IEh0bWxFbnRpdGllcygpXG5YbWxFbnRpdGllcyA9IHJlcXVpcmUoJ2h0bWwtZW50aXRpZXMnKS5YbWxFbnRpdGllc1xueG1sRW50aXRpZXMgPSBuZXcgWG1sRW50aXRpZXMoKVxuY3J5cHRvID0gcmVxdWlyZSgnY3J5cHRvJylcbnN0cmluZyA9IHJlcXVpcmUoJ3N0cmluZycpXG5cbm1vZHVsZS5leHBvcnRzID1cbiAgYWN0aXZhdGU6IC0+XG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmJhc2U2NC1lbmNvZGUnLCA9PiBAY29udmVydCBAZW5jb2RlQmFzZTY0XG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmJhc2U2NC1kZWNvZGUnLCA9PiBAY29udmVydCBAZGVjb2RlQmFzZTY0XG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmh0bWwtZW5jb2RlJywgPT4gQGNvbnZlcnQgQGVuY29kZUh0bWxcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246aHRtbC1kZWNvZGUnLCA9PiBAY29udmVydCBAZGVjb2RlSHRtbFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjp4bWwtZW5jb2RlJywgPT4gQGNvbnZlcnQgQGVuY29kZVhtbFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjp4bWwtZGVjb2RlJywgPT4gQGNvbnZlcnQgQGRlY29kZVhtbFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjpzcWwtZW5jb2RlJywgPT4gQGNvbnZlcnQgQGVuY29kZVNxbFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjpzcWwtZGVjb2RlJywgPT4gQGNvbnZlcnQgQGRlY29kZVNxbFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjp1cmwtZW5jb2RlJywgPT4gQGNvbnZlcnQgQGVuY29kZVVybFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjp1cmwtZGVjb2RlJywgPT4gQGNvbnZlcnQgQGRlY29kZVVybFxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjpoYXNoLW1kNScsID0+IEBjb252ZXJ0IEBoYXNoTUQ1XG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmhhc2gtc2hhMScsID0+IEBjb252ZXJ0IEBoYXNoU0hBMVxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjpoYXNoLXNoYTI1NicsID0+IEBjb252ZXJ0IEBoYXNoU0hBMjU2XG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmhhc2gtc2hhNTEyJywgPT4gQGNvbnZlcnQgQGhhc2hTSEE1MTJcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246Zm9ybWF0LWNhbWVsaXplJywgPT4gQGNvbnZlcnQgQGZvcm1hdENhbWVsaXplXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmZvcm1hdC1kYXNoZXJpemUnLCA9PiBAY29udmVydCBAZm9ybWF0RGFzaGVyaXplXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOmZvcm1hdC11bmRlcnNjb3JlJywgPT4gQGNvbnZlcnQgQGZvcm1hdFVuZGVyc2NvcmVcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246Zm9ybWF0LXNsdWdpZnknLCA9PiBAY29udmVydCBAZm9ybWF0U2x1Z2lmeVxuICAgIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXdvcmtzcGFjZScsICd0ZXh0LW1hbmlwdWxhdGlvbjpmb3JtYXQtaHVtYW5pemUnLCA9PiBAY29udmVydCBAZm9ybWF0SHVtYW5pemVcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246d2hpdGVzcGFjZS10cmltJywgPT4gQGNvbnZlcnQgQHdoaXRlc3BhY2VUcmltXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOndoaXRlc3BhY2UtY29sbGFwc2UnLCA9PiBAY29udmVydCBAd2hpdGVzcGFjZUNvbGxhcHNlXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOndoaXRlc3BhY2UtcmVtb3ZlJywgPT4gQGNvbnZlcnQgQHdoaXRlc3BhY2VSZW1vdmVcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246d2hpdGVzcGFjZS1lbXB0eWxpbmVzJywgPT4gQGNvbnZlcnQgQHdoaXRlc3BhY2VFbXB0eUxpbmVzXG4gICAgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3RleHQtbWFuaXB1bGF0aW9uOndoaXRlc3BhY2UtdGFiaWZ5JywgPT4gQGNvbnZlcnQgQHdoaXRlc3BhY2VUYWJpZnlcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246d2hpdGVzcGFjZS11bnRhYmlmeScsID0+IEBjb252ZXJ0IEB3aGl0ZXNwYWNlVW50YWJpZnlcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAndGV4dC1tYW5pcHVsYXRpb246c3RyaXAtcHVuY3R1YXRpb24nLCA9PiBAY29udmVydCBAc3RyaXBQdW5jdHVhdGlvblxuXG4gIGNvbnZlcnQ6IChjb252ZXJ0ZXIpIC0+XG4gICAgZWRpdG9yID0gYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgc2VsZWN0aW9ucyA9IGVkaXRvci5nZXRTZWxlY3Rpb25zKClcbiAgICAjaWYgc2VsZWN0aW9ucy5sZW5ndGggPT0gMSBhbmQgc2VsZWN0aW9uc1swXS5pc0VtcHR5XG4gICAgIyAgZWRpdG9yLm1vdmVUb0ZpcnN0Q2hhcmFjdGVyT2ZMaW5lKClcbiAgICAjICBlZGl0b3Iuc2VsZWN0VG9FbmRPZkxpbmUoKVxuICAgICMgIHNlbGVjdGlvbnMgPSBlZGl0b3IuZ2V0U2VsZWN0aW9ucygpXG4gICAgc2VsZWN0aW9uLmluc2VydFRleHQoY29udmVydGVyKHNlbGVjdGlvbi5nZXRUZXh0KCkpLCB7J3NlbGVjdCc6IHRydWV9KSBmb3Igc2VsZWN0aW9uIGluIHNlbGVjdGlvbnNcblxuICBlbmNvZGVCYXNlNjQ6ICh0ZXh0KSAtPlxuICAgIG5ldyBCdWZmZXIodGV4dCkudG9TdHJpbmcoJ2Jhc2U2NCcpXG5cbiAgZGVjb2RlQmFzZTY0OiAodGV4dCkgLT5cbiAgICBpZiAvXltBLVphLXowLTkrLz1dKyQvLnRlc3QodGV4dClcbiAgICAgIG5ldyBCdWZmZXIodGV4dCwgJ2Jhc2U2NCcpLnRvU3RyaW5nKCd1dGY4JylcbiAgICBlbHNlXG4gICAgICB0ZXh0XG5cbiAgZW5jb2RlSHRtbDogKHRleHQpIC0+XG4gICAgaHRtbEVudGl0aWVzLmVuY29kZU5vblVURih0ZXh0KVxuXG4gIGRlY29kZUh0bWw6ICh0ZXh0KSAtPlxuICAgIGh0bWxFbnRpdGllcy5kZWNvZGUodGV4dClcblxuICBlbmNvZGVYbWw6ICh0ZXh0KSAtPlxuICAgIHhtbEVudGl0aWVzLmVuY29kZU5vblVURih0ZXh0KVxuXG4gIGRlY29kZVhtbDogKHRleHQpIC0+XG4gICAgeG1sRW50aXRpZXMuZGVjb2RlKHRleHQpXG5cbiAgZW5jb2RlU3FsOiAodGV4dCkgLT5cbiAgICBlc2NhcGVfc3FsKHRleHQpXG5cbiAgZGVjb2RlU3FsOiAodGV4dCkgLT5cbiAgICB1bmVzY2FwZV9zcWwodGV4dClcblxuICBlbmNvZGVVcmw6ICh0ZXh0KSAtPlxuICAgIGVuY29kZVVSSUNvbXBvbmVudCh0ZXh0KVxuXG4gIGRlY29kZVVybDogKHRleHQpIC0+XG4gICAgZGVjb2RlVVJJQ29tcG9uZW50KHRleHQpXG5cbiAgaGFzaE1ENTogKHRleHQpIC0+XG4gICAgaGFzaCA9IGNyeXB0by5jcmVhdGVIYXNoKCdtZDUnKVxuICAgIGhhc2gudXBkYXRlKG5ldyBCdWZmZXIodGV4dCkpXG4gICAgaGFzaC5kaWdlc3QoJ2hleCcpXG5cbiAgaGFzaFNIQTE6ICh0ZXh0KSAtPlxuICAgIGhhc2ggPSBjcnlwdG8uY3JlYXRlSGFzaCgnc2hhMScpXG4gICAgaGFzaC51cGRhdGUobmV3IEJ1ZmZlcih0ZXh0KSlcbiAgICBoYXNoLmRpZ2VzdCgnaGV4JylcblxuICBoYXNoU0hBMjU2OiAodGV4dCkgLT5cbiAgICBoYXNoID0gY3J5cHRvLmNyZWF0ZUhhc2goJ3NoYTI1NicpXG4gICAgaGFzaC51cGRhdGUobmV3IEJ1ZmZlcih0ZXh0KSlcbiAgICBoYXNoLmRpZ2VzdCgnaGV4JylcblxuICBoYXNoU0hBNTEyOiAodGV4dCkgLT5cbiAgICBoYXNoID0gY3J5cHRvLmNyZWF0ZUhhc2goJ3NoYTUxMicpXG4gICAgaGFzaC51cGRhdGUobmV3IEJ1ZmZlcih0ZXh0KSlcbiAgICBoYXNoLmRpZ2VzdCgnaGV4JylcblxuICBmb3JtYXRDYW1lbGl6ZTogKHRleHQpIC0+XG4gICAgc3RyaW5nKHRleHQpLmNhbWVsaXplKCkuc1xuXG4gIGZvcm1hdERhc2hlcml6ZTogKHRleHQpIC0+XG4gICAgc3RyaW5nKHRleHQpLmRhc2hlcml6ZSgpLnNcblxuICBmb3JtYXRVbmRlcnNjb3JlOiAodGV4dCkgLT5cbiAgICBzdHJpbmcodGV4dCkudW5kZXJzY29yZSgpLnNcblxuICBmb3JtYXRTbHVnaWZ5OiAodGV4dCkgLT5cbiAgICBzdHJpbmcodGV4dCkuc2x1Z2lmeSgpLnNcblxuICBmb3JtYXRIdW1hbml6ZTogKHRleHQpIC0+XG4gICAgc3RyaW5nKHRleHQpLmh1bWFuaXplKCkuc1xuXG4gIHdoaXRlc3BhY2VUcmltOiAodGV4dCkgLT5cbiAgICBsaW5lcyA9IChzdHJpbmcobGluZSkucmVwbGFjZSgvXFxzKyQvLCBcIlwiKS5zIGZvciBsaW5lIGluIHRleHQuc3BsaXQoJ1xcbicpKVxuICAgIGxpbmVzLmpvaW4oJ1xcbicpXG5cbiAgd2hpdGVzcGFjZUNvbGxhcHNlOiAodGV4dCkgLT5cbiAgICBzdHJpbmcodGV4dCkuY29sbGFwc2VXaGl0ZXNwYWNlKCkuc1xuXG4gIHdoaXRlc3BhY2VSZW1vdmU6ICh0ZXh0KSAtPlxuICAgIHN0cmluZyh0ZXh0KS5jb2xsYXBzZVdoaXRlc3BhY2UoKS5zLnJlcGxhY2UoL1xccysvZywgJycpXG5cbiAgd2hpdGVzcGFjZUVtcHR5TGluZXM6ICh0ZXh0KSAtPlxuICAgIGxpbmVzID0gKGxpbmUgZm9yIGxpbmUgaW4gdGV4dC5zcGxpdCgnXFxuJykgd2hlbiBsaW5lLmxlbmd0aCA+IDApXG4gICAgbGluZXMuam9pbignXFxuJylcblxuICB3aGl0ZXNwYWNlVGFiaWZ5OiAodGV4dCkgLT5cbiAgICBlZGl0b3IgPSBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVUZXh0RWRpdG9yKClcbiAgICB0YWJMZW5ndGggPSBlZGl0b3IuZ2V0VGFiTGVuZ3RoKClcbiAgICBsaW5lcyA9ICh0YWJpZnkobGluZSwgdGFiTGVuZ3RoKSBmb3IgbGluZSBpbiB0ZXh0LnNwbGl0KCdcXG4nKSlcbiAgICBsaW5lcy5qb2luKCdcXG4nKVxuXG4gIHdoaXRlc3BhY2VVbnRhYmlmeTogKHRleHQpIC0+XG4gICAgZWRpdG9yID0gYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgdGFiTGVuZ3RoID0gZWRpdG9yLmdldFRhYkxlbmd0aCgpXG4gICAgbGluZXMgPSAodW50YWJpZnkobGluZSwgdGFiTGVuZ3RoKSBmb3IgbGluZSBpbiB0ZXh0LnNwbGl0KCdcXG4nKSlcbiAgICBsaW5lcy5qb2luKCdcXG4nKVxuXG4gIHN0cmlwUHVuY3R1YXRpb246ICh0ZXh0KSAtPlxuICAgIHN0cmluZyh0ZXh0KS5zdHJpcFB1bmN0dWF0aW9uKCkuc1xuXG4jIEhlbHBlciBmdW5jdGlvbnNcblxudGFiaWZ5ID0gKHN0ciwgdGFiTGVuZ3RoKSAtPlxuICBbc3RhcnQsIGNvdW50XSA9IGNvdW50VGFicyhzdHIsIHRhYkxlbmd0aClcbiAgdGFicyA9IHN0cmluZygnXFx0JykucmVwZWF0KGNvdW50IC8vIHRhYkxlbmd0aCkuc1xuICBzcGFjZXMgPSBzdHJpbmcoJyAnKS5yZXBlYXQoY291bnQgJSUgdGFiTGVuZ3RoKS5zXG4gIHRhYnMgKyBzcGFjZXMgKyBzdHIuc3Vic3RyKHN0YXJ0KVxuXG51bnRhYmlmeSA9IChzdHIsIHRhYkxlbmd0aCkgLT5cbiAgW3N0YXJ0LCBjb3VudF0gPSBjb3VudFRhYnMoc3RyLCB0YWJMZW5ndGgpXG4gIHNwYWNlcyA9IHN0cmluZygnICcpLnJlcGVhdChjb3VudCkuc1xuICBzcGFjZXMgKyBzdHIuc3Vic3RyKHN0YXJ0KVxuXG5jb3VudFRhYnMgPSAoc3RyLCB0YWJMZW5ndGgpIC0+XG4gIHN0YXJ0ID0gc3RyLnNlYXJjaCgvW15cXHNdLylcbiAgaWYgc3RhcnQgPCAwXG4gICAgc3RhcnQgPSBzdHIubGVuZ3RoXG4gIGNvdW50ID0gMFxuICBmb3IgY2ggaW4gc3RyLnN1YnN0cigwLCBzdGFydClcbiAgICBzd2l0Y2ggY2hcbiAgICAgIHdoZW4gJyAnIHRoZW4gY291bnQgKz0gMVxuICAgICAgd2hlbiAnXFx0JyB0aGVuIGNvdW50ID0gKGNvdW50IC8vIHRhYkxlbmd0aCArIDEpICogdGFiTGVuZ3RoXG4gIFtzdGFydCwgY291bnRdXG5cbmVzY2FwZV9zcWwgPSAoc3RyKSAtPlxuICBzdHIucmVwbGFjZSgvW1xcMFxcYlxcdFxcblxcclxcXFxcIiclXFx4MWFdL2csIChjaGFyKSAtPlxuICAgIHN3aXRjaCBjaGFyXG4gICAgICB3aGVuIFwiXFwwXCIgdGhlbiBcIlxcXFwwXCJcbiAgICAgIHdoZW4gXCJcXGJcIiB0aGVuIFwiXFxcXGJcIlxuICAgICAgd2hlbiBcIlxcdFwiIHRoZW4gXCJcXFxcdFwiXG4gICAgICB3aGVuIFwiXFxuXCIgdGhlbiBcIlxcXFxuXCJcbiAgICAgIHdoZW4gXCJcXHJcIiB0aGVuIFwiXFxcXHJcIlxuICAgICAgd2hlbiBcIlxcXCJcIiB0aGVuIFwiXFxcXFxcXCJcIlxuICAgICAgd2hlbiBcIidcIiB0aGVuIFwiXFxcXCdcIlxuICAgICAgd2hlbiBcIlxcXFxcIiB0aGVuIFwiXFxcXFxcXFxcIlxuICAgICAgd2hlbiBcIiVcIiB0aGVuIFwiXFxcXCVcIlxuICAgICAgd2hlbiBcIlxceDFhXCIgdGhlbiBcIlxcXFx6XCJcbiAgKVxuXG51bmVzY2FwZV9zcWwgPSAoc3RyKSAtPlxuICBzdHIucmVwbGFjZSgvXFxcXFswYnRuclwiJ1xcXFwlel0vZywgKGNoYXIpIC0+XG4gICAgc3dpdGNoIGNoYXJcbiAgICAgIHdoZW4gXCJcXFxcMFwiIHRoZW4gXCJcXDBcIlxuICAgICAgd2hlbiBcIlxcXFxiXCIgdGhlbiBcIlxcYlwiXG4gICAgICB3aGVuIFwiXFxcXHRcIiB0aGVuIFwiXFx0XCJcbiAgICAgIHdoZW4gXCJcXFxcblwiIHRoZW4gXCJcXG5cIlxuICAgICAgd2hlbiBcIlxcXFxyXCIgdGhlbiBcIlxcclwiXG4gICAgICB3aGVuIFwiXFxcXFxcXCJcIiB0aGVuIFwiXFxcIlwiXG4gICAgICB3aGVuIFwiXFxcXCdcIiB0aGVuIFwiJ1wiXG4gICAgICB3aGVuIFwiXFxcXFxcXFxcIiB0aGVuIFwiXFxcXFwiXG4gICAgICB3aGVuIFwiXFxcXCVcIiB0aGVuIFwiJVwiXG4gICAgICB3aGVuIFwiXFxcXHpcIiB0aGVuIFwiXFx4MWFcIlxuICApXG4iXX0=
