(function() {
  var CompositeDisposable, SublimeWordNavigation;

  CompositeDisposable = require('atom').CompositeDisposable;

  module.exports = SublimeWordNavigation = {
    subscriptions: null,
    activate: function(state) {
      this.subscriptions = new CompositeDisposable;
      return this.subscriptions.add(atom.commands.add('atom-text-editor', {
        'sublime-word-navigation:move-to-beginning-of-word': (function(_this) {
          return function() {
            return _this.moveToBeginningOfWord();
          };
        })(this),
        'sublime-word-navigation:move-to-end-of-word': (function(_this) {
          return function() {
            return _this.moveToEndOfWord();
          };
        })(this),
        'sublime-word-navigation:select-to-beginning-of-word': (function(_this) {
          return function() {
            return _this.selectToBeginningOfWord();
          };
        })(this),
        'sublime-word-navigation:select-to-end-of-word': (function(_this) {
          return function() {
            return _this.selectToEndOfWord();
          };
        })(this),
        'sublime-word-navigation:delete-to-beginning-of-word': (function(_this) {
          return function() {
            return _this.deleteToBeginningOfWord();
          };
        })(this),
        'sublime-word-navigation:delete-to-end-of-word': (function(_this) {
          return function() {
            return _this.deleteToEndOfWord();
          };
        })(this)
      }));
    },
    deactivate: function() {
      return this.subscriptions.dispose();
    },
    getEditor: function() {
      return atom.workspace.getActiveTextEditor();
    },
    getCursors: function() {
      return this.getEditor().getCursors();
    },
    getSelections: function() {
      return this.getEditor().getSelections();
    },
    moveCursorToBeginningOfWord: function(cursor) {
      var beginningOfWordPosition;
      beginningOfWordPosition = cursor.getBeginningOfCurrentWordBufferPosition({
        includeNonWordCharacters: false
      });
      if (cursor.isAtBeginningOfLine()) {
        cursor.moveUp();
        return cursor.moveToEndOfLine();
      } else if (beginningOfWordPosition.row < cursor.getBufferPosition().row) {
        return cursor.moveToBeginningOfLine();
      } else {
        return cursor.setBufferPosition(beginningOfWordPosition);
      }
    },
    moveCursorToEndOfWord: function(cursor) {
      var endOfWordPosition;
      endOfWordPosition = cursor.getEndOfCurrentWordBufferPosition({
        includeNonWordCharacters: false
      });
      if (cursor.isAtEndOfLine()) {
        cursor.moveDown();
        return cursor.moveToBeginningOfLine();
      } else if (endOfWordPosition.row > cursor.getBufferPosition().row) {
        return cursor.moveToEndOfLine();
      } else {
        return cursor.setBufferPosition(endOfWordPosition);
      }
    },
    moveToBeginningOfWord: function() {
      return this.getCursors().forEach(this.moveCursorToBeginningOfWord);
    },
    moveToEndOfWord: function() {
      return this.getCursors().forEach(this.moveCursorToEndOfWord);
    },
    selectToBeginningOfWord: function() {
      return this.getSelections().forEach((function(_this) {
        return function(selection) {
          return selection.modifySelection(function() {
            return _this.moveCursorToBeginningOfWord(selection.cursor);
          });
        };
      })(this));
    },
    selectToEndOfWord: function() {
      return this.getSelections().forEach((function(_this) {
        return function(selection) {
          return selection.modifySelection(function() {
            return _this.moveCursorToEndOfWord(selection.cursor);
          });
        };
      })(this));
    },
    deleteToBeginningOfWord: function() {
      return this.getEditor().mutateSelectedText((function(_this) {
        return function(selection) {
          if (selection.isEmpty()) {
            selection.modifySelection(function() {
              return _this.moveCursorToBeginningOfWord(selection.cursor);
            });
          }
          return selection.deleteSelectedText();
        };
      })(this));
    },
    deleteToEndOfWord: function() {
      return this.getEditor().mutateSelectedText((function(_this) {
        return function(selection) {
          if (selection.isEmpty()) {
            selection.modifySelection(function() {
              return _this.moveCursorToEndOfWord(selection.cursor);
            });
          }
          return selection.deleteSelectedText();
        };
      })(this));
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9zdWJsaW1lLXdvcmQtbmF2aWdhdGlvbi9saWIvc3VibGltZS13b3JkLW5hdmlnYXRpb24uY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQyxzQkFBdUIsT0FBQSxDQUFRLE1BQVI7O0VBRXhCLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLHFCQUFBLEdBQ2Y7SUFBQSxhQUFBLEVBQWUsSUFBZjtJQUVBLFFBQUEsRUFBVSxTQUFDLEtBQUQ7TUFFUixJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJO2FBR3JCLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0Isa0JBQWxCLEVBQ2pCO1FBQUEsbURBQUEsRUFBcUQsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEscUJBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFyRDtRQUNBLDZDQUFBLEVBQStDLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGVBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUQvQztRQUVBLHFEQUFBLEVBQXVELENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLHVCQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FGdkQ7UUFHQSwrQ0FBQSxFQUFpRCxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFBO21CQUFHLEtBQUMsQ0FBQSxpQkFBRCxDQUFBO1VBQUg7UUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBSGpEO1FBSUEscURBQUEsRUFBdUQsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQTttQkFBRyxLQUFDLENBQUEsdUJBQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUp2RDtRQUtBLCtDQUFBLEVBQWlELENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLGlCQUFELENBQUE7VUFBSDtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FMakQ7T0FEaUIsQ0FBbkI7SUFMUSxDQUZWO0lBZUEsVUFBQSxFQUFZLFNBQUE7YUFDVixJQUFDLENBQUEsYUFBYSxDQUFDLE9BQWYsQ0FBQTtJQURVLENBZlo7SUFrQkEsU0FBQSxFQUFXLFNBQUE7YUFDVCxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7SUFEUyxDQWxCWDtJQXFCQSxVQUFBLEVBQVksU0FBQTthQUNWLElBQUMsQ0FBQSxTQUFELENBQUEsQ0FBWSxDQUFDLFVBQWIsQ0FBQTtJQURVLENBckJaO0lBd0JBLGFBQUEsRUFBZSxTQUFBO2FBQ2IsSUFBQyxDQUFBLFNBQUQsQ0FBQSxDQUFZLENBQUMsYUFBYixDQUFBO0lBRGEsQ0F4QmY7SUEyQkEsMkJBQUEsRUFBNkIsU0FBQyxNQUFEO0FBQzNCLFVBQUE7TUFBQSx1QkFBQSxHQUEwQixNQUFNLENBQUMsdUNBQVAsQ0FBK0M7UUFDdkUsd0JBQUEsRUFBMEIsS0FENkM7T0FBL0M7TUFHMUIsSUFBRyxNQUFNLENBQUMsbUJBQVAsQ0FBQSxDQUFIO1FBQ0UsTUFBTSxDQUFDLE1BQVAsQ0FBQTtlQUNBLE1BQU0sQ0FBQyxlQUFQLENBQUEsRUFGRjtPQUFBLE1BR0ssSUFBRyx1QkFBdUIsQ0FBQyxHQUF4QixHQUE4QixNQUFNLENBQUMsaUJBQVAsQ0FBQSxDQUEwQixDQUFDLEdBQTVEO2VBQ0gsTUFBTSxDQUFDLHFCQUFQLENBQUEsRUFERztPQUFBLE1BQUE7ZUFHSCxNQUFNLENBQUMsaUJBQVAsQ0FBeUIsdUJBQXpCLEVBSEc7O0lBUHNCLENBM0I3QjtJQXVDQSxxQkFBQSxFQUF1QixTQUFDLE1BQUQ7QUFDckIsVUFBQTtNQUFBLGlCQUFBLEdBQW9CLE1BQU0sQ0FBQyxpQ0FBUCxDQUF5QztRQUMzRCx3QkFBQSxFQUEwQixLQURpQztPQUF6QztNQUdwQixJQUFHLE1BQU0sQ0FBQyxhQUFQLENBQUEsQ0FBSDtRQUNFLE1BQU0sQ0FBQyxRQUFQLENBQUE7ZUFDQSxNQUFNLENBQUMscUJBQVAsQ0FBQSxFQUZGO09BQUEsTUFHSyxJQUFHLGlCQUFpQixDQUFDLEdBQWxCLEdBQXdCLE1BQU0sQ0FBQyxpQkFBUCxDQUFBLENBQTBCLENBQUMsR0FBdEQ7ZUFDSCxNQUFNLENBQUMsZUFBUCxDQUFBLEVBREc7T0FBQSxNQUFBO2VBR0gsTUFBTSxDQUFDLGlCQUFQLENBQXlCLGlCQUF6QixFQUhHOztJQVBnQixDQXZDdkI7SUFtREEscUJBQUEsRUFBdUIsU0FBQTthQUNyQixJQUFDLENBQUEsVUFBRCxDQUFBLENBQWEsQ0FBQyxPQUFkLENBQXNCLElBQUMsQ0FBQSwyQkFBdkI7SUFEcUIsQ0FuRHZCO0lBc0RBLGVBQUEsRUFBaUIsU0FBQTthQUNmLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBYSxDQUFDLE9BQWQsQ0FBc0IsSUFBQyxDQUFBLHFCQUF2QjtJQURlLENBdERqQjtJQXlEQSx1QkFBQSxFQUF5QixTQUFBO2FBQ3ZCLElBQUMsQ0FBQSxhQUFELENBQUEsQ0FBZ0IsQ0FBQyxPQUFqQixDQUF5QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsU0FBRDtpQkFDdkIsU0FBUyxDQUFDLGVBQVYsQ0FBMEIsU0FBQTttQkFBRyxLQUFDLENBQUEsMkJBQUQsQ0FBNkIsU0FBUyxDQUFDLE1BQXZDO1VBQUgsQ0FBMUI7UUFEdUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCO0lBRHVCLENBekR6QjtJQTZEQSxpQkFBQSxFQUFtQixTQUFBO2FBQ2pCLElBQUMsQ0FBQSxhQUFELENBQUEsQ0FBZ0IsQ0FBQyxPQUFqQixDQUF5QixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsU0FBRDtpQkFDdkIsU0FBUyxDQUFDLGVBQVYsQ0FBMEIsU0FBQTttQkFBRyxLQUFDLENBQUEscUJBQUQsQ0FBdUIsU0FBUyxDQUFDLE1BQWpDO1VBQUgsQ0FBMUI7UUFEdUI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQXpCO0lBRGlCLENBN0RuQjtJQWlFQSx1QkFBQSxFQUF5QixTQUFBO2FBQ3ZCLElBQUMsQ0FBQSxTQUFELENBQUEsQ0FBWSxDQUFDLGtCQUFiLENBQWdDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxTQUFEO1VBQzlCLElBQUcsU0FBUyxDQUFDLE9BQVYsQ0FBQSxDQUFIO1lBQ0UsU0FBUyxDQUFDLGVBQVYsQ0FBMEIsU0FBQTtxQkFBRyxLQUFDLENBQUEsMkJBQUQsQ0FBNkIsU0FBUyxDQUFDLE1BQXZDO1lBQUgsQ0FBMUIsRUFERjs7aUJBRUEsU0FBUyxDQUFDLGtCQUFWLENBQUE7UUFIOEI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhDO0lBRHVCLENBakV6QjtJQXVFQSxpQkFBQSxFQUFtQixTQUFBO2FBQ2pCLElBQUMsQ0FBQSxTQUFELENBQUEsQ0FBWSxDQUFDLGtCQUFiLENBQWdDLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxTQUFEO1VBQzlCLElBQUcsU0FBUyxDQUFDLE9BQVYsQ0FBQSxDQUFIO1lBQ0UsU0FBUyxDQUFDLGVBQVYsQ0FBMEIsU0FBQTtxQkFBRyxLQUFDLENBQUEscUJBQUQsQ0FBdUIsU0FBUyxDQUFDLE1BQWpDO1lBQUgsQ0FBMUIsRUFERjs7aUJBRUEsU0FBUyxDQUFDLGtCQUFWLENBQUE7UUFIOEI7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWhDO0lBRGlCLENBdkVuQjs7QUFIRiIsInNvdXJjZXNDb250ZW50IjpbIntDb21wb3NpdGVEaXNwb3NhYmxlfSA9IHJlcXVpcmUgJ2F0b20nXG5cbm1vZHVsZS5leHBvcnRzID0gU3VibGltZVdvcmROYXZpZ2F0aW9uID1cbiAgc3Vic2NyaXB0aW9uczogbnVsbFxuXG4gIGFjdGl2YXRlOiAoc3RhdGUpIC0+XG4gICAgIyBFdmVudHMgc3Vic2NyaWJlZCB0byBpbiBhdG9tJ3Mgc3lzdGVtIGNhbiBiZSBlYXNpbHkgY2xlYW5lZCB1cCB3aXRoIGEgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgICMgUmVnaXN0ZXIgY29tbWFuZCB0aGF0IHRvZ2dsZXMgdGhpcyB2aWV3XG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkICdhdG9tLXRleHQtZWRpdG9yJyxcbiAgICAgICdzdWJsaW1lLXdvcmQtbmF2aWdhdGlvbjptb3ZlLXRvLWJlZ2lubmluZy1vZi13b3JkJzogPT4gQG1vdmVUb0JlZ2lubmluZ09mV29yZCgpXG4gICAgICAnc3VibGltZS13b3JkLW5hdmlnYXRpb246bW92ZS10by1lbmQtb2Ytd29yZCc6ID0+IEBtb3ZlVG9FbmRPZldvcmQoKVxuICAgICAgJ3N1YmxpbWUtd29yZC1uYXZpZ2F0aW9uOnNlbGVjdC10by1iZWdpbm5pbmctb2Ytd29yZCc6ID0+IEBzZWxlY3RUb0JlZ2lubmluZ09mV29yZCgpXG4gICAgICAnc3VibGltZS13b3JkLW5hdmlnYXRpb246c2VsZWN0LXRvLWVuZC1vZi13b3JkJzogPT4gQHNlbGVjdFRvRW5kT2ZXb3JkKClcbiAgICAgICdzdWJsaW1lLXdvcmQtbmF2aWdhdGlvbjpkZWxldGUtdG8tYmVnaW5uaW5nLW9mLXdvcmQnOiA9PiBAZGVsZXRlVG9CZWdpbm5pbmdPZldvcmQoKVxuICAgICAgJ3N1YmxpbWUtd29yZC1uYXZpZ2F0aW9uOmRlbGV0ZS10by1lbmQtb2Ytd29yZCc6ID0+IEBkZWxldGVUb0VuZE9mV29yZCgpXG5cbiAgZGVhY3RpdmF0ZTogLT5cbiAgICBAc3Vic2NyaXB0aW9ucy5kaXNwb3NlKClcblxuICBnZXRFZGl0b3I6IC0+XG4gICAgYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG5cbiAgZ2V0Q3Vyc29yczogLT5cbiAgICBAZ2V0RWRpdG9yKCkuZ2V0Q3Vyc29ycygpXG5cbiAgZ2V0U2VsZWN0aW9uczogLT5cbiAgICBAZ2V0RWRpdG9yKCkuZ2V0U2VsZWN0aW9ucygpXG5cbiAgbW92ZUN1cnNvclRvQmVnaW5uaW5nT2ZXb3JkOiAoY3Vyc29yKSAtPlxuICAgIGJlZ2lubmluZ09mV29yZFBvc2l0aW9uID0gY3Vyc29yLmdldEJlZ2lubmluZ09mQ3VycmVudFdvcmRCdWZmZXJQb3NpdGlvbih7XG4gICAgICBpbmNsdWRlTm9uV29yZENoYXJhY3RlcnM6IGZhbHNlXG4gICAgfSlcbiAgICBpZiBjdXJzb3IuaXNBdEJlZ2lubmluZ09mTGluZSgpXG4gICAgICBjdXJzb3IubW92ZVVwKClcbiAgICAgIGN1cnNvci5tb3ZlVG9FbmRPZkxpbmUoKVxuICAgIGVsc2UgaWYgYmVnaW5uaW5nT2ZXb3JkUG9zaXRpb24ucm93IDwgY3Vyc29yLmdldEJ1ZmZlclBvc2l0aW9uKCkucm93XG4gICAgICBjdXJzb3IubW92ZVRvQmVnaW5uaW5nT2ZMaW5lKClcbiAgICBlbHNlXG4gICAgICBjdXJzb3Iuc2V0QnVmZmVyUG9zaXRpb24oYmVnaW5uaW5nT2ZXb3JkUG9zaXRpb24pXG5cbiAgbW92ZUN1cnNvclRvRW5kT2ZXb3JkOiAoY3Vyc29yKSAtPlxuICAgIGVuZE9mV29yZFBvc2l0aW9uID0gY3Vyc29yLmdldEVuZE9mQ3VycmVudFdvcmRCdWZmZXJQb3NpdGlvbih7XG4gICAgICBpbmNsdWRlTm9uV29yZENoYXJhY3RlcnM6IGZhbHNlXG4gICAgfSlcbiAgICBpZiBjdXJzb3IuaXNBdEVuZE9mTGluZSgpXG4gICAgICBjdXJzb3IubW92ZURvd24oKVxuICAgICAgY3Vyc29yLm1vdmVUb0JlZ2lubmluZ09mTGluZSgpXG4gICAgZWxzZSBpZiBlbmRPZldvcmRQb3NpdGlvbi5yb3cgPiBjdXJzb3IuZ2V0QnVmZmVyUG9zaXRpb24oKS5yb3dcbiAgICAgIGN1cnNvci5tb3ZlVG9FbmRPZkxpbmUoKVxuICAgIGVsc2VcbiAgICAgIGN1cnNvci5zZXRCdWZmZXJQb3NpdGlvbihlbmRPZldvcmRQb3NpdGlvbilcblxuICBtb3ZlVG9CZWdpbm5pbmdPZldvcmQ6IC0+XG4gICAgQGdldEN1cnNvcnMoKS5mb3JFYWNoKEBtb3ZlQ3Vyc29yVG9CZWdpbm5pbmdPZldvcmQpXG5cbiAgbW92ZVRvRW5kT2ZXb3JkOiAtPlxuICAgIEBnZXRDdXJzb3JzKCkuZm9yRWFjaChAbW92ZUN1cnNvclRvRW5kT2ZXb3JkKVxuXG4gIHNlbGVjdFRvQmVnaW5uaW5nT2ZXb3JkOiAtPlxuICAgIEBnZXRTZWxlY3Rpb25zKCkuZm9yRWFjaCAoc2VsZWN0aW9uKSA9PlxuICAgICAgc2VsZWN0aW9uLm1vZGlmeVNlbGVjdGlvbiA9PiBAbW92ZUN1cnNvclRvQmVnaW5uaW5nT2ZXb3JkKHNlbGVjdGlvbi5jdXJzb3IpXG5cbiAgc2VsZWN0VG9FbmRPZldvcmQ6IC0+XG4gICAgQGdldFNlbGVjdGlvbnMoKS5mb3JFYWNoIChzZWxlY3Rpb24pID0+XG4gICAgICBzZWxlY3Rpb24ubW9kaWZ5U2VsZWN0aW9uID0+IEBtb3ZlQ3Vyc29yVG9FbmRPZldvcmQoc2VsZWN0aW9uLmN1cnNvcilcblxuICBkZWxldGVUb0JlZ2lubmluZ09mV29yZDogLT5cbiAgICBAZ2V0RWRpdG9yKCkubXV0YXRlU2VsZWN0ZWRUZXh0IChzZWxlY3Rpb24pID0+XG4gICAgICBpZiBzZWxlY3Rpb24uaXNFbXB0eSgpXG4gICAgICAgIHNlbGVjdGlvbi5tb2RpZnlTZWxlY3Rpb24gPT4gQG1vdmVDdXJzb3JUb0JlZ2lubmluZ09mV29yZChzZWxlY3Rpb24uY3Vyc29yKVxuICAgICAgc2VsZWN0aW9uLmRlbGV0ZVNlbGVjdGVkVGV4dCgpXG5cbiAgZGVsZXRlVG9FbmRPZldvcmQ6IC0+XG4gICAgQGdldEVkaXRvcigpLm11dGF0ZVNlbGVjdGVkVGV4dCAoc2VsZWN0aW9uKSA9PlxuICAgICAgaWYgc2VsZWN0aW9uLmlzRW1wdHkoKVxuICAgICAgICBzZWxlY3Rpb24ubW9kaWZ5U2VsZWN0aW9uID0+IEBtb3ZlQ3Vyc29yVG9FbmRPZldvcmQoc2VsZWN0aW9uLmN1cnNvcilcbiAgICAgIHNlbGVjdGlvbi5kZWxldGVTZWxlY3RlZFRleHQoKVxuIl19
