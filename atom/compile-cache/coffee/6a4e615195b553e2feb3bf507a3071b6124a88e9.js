(function() {
  var meta;

  meta = require("../package.json");

  module.exports = {
    getConfig: function(key) {
      if (key == null) {
        key = "";
      }
      if (key != null) {
        return atom.config.get(meta.name + "." + key);
      }
      return atom.config.get("" + meta.name);
    },
    notification: function(string, notification) {
      switch (this.getConfig("notificationStyle")) {
        case "Success":
          return atom.notifications.addSuccess(string, notification);
        case "Info":
          return atom.notifications.addInfo(string, notification);
        case "Warning":
          return atom.notifications.addWarning(string, notification);
        case "Error":
          return atom.notifications.addError(string, notification);
        default:
          return atom.notifications.addSuccess(string, notification);
      }
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9hdXRvLXVwZGF0ZS1wbHVzL2xpYi91dGlsLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsSUFBQSxHQUFPLE9BQUEsQ0FBUyxpQkFBVDs7RUFFUCxNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsU0FBQSxFQUFXLFNBQUMsR0FBRDs7UUFBQyxNQUFNOztNQUNoQixJQUFHLFdBQUg7QUFDRSxlQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFJLENBQUMsSUFBTixHQUFXLEdBQVgsR0FBYyxHQUFoQyxFQURUOztBQUdBLGFBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLEVBQUEsR0FBRyxJQUFJLENBQUMsSUFBeEI7SUFKRSxDQUFYO0lBTUEsWUFBQSxFQUFjLFNBQUMsTUFBRCxFQUFTLFlBQVQ7QUFDWixjQUFPLElBQUMsQ0FBQSxTQUFELENBQVcsbUJBQVgsQ0FBUDtBQUFBLGFBQ08sU0FEUDtpQkFDdUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixNQUE5QixFQUFzQyxZQUF0QztBQUR2QixhQUVPLE1BRlA7aUJBRW9CLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBbkIsQ0FBMkIsTUFBM0IsRUFBbUMsWUFBbkM7QUFGcEIsYUFHTyxTQUhQO2lCQUd1QixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQW5CLENBQThCLE1BQTlCLEVBQXNDLFlBQXRDO0FBSHZCLGFBSU8sT0FKUDtpQkFJcUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFuQixDQUE0QixNQUE1QixFQUFvQyxZQUFwQztBQUpyQjtBQU1JLGlCQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBbkIsQ0FBOEIsTUFBOUIsRUFBc0MsWUFBdEM7QUFOWDtJQURZLENBTmQ7O0FBSEYiLCJzb3VyY2VzQ29udGVudCI6WyJtZXRhID0gcmVxdWlyZSAoXCIuLi9wYWNrYWdlLmpzb25cIilcblxubW9kdWxlLmV4cG9ydHMgPVxuICBnZXRDb25maWc6IChrZXkgPSBcIlwiKSAtPlxuICAgIGlmIGtleT9cbiAgICAgIHJldHVybiBhdG9tLmNvbmZpZy5nZXQgXCIje21ldGEubmFtZX0uI3trZXl9XCJcblxuICAgIHJldHVybiBhdG9tLmNvbmZpZy5nZXQgXCIje21ldGEubmFtZX1cIlxuXG4gIG5vdGlmaWNhdGlvbjogKHN0cmluZywgbm90aWZpY2F0aW9uKSAtPlxuICAgIHN3aXRjaCBAZ2V0Q29uZmlnKFwibm90aWZpY2F0aW9uU3R5bGVcIilcbiAgICAgIHdoZW4gXCJTdWNjZXNzXCIgdGhlbiAgYXRvbS5ub3RpZmljYXRpb25zLmFkZFN1Y2Nlc3Moc3RyaW5nLCBub3RpZmljYXRpb24pXG4gICAgICB3aGVuIFwiSW5mb1wiIHRoZW4gIGF0b20ubm90aWZpY2F0aW9ucy5hZGRJbmZvKHN0cmluZywgbm90aWZpY2F0aW9uKVxuICAgICAgd2hlbiBcIldhcm5pbmdcIiB0aGVuICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhzdHJpbmcsIG5vdGlmaWNhdGlvbilcbiAgICAgIHdoZW4gXCJFcnJvclwiIHRoZW4gIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihzdHJpbmcsIG5vdGlmaWNhdGlvbilcbiAgICAgIGVsc2VcbiAgICAgICAgcmV0dXJuIGF0b20ubm90aWZpY2F0aW9ucy5hZGRTdWNjZXNzKHN0cmluZywgbm90aWZpY2F0aW9uKVxuIl19
