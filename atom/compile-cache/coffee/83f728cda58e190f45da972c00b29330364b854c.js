(function() {
  var BitbucketFile, Range, Shell, formatUrl, parseUrl;

  Shell = require('shell');

  Range = require('atom').Range;

  parseUrl = require('url').parse;

  formatUrl = require('url').format;

  module.exports = BitbucketFile = (function() {
    BitbucketFile.fromPath = function(filePath) {
      return new BitbucketFile(filePath);
    };

    function BitbucketFile(filePath1) {
      var rootDir, rootDirIndex;
      this.filePath = filePath1;
      rootDir = atom.project.relativizePath(this.filePath)[0];
      if (rootDir != null) {
        rootDirIndex = atom.project.getPaths().indexOf(rootDir);
        this.repo = atom.project.getRepositories()[rootDirIndex];
      }
    }

    BitbucketFile.prototype.open = function(lineRange) {
      if (this.isOpenable()) {
        return this.openUrlInBrowser(this.blobUrl() + this.getLineRangeSuffix(lineRange));
      } else {
        return this.reportValidationErrors();
      }
    };

    BitbucketFile.prototype.openOnMaster = function(lineRange) {
      if (this.isOpenable()) {
        return this.openUrlInBrowser(this.blobUrlForMaster() + this.getLineRangeSuffix(lineRange));
      } else {
        return this.reportValidationErrors();
      }
    };

    BitbucketFile.prototype.blame = function(lineRange) {
      if (this.isOpenable(true)) {
        return this.openUrlInBrowser(this.blameUrl() + this.getLineRangeSuffix(lineRange));
      } else {
        return this.reportValidationErrors(true);
      }
    };

    BitbucketFile.prototype.history = function() {
      if (this.isOpenable(true)) {
        return this.openUrlInBrowser(this.historyUrl());
      } else {
        return this.reportValidationErrors(true);
      }
    };

    BitbucketFile.prototype.copyUrl = function(lineRange) {
      if (this.isOpenable()) {
        return atom.clipboard.write(this.shaUrl() + this.getLineRangeSuffix(lineRange));
      } else {
        return this.reportValidationErrors();
      }
    };

    BitbucketFile.prototype.openBranchCompare = function() {
      if (this.isOpenable()) {
        return this.openUrlInBrowser(this.branchCompareUrl());
      } else {
        return this.reportValidationErrors();
      }
    };

    BitbucketFile.prototype.openIssues = function() {
      if (this.isOpenable(true)) {
        return this.openUrlInBrowser(this.issuesUrl());
      } else {
        return this.reportValidationErrors(true);
      }
    };

    BitbucketFile.prototype.openRepository = function() {
      if (this.isOpenable()) {
        return this.openUrlInBrowser(this.bitbucketRepoUrl());
      } else {
        return this.reportValidationErrors();
      }
    };

    BitbucketFile.prototype.getLineRangeSuffix = function(lineRange) {
      var endRow, startRow;
      if (lineRange && atom.config.get('open-on-bitbucket.includeLineNumbersInUrls')) {
        lineRange = Range.fromObject(lineRange);
        startRow = lineRange.start.row + 1;
        endRow = lineRange.end.row + 1;
        if (startRow === endRow) {
          if (this.isBitbucketCloudUrl(this.gitUrl())) {
            return "#cl-" + startRow;
          } else {
            return "#" + startRow;
          }
        } else {
          if (this.isBitbucketCloudUrl(this.gitUrl())) {
            return "#cl-" + startRow + ":" + endRow;
          } else {
            return "#" + startRow + "-" + endRow;
          }
        }
      } else {
        return '';
      }
    };

    BitbucketFile.prototype.isOpenable = function(disallowStash) {
      if (disallowStash == null) {
        disallowStash = false;
      }
      return this.validationErrors(disallowStash).length === 0;
    };

    BitbucketFile.prototype.validationErrors = function(disallowStash) {
      if (!this.repo) {
        return ["No repository found for path: " + this.filePath + "."];
      }
      if (!this.gitUrl()) {
        return ["No URL defined for remote: " + (this.remoteName())];
      }
      if (!this.bitbucketRepoUrl()) {
        return ["Remote URL is not hosted on Bitbucket: " + (this.gitUrl())];
      }
      if (disallowStash && this.isStashUrl(this.gitUrl())) {
        return ["This feature is only available when hosting repositories on Bitbucket (bitbucket.org)"];
      }
      return [];
    };

    BitbucketFile.prototype.reportValidationErrors = function(disallowStash) {
      var message;
      if (disallowStash == null) {
        disallowStash = false;
      }
      message = this.validationErrors(disallowStash).join('\n');
      return atom.notifications.addWarning(message);
    };

    BitbucketFile.prototype.openUrlInBrowser = function(url) {
      return Shell.openExternal(url);
    };

    BitbucketFile.prototype.blobUrl = function() {
      var baseUrl;
      baseUrl = this.bitbucketRepoUrl();
      if (this.isBitbucketCloudUrl(baseUrl)) {
        return baseUrl + "/src/" + (this.remoteBranchName()) + "/" + (this.encodeSegments(this.repoRelativePath()));
      } else {
        return baseUrl + "/browse/" + (this.encodeSegments(this.repoRelativePath())) + "?at=" + (this.remoteBranchName());
      }
    };

    BitbucketFile.prototype.blobUrlForMaster = function() {
      var baseUrl;
      baseUrl = this.bitbucketRepoUrl();
      if (this.isBitbucketCloudUrl(baseUrl)) {
        return baseUrl + "/src/master/" + (this.encodeSegments(this.repoRelativePath()));
      } else {
        return baseUrl + "/browse/" + (this.encodeSegments(this.repoRelativePath())) + "?at=master";
      }
    };

    BitbucketFile.prototype.shaUrl = function() {
      var baseUrl;
      baseUrl = this.bitbucketRepoUrl();
      if (this.isBitbucketCloudUrl(baseUrl)) {
        return baseUrl + "/src/" + (this.encodeSegments(this.sha())) + "/" + (this.encodeSegments(this.repoRelativePath()));
      } else {
        return baseUrl + "/browse/" + (this.encodeSegments(this.repoRelativePath())) + "?at=" + (this.encodeSegments(this.sha()));
      }
    };

    BitbucketFile.prototype.blameUrl = function() {
      return (this.bitbucketRepoUrl()) + "/annotate/" + (this.encodeSegments(this.branchName())) + "/" + (this.encodeSegments(this.repoRelativePath()));
    };

    BitbucketFile.prototype.historyUrl = function() {
      return (this.bitbucketRepoUrl()) + "/history-node/" + (this.encodeSegments(this.branchName())) + "/" + (this.encodeSegments(this.repoRelativePath()));
    };

    BitbucketFile.prototype.issuesUrl = function() {
      return (this.bitbucketRepoUrl()) + "/issues";
    };

    BitbucketFile.prototype.branchCompareUrl = function() {
      var baseUrl;
      baseUrl = this.bitbucketRepoUrl();
      if (this.isBitbucketCloudUrl(baseUrl)) {
        return baseUrl + "/branch/" + (this.encodeSegments(this.branchName()));
      } else {
        return baseUrl + "/compare/commits?sourceBranch=" + (this.encodeSegments(this.branchName()));
      }
    };

    BitbucketFile.prototype.encodeSegments = function(segments) {
      if (segments == null) {
        segments = '';
      }
      segments = segments.split('/');
      segments = segments.map(function(segment) {
        return encodeURIComponent(segment);
      });
      return segments.join('/');
    };

    BitbucketFile.prototype.gitUrl = function() {
      var ref, remoteOrBestGuess;
      remoteOrBestGuess = (ref = this.remoteName()) != null ? ref : 'origin';
      return this.repo.getConfigValue("remote." + remoteOrBestGuess + ".url", this.filePath);
    };

    BitbucketFile.prototype.bitbucketRepoUrl = function() {
      var url;
      url = this.gitUrl();
      if (this.isGithubUrl(url)) {

      } else if (this.isBitbucketCloudUrl(url)) {
        return this.bitbucketCloudRepoUrl(url);
      } else {
        return this.stashRepoUrlRepoUrl(url);
      }
    };

    BitbucketFile.prototype.bitbucketCloudRepoUrl = function(url) {
      if (url.match(/https?:\/\/[^\/]+\//)) {
        url = url.replace(/\.git$/, '');
      } else if (url.match(/^git[^@]*@[^:]+:/)) {
        url = url.replace(/^git[^@]*@([^:]+):(.+)$/, function(match, host, repoPath) {
          repoPath = repoPath.replace(/^\/+/, '');
          return ("http://" + host + "/" + repoPath).replace(/\.git$/, '');
        });
      } else if (url.match(/ssh:\/\/git@([^\/]+)\//)) {
        url = "http://" + (url.substring(10).replace(/\.git$/, ''));
      } else if (url.match(/^git:\/\/[^\/]+\//)) {
        url = "http" + (url.substring(3).replace(/\.git$/, ''));
      }
      url = url.replace(/\.git$/, '');
      url = url.replace(/\/+$/, '');
      return url;
    };

    BitbucketFile.prototype.stashRepoUrlRepoUrl = function(url) {
      var match, proj, ref, repo, urlObj;
      urlObj = parseUrl(this.bitbucketCloudRepoUrl(url));
      urlObj.host = urlObj.hostname;
      urlObj.auth = null;
      ref = urlObj.pathname.match(/(?:\/scm)?\/(.+)\/(.+)/), match = ref[0], proj = ref[1], repo = ref[2];
      urlObj.pathname = "/projects/" + proj + "/repos/" + repo;
      return formatUrl(urlObj);
    };

    BitbucketFile.prototype.isGithubUrl = function(url) {
      var host;
      if (url.indexOf('git@github.com') === 0) {
        return true;
      }
      try {
        host = parseUrl(url).host;
        return host === 'github.com';
      } catch (error) {}
    };

    BitbucketFile.prototype.isBitbucketCloudUrl = function(url) {
      var host;
      if (url.indexOf('git@bitbucket.org') === 0) {
        return true;
      }
      try {
        host = parseUrl(url).host;
        return host === 'bitbucket.org';
      } catch (error) {}
    };

    BitbucketFile.prototype.isStashUrl = function(url) {
      return !(this.isGithubUrl(url) || this.isBitbucketCloudUrl(url));
    };

    BitbucketFile.prototype.repoRelativePath = function() {
      return this.repo.getRepo(this.filePath).relativize(this.filePath);
    };

    BitbucketFile.prototype.remoteName = function() {
      var branchRemote, shortBranch;
      shortBranch = this.repo.getShortHead(this.filePath);
      if (!shortBranch) {
        return null;
      }
      branchRemote = this.repo.getConfigValue("branch." + shortBranch + ".remote", this.filePath);
      if (!((branchRemote != null ? branchRemote.length : void 0) > 0)) {
        return null;
      }
      return branchRemote;
    };

    BitbucketFile.prototype.sha = function() {
      return this.repo.getReferenceTarget('HEAD', this.filePath);
    };

    BitbucketFile.prototype.branchName = function() {
      var branchMerge, shortBranch;
      shortBranch = this.repo.getShortHead(this.filePath);
      if (!shortBranch) {
        return null;
      }
      branchMerge = this.repo.getConfigValue("branch." + shortBranch + ".merge", this.filePath);
      if (!((branchMerge != null ? branchMerge.length : void 0) > 11)) {
        return shortBranch;
      }
      if (branchMerge.indexOf('refs/heads/') !== 0) {
        return shortBranch;
      }
      return branchMerge.substring(11);
    };

    BitbucketFile.prototype.remoteBranchName = function() {
      if (this.remoteName() != null) {
        return this.encodeSegments(this.branchName());
      } else {
        return 'master';
      }
    };

    return BitbucketFile;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9vcGVuLW9uLWJpdGJ1Y2tldC9saWIvYml0YnVja2V0LWZpbGUuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxLQUFBLEdBQVEsT0FBQSxDQUFRLE9BQVI7O0VBQ1AsUUFBUyxPQUFBLENBQVEsTUFBUjs7RUFDVixRQUFBLEdBQVcsT0FBQSxDQUFRLEtBQVIsQ0FBYyxDQUFDOztFQUMxQixTQUFBLEdBQVksT0FBQSxDQUFRLEtBQVIsQ0FBYyxDQUFDOztFQUUzQixNQUFNLENBQUMsT0FBUCxHQUNNO0lBR0osYUFBQyxDQUFBLFFBQUQsR0FBVyxTQUFDLFFBQUQ7YUFDTCxJQUFBLGFBQUEsQ0FBYyxRQUFkO0lBREs7O0lBSUUsdUJBQUMsU0FBRDtBQUNYLFVBQUE7TUFEWSxJQUFDLENBQUEsV0FBRDtNQUNYLFVBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFiLENBQTRCLElBQUMsQ0FBQSxRQUE3QjtNQUNaLElBQUcsZUFBSDtRQUNFLFlBQUEsR0FBZSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQWIsQ0FBQSxDQUF1QixDQUFDLE9BQXhCLENBQWdDLE9BQWhDO1FBQ2YsSUFBQyxDQUFBLElBQUQsR0FBUSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWIsQ0FBQSxDQUErQixDQUFBLFlBQUEsRUFGekM7O0lBRlc7OzRCQU9iLElBQUEsR0FBTSxTQUFDLFNBQUQ7TUFDSixJQUFHLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBSDtlQUNFLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixJQUFDLENBQUEsT0FBRCxDQUFBLENBQUEsR0FBYSxJQUFDLENBQUEsa0JBQUQsQ0FBb0IsU0FBcEIsQ0FBL0IsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsc0JBQUQsQ0FBQSxFQUhGOztJQURJOzs0QkFPTixZQUFBLEdBQWMsU0FBQyxTQUFEO01BQ1osSUFBRyxJQUFDLENBQUEsVUFBRCxDQUFBLENBQUg7ZUFDRSxJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBQSxHQUFzQixJQUFDLENBQUEsa0JBQUQsQ0FBb0IsU0FBcEIsQ0FBeEMsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsc0JBQUQsQ0FBQSxFQUhGOztJQURZOzs0QkFPZCxLQUFBLEdBQU8sU0FBQyxTQUFEO01BQ0wsSUFBRyxJQUFDLENBQUEsVUFBRCxDQUFZLElBQVosQ0FBSDtlQUNFLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixJQUFDLENBQUEsUUFBRCxDQUFBLENBQUEsR0FBYyxJQUFDLENBQUEsa0JBQUQsQ0FBb0IsU0FBcEIsQ0FBaEMsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsc0JBQUQsQ0FBd0IsSUFBeEIsRUFIRjs7SUFESzs7NEJBTVAsT0FBQSxHQUFTLFNBQUE7TUFDUCxJQUFHLElBQUMsQ0FBQSxVQUFELENBQVksSUFBWixDQUFIO2VBQ0UsSUFBQyxDQUFBLGdCQUFELENBQWtCLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBbEIsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsc0JBQUQsQ0FBd0IsSUFBeEIsRUFIRjs7SUFETzs7NEJBTVQsT0FBQSxHQUFTLFNBQUMsU0FBRDtNQUNQLElBQUcsSUFBQyxDQUFBLFVBQUQsQ0FBQSxDQUFIO2VBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFmLENBQXFCLElBQUMsQ0FBQSxNQUFELENBQUEsQ0FBQSxHQUFZLElBQUMsQ0FBQSxrQkFBRCxDQUFvQixTQUFwQixDQUFqQyxFQURGO09BQUEsTUFBQTtlQUdFLElBQUMsQ0FBQSxzQkFBRCxDQUFBLEVBSEY7O0lBRE87OzRCQU1ULGlCQUFBLEdBQW1CLFNBQUE7TUFDakIsSUFBRyxJQUFDLENBQUEsVUFBRCxDQUFBLENBQUg7ZUFDRSxJQUFDLENBQUEsZ0JBQUQsQ0FBa0IsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBbEIsRUFERjtPQUFBLE1BQUE7ZUFHRSxJQUFDLENBQUEsc0JBQUQsQ0FBQSxFQUhGOztJQURpQjs7NEJBTW5CLFVBQUEsR0FBWSxTQUFBO01BQ1YsSUFBRyxJQUFDLENBQUEsVUFBRCxDQUFZLElBQVosQ0FBSDtlQUNFLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixJQUFDLENBQUEsU0FBRCxDQUFBLENBQWxCLEVBREY7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLHNCQUFELENBQXdCLElBQXhCLEVBSEY7O0lBRFU7OzRCQU1aLGNBQUEsR0FBZ0IsU0FBQTtNQUNkLElBQUcsSUFBQyxDQUFBLFVBQUQsQ0FBQSxDQUFIO2VBQ0UsSUFBQyxDQUFBLGdCQUFELENBQWtCLElBQUMsQ0FBQSxnQkFBRCxDQUFBLENBQWxCLEVBREY7T0FBQSxNQUFBO2VBR0UsSUFBQyxDQUFBLHNCQUFELENBQUEsRUFIRjs7SUFEYzs7NEJBTWhCLGtCQUFBLEdBQW9CLFNBQUMsU0FBRDtBQUNsQixVQUFBO01BQUEsSUFBRyxTQUFBLElBQWMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLDRDQUFoQixDQUFqQjtRQUNFLFNBQUEsR0FBWSxLQUFLLENBQUMsVUFBTixDQUFpQixTQUFqQjtRQUNaLFFBQUEsR0FBVyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQWhCLEdBQXNCO1FBQ2pDLE1BQUEsR0FBUyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQWQsR0FBb0I7UUFFN0IsSUFBRyxRQUFBLEtBQVksTUFBZjtVQUNFLElBQUcsSUFBQyxDQUFBLG1CQUFELENBQXFCLElBQUMsQ0FBQSxNQUFELENBQUEsQ0FBckIsQ0FBSDttQkFBd0MsTUFBQSxHQUFPLFNBQS9DO1dBQUEsTUFBQTttQkFBK0QsR0FBQSxHQUFJLFNBQW5FO1dBREY7U0FBQSxNQUFBO1VBR0UsSUFBRyxJQUFDLENBQUEsbUJBQUQsQ0FBcUIsSUFBQyxDQUFBLE1BQUQsQ0FBQSxDQUFyQixDQUFIO21CQUF3QyxNQUFBLEdBQU8sUUFBUCxHQUFnQixHQUFoQixHQUFtQixPQUEzRDtXQUFBLE1BQUE7bUJBQXlFLEdBQUEsR0FBSSxRQUFKLEdBQWEsR0FBYixHQUFnQixPQUF6RjtXQUhGO1NBTEY7T0FBQSxNQUFBO2VBVUUsR0FWRjs7SUFEa0I7OzRCQWNwQixVQUFBLEdBQVksU0FBQyxhQUFEOztRQUFDLGdCQUFnQjs7YUFDM0IsSUFBQyxDQUFBLGdCQUFELENBQWtCLGFBQWxCLENBQWdDLENBQUMsTUFBakMsS0FBMkM7SUFEakM7OzRCQUlaLGdCQUFBLEdBQWtCLFNBQUMsYUFBRDtNQUNoQixJQUFBLENBQU8sSUFBQyxDQUFBLElBQVI7QUFDRSxlQUFPLENBQUMsZ0NBQUEsR0FBaUMsSUFBQyxDQUFBLFFBQWxDLEdBQTJDLEdBQTVDLEVBRFQ7O01BR0EsSUFBQSxDQUFPLElBQUMsQ0FBQSxNQUFELENBQUEsQ0FBUDtBQUNFLGVBQU8sQ0FBQyw2QkFBQSxHQUE2QixDQUFDLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBRCxDQUE5QixFQURUOztNQUdBLElBQUEsQ0FBTyxJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFQO0FBQ0UsZUFBTyxDQUFDLHlDQUFBLEdBQXlDLENBQUMsSUFBQyxDQUFBLE1BQUQsQ0FBQSxDQUFELENBQTFDLEVBRFQ7O01BR0EsSUFBSSxhQUFBLElBQWtCLElBQUMsQ0FBQSxVQUFELENBQVksSUFBQyxDQUFBLE1BQUQsQ0FBQSxDQUFaLENBQXRCO0FBQ0UsZUFBTyxDQUFDLHVGQUFELEVBRFQ7O2FBR0E7SUFiZ0I7OzRCQWdCbEIsc0JBQUEsR0FBd0IsU0FBQyxhQUFEO0FBQ3RCLFVBQUE7O1FBRHVCLGdCQUFnQjs7TUFDdkMsT0FBQSxHQUFVLElBQUMsQ0FBQSxnQkFBRCxDQUFrQixhQUFsQixDQUFnQyxDQUFDLElBQWpDLENBQXNDLElBQXRDO2FBQ1YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFuQixDQUE4QixPQUE5QjtJQUZzQjs7NEJBS3hCLGdCQUFBLEdBQWtCLFNBQUMsR0FBRDthQUNoQixLQUFLLENBQUMsWUFBTixDQUFtQixHQUFuQjtJQURnQjs7NEJBSWxCLE9BQUEsR0FBUyxTQUFBO0FBQ1AsVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFDLENBQUEsZ0JBQUQsQ0FBQTtNQUVWLElBQUcsSUFBQyxDQUFBLG1CQUFELENBQXFCLE9BQXJCLENBQUg7ZUFDSyxPQUFELEdBQVMsT0FBVCxHQUFlLENBQUMsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBRCxDQUFmLEdBQW9DLEdBQXBDLEdBQXNDLENBQUMsSUFBQyxDQUFBLGNBQUQsQ0FBZ0IsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBaEIsQ0FBRCxFQUQxQztPQUFBLE1BQUE7ZUFHSyxPQUFELEdBQVMsVUFBVCxHQUFrQixDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxnQkFBRCxDQUFBLENBQWhCLENBQUQsQ0FBbEIsR0FBd0QsTUFBeEQsR0FBNkQsQ0FBQyxJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFELEVBSGpFOztJQUhPOzs0QkFTVCxnQkFBQSxHQUFrQixTQUFBO0FBQ2hCLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBQyxDQUFBLGdCQUFELENBQUE7TUFFVixJQUFHLElBQUMsQ0FBQSxtQkFBRCxDQUFxQixPQUFyQixDQUFIO2VBQ0ssT0FBRCxHQUFTLGNBQVQsR0FBc0IsQ0FBQyxJQUFDLENBQUEsY0FBRCxDQUFnQixJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFoQixDQUFELEVBRDFCO09BQUEsTUFBQTtlQUdLLE9BQUQsR0FBUyxVQUFULEdBQWtCLENBQUMsSUFBQyxDQUFBLGNBQUQsQ0FBZ0IsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBaEIsQ0FBRCxDQUFsQixHQUF3RCxhQUg1RDs7SUFIZ0I7OzRCQVVsQixNQUFBLEdBQVEsU0FBQTtBQUNOLFVBQUE7TUFBQSxPQUFBLEdBQVUsSUFBQyxDQUFBLGdCQUFELENBQUE7TUFFVixJQUFHLElBQUMsQ0FBQSxtQkFBRCxDQUFxQixPQUFyQixDQUFIO2VBQ0ssT0FBRCxHQUFTLE9BQVQsR0FBZSxDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxHQUFELENBQUEsQ0FBaEIsQ0FBRCxDQUFmLEdBQXdDLEdBQXhDLEdBQTBDLENBQUMsSUFBQyxDQUFBLGNBQUQsQ0FBZ0IsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBaEIsQ0FBRCxFQUQ5QztPQUFBLE1BQUE7ZUFHSyxPQUFELEdBQVMsVUFBVCxHQUFrQixDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxnQkFBRCxDQUFBLENBQWhCLENBQUQsQ0FBbEIsR0FBd0QsTUFBeEQsR0FBNkQsQ0FBQyxJQUFDLENBQUEsY0FBRCxDQUFnQixJQUFDLENBQUEsR0FBRCxDQUFBLENBQWhCLENBQUQsRUFIakU7O0lBSE07OzRCQVNSLFFBQUEsR0FBVSxTQUFBO2FBQ04sQ0FBQyxJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFELENBQUEsR0FBcUIsWUFBckIsR0FBZ0MsQ0FBQyxJQUFDLENBQUEsY0FBRCxDQUFnQixJQUFDLENBQUEsVUFBRCxDQUFBLENBQWhCLENBQUQsQ0FBaEMsR0FBZ0UsR0FBaEUsR0FBa0UsQ0FBQyxJQUFDLENBQUEsY0FBRCxDQUFnQixJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFoQixDQUFEO0lBRDVEOzs0QkFJVixVQUFBLEdBQVksU0FBQTthQUNSLENBQUMsSUFBQyxDQUFBLGdCQUFELENBQUEsQ0FBRCxDQUFBLEdBQXFCLGdCQUFyQixHQUFvQyxDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBaEIsQ0FBRCxDQUFwQyxHQUFvRSxHQUFwRSxHQUFzRSxDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxnQkFBRCxDQUFBLENBQWhCLENBQUQ7SUFEOUQ7OzRCQUlaLFNBQUEsR0FBVyxTQUFBO2FBQ1AsQ0FBQyxJQUFDLENBQUEsZ0JBQUQsQ0FBQSxDQUFELENBQUEsR0FBcUI7SUFEZDs7NEJBSVgsZ0JBQUEsR0FBa0IsU0FBQTtBQUNoQixVQUFBO01BQUEsT0FBQSxHQUFVLElBQUMsQ0FBQSxnQkFBRCxDQUFBO01BRVYsSUFBRyxJQUFDLENBQUEsbUJBQUQsQ0FBcUIsT0FBckIsQ0FBSDtlQUNLLE9BQUQsR0FBUyxVQUFULEdBQWtCLENBQUMsSUFBQyxDQUFBLGNBQUQsQ0FBZ0IsSUFBQyxDQUFBLFVBQUQsQ0FBQSxDQUFoQixDQUFELEVBRHRCO09BQUEsTUFBQTtlQUdLLE9BQUQsR0FBUyxnQ0FBVCxHQUF3QyxDQUFDLElBQUMsQ0FBQSxjQUFELENBQWdCLElBQUMsQ0FBQSxVQUFELENBQUEsQ0FBaEIsQ0FBRCxFQUg1Qzs7SUFIZ0I7OzRCQVFsQixjQUFBLEdBQWdCLFNBQUMsUUFBRDs7UUFBQyxXQUFTOztNQUN4QixRQUFBLEdBQVcsUUFBUSxDQUFDLEtBQVQsQ0FBZSxHQUFmO01BQ1gsUUFBQSxHQUFXLFFBQVEsQ0FBQyxHQUFULENBQWEsU0FBQyxPQUFEO2VBQWEsa0JBQUEsQ0FBbUIsT0FBbkI7TUFBYixDQUFiO2FBQ1gsUUFBUSxDQUFDLElBQVQsQ0FBYyxHQUFkO0lBSGM7OzRCQU1oQixNQUFBLEdBQVEsU0FBQTtBQUNOLFVBQUE7TUFBQSxpQkFBQSw2Q0FBb0M7YUFDcEMsSUFBQyxDQUFBLElBQUksQ0FBQyxjQUFOLENBQXFCLFNBQUEsR0FBVSxpQkFBVixHQUE0QixNQUFqRCxFQUF3RCxJQUFDLENBQUEsUUFBekQ7SUFGTTs7NEJBS1IsZ0JBQUEsR0FBa0IsU0FBQTtBQUNoQixVQUFBO01BQUEsR0FBQSxHQUFNLElBQUMsQ0FBQSxNQUFELENBQUE7TUFFTixJQUFHLElBQUMsQ0FBQSxXQUFELENBQWEsR0FBYixDQUFIO0FBQUE7T0FBQSxNQUVLLElBQUcsSUFBQyxDQUFBLG1CQUFELENBQXFCLEdBQXJCLENBQUg7QUFDSCxlQUFPLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixHQUF2QixFQURKO09BQUEsTUFBQTtBQUdILGVBQU8sSUFBQyxDQUFBLG1CQUFELENBQXFCLEdBQXJCLEVBSEo7O0lBTFc7OzRCQVdsQixxQkFBQSxHQUF1QixTQUFDLEdBQUQ7TUFDckIsSUFBRyxHQUFHLENBQUMsS0FBSixDQUFVLHFCQUFWLENBQUg7UUFDRSxHQUFBLEdBQU0sR0FBRyxDQUFDLE9BQUosQ0FBWSxRQUFaLEVBQXNCLEVBQXRCLEVBRFI7T0FBQSxNQUVLLElBQUcsR0FBRyxDQUFDLEtBQUosQ0FBVSxrQkFBVixDQUFIO1FBQ0gsR0FBQSxHQUFNLEdBQUcsQ0FBQyxPQUFKLENBQVkseUJBQVosRUFBdUMsU0FBQyxLQUFELEVBQVEsSUFBUixFQUFjLFFBQWQ7VUFDM0MsUUFBQSxHQUFXLFFBQVEsQ0FBQyxPQUFULENBQWlCLE1BQWpCLEVBQXlCLEVBQXpCO2lCQUNYLENBQUEsU0FBQSxHQUFVLElBQVYsR0FBZSxHQUFmLEdBQWtCLFFBQWxCLENBQTRCLENBQUMsT0FBN0IsQ0FBcUMsUUFBckMsRUFBK0MsRUFBL0M7UUFGMkMsQ0FBdkMsRUFESDtPQUFBLE1BSUEsSUFBRyxHQUFHLENBQUMsS0FBSixDQUFVLHdCQUFWLENBQUg7UUFDSCxHQUFBLEdBQU0sU0FBQSxHQUFTLENBQUMsR0FBRyxDQUFDLFNBQUosQ0FBYyxFQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEIsUUFBMUIsRUFBb0MsRUFBcEMsQ0FBRCxFQURaO09BQUEsTUFFQSxJQUFHLEdBQUcsQ0FBQyxLQUFKLENBQVUsbUJBQVYsQ0FBSDtRQUNILEdBQUEsR0FBTSxNQUFBLEdBQU0sQ0FBQyxHQUFHLENBQUMsU0FBSixDQUFjLENBQWQsQ0FBZ0IsQ0FBQyxPQUFqQixDQUF5QixRQUF6QixFQUFtQyxFQUFuQyxDQUFELEVBRFQ7O01BR0wsR0FBQSxHQUFNLEdBQUcsQ0FBQyxPQUFKLENBQVksUUFBWixFQUFzQixFQUF0QjtNQUNOLEdBQUEsR0FBTSxHQUFHLENBQUMsT0FBSixDQUFZLE1BQVosRUFBb0IsRUFBcEI7QUFFTixhQUFPO0lBZmM7OzRCQWtCdkIsbUJBQUEsR0FBcUIsU0FBQyxHQUFEO0FBQ25CLFVBQUE7TUFBQSxNQUFBLEdBQVMsUUFBQSxDQUFTLElBQUMsQ0FBQSxxQkFBRCxDQUF1QixHQUF2QixDQUFUO01BRVQsTUFBTSxDQUFDLElBQVAsR0FBYyxNQUFNLENBQUM7TUFDckIsTUFBTSxDQUFDLElBQVAsR0FBYztNQUVkLE1BQXNCLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBaEIsQ0FBc0Isd0JBQXRCLENBQXRCLEVBQUMsY0FBRCxFQUFRLGFBQVIsRUFBYztNQUNkLE1BQU0sQ0FBQyxRQUFQLEdBQWtCLFlBQUEsR0FBYSxJQUFiLEdBQWtCLFNBQWxCLEdBQTJCO0FBRTdDLGFBQU8sU0FBQSxDQUFVLE1BQVY7SUFUWTs7NEJBWXJCLFdBQUEsR0FBYSxTQUFDLEdBQUQ7QUFDWCxVQUFBO01BQUEsSUFBZSxHQUFHLENBQUMsT0FBSixDQUFZLGdCQUFaLENBQUEsS0FBaUMsQ0FBaEQ7QUFBQSxlQUFPLEtBQVA7O0FBRUE7UUFDRyxPQUFRLFFBQUEsQ0FBUyxHQUFUO2VBQ1QsSUFBQSxLQUFRLGFBRlY7T0FBQTtJQUhXOzs0QkFRYixtQkFBQSxHQUFxQixTQUFDLEdBQUQ7QUFDbkIsVUFBQTtNQUFBLElBQWUsR0FBRyxDQUFDLE9BQUosQ0FBWSxtQkFBWixDQUFBLEtBQW9DLENBQW5EO0FBQUEsZUFBTyxLQUFQOztBQUVBO1FBQ0csT0FBUSxRQUFBLENBQVMsR0FBVDtlQUNULElBQUEsS0FBUSxnQkFGVjtPQUFBO0lBSG1COzs0QkFRckIsVUFBQSxHQUFZLFNBQUMsR0FBRDtBQUNWLGFBQU8sQ0FBSSxDQUFDLElBQUMsQ0FBQSxXQUFELENBQWEsR0FBYixDQUFBLElBQXFCLElBQUMsQ0FBQSxtQkFBRCxDQUFxQixHQUFyQixDQUF0QjtJQUREOzs0QkFJWixnQkFBQSxHQUFrQixTQUFBO2FBQ2hCLElBQUMsQ0FBQSxJQUFJLENBQUMsT0FBTixDQUFjLElBQUMsQ0FBQSxRQUFmLENBQXdCLENBQUMsVUFBekIsQ0FBb0MsSUFBQyxDQUFBLFFBQXJDO0lBRGdCOzs0QkFJbEIsVUFBQSxHQUFZLFNBQUE7QUFDVixVQUFBO01BQUEsV0FBQSxHQUFjLElBQUMsQ0FBQSxJQUFJLENBQUMsWUFBTixDQUFtQixJQUFDLENBQUEsUUFBcEI7TUFDZCxJQUFBLENBQW1CLFdBQW5CO0FBQUEsZUFBTyxLQUFQOztNQUVBLFlBQUEsR0FBZSxJQUFDLENBQUEsSUFBSSxDQUFDLGNBQU4sQ0FBcUIsU0FBQSxHQUFVLFdBQVYsR0FBc0IsU0FBM0MsRUFBcUQsSUFBQyxDQUFBLFFBQXREO01BQ2YsSUFBQSxDQUFBLHlCQUFtQixZQUFZLENBQUUsZ0JBQWQsR0FBdUIsQ0FBMUMsQ0FBQTtBQUFBLGVBQU8sS0FBUDs7YUFFQTtJQVBVOzs0QkFVWixHQUFBLEdBQUssU0FBQTthQUNILElBQUMsQ0FBQSxJQUFJLENBQUMsa0JBQU4sQ0FBeUIsTUFBekIsRUFBaUMsSUFBQyxDQUFBLFFBQWxDO0lBREc7OzRCQUlMLFVBQUEsR0FBWSxTQUFBO0FBQ1YsVUFBQTtNQUFBLFdBQUEsR0FBYyxJQUFDLENBQUEsSUFBSSxDQUFDLFlBQU4sQ0FBbUIsSUFBQyxDQUFBLFFBQXBCO01BQ2QsSUFBQSxDQUFtQixXQUFuQjtBQUFBLGVBQU8sS0FBUDs7TUFFQSxXQUFBLEdBQWMsSUFBQyxDQUFBLElBQUksQ0FBQyxjQUFOLENBQXFCLFNBQUEsR0FBVSxXQUFWLEdBQXNCLFFBQTNDLEVBQW9ELElBQUMsQ0FBQSxRQUFyRDtNQUNkLElBQUEsQ0FBQSx3QkFBMEIsV0FBVyxDQUFFLGdCQUFiLEdBQXNCLEVBQWhELENBQUE7QUFBQSxlQUFPLFlBQVA7O01BQ0EsSUFBMEIsV0FBVyxDQUFDLE9BQVosQ0FBb0IsYUFBcEIsQ0FBQSxLQUFzQyxDQUFoRTtBQUFBLGVBQU8sWUFBUDs7YUFFQSxXQUFXLENBQUMsU0FBWixDQUFzQixFQUF0QjtJQVJVOzs0QkFXWixnQkFBQSxHQUFrQixTQUFBO01BQ2hCLElBQUcseUJBQUg7ZUFDRSxJQUFDLENBQUEsY0FBRCxDQUFnQixJQUFDLENBQUEsVUFBRCxDQUFBLENBQWhCLEVBREY7T0FBQSxNQUFBO2VBR0UsU0FIRjs7SUFEZ0I7Ozs7O0FBdFFwQiIsInNvdXJjZXNDb250ZW50IjpbIlNoZWxsID0gcmVxdWlyZSAnc2hlbGwnXG57UmFuZ2V9ID0gcmVxdWlyZSAnYXRvbSdcbnBhcnNlVXJsID0gcmVxdWlyZSgndXJsJykucGFyc2VcbmZvcm1hdFVybCA9IHJlcXVpcmUoJ3VybCcpLmZvcm1hdFxuXG5tb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBCaXRidWNrZXRGaWxlXG5cbiAgIyBQdWJsaWNcbiAgQGZyb21QYXRoOiAoZmlsZVBhdGgpIC0+XG4gICAgbmV3IEJpdGJ1Y2tldEZpbGUoZmlsZVBhdGgpXG5cbiAgIyBJbnRlcm5hbFxuICBjb25zdHJ1Y3RvcjogKEBmaWxlUGF0aCkgLT5cbiAgICBbcm9vdERpcl0gPSBhdG9tLnByb2plY3QucmVsYXRpdml6ZVBhdGgoQGZpbGVQYXRoKVxuICAgIGlmIHJvb3REaXI/XG4gICAgICByb290RGlySW5kZXggPSBhdG9tLnByb2plY3QuZ2V0UGF0aHMoKS5pbmRleE9mKHJvb3REaXIpXG4gICAgICBAcmVwbyA9IGF0b20ucHJvamVjdC5nZXRSZXBvc2l0b3JpZXMoKVtyb290RGlySW5kZXhdXG5cbiAgIyBQdWJsaWNcbiAgb3BlbjogKGxpbmVSYW5nZSkgLT5cbiAgICBpZiBAaXNPcGVuYWJsZSgpXG4gICAgICBAb3BlblVybEluQnJvd3NlcihAYmxvYlVybCgpICsgQGdldExpbmVSYW5nZVN1ZmZpeChsaW5lUmFuZ2UpKVxuICAgIGVsc2VcbiAgICAgIEByZXBvcnRWYWxpZGF0aW9uRXJyb3JzKClcblxuICAjIFB1YmxpY1xuICBvcGVuT25NYXN0ZXI6IChsaW5lUmFuZ2UpIC0+XG4gICAgaWYgQGlzT3BlbmFibGUoKVxuICAgICAgQG9wZW5VcmxJbkJyb3dzZXIoQGJsb2JVcmxGb3JNYXN0ZXIoKSArIEBnZXRMaW5lUmFuZ2VTdWZmaXgobGluZVJhbmdlKSlcbiAgICBlbHNlXG4gICAgICBAcmVwb3J0VmFsaWRhdGlvbkVycm9ycygpXG5cbiAgIyBQdWJsaWNcbiAgYmxhbWU6IChsaW5lUmFuZ2UpIC0+XG4gICAgaWYgQGlzT3BlbmFibGUodHJ1ZSlcbiAgICAgIEBvcGVuVXJsSW5Ccm93c2VyKEBibGFtZVVybCgpICsgQGdldExpbmVSYW5nZVN1ZmZpeChsaW5lUmFuZ2UpKVxuICAgIGVsc2VcbiAgICAgIEByZXBvcnRWYWxpZGF0aW9uRXJyb3JzKHRydWUpXG5cbiAgaGlzdG9yeTogLT5cbiAgICBpZiBAaXNPcGVuYWJsZSh0cnVlKVxuICAgICAgQG9wZW5VcmxJbkJyb3dzZXIoQGhpc3RvcnlVcmwoKSlcbiAgICBlbHNlXG4gICAgICBAcmVwb3J0VmFsaWRhdGlvbkVycm9ycyh0cnVlKVxuXG4gIGNvcHlVcmw6IChsaW5lUmFuZ2UpIC0+XG4gICAgaWYgQGlzT3BlbmFibGUoKVxuICAgICAgYXRvbS5jbGlwYm9hcmQud3JpdGUoQHNoYVVybCgpICsgQGdldExpbmVSYW5nZVN1ZmZpeChsaW5lUmFuZ2UpKVxuICAgIGVsc2VcbiAgICAgIEByZXBvcnRWYWxpZGF0aW9uRXJyb3JzKClcblxuICBvcGVuQnJhbmNoQ29tcGFyZTogLT5cbiAgICBpZiBAaXNPcGVuYWJsZSgpXG4gICAgICBAb3BlblVybEluQnJvd3NlcihAYnJhbmNoQ29tcGFyZVVybCgpKVxuICAgIGVsc2VcbiAgICAgIEByZXBvcnRWYWxpZGF0aW9uRXJyb3JzKClcblxuICBvcGVuSXNzdWVzOiAtPlxuICAgIGlmIEBpc09wZW5hYmxlKHRydWUpXG4gICAgICBAb3BlblVybEluQnJvd3NlcihAaXNzdWVzVXJsKCkpXG4gICAgZWxzZVxuICAgICAgQHJlcG9ydFZhbGlkYXRpb25FcnJvcnModHJ1ZSlcblxuICBvcGVuUmVwb3NpdG9yeTogLT5cbiAgICBpZiBAaXNPcGVuYWJsZSgpXG4gICAgICBAb3BlblVybEluQnJvd3NlcihAYml0YnVja2V0UmVwb1VybCgpKVxuICAgIGVsc2VcbiAgICAgIEByZXBvcnRWYWxpZGF0aW9uRXJyb3JzKClcblxuICBnZXRMaW5lUmFuZ2VTdWZmaXg6IChsaW5lUmFuZ2UpIC0+XG4gICAgaWYgbGluZVJhbmdlIGFuZCBhdG9tLmNvbmZpZy5nZXQoJ29wZW4tb24tYml0YnVja2V0LmluY2x1ZGVMaW5lTnVtYmVyc0luVXJscycpXG4gICAgICBsaW5lUmFuZ2UgPSBSYW5nZS5mcm9tT2JqZWN0KGxpbmVSYW5nZSlcbiAgICAgIHN0YXJ0Um93ID0gbGluZVJhbmdlLnN0YXJ0LnJvdyArIDFcbiAgICAgIGVuZFJvdyA9IGxpbmVSYW5nZS5lbmQucm93ICsgMVxuXG4gICAgICBpZiBzdGFydFJvdyBpcyBlbmRSb3dcbiAgICAgICAgaWYgQGlzQml0YnVja2V0Q2xvdWRVcmwoQGdpdFVybCgpKSB0aGVuIFwiI2NsLSN7c3RhcnRSb3d9XCIgZWxzZSBcIiMje3N0YXJ0Um93fVwiXG4gICAgICBlbHNlXG4gICAgICAgIGlmIEBpc0JpdGJ1Y2tldENsb3VkVXJsKEBnaXRVcmwoKSkgdGhlbiBcIiNjbC0je3N0YXJ0Um93fToje2VuZFJvd31cIiBlbHNlIFwiIyN7c3RhcnRSb3d9LSN7ZW5kUm93fVwiXG4gICAgZWxzZVxuICAgICAgJydcblxuICAjIFB1YmxpY1xuICBpc09wZW5hYmxlOiAoZGlzYWxsb3dTdGFzaCA9IGZhbHNlKSAtPlxuICAgIEB2YWxpZGF0aW9uRXJyb3JzKGRpc2FsbG93U3Rhc2gpLmxlbmd0aCBpcyAwXG5cbiAgIyBQdWJsaWNcbiAgdmFsaWRhdGlvbkVycm9yczogKGRpc2FsbG93U3Rhc2gpIC0+XG4gICAgdW5sZXNzIEByZXBvXG4gICAgICByZXR1cm4gW1wiTm8gcmVwb3NpdG9yeSBmb3VuZCBmb3IgcGF0aDogI3tAZmlsZVBhdGh9LlwiXVxuXG4gICAgdW5sZXNzIEBnaXRVcmwoKVxuICAgICAgcmV0dXJuIFtcIk5vIFVSTCBkZWZpbmVkIGZvciByZW1vdGU6ICN7QHJlbW90ZU5hbWUoKX1cIl1cblxuICAgIHVubGVzcyBAYml0YnVja2V0UmVwb1VybCgpXG4gICAgICByZXR1cm4gW1wiUmVtb3RlIFVSTCBpcyBub3QgaG9zdGVkIG9uIEJpdGJ1Y2tldDogI3tAZ2l0VXJsKCl9XCJdXG5cbiAgICBpZiAoZGlzYWxsb3dTdGFzaCBhbmQgQGlzU3Rhc2hVcmwoQGdpdFVybCgpKSlcbiAgICAgIHJldHVybiBbXCJUaGlzIGZlYXR1cmUgaXMgb25seSBhdmFpbGFibGUgd2hlbiBob3N0aW5nIHJlcG9zaXRvcmllcyBvbiBCaXRidWNrZXQgKGJpdGJ1Y2tldC5vcmcpXCJdXG5cbiAgICBbXVxuXG4gICMgSW50ZXJuYWxcbiAgcmVwb3J0VmFsaWRhdGlvbkVycm9yczogKGRpc2FsbG93U3Rhc2ggPSBmYWxzZSkgLT5cbiAgICBtZXNzYWdlID0gQHZhbGlkYXRpb25FcnJvcnMoZGlzYWxsb3dTdGFzaCkuam9pbignXFxuJylcbiAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhtZXNzYWdlKVxuXG4gICMgSW50ZXJuYWxcbiAgb3BlblVybEluQnJvd3NlcjogKHVybCkgLT5cbiAgICBTaGVsbC5vcGVuRXh0ZXJuYWwgdXJsXG5cbiAgIyBJbnRlcm5hbFxuICBibG9iVXJsOiAtPlxuICAgIGJhc2VVcmwgPSBAYml0YnVja2V0UmVwb1VybCgpXG5cbiAgICBpZiBAaXNCaXRidWNrZXRDbG91ZFVybChiYXNlVXJsKVxuICAgICAgXCIje2Jhc2VVcmx9L3NyYy8je0ByZW1vdGVCcmFuY2hOYW1lKCl9LyN7QGVuY29kZVNlZ21lbnRzKEByZXBvUmVsYXRpdmVQYXRoKCkpfVwiXG4gICAgZWxzZVxuICAgICAgXCIje2Jhc2VVcmx9L2Jyb3dzZS8je0BlbmNvZGVTZWdtZW50cyhAcmVwb1JlbGF0aXZlUGF0aCgpKX0/YXQ9I3tAcmVtb3RlQnJhbmNoTmFtZSgpfVwiXG5cbiAgIyBJbnRlcm5hbFxuICBibG9iVXJsRm9yTWFzdGVyOiAtPlxuICAgIGJhc2VVcmwgPSBAYml0YnVja2V0UmVwb1VybCgpXG5cbiAgICBpZiBAaXNCaXRidWNrZXRDbG91ZFVybChiYXNlVXJsKVxuICAgICAgXCIje2Jhc2VVcmx9L3NyYy9tYXN0ZXIvI3tAZW5jb2RlU2VnbWVudHMoQHJlcG9SZWxhdGl2ZVBhdGgoKSl9XCJcbiAgICBlbHNlXG4gICAgICBcIiN7YmFzZVVybH0vYnJvd3NlLyN7QGVuY29kZVNlZ21lbnRzKEByZXBvUmVsYXRpdmVQYXRoKCkpfT9hdD1tYXN0ZXJcIlxuXG5cbiAgIyBJbnRlcm5hbFxuICBzaGFVcmw6IC0+XG4gICAgYmFzZVVybCA9IEBiaXRidWNrZXRSZXBvVXJsKClcblxuICAgIGlmIEBpc0JpdGJ1Y2tldENsb3VkVXJsKGJhc2VVcmwpXG4gICAgICBcIiN7YmFzZVVybH0vc3JjLyN7QGVuY29kZVNlZ21lbnRzKEBzaGEoKSl9LyN7QGVuY29kZVNlZ21lbnRzKEByZXBvUmVsYXRpdmVQYXRoKCkpfVwiXG4gICAgZWxzZVxuICAgICAgXCIje2Jhc2VVcmx9L2Jyb3dzZS8je0BlbmNvZGVTZWdtZW50cyhAcmVwb1JlbGF0aXZlUGF0aCgpKX0/YXQ9I3tAZW5jb2RlU2VnbWVudHMoQHNoYSgpKX1cIlxuXG4gICMgSW50ZXJuYWxcbiAgYmxhbWVVcmw6IC0+XG4gICAgXCIje0BiaXRidWNrZXRSZXBvVXJsKCl9L2Fubm90YXRlLyN7QGVuY29kZVNlZ21lbnRzKEBicmFuY2hOYW1lKCkpfS8je0BlbmNvZGVTZWdtZW50cyhAcmVwb1JlbGF0aXZlUGF0aCgpKX1cIlxuXG4gICMgSW50ZXJuYWxcbiAgaGlzdG9yeVVybDogLT5cbiAgICBcIiN7QGJpdGJ1Y2tldFJlcG9VcmwoKX0vaGlzdG9yeS1ub2RlLyN7QGVuY29kZVNlZ21lbnRzKEBicmFuY2hOYW1lKCkpfS8je0BlbmNvZGVTZWdtZW50cyhAcmVwb1JlbGF0aXZlUGF0aCgpKX1cIlxuXG4gICMgSW50ZXJuYWxcbiAgaXNzdWVzVXJsOiAtPlxuICAgIFwiI3tAYml0YnVja2V0UmVwb1VybCgpfS9pc3N1ZXNcIlxuXG4gICMgSW50ZXJuYWxcbiAgYnJhbmNoQ29tcGFyZVVybDogLT5cbiAgICBiYXNlVXJsID0gQGJpdGJ1Y2tldFJlcG9VcmwoKVxuXG4gICAgaWYgQGlzQml0YnVja2V0Q2xvdWRVcmwoYmFzZVVybClcbiAgICAgIFwiI3tiYXNlVXJsfS9icmFuY2gvI3tAZW5jb2RlU2VnbWVudHMoQGJyYW5jaE5hbWUoKSl9XCJcbiAgICBlbHNlXG4gICAgICBcIiN7YmFzZVVybH0vY29tcGFyZS9jb21taXRzP3NvdXJjZUJyYW5jaD0je0BlbmNvZGVTZWdtZW50cyhAYnJhbmNoTmFtZSgpKX1cIlxuXG4gIGVuY29kZVNlZ21lbnRzOiAoc2VnbWVudHM9JycpIC0+XG4gICAgc2VnbWVudHMgPSBzZWdtZW50cy5zcGxpdCgnLycpXG4gICAgc2VnbWVudHMgPSBzZWdtZW50cy5tYXAgKHNlZ21lbnQpIC0+IGVuY29kZVVSSUNvbXBvbmVudChzZWdtZW50KVxuICAgIHNlZ21lbnRzLmpvaW4oJy8nKVxuXG4gICMgSW50ZXJuYWxcbiAgZ2l0VXJsOiAtPlxuICAgIHJlbW90ZU9yQmVzdEd1ZXNzID0gQHJlbW90ZU5hbWUoKSA/ICdvcmlnaW4nXG4gICAgQHJlcG8uZ2V0Q29uZmlnVmFsdWUoXCJyZW1vdGUuI3tyZW1vdGVPckJlc3RHdWVzc30udXJsXCIsIEBmaWxlUGF0aClcblxuICAjIEludGVybmFsXG4gIGJpdGJ1Y2tldFJlcG9Vcmw6IC0+XG4gICAgdXJsID0gQGdpdFVybCgpXG5cbiAgICBpZiBAaXNHaXRodWJVcmwodXJsKVxuICAgICAgcmV0dXJuXG4gICAgZWxzZSBpZiBAaXNCaXRidWNrZXRDbG91ZFVybCh1cmwpXG4gICAgICByZXR1cm4gQGJpdGJ1Y2tldENsb3VkUmVwb1VybCh1cmwpXG4gICAgZWxzZVxuICAgICAgcmV0dXJuIEBzdGFzaFJlcG9VcmxSZXBvVXJsKHVybClcblxuICAjIEludGVybmFsXG4gIGJpdGJ1Y2tldENsb3VkUmVwb1VybDogKHVybCkgLT5cbiAgICBpZiB1cmwubWF0Y2ggL2h0dHBzPzpcXC9cXC9bXlxcL10rXFwvLyAjIGUuZy4sIGh0dHBzOi8vYml0YnVja2V0Lm9yZy9mb28vYmFyLmdpdCBvciBodHRwOi8vYml0YnVja2V0Lm9yZy9mb28vYmFyLmdpdFxuICAgICAgdXJsID0gdXJsLnJlcGxhY2UoL1xcLmdpdCQvLCAnJylcbiAgICBlbHNlIGlmIHVybC5tYXRjaCAvXmdpdFteQF0qQFteOl0rOi8gICAgIyBlLmcuLCBnaXRAYml0YnVja2V0Lm9yZzpmb28vYmFyLmdpdFxuICAgICAgdXJsID0gdXJsLnJlcGxhY2UgL15naXRbXkBdKkAoW146XSspOiguKykkLywgKG1hdGNoLCBob3N0LCByZXBvUGF0aCkgLT5cbiAgICAgICAgcmVwb1BhdGggPSByZXBvUGF0aC5yZXBsYWNlKC9eXFwvKy8sICcnKSAjIHJlcGxhY2UgbGVhZGluZyBzbGFzaGVzXG4gICAgICAgIFwiaHR0cDovLyN7aG9zdH0vI3tyZXBvUGF0aH1cIi5yZXBsYWNlKC9cXC5naXQkLywgJycpXG4gICAgZWxzZSBpZiB1cmwubWF0Y2ggL3NzaDpcXC9cXC9naXRAKFteXFwvXSspXFwvLyAgICAjIGUuZy4sIHNzaDovL2dpdEBiaXRidWNrZXQub3JnL2Zvby9iYXIuZ2l0XG4gICAgICB1cmwgPSBcImh0dHA6Ly8je3VybC5zdWJzdHJpbmcoMTApLnJlcGxhY2UoL1xcLmdpdCQvLCAnJyl9XCJcbiAgICBlbHNlIGlmIHVybC5tYXRjaCAvXmdpdDpcXC9cXC9bXlxcL10rXFwvLyAjIGUuZy4sIGdpdDovL2JpdGJ1Y2tldC5vcmcvZm9vL2Jhci5naXRcbiAgICAgIHVybCA9IFwiaHR0cCN7dXJsLnN1YnN0cmluZygzKS5yZXBsYWNlKC9cXC5naXQkLywgJycpfVwiXG5cbiAgICB1cmwgPSB1cmwucmVwbGFjZSgvXFwuZ2l0JC8sICcnKVxuICAgIHVybCA9IHVybC5yZXBsYWNlKC9cXC8rJC8sICcnKVxuXG4gICAgcmV0dXJuIHVybFxuXG4gICMgSW50ZXJuYWxcbiAgc3Rhc2hSZXBvVXJsUmVwb1VybDogKHVybCkgLT5cbiAgICB1cmxPYmogPSBwYXJzZVVybChAYml0YnVja2V0Q2xvdWRSZXBvVXJsKHVybCkpXG5cbiAgICB1cmxPYmouaG9zdCA9IHVybE9iai5ob3N0bmFtZVxuICAgIHVybE9iai5hdXRoID0gbnVsbFxuXG4gICAgW21hdGNoLCBwcm9qLCByZXBvXSA9IHVybE9iai5wYXRobmFtZS5tYXRjaCAvKD86XFwvc2NtKT9cXC8oLispXFwvKC4rKS9cbiAgICB1cmxPYmoucGF0aG5hbWUgPSBcIi9wcm9qZWN0cy8je3Byb2p9L3JlcG9zLyN7cmVwb31cIlxuXG4gICAgcmV0dXJuIGZvcm1hdFVybCh1cmxPYmopXG5cbiAgIyBJbnRlcm5hbFxuICBpc0dpdGh1YlVybDogKHVybCkgLT5cbiAgICByZXR1cm4gdHJ1ZSBpZiB1cmwuaW5kZXhPZignZ2l0QGdpdGh1Yi5jb20nKSBpcyAwXG5cbiAgICB0cnlcbiAgICAgIHtob3N0fSA9IHBhcnNlVXJsKHVybClcbiAgICAgIGhvc3QgaXMgJ2dpdGh1Yi5jb20nXG5cbiAgIyBJbnRlcm5hbFxuICBpc0JpdGJ1Y2tldENsb3VkVXJsOiAodXJsKSAtPlxuICAgIHJldHVybiB0cnVlIGlmIHVybC5pbmRleE9mKCdnaXRAYml0YnVja2V0Lm9yZycpIGlzIDBcblxuICAgIHRyeVxuICAgICAge2hvc3R9ID0gcGFyc2VVcmwodXJsKVxuICAgICAgaG9zdCBpcyAnYml0YnVja2V0Lm9yZydcblxuICAjIEludGVybmFsXG4gIGlzU3Rhc2hVcmw6ICh1cmwpIC0+XG4gICAgcmV0dXJuIG5vdCAoQGlzR2l0aHViVXJsKHVybCkgb3IgQGlzQml0YnVja2V0Q2xvdWRVcmwodXJsKSlcblxuICAjIEludGVybmFsXG4gIHJlcG9SZWxhdGl2ZVBhdGg6IC0+XG4gICAgQHJlcG8uZ2V0UmVwbyhAZmlsZVBhdGgpLnJlbGF0aXZpemUoQGZpbGVQYXRoKVxuXG4gICMgSW50ZXJuYWxcbiAgcmVtb3RlTmFtZTogLT5cbiAgICBzaG9ydEJyYW5jaCA9IEByZXBvLmdldFNob3J0SGVhZChAZmlsZVBhdGgpXG4gICAgcmV0dXJuIG51bGwgdW5sZXNzIHNob3J0QnJhbmNoXG5cbiAgICBicmFuY2hSZW1vdGUgPSBAcmVwby5nZXRDb25maWdWYWx1ZShcImJyYW5jaC4je3Nob3J0QnJhbmNofS5yZW1vdGVcIiwgQGZpbGVQYXRoKVxuICAgIHJldHVybiBudWxsIHVubGVzcyBicmFuY2hSZW1vdGU/Lmxlbmd0aCA+IDBcblxuICAgIGJyYW5jaFJlbW90ZVxuXG4gICMgSW50ZXJuYWxcbiAgc2hhOiAtPlxuICAgIEByZXBvLmdldFJlZmVyZW5jZVRhcmdldCgnSEVBRCcsIEBmaWxlUGF0aClcblxuICAjIEludGVybmFsXG4gIGJyYW5jaE5hbWU6IC0+XG4gICAgc2hvcnRCcmFuY2ggPSBAcmVwby5nZXRTaG9ydEhlYWQoQGZpbGVQYXRoKVxuICAgIHJldHVybiBudWxsIHVubGVzcyBzaG9ydEJyYW5jaFxuXG4gICAgYnJhbmNoTWVyZ2UgPSBAcmVwby5nZXRDb25maWdWYWx1ZShcImJyYW5jaC4je3Nob3J0QnJhbmNofS5tZXJnZVwiLCBAZmlsZVBhdGgpXG4gICAgcmV0dXJuIHNob3J0QnJhbmNoIHVubGVzcyBicmFuY2hNZXJnZT8ubGVuZ3RoID4gMTFcbiAgICByZXR1cm4gc2hvcnRCcmFuY2ggdW5sZXNzIGJyYW5jaE1lcmdlLmluZGV4T2YoJ3JlZnMvaGVhZHMvJykgaXMgMFxuXG4gICAgYnJhbmNoTWVyZ2Uuc3Vic3RyaW5nKDExKVxuXG4gICMgSW50ZXJuYWxcbiAgcmVtb3RlQnJhbmNoTmFtZTogLT5cbiAgICBpZiBAcmVtb3RlTmFtZSgpP1xuICAgICAgQGVuY29kZVNlZ21lbnRzKEBicmFuY2hOYW1lKCkpXG4gICAgZWxzZVxuICAgICAgJ21hc3RlcidcbiJdfQ==
