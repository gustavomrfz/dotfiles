(function() {
  var BufferedProcess, CompositeDisposable, lint, ref;

  ref = require('atom'), BufferedProcess = ref.BufferedProcess, CompositeDisposable = ref.CompositeDisposable;

  lint = function(editor, command, options) {
    var file, helpers, regex;
    helpers = require('atom-linter');
    regex = '(?<file>.+):(?<line>\\d+)\\s-\\s(?<message>.+)';
    file = editor.getPath();
    return new Promise(function(resolve, reject) {
      var args, stdout;
      stdout = '';
      args = ['--without-color', '--silent', options, file].filter(Boolean);
      return new BufferedProcess({
        command: command,
        args: args,
        stdout: function(data) {
          return stdout += data;
        },
        exit: function() {
          var warnings;
          warnings = helpers.parse(stdout, regex).map(function(message) {
            message.type = 'warning';
            return message;
          });
          return resolve(warnings);
        }
      });
    });
  };

  module.exports = {
    config: {
      executablePath: {
        title: 'rails_best_practices Executable Path',
        description: 'The path to `rails_best_practices` executable',
        type: 'string',
        "default": 'rails_best_practices'
      },
      extraOptions: {
        title: 'Extra Options',
        description: 'Options for `rails_best_practices` command',
        type: 'string',
        "default": ''
      }
    },
    activate: function(state) {
      var linterName;
      linterName = 'linter-rails-best-practices';
      this.subscriptions = new CompositeDisposable;
      this.subscriptions.add(atom.config.observe(linterName + ".executablePath", (function(_this) {
        return function(executablePath) {
          return _this.executablePath = executablePath;
        };
      })(this)));
      return this.subscriptions.add(atom.config.observe(linterName + ".extraOptions", (function(_this) {
        return function(extraOptions) {
          return _this.extraOptions = extraOptions;
        };
      })(this)));
    },
    deactivate: function() {
      return this.subscriptions.dispose();
    },
    provideLinter: function() {
      var provider;
      return provider = {
        grammarScopes: ['source.ruby', 'source.ruby.rails', 'source.ruby.rabl', 'text.html.ruby', 'text.html.erb', 'text.haml', 'text.slim'],
        scope: 'file',
        lintOnFly: true,
        lint: (function(_this) {
          return function(editor) {
            return lint(editor, _this.executablePath, _this.extraOptions);
          };
        })(this)
      };
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9saW50ZXItcmFpbHMtYmVzdC1wcmFjdGljZXMvbGliL2xpbnRlci1yYWlscy1iZXN0LXByYWN0aWNlcy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLE1BQXlDLE9BQUEsQ0FBUSxNQUFSLENBQXpDLEVBQUMscUNBQUQsRUFBa0I7O0VBRWxCLElBQUEsR0FBTyxTQUFDLE1BQUQsRUFBUyxPQUFULEVBQWtCLE9BQWxCO0FBQ0wsUUFBQTtJQUFBLE9BQUEsR0FBVSxPQUFBLENBQVEsYUFBUjtJQUNWLEtBQUEsR0FBUTtJQUNSLElBQUEsR0FBTyxNQUFNLENBQUMsT0FBUCxDQUFBO1dBRUgsSUFBQSxPQUFBLENBQVEsU0FBQyxPQUFELEVBQVUsTUFBVjtBQUNWLFVBQUE7TUFBQSxNQUFBLEdBQVM7TUFDVCxJQUFBLEdBQU8sQ0FBQyxpQkFBRCxFQUFvQixVQUFwQixFQUFnQyxPQUFoQyxFQUF5QyxJQUF6QyxDQUE4QyxDQUFDLE1BQS9DLENBQXNELE9BQXREO2FBRUgsSUFBQSxlQUFBLENBQ0Y7UUFBQSxPQUFBLEVBQVMsT0FBVDtRQUNBLElBQUEsRUFBTSxJQUROO1FBRUEsTUFBQSxFQUFRLFNBQUMsSUFBRDtpQkFBVSxNQUFBLElBQVU7UUFBcEIsQ0FGUjtRQUdBLElBQUEsRUFBTSxTQUFBO0FBQ0osY0FBQTtVQUFBLFFBQUEsR0FBVyxPQUFPLENBQUMsS0FBUixDQUFjLE1BQWQsRUFBc0IsS0FBdEIsQ0FBNEIsQ0FBQyxHQUE3QixDQUFpQyxTQUFDLE9BQUQ7WUFDMUMsT0FBTyxDQUFDLElBQVIsR0FBZTttQkFDZjtVQUYwQyxDQUFqQztpQkFHWCxPQUFBLENBQVEsUUFBUjtRQUpJLENBSE47T0FERTtJQUpNLENBQVI7RUFMQzs7RUFtQlAsTUFBTSxDQUFDLE9BQVAsR0FDRTtJQUFBLE1BQUEsRUFDRTtNQUFBLGNBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxzQ0FBUDtRQUNBLFdBQUEsRUFBYSwrQ0FEYjtRQUVBLElBQUEsRUFBTSxRQUZOO1FBR0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxzQkFIVDtPQURGO01BS0EsWUFBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLGVBQVA7UUFDQSxXQUFBLEVBQWEsNENBRGI7UUFFQSxJQUFBLEVBQU0sUUFGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsRUFIVDtPQU5GO0tBREY7SUFZQSxRQUFBLEVBQVUsU0FBQyxLQUFEO0FBQ1IsVUFBQTtNQUFBLFVBQUEsR0FBYTtNQUViLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7TUFFckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBWixDQUF1QixVQUFELEdBQVksaUJBQWxDLEVBQ2pCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxjQUFEO2lCQUFvQixLQUFDLENBQUEsY0FBRCxHQUFrQjtRQUF0QztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FEaUIsQ0FBbkI7YUFHQSxJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFaLENBQXVCLFVBQUQsR0FBWSxlQUFsQyxFQUNqQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsWUFBRDtpQkFBa0IsS0FBQyxDQUFBLFlBQUQsR0FBZ0I7UUFBbEM7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBRGlCLENBQW5CO0lBUlEsQ0FaVjtJQXVCQSxVQUFBLEVBQVksU0FBQTthQUNWLElBQUMsQ0FBQSxhQUFhLENBQUMsT0FBZixDQUFBO0lBRFUsQ0F2Qlo7SUEwQkEsYUFBQSxFQUFlLFNBQUE7QUFDYixVQUFBO2FBQUEsUUFBQSxHQUNFO1FBQUEsYUFBQSxFQUFlLENBQ2IsYUFEYSxFQUViLG1CQUZhLEVBR2Isa0JBSGEsRUFJYixnQkFKYSxFQUtiLGVBTGEsRUFNYixXQU5hLEVBT2IsV0FQYSxDQUFmO1FBU0EsS0FBQSxFQUFPLE1BVFA7UUFVQSxTQUFBLEVBQVcsSUFWWDtRQVdBLElBQUEsRUFBTSxDQUFBLFNBQUEsS0FBQTtpQkFBQSxTQUFDLE1BQUQ7bUJBQVksSUFBQSxDQUFLLE1BQUwsRUFBYSxLQUFDLENBQUEsY0FBZCxFQUE4QixLQUFDLENBQUEsWUFBL0I7VUFBWjtRQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FYTjs7SUFGVyxDQTFCZjs7QUF0QkYiLCJzb3VyY2VzQ29udGVudCI6WyJ7QnVmZmVyZWRQcm9jZXNzLCBDb21wb3NpdGVEaXNwb3NhYmxlfSA9IHJlcXVpcmUgJ2F0b20nXG5cbmxpbnQgPSAoZWRpdG9yLCBjb21tYW5kLCBvcHRpb25zKSAtPlxuICBoZWxwZXJzID0gcmVxdWlyZSgnYXRvbS1saW50ZXInKVxuICByZWdleCA9ICcoPzxmaWxlPi4rKTooPzxsaW5lPlxcXFxkKylcXFxccy1cXFxccyg/PG1lc3NhZ2U+LispJ1xuICBmaWxlID0gZWRpdG9yLmdldFBhdGgoKVxuXG4gIG5ldyBQcm9taXNlIChyZXNvbHZlLCByZWplY3QpIC0+XG4gICAgc3Rkb3V0ID0gJydcbiAgICBhcmdzID0gWyctLXdpdGhvdXQtY29sb3InLCAnLS1zaWxlbnQnLCBvcHRpb25zLCBmaWxlXS5maWx0ZXIoQm9vbGVhbilcblxuICAgIG5ldyBCdWZmZXJlZFByb2Nlc3NcbiAgICAgIGNvbW1hbmQ6IGNvbW1hbmRcbiAgICAgIGFyZ3M6IGFyZ3NcbiAgICAgIHN0ZG91dDogKGRhdGEpIC0+IHN0ZG91dCArPSBkYXRhXG4gICAgICBleGl0OiAtPlxuICAgICAgICB3YXJuaW5ncyA9IGhlbHBlcnMucGFyc2Uoc3Rkb3V0LCByZWdleCkubWFwIChtZXNzYWdlKSAtPlxuICAgICAgICAgIG1lc3NhZ2UudHlwZSA9ICd3YXJuaW5nJ1xuICAgICAgICAgIG1lc3NhZ2VcbiAgICAgICAgcmVzb2x2ZSB3YXJuaW5nc1xuXG5tb2R1bGUuZXhwb3J0cyA9XG4gIGNvbmZpZzpcbiAgICBleGVjdXRhYmxlUGF0aDpcbiAgICAgIHRpdGxlOiAncmFpbHNfYmVzdF9wcmFjdGljZXMgRXhlY3V0YWJsZSBQYXRoJ1xuICAgICAgZGVzY3JpcHRpb246ICdUaGUgcGF0aCB0byBgcmFpbHNfYmVzdF9wcmFjdGljZXNgIGV4ZWN1dGFibGUnXG4gICAgICB0eXBlOiAnc3RyaW5nJ1xuICAgICAgZGVmYXVsdDogJ3JhaWxzX2Jlc3RfcHJhY3RpY2VzJ1xuICAgIGV4dHJhT3B0aW9uczpcbiAgICAgIHRpdGxlOiAnRXh0cmEgT3B0aW9ucydcbiAgICAgIGRlc2NyaXB0aW9uOiAnT3B0aW9ucyBmb3IgYHJhaWxzX2Jlc3RfcHJhY3RpY2VzYCBjb21tYW5kJ1xuICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgIGRlZmF1bHQ6ICcnXG5cbiAgYWN0aXZhdGU6IChzdGF0ZSkgLT5cbiAgICBsaW50ZXJOYW1lID0gJ2xpbnRlci1yYWlscy1iZXN0LXByYWN0aWNlcydcblxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbmZpZy5vYnNlcnZlIFwiI3tsaW50ZXJOYW1lfS5leGVjdXRhYmxlUGF0aFwiLFxuICAgICAgKGV4ZWN1dGFibGVQYXRoKSA9PiBAZXhlY3V0YWJsZVBhdGggPSBleGVjdXRhYmxlUGF0aFxuXG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29uZmlnLm9ic2VydmUgXCIje2xpbnRlck5hbWV9LmV4dHJhT3B0aW9uc1wiLFxuICAgICAgKGV4dHJhT3B0aW9ucykgPT4gQGV4dHJhT3B0aW9ucyA9IGV4dHJhT3B0aW9uc1xuXG4gIGRlYWN0aXZhdGU6IC0+XG4gICAgQHN1YnNjcmlwdGlvbnMuZGlzcG9zZSgpXG5cbiAgcHJvdmlkZUxpbnRlcjogLT5cbiAgICBwcm92aWRlciA9XG4gICAgICBncmFtbWFyU2NvcGVzOiBbXG4gICAgICAgICdzb3VyY2UucnVieScsXG4gICAgICAgICdzb3VyY2UucnVieS5yYWlscycsXG4gICAgICAgICdzb3VyY2UucnVieS5yYWJsJyxcbiAgICAgICAgJ3RleHQuaHRtbC5ydWJ5JyxcbiAgICAgICAgJ3RleHQuaHRtbC5lcmInLFxuICAgICAgICAndGV4dC5oYW1sJyxcbiAgICAgICAgJ3RleHQuc2xpbSdcbiAgICAgIF1cbiAgICAgIHNjb3BlOiAnZmlsZSdcbiAgICAgIGxpbnRPbkZseTogdHJ1ZVxuICAgICAgbGludDogKGVkaXRvcikgPT4gbGludCBlZGl0b3IsIEBleGVjdXRhYmxlUGF0aCwgQGV4dHJhT3B0aW9uc1xuIl19
