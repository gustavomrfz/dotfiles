(function() {
  var BitbucketFile, getActivePath, getSelectedRange;

  BitbucketFile = require('./bitbucket-file');

  module.exports = {
    config: {
      includeLineNumbersInUrls: {
        "default": true,
        type: 'boolean',
        description: 'Include the line range selected in the editor when opening or copying URLs to the clipboard. When opened in the browser, the Bitbucket page will automatically scroll to the selected line range.'
      }
    },
    activate: function() {
      return atom.commands.add('atom-pane', {
        'open-on-bitbucket:file': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).open(getSelectedRange());
          }
        },
        'open-on-bitbucket:file-on-master': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).openOnMaster(getSelectedRange());
          }
        },
        'open-on-bitbucket:blame': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).blame(getSelectedRange());
          }
        },
        'open-on-bitbucket:history': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).history();
          }
        },
        'open-on-bitbucket:issues': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).openIssues();
          }
        },
        'open-on-bitbucket:copy-url': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).copyUrl(getSelectedRange());
          }
        },
        'open-on-bitbucket:branch-compare': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).openBranchCompare();
          }
        },
        'open-on-bitbucket:repository': function() {
          var itemPath;
          if (itemPath = getActivePath()) {
            return BitbucketFile.fromPath(itemPath).openRepository();
          }
        }
      });
    }
  };

  getActivePath = function() {
    var ref;
    return (ref = atom.workspace.getActivePaneItem()) != null ? typeof ref.getPath === "function" ? ref.getPath() : void 0 : void 0;
  };

  getSelectedRange = function() {
    var ref;
    return (ref = atom.workspace.getActivePaneItem()) != null ? typeof ref.getSelectedBufferRange === "function" ? ref.getSelectedBufferRange() : void 0 : void 0;
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9vcGVuLW9uLWJpdGJ1Y2tldC9saWIvbWFpbi5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQSxNQUFBOztFQUFBLGFBQUEsR0FBaUIsT0FBQSxDQUFRLGtCQUFSOztFQUVqQixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsTUFBQSxFQUNFO01BQUEsd0JBQUEsRUFDRTtRQUFBLENBQUEsT0FBQSxDQUFBLEVBQVMsSUFBVDtRQUNBLElBQUEsRUFBTSxTQUROO1FBRUEsV0FBQSxFQUFhLG1NQUZiO09BREY7S0FERjtJQU1BLFFBQUEsRUFBVSxTQUFBO2FBQ1IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLFdBQWxCLEVBQ0U7UUFBQSx3QkFBQSxFQUEwQixTQUFBO0FBQ3hCLGNBQUE7VUFBQSxJQUFHLFFBQUEsR0FBVyxhQUFBLENBQUEsQ0FBZDttQkFDRSxhQUFhLENBQUMsUUFBZCxDQUF1QixRQUF2QixDQUFnQyxDQUFDLElBQWpDLENBQXNDLGdCQUFBLENBQUEsQ0FBdEMsRUFERjs7UUFEd0IsQ0FBMUI7UUFJQSxrQ0FBQSxFQUFvQyxTQUFBO0FBQ2xDLGNBQUE7VUFBQSxJQUFHLFFBQUEsR0FBVyxhQUFBLENBQUEsQ0FBZDttQkFDRSxhQUFhLENBQUMsUUFBZCxDQUF1QixRQUF2QixDQUFnQyxDQUFDLFlBQWpDLENBQThDLGdCQUFBLENBQUEsQ0FBOUMsRUFERjs7UUFEa0MsQ0FKcEM7UUFRQSx5QkFBQSxFQUEyQixTQUFBO0FBQ3pCLGNBQUE7VUFBQSxJQUFHLFFBQUEsR0FBVyxhQUFBLENBQUEsQ0FBZDttQkFDRSxhQUFhLENBQUMsUUFBZCxDQUF1QixRQUF2QixDQUFnQyxDQUFDLEtBQWpDLENBQXVDLGdCQUFBLENBQUEsQ0FBdkMsRUFERjs7UUFEeUIsQ0FSM0I7UUFZQSwyQkFBQSxFQUE2QixTQUFBO0FBQzNCLGNBQUE7VUFBQSxJQUFHLFFBQUEsR0FBVyxhQUFBLENBQUEsQ0FBZDttQkFDRSxhQUFhLENBQUMsUUFBZCxDQUF1QixRQUF2QixDQUFnQyxDQUFDLE9BQWpDLENBQUEsRUFERjs7UUFEMkIsQ0FaN0I7UUFnQkEsMEJBQUEsRUFBNEIsU0FBQTtBQUMxQixjQUFBO1VBQUEsSUFBRyxRQUFBLEdBQVcsYUFBQSxDQUFBLENBQWQ7bUJBQ0UsYUFBYSxDQUFDLFFBQWQsQ0FBdUIsUUFBdkIsQ0FBZ0MsQ0FBQyxVQUFqQyxDQUFBLEVBREY7O1FBRDBCLENBaEI1QjtRQW9CQSw0QkFBQSxFQUE4QixTQUFBO0FBQzVCLGNBQUE7VUFBQSxJQUFHLFFBQUEsR0FBVyxhQUFBLENBQUEsQ0FBZDttQkFDRSxhQUFhLENBQUMsUUFBZCxDQUF1QixRQUF2QixDQUFnQyxDQUFDLE9BQWpDLENBQXlDLGdCQUFBLENBQUEsQ0FBekMsRUFERjs7UUFENEIsQ0FwQjlCO1FBd0JBLGtDQUFBLEVBQW9DLFNBQUE7QUFDbEMsY0FBQTtVQUFBLElBQUcsUUFBQSxHQUFXLGFBQUEsQ0FBQSxDQUFkO21CQUNFLGFBQWEsQ0FBQyxRQUFkLENBQXVCLFFBQXZCLENBQWdDLENBQUMsaUJBQWpDLENBQUEsRUFERjs7UUFEa0MsQ0F4QnBDO1FBNEJBLDhCQUFBLEVBQWdDLFNBQUE7QUFDOUIsY0FBQTtVQUFBLElBQUcsUUFBQSxHQUFXLGFBQUEsQ0FBQSxDQUFkO21CQUNFLGFBQWEsQ0FBQyxRQUFkLENBQXVCLFFBQXZCLENBQWdDLENBQUMsY0FBakMsQ0FBQSxFQURGOztRQUQ4QixDQTVCaEM7T0FERjtJQURRLENBTlY7OztFQXdDRixhQUFBLEdBQWdCLFNBQUE7QUFDZCxRQUFBO3VHQUFrQyxDQUFFO0VBRHRCOztFQUdoQixnQkFBQSxHQUFtQixTQUFBO0FBQ2pCLFFBQUE7c0hBQWtDLENBQUU7RUFEbkI7QUE5Q25CIiwic291cmNlc0NvbnRlbnQiOlsiQml0YnVja2V0RmlsZSAgPSByZXF1aXJlICcuL2JpdGJ1Y2tldC1maWxlJ1xuXG5tb2R1bGUuZXhwb3J0cyA9XG4gIGNvbmZpZzpcbiAgICBpbmNsdWRlTGluZU51bWJlcnNJblVybHM6XG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgICB0eXBlOiAnYm9vbGVhbidcbiAgICAgIGRlc2NyaXB0aW9uOiAnSW5jbHVkZSB0aGUgbGluZSByYW5nZSBzZWxlY3RlZCBpbiB0aGUgZWRpdG9yIHdoZW4gb3BlbmluZyBvciBjb3B5aW5nIFVSTHMgdG8gdGhlIGNsaXBib2FyZC4gV2hlbiBvcGVuZWQgaW4gdGhlIGJyb3dzZXIsIHRoZSBCaXRidWNrZXQgcGFnZSB3aWxsIGF1dG9tYXRpY2FsbHkgc2Nyb2xsIHRvIHRoZSBzZWxlY3RlZCBsaW5lIHJhbmdlLidcblxuICBhY3RpdmF0ZTogLT5cbiAgICBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS1wYW5lJyxcbiAgICAgICdvcGVuLW9uLWJpdGJ1Y2tldDpmaWxlJzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5vcGVuKGdldFNlbGVjdGVkUmFuZ2UoKSlcblxuICAgICAgJ29wZW4tb24tYml0YnVja2V0OmZpbGUtb24tbWFzdGVyJzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5vcGVuT25NYXN0ZXIoZ2V0U2VsZWN0ZWRSYW5nZSgpKVxuXG4gICAgICAnb3Blbi1vbi1iaXRidWNrZXQ6YmxhbWUnOiAtPlxuICAgICAgICBpZiBpdGVtUGF0aCA9IGdldEFjdGl2ZVBhdGgoKVxuICAgICAgICAgIEJpdGJ1Y2tldEZpbGUuZnJvbVBhdGgoaXRlbVBhdGgpLmJsYW1lKGdldFNlbGVjdGVkUmFuZ2UoKSlcblxuICAgICAgJ29wZW4tb24tYml0YnVja2V0Omhpc3RvcnknOiAtPlxuICAgICAgICBpZiBpdGVtUGF0aCA9IGdldEFjdGl2ZVBhdGgoKVxuICAgICAgICAgIEJpdGJ1Y2tldEZpbGUuZnJvbVBhdGgoaXRlbVBhdGgpLmhpc3RvcnkoKVxuXG4gICAgICAnb3Blbi1vbi1iaXRidWNrZXQ6aXNzdWVzJzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5vcGVuSXNzdWVzKClcblxuICAgICAgJ29wZW4tb24tYml0YnVja2V0OmNvcHktdXJsJzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5jb3B5VXJsKGdldFNlbGVjdGVkUmFuZ2UoKSlcblxuICAgICAgJ29wZW4tb24tYml0YnVja2V0OmJyYW5jaC1jb21wYXJlJzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5vcGVuQnJhbmNoQ29tcGFyZSgpXG5cbiAgICAgICdvcGVuLW9uLWJpdGJ1Y2tldDpyZXBvc2l0b3J5JzogLT5cbiAgICAgICAgaWYgaXRlbVBhdGggPSBnZXRBY3RpdmVQYXRoKClcbiAgICAgICAgICBCaXRidWNrZXRGaWxlLmZyb21QYXRoKGl0ZW1QYXRoKS5vcGVuUmVwb3NpdG9yeSgpXG5cbmdldEFjdGl2ZVBhdGggPSAtPlxuICBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVQYW5lSXRlbSgpPy5nZXRQYXRoPygpXG5cbmdldFNlbGVjdGVkUmFuZ2UgPSAtPlxuICBhdG9tLndvcmtzcGFjZS5nZXRBY3RpdmVQYW5lSXRlbSgpPy5nZXRTZWxlY3RlZEJ1ZmZlclJhbmdlPygpXG4iXX0=
