(function() {
  var CompositeDisposable, ERB_CLOSER_REGEX, ERB_OPENER_REGEX, ERB_REGEX, Range;

  Range = require('atom').Range;

  CompositeDisposable = require('atom').CompositeDisposable;

  ERB_REGEX = '<%(=?|-?|#?)\s{2}(-?)%>';

  ERB_OPENER_REGEX = '<%[\\=\\#\\-]?';

  ERB_CLOSER_REGEX = "-?%>";

  module.exports = {
    config: {
      erbBlocks: {
        type: 'array',
        "default": [['<%=', '%>'], ['<%', '%>'], ['<%#', '%>']],
        items: {
          type: 'array'
        }
      }
    },
    activate: function() {
      this.subscriptions = new CompositeDisposable;
      return this.subscriptions.add(atom.commands.add('atom-workspace', 'rails-snippets:toggleErb', (function(_this) {
        return function() {
          return _this.toggleErb();
        };
      })(this)));
    },
    toggleErb: function() {
      var delegate, editor, hasTextSelected, j, len, ref, results, selectedText, selection;
      editor = atom.workspace.getActiveTextEditor();
      ref = editor.getSelections();
      results = [];
      for (j = 0, len = ref.length; j < len; j += 1) {
        selection = ref[j];
        hasTextSelected = !selection.isEmpty();
        selectedText = selection.getText();
        delegate = this;
        results.push(editor.transact(function() {
          var closer, currentCursor, opener, ref1, textToRestoreRange;
          selection.deleteSelectedText();
          currentCursor = selection.cursor;
          ref1 = delegate.findSorroundingBlocks(editor, currentCursor), opener = ref1[0], closer = ref1[1];
          if ((opener != null) && (closer != null)) {
            delegate.replaceErbBlock(editor, opener, closer, currentCursor);
          } else {
            delegate.insertErbBlock(editor, currentCursor);
          }
          if (hasTextSelected) {
            textToRestoreRange = editor.getBuffer().insert(currentCursor.getBufferPosition(), selectedText);
            return selection.setBufferRange(textToRestoreRange);
          }
        }));
      }
      return results;
    },
    findSorroundingBlocks: function(editor, currentCursor) {
      var closer, containingLine, foundClosers, foundOpeners, leftRange, opener, rightRange;
      opener = closer = null;
      containingLine = currentCursor.getCurrentLineBufferRange();
      leftRange = new Range(containingLine.start, currentCursor.getBufferPosition());
      rightRange = new Range(currentCursor.getBufferPosition(), containingLine.end);
      foundOpeners = [];
      editor.getBuffer().scanInRange(new RegExp(ERB_OPENER_REGEX, 'g'), leftRange, function(result) {
        return foundOpeners.push(result.range);
      });
      if (foundOpeners) {
        opener = foundOpeners[foundOpeners.length - 1];
      }
      foundClosers = [];
      editor.getBuffer().scanInRange(new RegExp(ERB_CLOSER_REGEX, 'g'), rightRange, function(result) {
        return foundClosers.push(result.range);
      });
      if (foundClosers) {
        closer = foundClosers[0];
      }
      return [opener, closer];
    },
    insertErbBlock: function(editor, currentCursor) {
      var closingBlock, defaultBlock, desiredPosition, openingTag;
      defaultBlock = atom.config.get('rails-snippets.erbBlocks')[0];
      desiredPosition = null;
      openingTag = editor.getBuffer().insert(currentCursor.getBufferPosition(), defaultBlock[0] + ' ');
      desiredPosition = currentCursor.getBufferPosition();
      closingBlock = editor.getBuffer().insert(currentCursor.getBufferPosition(), ' ' + defaultBlock[1]);
      return currentCursor.setBufferPosition(desiredPosition);
    },
    replaceErbBlock: function(editor, opener, closer, currentCursor) {
      var closingBracket, nextBlock, openingBracket;
      openingBracket = editor.getBuffer().getTextInRange(opener);
      closingBracket = editor.getBuffer().getTextInRange(closer);
      nextBlock = this.getNextErbBlock(editor, openingBracket, closingBracket);
      editor.getBuffer().setTextInRange(closer, nextBlock[1]);
      return editor.getBuffer().setTextInRange(opener, nextBlock[0]);
    },
    getNextErbBlock: function(editor, openingBracket, closingBracket) {
      var block, i, j, len, ref;
      ref = atom.config.get('rails-snippets.erbBlocks');
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        block = ref[i];
        if (JSON.stringify([openingBracket, closingBracket]) === JSON.stringify(block)) {
          if ((i + 1) >= atom.config.get('rails-snippets.erbBlocks').length) {
            return atom.config.get('rails-snippets.erbBlocks')[0];
          } else {
            return atom.config.get('rails-snippets.erbBlocks')[i + 1];
          }
        }
      }
      return atom.config.get('rails-snippets.erbBlocks')[0];
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9yYWlscy1zbmlwcGV0cy9saWIvcmFpbHMtc25pcHBldHMuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0FBQUEsTUFBQTs7RUFBQyxRQUFTLE9BQUEsQ0FBUSxNQUFSOztFQUNULHNCQUF1QixPQUFBLENBQVEsTUFBUjs7RUFFeEIsU0FBQSxHQUFZOztFQUVaLGdCQUFBLEdBQW1COztFQUVuQixnQkFBQSxHQUFtQjs7RUFFbkIsTUFBTSxDQUFDLE9BQVAsR0FDRTtJQUFBLE1BQUEsRUFDRTtNQUFBLFNBQUEsRUFDRTtRQUFBLElBQUEsRUFBTSxPQUFOO1FBQ0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxDQUFDLENBQUMsS0FBRCxFQUFRLElBQVIsQ0FBRCxFQUFnQixDQUFDLElBQUQsRUFBTyxJQUFQLENBQWhCLEVBQThCLENBQUMsS0FBRCxFQUFRLElBQVIsQ0FBOUIsQ0FEVDtRQUVBLEtBQUEsRUFDRTtVQUFBLElBQUEsRUFBTSxPQUFOO1NBSEY7T0FERjtLQURGO0lBT0EsUUFBQSxFQUFVLFNBQUE7TUFDUixJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJO2FBQ3JCLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLDBCQUFwQyxFQUFnRSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLFNBQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFoRSxDQUFuQjtJQUZRLENBUFY7SUFXQSxTQUFBLEVBQVcsU0FBQTtBQUNULFVBQUE7TUFBQSxNQUFBLEdBQVMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBZixDQUFBO0FBQ1Q7QUFBQTtXQUFBLHdDQUFBOztRQUNFLGVBQUEsR0FBa0IsQ0FBQyxTQUFTLENBQUMsT0FBVixDQUFBO1FBQ25CLFlBQUEsR0FBZSxTQUFTLENBQUMsT0FBVixDQUFBO1FBQ2YsUUFBQSxHQUFXO3FCQUVYLE1BQU0sQ0FBQyxRQUFQLENBQWdCLFNBQUE7QUFDZCxjQUFBO1VBQUEsU0FBUyxDQUFDLGtCQUFWLENBQUE7VUFDQSxhQUFBLEdBQWdCLFNBQVMsQ0FBQztVQUUxQixPQUFtQixRQUFRLENBQUMscUJBQVQsQ0FBK0IsTUFBL0IsRUFBdUMsYUFBdkMsQ0FBbkIsRUFBQyxnQkFBRCxFQUFTO1VBQ1QsSUFBRyxnQkFBQSxJQUFZLGdCQUFmO1lBRUUsUUFBUSxDQUFDLGVBQVQsQ0FBeUIsTUFBekIsRUFBaUMsTUFBakMsRUFBeUMsTUFBekMsRUFBaUQsYUFBakQsRUFGRjtXQUFBLE1BQUE7WUFLRSxRQUFRLENBQUMsY0FBVCxDQUF3QixNQUF4QixFQUFnQyxhQUFoQyxFQUxGOztVQU9BLElBQUcsZUFBSDtZQUNFLGtCQUFBLEdBQXFCLE1BQU0sQ0FBQyxTQUFQLENBQUEsQ0FBa0IsQ0FBQyxNQUFuQixDQUEwQixhQUFhLENBQUMsaUJBQWQsQ0FBQSxDQUExQixFQUE2RCxZQUE3RDttQkFDckIsU0FBUyxDQUFDLGNBQVYsQ0FBeUIsa0JBQXpCLEVBRkY7O1FBWmMsQ0FBaEI7QUFMRjs7SUFGUyxDQVhYO0lBbUNBLHFCQUFBLEVBQXVCLFNBQUMsTUFBRCxFQUFTLGFBQVQ7QUFDckIsVUFBQTtNQUFBLE1BQUEsR0FBUyxNQUFBLEdBQVM7TUFFbEIsY0FBQSxHQUFpQixhQUFhLENBQUMseUJBQWQsQ0FBQTtNQUdqQixTQUFBLEdBQWlCLElBQUEsS0FBQSxDQUFNLGNBQWMsQ0FBQyxLQUFyQixFQUE0QixhQUFhLENBQUMsaUJBQWQsQ0FBQSxDQUE1QjtNQUNqQixVQUFBLEdBQWlCLElBQUEsS0FBQSxDQUFNLGFBQWEsQ0FBQyxpQkFBZCxDQUFBLENBQU4sRUFBeUMsY0FBYyxDQUFDLEdBQXhEO01BR2pCLFlBQUEsR0FBZTtNQUNmLE1BQU0sQ0FBQyxTQUFQLENBQUEsQ0FBa0IsQ0FBQyxXQUFuQixDQUFtQyxJQUFBLE1BQUEsQ0FBTyxnQkFBUCxFQUF5QixHQUF6QixDQUFuQyxFQUFrRSxTQUFsRSxFQUE2RSxTQUFDLE1BQUQ7ZUFDM0UsWUFBWSxDQUFDLElBQWIsQ0FBa0IsTUFBTSxDQUFDLEtBQXpCO01BRDJFLENBQTdFO01BR0EsSUFBa0QsWUFBbEQ7UUFBQSxNQUFBLEdBQVMsWUFBYSxDQUFBLFlBQVksQ0FBQyxNQUFiLEdBQXNCLENBQXRCLEVBQXRCOztNQUdBLFlBQUEsR0FBZTtNQUNmLE1BQU0sQ0FBQyxTQUFQLENBQUEsQ0FBa0IsQ0FBQyxXQUFuQixDQUFtQyxJQUFBLE1BQUEsQ0FBTyxnQkFBUCxFQUF5QixHQUF6QixDQUFuQyxFQUFrRSxVQUFsRSxFQUE4RSxTQUFDLE1BQUQ7ZUFDNUUsWUFBWSxDQUFDLElBQWIsQ0FBa0IsTUFBTSxDQUFDLEtBQXpCO01BRDRFLENBQTlFO01BR0EsSUFBNEIsWUFBNUI7UUFBQSxNQUFBLEdBQVMsWUFBYSxDQUFBLENBQUEsRUFBdEI7O0FBQ0EsYUFBTyxDQUFDLE1BQUQsRUFBUyxNQUFUO0lBdEJjLENBbkN2QjtJQTJEQSxjQUFBLEVBQWdCLFNBQUMsTUFBRCxFQUFTLGFBQVQ7QUFFZCxVQUFBO01BQUEsWUFBQSxHQUFlLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFnQiwwQkFBaEIsQ0FBNEMsQ0FBQSxDQUFBO01BQzNELGVBQUEsR0FBa0I7TUFFbEIsVUFBQSxHQUFhLE1BQU0sQ0FBQyxTQUFQLENBQUEsQ0FBa0IsQ0FBQyxNQUFuQixDQUEwQixhQUFhLENBQUMsaUJBQWQsQ0FBQSxDQUExQixFQUE2RCxZQUFhLENBQUEsQ0FBQSxDQUFiLEdBQWtCLEdBQS9FO01BRWIsZUFBQSxHQUFrQixhQUFhLENBQUMsaUJBQWQsQ0FBQTtNQUVsQixZQUFBLEdBQWUsTUFBTSxDQUFDLFNBQVAsQ0FBQSxDQUFrQixDQUFDLE1BQW5CLENBQTBCLGFBQWEsQ0FBQyxpQkFBZCxDQUFBLENBQTFCLEVBQTZELEdBQUEsR0FBTSxZQUFhLENBQUEsQ0FBQSxDQUFoRjthQUNmLGFBQWEsQ0FBQyxpQkFBZCxDQUFpQyxlQUFqQztJQVZjLENBM0RoQjtJQXVFQSxlQUFBLEVBQWlCLFNBQUMsTUFBRCxFQUFTLE1BQVQsRUFBaUIsTUFBakIsRUFBeUIsYUFBekI7QUFFZixVQUFBO01BQUEsY0FBQSxHQUFpQixNQUFNLENBQUMsU0FBUCxDQUFBLENBQWtCLENBQUMsY0FBbkIsQ0FBa0MsTUFBbEM7TUFDakIsY0FBQSxHQUFpQixNQUFNLENBQUMsU0FBUCxDQUFBLENBQWtCLENBQUMsY0FBbkIsQ0FBa0MsTUFBbEM7TUFDakIsU0FBQSxHQUFZLElBQUMsQ0FBQSxlQUFELENBQWlCLE1BQWpCLEVBQXlCLGNBQXpCLEVBQXlDLGNBQXpDO01BRVosTUFBTSxDQUFDLFNBQVAsQ0FBQSxDQUFrQixDQUFDLGNBQW5CLENBQWtDLE1BQWxDLEVBQTBDLFNBQVUsQ0FBQSxDQUFBLENBQXBEO2FBQ0EsTUFBTSxDQUFDLFNBQVAsQ0FBQSxDQUFrQixDQUFDLGNBQW5CLENBQWtDLE1BQWxDLEVBQTBDLFNBQVUsQ0FBQSxDQUFBLENBQXBEO0lBUGUsQ0F2RWpCO0lBZ0ZBLGVBQUEsRUFBaUIsU0FBQyxNQUFELEVBQVMsY0FBVCxFQUF5QixjQUF6QjtBQUNmLFVBQUE7QUFBQTtBQUFBLFdBQUEsNkNBQUE7O1FBQ0UsSUFBRyxJQUFJLENBQUMsU0FBTCxDQUFlLENBQUMsY0FBRCxFQUFpQixjQUFqQixDQUFmLENBQUEsS0FBb0QsSUFBSSxDQUFDLFNBQUwsQ0FBZSxLQUFmLENBQXZEO1VBRUUsSUFBRyxDQUFDLENBQUEsR0FBSSxDQUFMLENBQUEsSUFBVyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMEJBQWhCLENBQTJDLENBQUMsTUFBMUQ7QUFDRSxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMEJBQWhCLENBQTRDLENBQUEsQ0FBQSxFQURyRDtXQUFBLE1BQUE7QUFHRSxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBZ0IsMEJBQWhCLENBQTRDLENBQUEsQ0FBQSxHQUFJLENBQUosRUFIckQ7V0FGRjs7QUFERjtBQVNBLGFBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLDBCQUFoQixDQUE0QyxDQUFBLENBQUE7SUFWcEMsQ0FoRmpCOztBQVZGIiwic291cmNlc0NvbnRlbnQiOlsiIyBtdWNoIGxvdmUgdG8gQGVkZG9ycmUgPDMgPDNcbntSYW5nZX0gPSByZXF1aXJlICdhdG9tJ1xue0NvbXBvc2l0ZURpc3Bvc2FibGV9ID0gcmVxdWlyZSAnYXRvbSdcblxuRVJCX1JFR0VYID0gJzwlKD0/fC0/fCM/KVxcc3syfSgtPyklPidcbiMgbWF0Y2hlcyB0aGUgb3BlbmluZyBicmFja2V0XG5FUkJfT1BFTkVSX1JFR0VYID0gJzwlW1xcXFw9XFxcXCNcXFxcLV0/J1xuIyBtYXRjaGVzIHRoZSBjbG9zaW5nIGJyYWNrZXQuXG5FUkJfQ0xPU0VSX1JFR0VYID0gXCItPyU+XCJcblxubW9kdWxlLmV4cG9ydHMgPVxuICBjb25maWc6XG4gICAgZXJiQmxvY2tzOlxuICAgICAgdHlwZTogJ2FycmF5J1xuICAgICAgZGVmYXVsdDogW1snPCU9JywgJyU+J10sIFsnPCUnLCAnJT4nXSwgWyc8JSMnLCAnJT4nXV1cbiAgICAgIGl0ZW1zOlxuICAgICAgICB0eXBlOiAnYXJyYXknXG5cbiAgYWN0aXZhdGU6IC0+XG4gICAgQHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCAnYXRvbS13b3Jrc3BhY2UnLCAncmFpbHMtc25pcHBldHM6dG9nZ2xlRXJiJywgPT4gQHRvZ2dsZUVyYigpXG5cbiAgdG9nZ2xlRXJiOiAtPlxuICAgIGVkaXRvciA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKVxuICAgIGZvciBzZWxlY3Rpb24gaW4gZWRpdG9yLmdldFNlbGVjdGlvbnMoKSBieSAxXG4gICAgICBoYXNUZXh0U2VsZWN0ZWQgPSAhc2VsZWN0aW9uLmlzRW1wdHkoKVxuICAgICAgc2VsZWN0ZWRUZXh0ID0gc2VsZWN0aW9uLmdldFRleHQoKVxuICAgICAgZGVsZWdhdGUgPSBAXG5cbiAgICAgIGVkaXRvci50cmFuc2FjdCAtPlxuICAgICAgICBzZWxlY3Rpb24uZGVsZXRlU2VsZWN0ZWRUZXh0KClcbiAgICAgICAgY3VycmVudEN1cnNvciA9IHNlbGVjdGlvbi5jdXJzb3JcbiAgICAgICAgIyBzZWFyY2hpbmcgZm9yIG9wZW5pbmcgYW5kIGNsb3NpbmcgYnJhY2tldHNcbiAgICAgICAgW29wZW5lciwgY2xvc2VyXSA9IGRlbGVnYXRlLmZpbmRTb3Jyb3VuZGluZ0Jsb2NrcyBlZGl0b3IsIGN1cnJlbnRDdXJzb3JcbiAgICAgICAgaWYgb3BlbmVyPyBhbmQgY2xvc2VyP1xuICAgICAgICAgICMgaWYgYm90aCBicmFja2V0cyBmb3VuZCAtIHJlcGxhY2luZyB0aGVtIHdpdGggdGhlIG5leHQgb25lcy5cbiAgICAgICAgICBkZWxlZ2F0ZS5yZXBsYWNlRXJiQmxvY2sgZWRpdG9yLCBvcGVuZXIsIGNsb3NlciwgY3VycmVudEN1cnNvclxuICAgICAgICBlbHNlXG4gICAgICAgICAgIyBpZiBhbnkgb2YgdGhlIGJyYWNrZXRzIHdlcmUndCBmb3VuZCAtIGluc2VydGluZyBuZXcgb25lcy5cbiAgICAgICAgICBkZWxlZ2F0ZS5pbnNlcnRFcmJCbG9jayBlZGl0b3IsIGN1cnJlbnRDdXJzb3JcblxuICAgICAgICBpZiBoYXNUZXh0U2VsZWN0ZWRcbiAgICAgICAgICB0ZXh0VG9SZXN0b3JlUmFuZ2UgPSBlZGl0b3IuZ2V0QnVmZmVyKCkuaW5zZXJ0IGN1cnJlbnRDdXJzb3IuZ2V0QnVmZmVyUG9zaXRpb24oKSwgc2VsZWN0ZWRUZXh0XG4gICAgICAgICAgc2VsZWN0aW9uLnNldEJ1ZmZlclJhbmdlIHRleHRUb1Jlc3RvcmVSYW5nZVxuXG5cbiAgZmluZFNvcnJvdW5kaW5nQmxvY2tzOiAoZWRpdG9yLCBjdXJyZW50Q3Vyc29yKSAtPlxuICAgIG9wZW5lciA9IGNsb3NlciA9IG51bGxcbiAgICAjIGdyYWJiaW5nIHRoZSB3aG9sZSBsaW5lXG4gICAgY29udGFpbmluZ0xpbmUgPSBjdXJyZW50Q3Vyc29yLmdldEN1cnJlbnRMaW5lQnVmZmVyUmFuZ2UoKVxuXG4gICAgIyBvbmUgcmVnaW9uIHRvIHRoZSBsZWZ0IG9mIHRoZSBjdXJzb3IgYW5kIG9uZSB0byB0aGUgcmlnaHRcbiAgICBsZWZ0UmFuZ2UgID0gbmV3IFJhbmdlIGNvbnRhaW5pbmdMaW5lLnN0YXJ0LCBjdXJyZW50Q3Vyc29yLmdldEJ1ZmZlclBvc2l0aW9uKClcbiAgICByaWdodFJhbmdlID0gbmV3IFJhbmdlIGN1cnJlbnRDdXJzb3IuZ2V0QnVmZmVyUG9zaXRpb24oKSwgY29udGFpbmluZ0xpbmUuZW5kXG5cbiAgICAjIHNlYXJjaGluZyBpbiB0aGUgbGVmdCByYW5nZSBmb3IgYW4gb3BlbmluZyBicmFja2V0XG4gICAgZm91bmRPcGVuZXJzID0gW11cbiAgICBlZGl0b3IuZ2V0QnVmZmVyKCkuc2NhbkluUmFuZ2UgbmV3IFJlZ0V4cChFUkJfT1BFTkVSX1JFR0VYLCAnZycpLCBsZWZ0UmFuZ2UsIChyZXN1bHQpIC0+XG4gICAgICBmb3VuZE9wZW5lcnMucHVzaCByZXN1bHQucmFuZ2VcbiAgICAjIGlmIGZvdW5kLCBzZXR0aW5nIGEgcmFuZ2UgZm9yIGl0LCB1c2luZyB0aGUgbGFzdCBtYXRjaCAtIHRoZSByaWdodG1vc3QgYnJhY2tldCBmb3VuZFxuICAgIG9wZW5lciA9IGZvdW5kT3BlbmVyc1tmb3VuZE9wZW5lcnMubGVuZ3RoIC0gMV0gaWYgZm91bmRPcGVuZXJzXG5cbiAgICAjIHNlYXJjaGluZyBpbiB0aGUgcmlnaHQgcmFuZ2UgZm9yIGFuIG9wZW5pbmcgYnJhY2tldFxuICAgIGZvdW5kQ2xvc2VycyA9IFtdXG4gICAgZWRpdG9yLmdldEJ1ZmZlcigpLnNjYW5JblJhbmdlIG5ldyBSZWdFeHAoRVJCX0NMT1NFUl9SRUdFWCwgJ2cnKSwgcmlnaHRSYW5nZSwgKHJlc3VsdCkgLT5cbiAgICAgIGZvdW5kQ2xvc2Vycy5wdXNoIHJlc3VsdC5yYW5nZVxuICAgICMgaWYgZm91bmQsIHNldHRpbmcgYSBuZXcgcmFuZ2UsIHVzaW5nIHRoZSBmaXJzdCBtYXRjaCAtIHRoZSBsZWZ0bW9zdCBicmFja2V0IGZvdW5kXG4gICAgY2xvc2VyID0gZm91bmRDbG9zZXJzWzBdIGlmIGZvdW5kQ2xvc2Vyc1xuICAgIHJldHVybiBbb3BlbmVyLCBjbG9zZXJdXG5cbiAgaW5zZXJ0RXJiQmxvY2s6IChlZGl0b3IsIGN1cnJlbnRDdXJzb3IpIC0+XG4gICAgIyBpbnNlcnRpbmcgdGhlIGZpcnN0IGJsb2NrIGluIHRoZSBsaXN0XG4gICAgZGVmYXVsdEJsb2NrID0gYXRvbS5jb25maWcuZ2V0KCdyYWlscy1zbmlwcGV0cy5lcmJCbG9ja3MnKVswXVxuICAgIGRlc2lyZWRQb3NpdGlvbiA9IG51bGxcbiAgICAjIGluc2VydGluZyBvcGVuaW5nIGJyYWNrZXRcbiAgICBvcGVuaW5nVGFnID0gZWRpdG9yLmdldEJ1ZmZlcigpLmluc2VydCBjdXJyZW50Q3Vyc29yLmdldEJ1ZmZlclBvc2l0aW9uKCksIGRlZmF1bHRCbG9ja1swXSArICcgJ1xuICAgICMgc3RvcmluZyBwb3NpdGlvbiBiZXR3ZWVuIGJyYWNrZXRzXG4gICAgZGVzaXJlZFBvc2l0aW9uID0gY3VycmVudEN1cnNvci5nZXRCdWZmZXJQb3NpdGlvbigpXG4gICAgIyBpbnNlcnRpbmcgY2xvc2luZyBicmFja2V0XG4gICAgY2xvc2luZ0Jsb2NrID0gZWRpdG9yLmdldEJ1ZmZlcigpLmluc2VydCBjdXJyZW50Q3Vyc29yLmdldEJ1ZmZlclBvc2l0aW9uKCksICcgJyArIGRlZmF1bHRCbG9ja1sxXVxuICAgIGN1cnJlbnRDdXJzb3Iuc2V0QnVmZmVyUG9zaXRpb24oIGRlc2lyZWRQb3NpdGlvbiApXG5cbiAgcmVwbGFjZUVyYkJsb2NrOiAoZWRpdG9yLCBvcGVuZXIsIGNsb3NlciwgY3VycmVudEN1cnNvcikgLT5cbiAgICAjIGdldHRpbmcgdGhlIG5leHQgYmxvY2sgaW4gdGhlIGxpc3RcbiAgICBvcGVuaW5nQnJhY2tldCA9IGVkaXRvci5nZXRCdWZmZXIoKS5nZXRUZXh0SW5SYW5nZSBvcGVuZXJcbiAgICBjbG9zaW5nQnJhY2tldCA9IGVkaXRvci5nZXRCdWZmZXIoKS5nZXRUZXh0SW5SYW5nZSBjbG9zZXJcbiAgICBuZXh0QmxvY2sgPSBAZ2V0TmV4dEVyYkJsb2NrIGVkaXRvciwgb3BlbmluZ0JyYWNrZXQsIGNsb3NpbmdCcmFja2V0XG4gICAgIyByZXBsYWNpbmcgaW4gcmV2ZXJzZSBvcmRlciBiZWNhdXNlIGxpbmUgbGVuZ3RoIG1pZ2h0IGNoYW5nZVxuICAgIGVkaXRvci5nZXRCdWZmZXIoKS5zZXRUZXh0SW5SYW5nZSBjbG9zZXIsIG5leHRCbG9ja1sxXVxuICAgIGVkaXRvci5nZXRCdWZmZXIoKS5zZXRUZXh0SW5SYW5nZSBvcGVuZXIsIG5leHRCbG9ja1swXVxuXG4gIGdldE5leHRFcmJCbG9jazogKGVkaXRvciwgb3BlbmluZ0JyYWNrZXQsIGNsb3NpbmdCcmFja2V0KSAtPlxuICAgIGZvciBibG9jaywgaSBpbiBhdG9tLmNvbmZpZy5nZXQoJ3JhaWxzLXNuaXBwZXRzLmVyYkJsb2NrcycpXG4gICAgICBpZiBKU09OLnN0cmluZ2lmeShbb3BlbmluZ0JyYWNrZXQsIGNsb3NpbmdCcmFja2V0XSkgPT0gSlNPTi5zdHJpbmdpZnkoYmxvY2spXG4gICAgICAgICMgaWYgb3V0c2lkZSBvZiBzY29wZSAtIHJldHVybmluZyB0aGUgZmlyc3QgYmxvY2tcbiAgICAgICAgaWYgKGkgKyAxKSA+PSBhdG9tLmNvbmZpZy5nZXQoJ3JhaWxzLXNuaXBwZXRzLmVyYkJsb2NrcycpLmxlbmd0aFxuICAgICAgICAgIHJldHVybiBhdG9tLmNvbmZpZy5nZXQoJ3JhaWxzLXNuaXBwZXRzLmVyYkJsb2NrcycpWzBdXG4gICAgICAgIGVsc2VcbiAgICAgICAgICByZXR1cm4gYXRvbS5jb25maWcuZ2V0KCdyYWlscy1zbmlwcGV0cy5lcmJCbG9ja3MnKVtpICsgMV1cblxuICAgICMgaW4gY2FzZSB3ZSBoYXZlbid0IGZvdW5kIHRoZSBibG9jayBpbiB0aGUgbGlzdCwgcmV0dXJuaW5nIHRoZSBmaXJzdCBvbmVcbiAgICByZXR1cm4gYXRvbS5jb25maWcuZ2V0KCdyYWlscy1zbmlwcGV0cy5lcmJCbG9ja3MnKVswXVxuIl19
