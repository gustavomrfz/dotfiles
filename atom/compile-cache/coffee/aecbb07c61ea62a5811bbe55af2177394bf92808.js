(function() {
  var $, _, path;

  _ = require('underscore-plus');

  $ = require('atom-space-pen-views').$;

  path = require('path');

  describe("RubyBlock", function() {
    var bottomPanels, editor, editorElement, editorView, getResultDecorations, lineNumbers, markers, ref, rubyBlockElement, workspaceElement;
    ref = [], workspaceElement = ref[0], editor = ref[1], editorView = ref[2], editorElement = ref[3], markers = ref[4], lineNumbers = ref[5], rubyBlockElement = ref[6], bottomPanels = ref[7];
    getResultDecorations = function(editor, clazz, type) {
      var decoration, decorations, i, id, len, markerId, markerIdForDecorations, ref1, resultDecorations;
      if (editor.decorationsStateForScreenRowRange != null) {
        resultDecorations = [];
        ref1 = editor.decorationsStateForScreenRowRange(0, editor.getLineCount());
        for (id in ref1) {
          decoration = ref1[id];
          if (decoration.properties["class"] === clazz && decoration.properties.type === type) {
            resultDecorations.push(decoration);
          }
        }
      } else {
        markerIdForDecorations = editor.decorationsForScreenRowRange(0, editor.getLineCount());
        resultDecorations = [];
        for (markerId in markerIdForDecorations) {
          decorations = markerIdForDecorations[markerId];
          for (i = 0, len = decorations.length; i < len; i++) {
            decoration = decorations[i];
            if (decoration.getProperties()["class"] === clazz && decoration.getProperties.type === type) {
              resultDecorations.push(decoration);
            }
          }
        }
      }
      return resultDecorations;
    };
    beforeEach(function() {
      workspaceElement = atom.views.getView(atom.workspace);
      atom.project.setPaths([path.join(__dirname, 'fixtures')]);
      waitsForPromise(function() {
        return atom.workspace.open('test.rb');
      });
      waitsForPromise(function() {
        return atom.packages.activatePackage('language-ruby');
      });
      return runs(function() {
        var activationPromise;
        jasmine.attachToDOM(workspaceElement);
        editor = atom.workspace.getActiveTextEditor();
        editorView = editor.getElement();
        return activationPromise = atom.packages.activatePackage("ruby-block").then(function(arg) {
          var findView, mainModule;
          mainModule = arg.mainModule;
          mainModule.createViews();
          return findView = mainModule.findView, mainModule;
        });
      });
    });
    describe("when cursor is on the 'end'", function() {
      describe("when highlightLineNumber option is 'true'", function() {
        beforeEach(function() {
          atom.config.set('ruby-block.highlightLineNumber', true);
          spyOn(_._, "now").andCallFake(function() {
            return window.now;
          });
          editor.setCursorBufferPosition([3, 0]);
          advanceClock(100);
          return bottomPanels = atom.workspace.getBottomPanels();
        });
        it('highlights line', function() {
          return expect(getResultDecorations(editor, 'ruby-block-highlight', 'highlight')).toHaveLength(1);
        });
        it('highlights gutter', function() {
          return expect(getResultDecorations(editor, 'ruby-block-highlight', 'line-number')).toHaveLength(1);
        });
        return it('shows view in bottom panel', function() {
          return expect(workspaceElement.querySelector('.ruby-block')).toBe(bottomPanels[0].item);
        });
      });
      return describe("when highlightLineNumber option is 'false'", function() {
        beforeEach(function() {
          atom.config.set('ruby-block.highlightLineNumber', false);
          spyOn(_._, "now").andCallFake(function() {
            return window.now;
          });
          editor.setCursorBufferPosition([3, 0]);
          advanceClock(100);
          return bottomPanels = atom.workspace.getBottomPanels();
        });
        it('highlights line', function() {
          return expect(getResultDecorations(editor, 'ruby-block-highlight', 'highlight')).toHaveLength(1);
        });
        it('highlights gutter', function() {
          return expect(getResultDecorations(editor, 'ruby-block-highlight', 'line-number')).toHaveLength(0);
        });
        return it('shows view in bottom panel', function() {
          return expect(workspaceElement.querySelector('.ruby-block')).toBe(bottomPanels[0].item);
        });
      });
    });
    return describe("when cursor is not on the 'end'", function() {
      beforeEach(function() {
        editor.setCursorBufferPosition([4, 0]);
        return advanceClock(100);
      });
      it('highlights line', function() {
        return expect(getResultDecorations(editor, 'ruby-block-highlight', 'highlight')).toHaveLength(0);
      });
      it('highlights gutter', function() {
        return expect(getResultDecorations(editor, 'ruby-block-highlight', 'line-number')).toHaveLength(0);
      });
      return it('shows view in bottom panel', function() {
        return expect(bottomPanels).toHaveLength(0);
      });
    });
  });

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9ydWJ5LWJsb2NrL3NwZWMvcnVieS1ibG9jay1zcGVjLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsQ0FBQSxHQUFJLE9BQUEsQ0FBUSxpQkFBUjs7RUFDSCxJQUFLLE9BQUEsQ0FBUSxzQkFBUjs7RUFFTixJQUFBLEdBQU8sT0FBQSxDQUFRLE1BQVI7O0VBRVAsUUFBQSxDQUFTLFdBQVQsRUFBc0IsU0FBQTtBQUNwQixRQUFBO0lBQUEsTUFBK0csRUFBL0csRUFBQyx5QkFBRCxFQUFtQixlQUFuQixFQUEyQixtQkFBM0IsRUFBdUMsc0JBQXZDLEVBQXNELGdCQUF0RCxFQUErRCxvQkFBL0QsRUFBNEUseUJBQTVFLEVBQThGO0lBRTlGLG9CQUFBLEdBQXVCLFNBQUMsTUFBRCxFQUFTLEtBQVQsRUFBZ0IsSUFBaEI7QUFDckIsVUFBQTtNQUFBLElBQUcsZ0RBQUg7UUFDRSxpQkFBQSxHQUFvQjtBQUNwQjtBQUFBLGFBQUEsVUFBQTs7VUFDRSxJQUFHLFVBQVUsQ0FBQyxVQUFVLEVBQUMsS0FBRCxFQUFyQixLQUErQixLQUEvQixJQUF5QyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQXRCLEtBQThCLElBQTFFO1lBQ0UsaUJBQWlCLENBQUMsSUFBbEIsQ0FBdUIsVUFBdkIsRUFERjs7QUFERixTQUZGO09BQUEsTUFBQTtRQU1FLHNCQUFBLEdBQXlCLE1BQU0sQ0FBQyw0QkFBUCxDQUFvQyxDQUFwQyxFQUF1QyxNQUFNLENBQUMsWUFBUCxDQUFBLENBQXZDO1FBQ3pCLGlCQUFBLEdBQW9CO0FBQ3BCLGFBQUEsa0NBQUE7O0FBQ0UsZUFBQSw2Q0FBQTs7WUFDRSxJQUFxQyxVQUFVLENBQUMsYUFBWCxDQUFBLENBQTBCLEVBQUMsS0FBRCxFQUExQixLQUFvQyxLQUFwQyxJQUE4QyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQXpCLEtBQWlDLElBQXBIO2NBQUEsaUJBQWlCLENBQUMsSUFBbEIsQ0FBdUIsVUFBdkIsRUFBQTs7QUFERjtBQURGLFNBUkY7O2FBV0E7SUFacUI7SUFjdkIsVUFBQSxDQUFXLFNBQUE7TUFDVCxnQkFBQSxHQUFtQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVgsQ0FBbUIsSUFBSSxDQUFDLFNBQXhCO01BQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBYixDQUFzQixDQUFDLElBQUksQ0FBQyxJQUFMLENBQVUsU0FBVixFQUFxQixVQUFyQixDQUFELENBQXRCO01BRUEsZUFBQSxDQUFnQixTQUFBO2VBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFmLENBQW9CLFNBQXBCO01BRGMsQ0FBaEI7TUFHQSxlQUFBLENBQWdCLFNBQUE7ZUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsZUFBOUI7TUFEYyxDQUFoQjthQUdBLElBQUEsQ0FBSyxTQUFBO0FBQ0gsWUFBQTtRQUFBLE9BQU8sQ0FBQyxXQUFSLENBQW9CLGdCQUFwQjtRQUNBLE1BQUEsR0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFmLENBQUE7UUFDVCxVQUFBLEdBQWEsTUFBTSxDQUFDLFVBQVAsQ0FBQTtlQUViLGlCQUFBLEdBQW9CLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZCxDQUE4QixZQUE5QixDQUEyQyxDQUFDLElBQTVDLENBQWlELFNBQUMsR0FBRDtBQUNuRSxjQUFBO1VBRHFFLGFBQUQ7VUFDcEUsVUFBVSxDQUFDLFdBQVgsQ0FBQTtpQkFDQyw4QkFBRCxFQUFhO1FBRnNELENBQWpEO01BTGpCLENBQUw7SUFWUyxDQUFYO0lBbUJBLFFBQUEsQ0FBUyw2QkFBVCxFQUF3QyxTQUFBO01BQ3RDLFFBQUEsQ0FBUywyQ0FBVCxFQUFzRCxTQUFBO1FBQ3BELFVBQUEsQ0FBVyxTQUFBO1VBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLGdDQUFoQixFQUFrRCxJQUFsRDtVQUNBLEtBQUEsQ0FBTSxDQUFDLENBQUMsQ0FBUixFQUFXLEtBQVgsQ0FBaUIsQ0FBQyxXQUFsQixDQUE4QixTQUFBO21CQUFHLE1BQU0sQ0FBQztVQUFWLENBQTlCO1VBQ0EsTUFBTSxDQUFDLHVCQUFQLENBQStCLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FBL0I7VUFDQSxZQUFBLENBQWEsR0FBYjtpQkFDQSxZQUFBLEdBQWUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFmLENBQUE7UUFMTixDQUFYO1FBT0EsRUFBQSxDQUFHLGlCQUFILEVBQXNCLFNBQUE7aUJBQ3BCLE1BQUEsQ0FBTyxvQkFBQSxDQUFxQixNQUFyQixFQUE2QixzQkFBN0IsRUFBcUQsV0FBckQsQ0FBUCxDQUF5RSxDQUFDLFlBQTFFLENBQXVGLENBQXZGO1FBRG9CLENBQXRCO1FBR0EsRUFBQSxDQUFHLG1CQUFILEVBQXdCLFNBQUE7aUJBQ3RCLE1BQUEsQ0FBTyxvQkFBQSxDQUFxQixNQUFyQixFQUE2QixzQkFBN0IsRUFBcUQsYUFBckQsQ0FBUCxDQUEyRSxDQUFDLFlBQTVFLENBQXlGLENBQXpGO1FBRHNCLENBQXhCO2VBR0EsRUFBQSxDQUFHLDRCQUFILEVBQWlDLFNBQUE7aUJBQy9CLE1BQUEsQ0FBTyxnQkFBZ0IsQ0FBQyxhQUFqQixDQUErQixhQUEvQixDQUFQLENBQXFELENBQUMsSUFBdEQsQ0FBMkQsWUFBYSxDQUFBLENBQUEsQ0FBRSxDQUFDLElBQTNFO1FBRCtCLENBQWpDO01BZG9ELENBQXREO2FBaUJBLFFBQUEsQ0FBUyw0Q0FBVCxFQUF1RCxTQUFBO1FBQ3JELFVBQUEsQ0FBVyxTQUFBO1VBQ1QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQWdCLGdDQUFoQixFQUFrRCxLQUFsRDtVQUNBLEtBQUEsQ0FBTSxDQUFDLENBQUMsQ0FBUixFQUFXLEtBQVgsQ0FBaUIsQ0FBQyxXQUFsQixDQUE4QixTQUFBO21CQUFHLE1BQU0sQ0FBQztVQUFWLENBQTlCO1VBQ0EsTUFBTSxDQUFDLHVCQUFQLENBQStCLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FBL0I7VUFDQSxZQUFBLENBQWEsR0FBYjtpQkFDQSxZQUFBLEdBQWUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFmLENBQUE7UUFMTixDQUFYO1FBT0EsRUFBQSxDQUFHLGlCQUFILEVBQXNCLFNBQUE7aUJBQ3BCLE1BQUEsQ0FBTyxvQkFBQSxDQUFxQixNQUFyQixFQUE2QixzQkFBN0IsRUFBcUQsV0FBckQsQ0FBUCxDQUF5RSxDQUFDLFlBQTFFLENBQXVGLENBQXZGO1FBRG9CLENBQXRCO1FBR0EsRUFBQSxDQUFHLG1CQUFILEVBQXdCLFNBQUE7aUJBQ3RCLE1BQUEsQ0FBTyxvQkFBQSxDQUFxQixNQUFyQixFQUE2QixzQkFBN0IsRUFBcUQsYUFBckQsQ0FBUCxDQUEyRSxDQUFDLFlBQTVFLENBQXlGLENBQXpGO1FBRHNCLENBQXhCO2VBR0EsRUFBQSxDQUFHLDRCQUFILEVBQWlDLFNBQUE7aUJBQy9CLE1BQUEsQ0FBTyxnQkFBZ0IsQ0FBQyxhQUFqQixDQUErQixhQUEvQixDQUFQLENBQXFELENBQUMsSUFBdEQsQ0FBMkQsWUFBYSxDQUFBLENBQUEsQ0FBRSxDQUFDLElBQTNFO1FBRCtCLENBQWpDO01BZHFELENBQXZEO0lBbEJzQyxDQUF4QztXQW9DQSxRQUFBLENBQVMsaUNBQVQsRUFBNEMsU0FBQTtNQUMxQyxVQUFBLENBQVcsU0FBQTtRQUNULE1BQU0sQ0FBQyx1QkFBUCxDQUErQixDQUFDLENBQUQsRUFBSSxDQUFKLENBQS9CO2VBQ0EsWUFBQSxDQUFhLEdBQWI7TUFGUyxDQUFYO01BSUEsRUFBQSxDQUFHLGlCQUFILEVBQXNCLFNBQUE7ZUFDcEIsTUFBQSxDQUFPLG9CQUFBLENBQXFCLE1BQXJCLEVBQTZCLHNCQUE3QixFQUFxRCxXQUFyRCxDQUFQLENBQXlFLENBQUMsWUFBMUUsQ0FBdUYsQ0FBdkY7TUFEb0IsQ0FBdEI7TUFHQSxFQUFBLENBQUcsbUJBQUgsRUFBd0IsU0FBQTtlQUN0QixNQUFBLENBQU8sb0JBQUEsQ0FBcUIsTUFBckIsRUFBNkIsc0JBQTdCLEVBQXFELGFBQXJELENBQVAsQ0FBMkUsQ0FBQyxZQUE1RSxDQUF5RixDQUF6RjtNQURzQixDQUF4QjthQUdBLEVBQUEsQ0FBRyw0QkFBSCxFQUFpQyxTQUFBO2VBQy9CLE1BQUEsQ0FBTyxZQUFQLENBQW9CLENBQUMsWUFBckIsQ0FBa0MsQ0FBbEM7TUFEK0IsQ0FBakM7SUFYMEMsQ0FBNUM7RUF4RW9CLENBQXRCO0FBTEEiLCJzb3VyY2VzQ29udGVudCI6WyJfID0gcmVxdWlyZSAndW5kZXJzY29yZS1wbHVzJ1xueyR9ID0gcmVxdWlyZSAnYXRvbS1zcGFjZS1wZW4tdmlld3MnXG5cbnBhdGggPSByZXF1aXJlICdwYXRoJ1xuXG5kZXNjcmliZSBcIlJ1YnlCbG9ja1wiLCAtPlxuICBbd29ya3NwYWNlRWxlbWVudCwgZWRpdG9yLCBlZGl0b3JWaWV3LCBlZGl0b3JFbGVtZW50LCBtYXJrZXJzLCBsaW5lTnVtYmVycywgcnVieUJsb2NrRWxlbWVudCwgYm90dG9tUGFuZWxzXSA9ICBbXVxuXG4gIGdldFJlc3VsdERlY29yYXRpb25zID0gKGVkaXRvciwgY2xhenosIHR5cGUpIC0+XG4gICAgaWYgZWRpdG9yLmRlY29yYXRpb25zU3RhdGVGb3JTY3JlZW5Sb3dSYW5nZT9cbiAgICAgIHJlc3VsdERlY29yYXRpb25zID0gW11cbiAgICAgIGZvciBpZCwgZGVjb3JhdGlvbiBvZiBlZGl0b3IuZGVjb3JhdGlvbnNTdGF0ZUZvclNjcmVlblJvd1JhbmdlKDAsIGVkaXRvci5nZXRMaW5lQ291bnQoKSlcbiAgICAgICAgaWYgZGVjb3JhdGlvbi5wcm9wZXJ0aWVzLmNsYXNzIGlzIGNsYXp6IGFuZCBkZWNvcmF0aW9uLnByb3BlcnRpZXMudHlwZSBpcyB0eXBlXG4gICAgICAgICAgcmVzdWx0RGVjb3JhdGlvbnMucHVzaChkZWNvcmF0aW9uKVxuICAgIGVsc2VcbiAgICAgIG1hcmtlcklkRm9yRGVjb3JhdGlvbnMgPSBlZGl0b3IuZGVjb3JhdGlvbnNGb3JTY3JlZW5Sb3dSYW5nZSgwLCBlZGl0b3IuZ2V0TGluZUNvdW50KCkpXG4gICAgICByZXN1bHREZWNvcmF0aW9ucyA9IFtdXG4gICAgICBmb3IgbWFya2VySWQsIGRlY29yYXRpb25zIG9mIG1hcmtlcklkRm9yRGVjb3JhdGlvbnNcbiAgICAgICAgZm9yIGRlY29yYXRpb24gaW4gZGVjb3JhdGlvbnNcbiAgICAgICAgICByZXN1bHREZWNvcmF0aW9ucy5wdXNoIGRlY29yYXRpb24gaWYgZGVjb3JhdGlvbi5nZXRQcm9wZXJ0aWVzKCkuY2xhc3MgaXMgY2xhenogYW5kIGRlY29yYXRpb24uZ2V0UHJvcGVydGllcy50eXBlIGlzIHR5cGVcbiAgICByZXN1bHREZWNvcmF0aW9uc1xuICAgIFxuICBiZWZvcmVFYWNoIC0+XG4gICAgd29ya3NwYWNlRWxlbWVudCA9IGF0b20udmlld3MuZ2V0VmlldyhhdG9tLndvcmtzcGFjZSlcbiAgICBhdG9tLnByb2plY3Quc2V0UGF0aHMoW3BhdGguam9pbihfX2Rpcm5hbWUsICdmaXh0dXJlcycpXSlcblxuICAgIHdhaXRzRm9yUHJvbWlzZSAtPiBcbiAgICAgIGF0b20ud29ya3NwYWNlLm9wZW4oJ3Rlc3QucmInKVxuICAgICAgXG4gICAgd2FpdHNGb3JQcm9taXNlIC0+IFxuICAgICAgYXRvbS5wYWNrYWdlcy5hY3RpdmF0ZVBhY2thZ2UoJ2xhbmd1YWdlLXJ1YnknKVxuXG4gICAgcnVucyAtPlxuICAgICAgamFzbWluZS5hdHRhY2hUb0RPTSh3b3Jrc3BhY2VFbGVtZW50KVxuICAgICAgZWRpdG9yID0gYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlVGV4dEVkaXRvcigpXG4gICAgICBlZGl0b3JWaWV3ID0gZWRpdG9yLmdldEVsZW1lbnQoKVxuXG4gICAgICBhY3RpdmF0aW9uUHJvbWlzZSA9IGF0b20ucGFja2FnZXMuYWN0aXZhdGVQYWNrYWdlKFwicnVieS1ibG9ja1wiKS50aGVuICh7bWFpbk1vZHVsZX0pIC0+XG4gICAgICAgIG1haW5Nb2R1bGUuY3JlYXRlVmlld3MoKVxuICAgICAgICB7ZmluZFZpZXd9ID0gbWFpbk1vZHVsZVxuXG4gIGRlc2NyaWJlIFwid2hlbiBjdXJzb3IgaXMgb24gdGhlICdlbmQnXCIsIC0+XG4gICAgZGVzY3JpYmUgXCJ3aGVuIGhpZ2hsaWdodExpbmVOdW1iZXIgb3B0aW9uIGlzICd0cnVlJ1wiLCAtPlxuICAgICAgYmVmb3JlRWFjaCAtPlxuICAgICAgICBhdG9tLmNvbmZpZy5zZXQgJ3J1YnktYmxvY2suaGlnaGxpZ2h0TGluZU51bWJlcicsIHRydWVcbiAgICAgICAgc3B5T24oXy5fLCBcIm5vd1wiKS5hbmRDYWxsRmFrZSAtPiB3aW5kb3cubm93XG4gICAgICAgIGVkaXRvci5zZXRDdXJzb3JCdWZmZXJQb3NpdGlvbiBbMywgMF1cbiAgICAgICAgYWR2YW5jZUNsb2NrKDEwMClcbiAgICAgICAgYm90dG9tUGFuZWxzID0gYXRvbS53b3Jrc3BhY2UuZ2V0Qm90dG9tUGFuZWxzKClcbiAgICAgICAgXG4gICAgICBpdCAnaGlnaGxpZ2h0cyBsaW5lJywgLT5cbiAgICAgICAgZXhwZWN0KGdldFJlc3VsdERlY29yYXRpb25zKGVkaXRvciwgJ3J1YnktYmxvY2staGlnaGxpZ2h0JywgJ2hpZ2hsaWdodCcpKS50b0hhdmVMZW5ndGggMVxuICAgICAgICBcbiAgICAgIGl0ICdoaWdobGlnaHRzIGd1dHRlcicsIC0+XG4gICAgICAgIGV4cGVjdChnZXRSZXN1bHREZWNvcmF0aW9ucyhlZGl0b3IsICdydWJ5LWJsb2NrLWhpZ2hsaWdodCcsICdsaW5lLW51bWJlcicpKS50b0hhdmVMZW5ndGggMVxuICAgICAgICBcbiAgICAgIGl0ICdzaG93cyB2aWV3IGluIGJvdHRvbSBwYW5lbCcsIC0+XG4gICAgICAgIGV4cGVjdCh3b3Jrc3BhY2VFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5ydWJ5LWJsb2NrJykpLnRvQmUgYm90dG9tUGFuZWxzWzBdLml0ZW1cblxuICAgIGRlc2NyaWJlIFwid2hlbiBoaWdobGlnaHRMaW5lTnVtYmVyIG9wdGlvbiBpcyAnZmFsc2UnXCIsIC0+XG4gICAgICBiZWZvcmVFYWNoIC0+XG4gICAgICAgIGF0b20uY29uZmlnLnNldCAncnVieS1ibG9jay5oaWdobGlnaHRMaW5lTnVtYmVyJywgZmFsc2VcbiAgICAgICAgc3B5T24oXy5fLCBcIm5vd1wiKS5hbmRDYWxsRmFrZSAtPiB3aW5kb3cubm93XG4gICAgICAgIGVkaXRvci5zZXRDdXJzb3JCdWZmZXJQb3NpdGlvbiBbMywgMF1cbiAgICAgICAgYWR2YW5jZUNsb2NrKDEwMClcbiAgICAgICAgYm90dG9tUGFuZWxzID0gYXRvbS53b3Jrc3BhY2UuZ2V0Qm90dG9tUGFuZWxzKClcbiAgICAgICAgXG4gICAgICBpdCAnaGlnaGxpZ2h0cyBsaW5lJywgLT5cbiAgICAgICAgZXhwZWN0KGdldFJlc3VsdERlY29yYXRpb25zKGVkaXRvciwgJ3J1YnktYmxvY2staGlnaGxpZ2h0JywgJ2hpZ2hsaWdodCcpKS50b0hhdmVMZW5ndGggMVxuXG4gICAgICBpdCAnaGlnaGxpZ2h0cyBndXR0ZXInLCAtPlxuICAgICAgICBleHBlY3QoZ2V0UmVzdWx0RGVjb3JhdGlvbnMoZWRpdG9yLCAncnVieS1ibG9jay1oaWdobGlnaHQnLCAnbGluZS1udW1iZXInKSkudG9IYXZlTGVuZ3RoIDBcblxuICAgICAgaXQgJ3Nob3dzIHZpZXcgaW4gYm90dG9tIHBhbmVsJywgLT5cbiAgICAgICAgZXhwZWN0KHdvcmtzcGFjZUVsZW1lbnQucXVlcnlTZWxlY3RvcignLnJ1YnktYmxvY2snKSkudG9CZSBib3R0b21QYW5lbHNbMF0uaXRlbVxuXG5cbiAgZGVzY3JpYmUgXCJ3aGVuIGN1cnNvciBpcyBub3Qgb24gdGhlICdlbmQnXCIsIC0+XG4gICAgYmVmb3JlRWFjaCAtPlxuICAgICAgZWRpdG9yLnNldEN1cnNvckJ1ZmZlclBvc2l0aW9uIFs0LCAwXVxuICAgICAgYWR2YW5jZUNsb2NrKDEwMClcblxuICAgIGl0ICdoaWdobGlnaHRzIGxpbmUnLCAtPlxuICAgICAgZXhwZWN0KGdldFJlc3VsdERlY29yYXRpb25zKGVkaXRvciwgJ3J1YnktYmxvY2staGlnaGxpZ2h0JywgJ2hpZ2hsaWdodCcpKS50b0hhdmVMZW5ndGggMFxuXG4gICAgaXQgJ2hpZ2hsaWdodHMgZ3V0dGVyJywgLT5cbiAgICAgIGV4cGVjdChnZXRSZXN1bHREZWNvcmF0aW9ucyhlZGl0b3IsICdydWJ5LWJsb2NrLWhpZ2hsaWdodCcsICdsaW5lLW51bWJlcicpKS50b0hhdmVMZW5ndGggMFxuXG4gICAgaXQgJ3Nob3dzIHZpZXcgaW4gYm90dG9tIHBhbmVsJywgLT5cbiAgICAgIGV4cGVjdChib3R0b21QYW5lbHMpLnRvSGF2ZUxlbmd0aCAwXG4gICAgICBcbiJdfQ==
