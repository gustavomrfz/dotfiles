(function() {
  var CompositeDisposable, SublimePanes, SublimePanesView;

  SublimePanesView = require('./sublime-panes-view');

  CompositeDisposable = require('atom').CompositeDisposable;

  module.exports = SublimePanes = {
    sublimePanesView: null,
    modalPanel: null,
    subscriptions: null,
    activeHandler: null,
    hiddenTab: null,
    newItem: null,
    active: null,
    register: null,
    activate: function(state) {
      this.sublimePanesView = new SublimePanesView(state.sublimePanesViewState);
      this.modalPanel = atom.workspace.addModalPanel({
        item: this.sublimePanesView.getElement(),
        visible: false
      });
      this.subscriptions = new CompositeDisposable;
      this.activeHandler = new CompositeDisposable;
      this.subscriptions.add(atom.commands.add('atom-workspace', {
        'sublime-panes:toggle': (function(_this) {
          return function() {
            return _this.toggle();
          };
        })(this)
      }));
      this.register = atom.workspace.onDidAddPaneItem((function(_this) {
        return function(event) {
          return _this.registerInterest(event);
        };
      })(this));
      return this.active = true;
    },
    deactivate: function() {
      this.modalPanel.destroy();
      this.subscriptions.dispose();
      this.sublimePanesView.destroy();
      this.activeHandler.dispose();
      this.newItem.destroy();
      return this.register.dispose();
    },
    serialize: function() {
      return {
        sublimePanesViewState: this.sublimePanesView.serialize()
      };
    },
    toggle: function() {
      if (this.active) {
        this.register.dispose();
        return this.active = false;
      } else {
        this.register = atom.workspace.onDidAddPaneItem((function(_this) {
          return function(event) {
            return _this.registerInterest(event);
          };
        })(this));
        return this.active = true;
      }
    },
    registerInterest: function(event) {
      this.cleanUp();
      this.activeHandler = new CompositeDisposable;
      return this.activeHandler.add(event.pane.onDidChangeActiveItem((function(_this) {
        return function(item) {
          return _this.catchItem(item);
        };
      })(this)));
    },
    catchItem: function(activeItem) {
      var element, i, len, pane, ref;
      this.activeHandler.dispose();
      this.newItem = activeItem;
      pane = atom.workspace.paneForItem(this.newItem);
      ref = atom.views.getView(pane).getElementsByClassName('active');
      for (i = 0, len = ref.length; i < len; i++) {
        element = ref[i];
        this.hiddenTab = element;
        element.parentNode.removeChild(element);
      }
      this.activeHandler = new CompositeDisposable;
      this.activeHandler.add(this.newItem.onDidChangeModified((function(_this) {
        return function() {
          return _this.restoreTab();
        };
      })(this)));
      return this.activeHandler.add(pane.onDidChangeActiveItem((function(_this) {
        return function() {
          return _this.cleanUp();
        };
      })(this)));
    },
    restoreTab: function() {
      var element, i, len, pane, ref, results;
      this.activeHandler.dispose();
      this.newItem = null;
      pane = atom.workspace.getActivePane();
      ref = atom.views.getView(pane).getElementsByClassName('tab-bar');
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        element = ref[i];
        results.push(element.appendChild(this.hiddenTab));
      }
      return results;
    },
    cleanUp: function() {
      var pane;
      this.activeHandler.dispose();
      if (this.newItem != null) {
        pane = atom.workspace.paneForItem(this.newItem);
        pane.destroyItem(this.newItem);
        return this.newItem = null;
      }
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9zdWJsaW1lLXBhbmVzL2xpYi9zdWJsaW1lLXBhbmVzLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsZ0JBQUEsR0FBbUIsT0FBQSxDQUFRLHNCQUFSOztFQUNsQixzQkFBdUIsT0FBQSxDQUFRLE1BQVI7O0VBRXhCLE1BQU0sQ0FBQyxPQUFQLEdBQWlCLFlBQUEsR0FDZjtJQUFBLGdCQUFBLEVBQWtCLElBQWxCO0lBQ0EsVUFBQSxFQUFZLElBRFo7SUFFQSxhQUFBLEVBQWUsSUFGZjtJQUdBLGFBQUEsRUFBZSxJQUhmO0lBSUEsU0FBQSxFQUFXLElBSlg7SUFLQSxPQUFBLEVBQVMsSUFMVDtJQU1BLE1BQUEsRUFBUSxJQU5SO0lBT0EsUUFBQSxFQUFVLElBUFY7SUFTQSxRQUFBLEVBQVUsU0FBQyxLQUFEO01BQ1IsSUFBQyxDQUFBLGdCQUFELEdBQXdCLElBQUEsZ0JBQUEsQ0FBaUIsS0FBSyxDQUFDLHFCQUF2QjtNQUN4QixJQUFDLENBQUEsVUFBRCxHQUFjLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBZixDQUE2QjtRQUFBLElBQUEsRUFBTSxJQUFDLENBQUEsZ0JBQWdCLENBQUMsVUFBbEIsQ0FBQSxDQUFOO1FBQXNDLE9BQUEsRUFBUyxLQUEvQztPQUE3QjtNQUdkLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7TUFDckIsSUFBQyxDQUFBLGFBQUQsR0FBaUIsSUFBSTtNQUdyQixJQUFDLENBQUEsYUFBYSxDQUFDLEdBQWYsQ0FBbUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFkLENBQWtCLGdCQUFsQixFQUFvQztRQUFBLHNCQUFBLEVBQXdCLENBQUEsU0FBQSxLQUFBO2lCQUFBLFNBQUE7bUJBQUcsS0FBQyxDQUFBLE1BQUQsQ0FBQTtVQUFIO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF4QjtPQUFwQyxDQUFuQjtNQUVBLElBQUMsQ0FBQSxRQUFELEdBQVksSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZixDQUFnQyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsS0FBRDtpQkFBVyxLQUFDLENBQUEsZ0JBQUQsQ0FBa0IsS0FBbEI7UUFBWDtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBaEM7YUFFWixJQUFDLENBQUEsTUFBRCxHQUFVO0lBYkYsQ0FUVjtJQXdCQSxVQUFBLEVBQVksU0FBQTtNQUNWLElBQUMsQ0FBQSxVQUFVLENBQUMsT0FBWixDQUFBO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQUE7TUFDQSxJQUFDLENBQUEsZ0JBQWdCLENBQUMsT0FBbEIsQ0FBQTtNQUNBLElBQUMsQ0FBQSxhQUFhLENBQUMsT0FBZixDQUFBO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxPQUFULENBQUE7YUFDQSxJQUFDLENBQUEsUUFBUSxDQUFDLE9BQVYsQ0FBQTtJQU5VLENBeEJaO0lBZ0NBLFNBQUEsRUFBVyxTQUFBO2FBQ1Q7UUFBQSxxQkFBQSxFQUF1QixJQUFDLENBQUEsZ0JBQWdCLENBQUMsU0FBbEIsQ0FBQSxDQUF2Qjs7SUFEUyxDQWhDWDtJQW1DQSxNQUFBLEVBQVEsU0FBQTtNQUNOLElBQUcsSUFBQyxDQUFBLE1BQUo7UUFDRSxJQUFDLENBQUEsUUFBUSxDQUFDLE9BQVYsQ0FBQTtlQUNBLElBQUMsQ0FBQSxNQUFELEdBQVUsTUFGWjtPQUFBLE1BQUE7UUFJRSxJQUFDLENBQUEsUUFBRCxHQUFZLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWYsQ0FBZ0MsQ0FBQSxTQUFBLEtBQUE7aUJBQUEsU0FBQyxLQUFEO21CQUFXLEtBQUMsQ0FBQSxnQkFBRCxDQUFrQixLQUFsQjtVQUFYO1FBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFoQztlQUNaLElBQUMsQ0FBQSxNQUFELEdBQVUsS0FMWjs7SUFETSxDQW5DUjtJQTRDQSxnQkFBQSxFQUFrQixTQUFDLEtBQUQ7TUFDaEIsSUFBQyxDQUFBLE9BQUQsQ0FBQTtNQUNBLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7YUFDckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLEtBQUssQ0FBQyxJQUFJLENBQUMscUJBQVgsQ0FBaUMsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFDLElBQUQ7aUJBQVUsS0FBQyxDQUFBLFNBQUQsQ0FBVyxJQUFYO1FBQVY7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQWpDLENBQW5CO0lBSGdCLENBNUNsQjtJQWlEQSxTQUFBLEVBQVcsU0FBQyxVQUFEO0FBQ1QsVUFBQTtNQUFBLElBQUMsQ0FBQSxhQUFhLENBQUMsT0FBZixDQUFBO01BQ0EsSUFBQyxDQUFBLE9BQUQsR0FBVztNQUNYLElBQUEsR0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQWYsQ0FBMkIsSUFBQyxDQUFBLE9BQTVCO0FBQ1A7QUFBQSxXQUFBLHFDQUFBOztRQUNFLElBQUMsQ0FBQSxTQUFELEdBQWE7UUFDYixPQUFPLENBQUMsVUFBVSxDQUFDLFdBQW5CLENBQStCLE9BQS9CO0FBRkY7TUFHQSxJQUFDLENBQUEsYUFBRCxHQUFpQixJQUFJO01BQ3JCLElBQUMsQ0FBQSxhQUFhLENBQUMsR0FBZixDQUFtQixJQUFDLENBQUEsT0FBTyxDQUFDLG1CQUFULENBQTZCLENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQTtpQkFBRyxLQUFDLENBQUEsVUFBRCxDQUFBO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBLENBQTdCLENBQW5CO2FBQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxxQkFBTCxDQUEyQixDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQUcsS0FBQyxDQUFBLE9BQUQsQ0FBQTtRQUFIO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUEzQixDQUFuQjtJQVRTLENBakRYO0lBNERBLFVBQUEsRUFBWSxTQUFBO0FBQ1YsVUFBQTtNQUFBLElBQUMsQ0FBQSxhQUFhLENBQUMsT0FBZixDQUFBO01BQ0EsSUFBQyxDQUFBLE9BQUQsR0FBVztNQUNYLElBQUEsR0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWYsQ0FBQTtBQUNQO0FBQUE7V0FBQSxxQ0FBQTs7cUJBQ0UsT0FBTyxDQUFDLFdBQVIsQ0FBb0IsSUFBQyxDQUFBLFNBQXJCO0FBREY7O0lBSlUsQ0E1RFo7SUFtRUEsT0FBQSxFQUFTLFNBQUE7QUFDUCxVQUFBO01BQUEsSUFBQyxDQUFBLGFBQWEsQ0FBQyxPQUFmLENBQUE7TUFDQSxJQUFHLG9CQUFIO1FBQ0UsSUFBQSxHQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBZixDQUEyQixJQUFDLENBQUEsT0FBNUI7UUFDUCxJQUFJLENBQUMsV0FBTCxDQUFpQixJQUFDLENBQUEsT0FBbEI7ZUFDQSxJQUFDLENBQUEsT0FBRCxHQUFXLEtBSGI7O0lBRk8sQ0FuRVQ7O0FBSkYiLCJzb3VyY2VzQ29udGVudCI6WyJTdWJsaW1lUGFuZXNWaWV3ID0gcmVxdWlyZSAnLi9zdWJsaW1lLXBhbmVzLXZpZXcnXG57Q29tcG9zaXRlRGlzcG9zYWJsZX0gPSByZXF1aXJlICdhdG9tJ1xuXG5tb2R1bGUuZXhwb3J0cyA9IFN1YmxpbWVQYW5lcyA9XG4gIHN1YmxpbWVQYW5lc1ZpZXc6IG51bGxcbiAgbW9kYWxQYW5lbDogbnVsbFxuICBzdWJzY3JpcHRpb25zOiBudWxsXG4gIGFjdGl2ZUhhbmRsZXI6IG51bGxcbiAgaGlkZGVuVGFiOiBudWxsXG4gIG5ld0l0ZW06IG51bGxcbiAgYWN0aXZlOiBudWxsXG4gIHJlZ2lzdGVyOiBudWxsXG5cbiAgYWN0aXZhdGU6IChzdGF0ZSkgLT5cbiAgICBAc3VibGltZVBhbmVzVmlldyA9IG5ldyBTdWJsaW1lUGFuZXNWaWV3KHN0YXRlLnN1YmxpbWVQYW5lc1ZpZXdTdGF0ZSlcbiAgICBAbW9kYWxQYW5lbCA9IGF0b20ud29ya3NwYWNlLmFkZE1vZGFsUGFuZWwoaXRlbTogQHN1YmxpbWVQYW5lc1ZpZXcuZ2V0RWxlbWVudCgpLCB2aXNpYmxlOiBmYWxzZSlcblxuICAgICMgRXZlbnRzIHN1YnNjcmliZWQgdG8gaW4gYXRvbSdzIHN5c3RlbSBjYW4gYmUgZWFzaWx5IGNsZWFuZWQgdXAgd2l0aCBhIENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBAc3Vic2NyaXB0aW9ucyA9IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlXG4gICAgQGFjdGl2ZUhhbmRsZXIgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZVxuXG4gICAgIyBSZWdpc3RlciBjb21tYW5kIHRoYXQgdG9nZ2xlcyB0aGlzIHZpZXdcbiAgICBAc3Vic2NyaXB0aW9ucy5hZGQgYXRvbS5jb21tYW5kcy5hZGQgJ2F0b20td29ya3NwYWNlJywgJ3N1YmxpbWUtcGFuZXM6dG9nZ2xlJzogPT4gQHRvZ2dsZSgpXG5cbiAgICBAcmVnaXN0ZXIgPSBhdG9tLndvcmtzcGFjZS5vbkRpZEFkZFBhbmVJdGVtIChldmVudCkgPT4gQHJlZ2lzdGVySW50ZXJlc3QoZXZlbnQpXG5cbiAgICBAYWN0aXZlID0gdHJ1ZVxuXG4gIGRlYWN0aXZhdGU6IC0+XG4gICAgQG1vZGFsUGFuZWwuZGVzdHJveSgpXG4gICAgQHN1YnNjcmlwdGlvbnMuZGlzcG9zZSgpXG4gICAgQHN1YmxpbWVQYW5lc1ZpZXcuZGVzdHJveSgpXG4gICAgQGFjdGl2ZUhhbmRsZXIuZGlzcG9zZSgpXG4gICAgQG5ld0l0ZW0uZGVzdHJveSgpXG4gICAgQHJlZ2lzdGVyLmRpc3Bvc2UoKVxuXG4gIHNlcmlhbGl6ZTogLT5cbiAgICBzdWJsaW1lUGFuZXNWaWV3U3RhdGU6IEBzdWJsaW1lUGFuZXNWaWV3LnNlcmlhbGl6ZSgpXG5cbiAgdG9nZ2xlOiAtPlxuICAgIGlmIEBhY3RpdmVcbiAgICAgIEByZWdpc3Rlci5kaXNwb3NlKClcbiAgICAgIEBhY3RpdmUgPSBmYWxzZVxuICAgIGVsc2VcbiAgICAgIEByZWdpc3RlciA9IGF0b20ud29ya3NwYWNlLm9uRGlkQWRkUGFuZUl0ZW0gKGV2ZW50KSA9PiBAcmVnaXN0ZXJJbnRlcmVzdChldmVudClcbiAgICAgIEBhY3RpdmUgPSB0cnVlXG5cblxuICByZWdpc3RlckludGVyZXN0OiAoZXZlbnQpIC0+XG4gICAgQGNsZWFuVXAoKVxuICAgIEBhY3RpdmVIYW5kbGVyID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcbiAgICBAYWN0aXZlSGFuZGxlci5hZGQgZXZlbnQucGFuZS5vbkRpZENoYW5nZUFjdGl2ZUl0ZW0gKGl0ZW0pID0+IEBjYXRjaEl0ZW0oaXRlbSlcblxuICBjYXRjaEl0ZW06IChhY3RpdmVJdGVtKSAtPlxuICAgIEBhY3RpdmVIYW5kbGVyLmRpc3Bvc2UoKVxuICAgIEBuZXdJdGVtID0gYWN0aXZlSXRlbVxuICAgIHBhbmUgPSBhdG9tLndvcmtzcGFjZS5wYW5lRm9ySXRlbShAbmV3SXRlbSlcbiAgICBmb3IgZWxlbWVudCBpbiBhdG9tLnZpZXdzLmdldFZpZXcocGFuZSkuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYWN0aXZlJylcbiAgICAgIEBoaWRkZW5UYWIgPSBlbGVtZW50XG4gICAgICBlbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZWxlbWVudClcbiAgICBAYWN0aXZlSGFuZGxlciA9IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlXG4gICAgQGFjdGl2ZUhhbmRsZXIuYWRkIEBuZXdJdGVtLm9uRGlkQ2hhbmdlTW9kaWZpZWQgPT4gQHJlc3RvcmVUYWIoKVxuICAgIEBhY3RpdmVIYW5kbGVyLmFkZCBwYW5lLm9uRGlkQ2hhbmdlQWN0aXZlSXRlbSA9PiBAY2xlYW5VcCgpXG5cbiAgcmVzdG9yZVRhYjogLT5cbiAgICBAYWN0aXZlSGFuZGxlci5kaXNwb3NlKClcbiAgICBAbmV3SXRlbSA9IG51bGxcbiAgICBwYW5lID0gYXRvbS53b3Jrc3BhY2UuZ2V0QWN0aXZlUGFuZSgpXG4gICAgZm9yIGVsZW1lbnQgaW4gYXRvbS52aWV3cy5nZXRWaWV3KHBhbmUpLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3RhYi1iYXInKVxuICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZCBAaGlkZGVuVGFiXG5cbiAgY2xlYW5VcDogLT5cbiAgICBAYWN0aXZlSGFuZGxlci5kaXNwb3NlKClcbiAgICBpZiBAbmV3SXRlbT9cbiAgICAgIHBhbmUgPSBhdG9tLndvcmtzcGFjZS5wYW5lRm9ySXRlbShAbmV3SXRlbSlcbiAgICAgIHBhbmUuZGVzdHJveUl0ZW0oQG5ld0l0ZW0pXG4gICAgICBAbmV3SXRlbSA9IG51bGxcbiJdfQ==
