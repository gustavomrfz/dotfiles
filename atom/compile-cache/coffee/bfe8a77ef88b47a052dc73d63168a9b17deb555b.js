(function() {
  var ATOM_BUNDLE_IDENTIFIER, BufferedProcess, INSTALLATION_LINE_PATTERN, Util, meta,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  meta = require("../package.json");

  Util = require("./util");

  BufferedProcess = require("atom").BufferedProcess;

  ATOM_BUNDLE_IDENTIFIER = "com.github.atom";

  INSTALLATION_LINE_PATTERN = /^Installing +([^@]+)@(\S+).+\s+(\S+)$/;

  module.exports = {
    updatePackages: function(isAutoUpdate) {
      if (isAutoUpdate == null) {
        isAutoUpdate = true;
      }
      return this.runApmUpgrade((function(_this) {
        return function(log) {
          var entries, packageWording, summary, updateWording;
          entries = _this.parseLog(log);
          summary = _this.generateSummary(entries, isAutoUpdate);
          updateWording = isAutoUpdate === true ? "Auto-updating" : "Updating";
          packageWording = entries.length === 1 ? "package" : "packages";
          if (entries.length > 0) {
            require("./ga").sendEvent("package-updater", updateWording + " " + entries.length + " " + packageWording + " (Atom v" + atom.appVersion + " " + (atom.getReleaseChannel()) + ")");
            if (Util.getConfig("debugMode")) {
              console.log(updateWording + " " + entries.length + " " + packageWording);
            }
          }
          if (!summary) {
            return;
          }
          return _this.notify({
            title: "Atom Package Updates",
            message: summary,
            sender: ATOM_BUNDLE_IDENTIFIER,
            activate: ATOM_BUNDLE_IDENTIFIER
          });
        };
      })(this));
    },
    runApmUpgrade: function(callback) {
      var args, availablePackage, availablePackages, command, excludedPackage, excludedPackages, exit, i, includedPackage, includedPackages, index, j, k, len, len1, len2, log, stdout;
      command = atom.packages.getApmPath();
      availablePackages = atom.packages.getAvailablePackageNames();
      includedPackages = atom.config.get(meta.name + ".includedPackages");
      excludedPackages = atom.config.get(meta.name + ".excludedPackages");
      args = ["upgrade"];
      if (includedPackages.length > 0) {
        if (Util.getConfig("debugMode")) {
          console.log("Packages included in update:");
        }
        for (i = 0, len = includedPackages.length; i < len; i++) {
          includedPackage = includedPackages[i];
          if (Util.getConfig("debugMode")) {
            console.log("- " + includedPackage);
          }
          args.push(includedPackage);
        }
      } else if (excludedPackages.length > 0) {
        if (Util.getConfig("debugMode")) {
          console.log("Packages excluded from update:");
        }
        for (j = 0, len1 = excludedPackages.length; j < len1; j++) {
          excludedPackage = excludedPackages[j];
          if (indexOf.call(availablePackages, excludedPackage) >= 0) {
            if (Util.getConfig("debugMode")) {
              console.log("- " + excludedPackage);
            }
            index = availablePackages.indexOf(excludedPackage);
            if (index) {
              availablePackages.splice(index, 1);
            }
          }
        }
      } else {
        for (k = 0, len2 = availablePackages.length; k < len2; k++) {
          availablePackage = availablePackages[k];
          args.push(availablePackage);
        }
      }
      args.push("--no-confirm");
      args.push("--no-color");
      log = "";
      stdout = function(data) {
        return log += data;
      };
      exit = function(exitCode) {
        return callback(log);
      };
      return new BufferedProcess({
        command: command,
        args: args,
        stdout: stdout,
        exit: exit
      });
    },
    parseLog: function(log) {
      var _match, i, len, line, lines, matches, name, result, results, version;
      lines = log.split("\n");
      results = [];
      for (i = 0, len = lines.length; i < len; i++) {
        line = lines[i];
        matches = line.match(INSTALLATION_LINE_PATTERN);
        if (matches == null) {
          continue;
        }
        _match = matches[0], name = matches[1], version = matches[2], result = matches[3];
        results.push({
          "name": name,
          "version": version,
          "isInstalled": result === "\u2713"
        });
      }
      return results;
    },
    generateSummary: function(entries, isAutoUpdate) {
      var names, successfulEntries, summary;
      if (isAutoUpdate == null) {
        isAutoUpdate = true;
      }
      successfulEntries = entries.filter(function(entry) {
        return entry.isInstalled;
      });
      if (!(successfulEntries.length > 0)) {
        return null;
      }
      names = successfulEntries.map(function(entry) {
        return entry.name;
      });
      summary = successfulEntries.length <= atom.config.get(meta.name + ".maximumPackageDetail") ? this.generateEnumerationExpression(names) : successfulEntries.length + " packages";
      summary += successfulEntries.length === 1 ? " has" : " have";
      summary += " been updated";
      if (isAutoUpdate) {
        summary += " automatically";
      }
      summary += ".";
      return summary;
    },
    generateEnumerationExpression: function(items) {
      var expression, i, index, item, len;
      expression = "";
      for (index = i = 0, len = items.length; i < len; index = ++i) {
        item = items[index];
        if (index > 0) {
          if (index + 1 < items.length) {
            expression += ", ";
          } else {
            expression += " and ";
          }
        }
        expression += item;
      }
      return expression;
    },
    notify: function(notification) {
      var notifyOptions;
      if (atom.config.get(meta.name + ".updateNotification")) {
        require("./ga").sendEvent("package-updater", "Show Notification");
        if (Util.getConfig("debugMode")) {
          console.log("Show Notification");
        }
        notifyOptions = {
          detail: notification.message,
          dismissable: !atom.config.get(meta.name + ".dismissNotification"),
          buttons: [
            {
              text: "Restart",
              className: "icon icon-sync",
              onDidClick: function() {
                require("./ga").sendEvent("package-updater", "Restart Application");
                if (Util.getConfig("debugMode")) {
                  console.log("Restart Application");
                }
                return atom.restartApplication();
              }
            }
          ]
        };
        return Util.notification("**" + meta.name + "**", notifyOptions);
      }
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9hdXRvLXVwZGF0ZS1wbHVzL2xpYi9wYWNrYWdlLXVwZGF0ZXIuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQSw4RUFBQTtJQUFBOztFQUFBLElBQUEsR0FBTyxPQUFBLENBQVEsaUJBQVI7O0VBQ1AsSUFBQSxHQUFPLE9BQUEsQ0FBUSxRQUFSOztFQUNOLGtCQUFtQixPQUFBLENBQVEsTUFBUjs7RUFFcEIsc0JBQUEsR0FBeUI7O0VBQ3pCLHlCQUFBLEdBQTRCOztFQUU1QixNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsY0FBQSxFQUFnQixTQUFDLFlBQUQ7O1FBQUMsZUFBZTs7YUFDOUIsSUFBQyxDQUFBLGFBQUQsQ0FBZSxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUMsR0FBRDtBQUNiLGNBQUE7VUFBQSxPQUFBLEdBQVUsS0FBQyxDQUFBLFFBQUQsQ0FBVSxHQUFWO1VBQ1YsT0FBQSxHQUFVLEtBQUMsQ0FBQSxlQUFELENBQWlCLE9BQWpCLEVBQTBCLFlBQTFCO1VBRVYsYUFBQSxHQUFtQixZQUFBLEtBQWdCLElBQW5CLEdBQTZCLGVBQTdCLEdBQWtEO1VBQ2xFLGNBQUEsR0FBbUIsT0FBTyxDQUFDLE1BQVIsS0FBa0IsQ0FBckIsR0FBNEIsU0FBNUIsR0FBMkM7VUFFM0QsSUFBRyxPQUFPLENBQUMsTUFBUixHQUFpQixDQUFwQjtZQUNFLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixpQkFBMUIsRUFBZ0QsYUFBRCxHQUFlLEdBQWYsR0FBa0IsT0FBTyxDQUFDLE1BQTFCLEdBQWlDLEdBQWpDLEdBQW9DLGNBQXBDLEdBQW1ELFVBQW5ELEdBQTZELElBQUksQ0FBQyxVQUFsRSxHQUE2RSxHQUE3RSxHQUErRSxDQUFDLElBQUksQ0FBQyxpQkFBTCxDQUFBLENBQUQsQ0FBL0UsR0FBeUcsR0FBeEo7WUFDQSxJQUF1RSxJQUFJLENBQUMsU0FBTCxDQUFlLFdBQWYsQ0FBdkU7Y0FBQSxPQUFPLENBQUMsR0FBUixDQUFlLGFBQUQsR0FBZSxHQUFmLEdBQWtCLE9BQU8sQ0FBQyxNQUExQixHQUFpQyxHQUFqQyxHQUFvQyxjQUFsRCxFQUFBO2FBRkY7O1VBSUEsSUFBQSxDQUFjLE9BQWQ7QUFBQSxtQkFBQTs7aUJBQ0EsS0FBQyxDQUFBLE1BQUQsQ0FDRTtZQUFBLEtBQUEsRUFBTyxzQkFBUDtZQUNBLE9BQUEsRUFBUyxPQURUO1lBRUEsTUFBQSxFQUFRLHNCQUZSO1lBR0EsUUFBQSxFQUFVLHNCQUhWO1dBREY7UUFaYTtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBZjtJQURjLENBQWhCO0lBbUJBLGFBQUEsRUFBZSxTQUFDLFFBQUQ7QUFDYixVQUFBO01BQUEsT0FBQSxHQUFVLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBZCxDQUFBO01BRVYsaUJBQUEsR0FBb0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyx3QkFBZCxDQUFBO01BQ3BCLGdCQUFBLEdBQW1CLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFJLENBQUMsSUFBTixHQUFXLG1CQUE3QjtNQUNuQixnQkFBQSxHQUFtQixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBbUIsSUFBSSxDQUFDLElBQU4sR0FBVyxtQkFBN0I7TUFFbkIsSUFBQSxHQUFPLENBQUMsU0FBRDtNQUVQLElBQUcsZ0JBQWdCLENBQUMsTUFBakIsR0FBMEIsQ0FBN0I7UUFDRSxJQUE4QyxJQUFJLENBQUMsU0FBTCxDQUFlLFdBQWYsQ0FBOUM7VUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLDhCQUFaLEVBQUE7O0FBRUEsYUFBQSxrREFBQTs7VUFDRSxJQUFzQyxJQUFJLENBQUMsU0FBTCxDQUFlLFdBQWYsQ0FBdEM7WUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLElBQUEsR0FBSyxlQUFqQixFQUFBOztVQUNBLElBQUksQ0FBQyxJQUFMLENBQVUsZUFBVjtBQUZGLFNBSEY7T0FBQSxNQU9LLElBQUcsZ0JBQWdCLENBQUMsTUFBakIsR0FBMEIsQ0FBN0I7UUFDSCxJQUFnRCxJQUFJLENBQUMsU0FBTCxDQUFlLFdBQWYsQ0FBaEQ7VUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLGdDQUFaLEVBQUE7O0FBRUEsYUFBQSxvREFBQTs7VUFDRSxJQUFHLGFBQW1CLGlCQUFuQixFQUFBLGVBQUEsTUFBSDtZQUNFLElBQXNDLElBQUksQ0FBQyxTQUFMLENBQWUsV0FBZixDQUF0QztjQUFBLE9BQU8sQ0FBQyxHQUFSLENBQVksSUFBQSxHQUFLLGVBQWpCLEVBQUE7O1lBQ0EsS0FBQSxHQUFRLGlCQUFpQixDQUFDLE9BQWxCLENBQTBCLGVBQTFCO1lBQ1IsSUFBcUMsS0FBckM7Y0FBQSxpQkFBaUIsQ0FBQyxNQUFsQixDQUF5QixLQUF6QixFQUFnQyxDQUFoQyxFQUFBO2FBSEY7O0FBREYsU0FIRztPQUFBLE1BQUE7QUFVSCxhQUFBLHFEQUFBOztVQUNFLElBQUksQ0FBQyxJQUFMLENBQVUsZ0JBQVY7QUFERixTQVZHOztNQWFMLElBQUksQ0FBQyxJQUFMLENBQVUsY0FBVjtNQUNBLElBQUksQ0FBQyxJQUFMLENBQVUsWUFBVjtNQUVBLEdBQUEsR0FBTTtNQUVOLE1BQUEsR0FBUyxTQUFDLElBQUQ7ZUFDUCxHQUFBLElBQU87TUFEQTtNQUdULElBQUEsR0FBTyxTQUFDLFFBQUQ7ZUFDTCxRQUFBLENBQVMsR0FBVDtNQURLO2FBR0gsSUFBQSxlQUFBLENBQWdCO1FBQUMsU0FBQSxPQUFEO1FBQVUsTUFBQSxJQUFWO1FBQWdCLFFBQUEsTUFBaEI7UUFBd0IsTUFBQSxJQUF4QjtPQUFoQjtJQXhDUyxDQW5CZjtJQWdFQSxRQUFBLEVBQVUsU0FBQyxHQUFEO0FBQ1IsVUFBQTtNQUFBLEtBQUEsR0FBUSxHQUFHLENBQUMsS0FBSixDQUFVLElBQVY7QUFFUjtXQUFBLHVDQUFBOztRQUNFLE9BQUEsR0FBVSxJQUFJLENBQUMsS0FBTCxDQUFXLHlCQUFYO1FBQ1YsSUFBZ0IsZUFBaEI7QUFBQSxtQkFBQTs7UUFDQyxtQkFBRCxFQUFTLGlCQUFULEVBQWUsb0JBQWYsRUFBd0I7cUJBRXhCO1VBQUEsTUFBQSxFQUFRLElBQVI7VUFDQSxTQUFBLEVBQVcsT0FEWDtVQUVBLGFBQUEsRUFBZSxNQUFBLEtBQVUsUUFGekI7O0FBTEY7O0lBSFEsQ0FoRVY7SUE0RUEsZUFBQSxFQUFpQixTQUFDLE9BQUQsRUFBVSxZQUFWO0FBQ2YsVUFBQTs7UUFEeUIsZUFBZTs7TUFDeEMsaUJBQUEsR0FBb0IsT0FBTyxDQUFDLE1BQVIsQ0FBZSxTQUFDLEtBQUQ7ZUFDakMsS0FBSyxDQUFDO01BRDJCLENBQWY7TUFFcEIsSUFBQSxDQUFBLENBQW1CLGlCQUFpQixDQUFDLE1BQWxCLEdBQTJCLENBQTlDLENBQUE7QUFBQSxlQUFPLEtBQVA7O01BRUEsS0FBQSxHQUFRLGlCQUFpQixDQUFDLEdBQWxCLENBQXNCLFNBQUMsS0FBRDtlQUM1QixLQUFLLENBQUM7TUFEc0IsQ0FBdEI7TUFHUixPQUFBLEdBQ0ssaUJBQWlCLENBQUMsTUFBbEIsSUFBNEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFaLENBQW1CLElBQUksQ0FBQyxJQUFOLEdBQVcsdUJBQTdCLENBQS9CLEdBQ0UsSUFBQyxDQUFBLDZCQUFELENBQStCLEtBQS9CLENBREYsR0FHSyxpQkFBaUIsQ0FBQyxNQUFuQixHQUEwQjtNQUVoQyxPQUFBLElBQWMsaUJBQWlCLENBQUMsTUFBbEIsS0FBNEIsQ0FBL0IsR0FBc0MsTUFBdEMsR0FBa0Q7TUFDN0QsT0FBQSxJQUFXO01BQ1gsSUFBK0IsWUFBL0I7UUFBQSxPQUFBLElBQVcsaUJBQVg7O01BQ0EsT0FBQSxJQUFXO2FBQ1g7SUFsQmUsQ0E1RWpCO0lBZ0dBLDZCQUFBLEVBQStCLFNBQUMsS0FBRDtBQUM3QixVQUFBO01BQUEsVUFBQSxHQUFhO0FBRWIsV0FBQSx1REFBQTs7UUFDRSxJQUFHLEtBQUEsR0FBUSxDQUFYO1VBQ0UsSUFBRyxLQUFBLEdBQVEsQ0FBUixHQUFZLEtBQUssQ0FBQyxNQUFyQjtZQUNFLFVBQUEsSUFBYyxLQURoQjtXQUFBLE1BQUE7WUFHRSxVQUFBLElBQWMsUUFIaEI7V0FERjs7UUFNQSxVQUFBLElBQWM7QUFQaEI7YUFTQTtJQVo2QixDQWhHL0I7SUE4R0EsTUFBQSxFQUFRLFNBQUMsWUFBRDtBQUNOLFVBQUE7TUFBQSxJQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFJLENBQUMsSUFBTixHQUFXLHFCQUE3QixDQUFIO1FBQ0UsT0FBQSxDQUFRLE1BQVIsQ0FBZSxDQUFDLFNBQWhCLENBQTBCLGlCQUExQixFQUE2QyxtQkFBN0M7UUFDQSxJQUFvQyxJQUFJLENBQUMsU0FBTCxDQUFlLFdBQWYsQ0FBcEM7VUFBQSxPQUFPLENBQUMsR0FBUixDQUFZLG1CQUFaLEVBQUE7O1FBRUEsYUFBQSxHQUFnQjtVQUNkLE1BQUEsRUFBUSxZQUFZLENBQUMsT0FEUDtVQUVkLFdBQUEsRUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBWixDQUFtQixJQUFJLENBQUMsSUFBTixHQUFXLHNCQUE3QixDQUZBO1VBR2QsT0FBQSxFQUFTO1lBQ0w7Y0FDQSxJQUFBLEVBQU0sU0FETjtjQUVBLFNBQUEsRUFBVyxnQkFGWDtjQUdBLFVBQUEsRUFBWSxTQUFBO2dCQUNWLE9BQUEsQ0FBUSxNQUFSLENBQWUsQ0FBQyxTQUFoQixDQUEwQixpQkFBMUIsRUFBNkMscUJBQTdDO2dCQUNBLElBQXNDLElBQUksQ0FBQyxTQUFMLENBQWUsV0FBZixDQUF0QztrQkFBQSxPQUFPLENBQUMsR0FBUixDQUFZLHFCQUFaLEVBQUE7O3VCQUVBLElBQUksQ0FBQyxrQkFBTCxDQUFBO2NBSlUsQ0FIWjthQURLO1dBSEs7O2VBZ0JoQixJQUFJLENBQUMsWUFBTCxDQUFrQixJQUFBLEdBQUssSUFBSSxDQUFDLElBQVYsR0FBZSxJQUFqQyxFQUFzQyxhQUF0QyxFQXBCRjs7SUFETSxDQTlHUjs7QUFSRiIsInNvdXJjZXNDb250ZW50IjpbIm1ldGEgPSByZXF1aXJlIFwiLi4vcGFja2FnZS5qc29uXCJcblV0aWwgPSByZXF1aXJlIFwiLi91dGlsXCJcbntCdWZmZXJlZFByb2Nlc3N9ID0gcmVxdWlyZSBcImF0b21cIlxuXG5BVE9NX0JVTkRMRV9JREVOVElGSUVSID0gXCJjb20uZ2l0aHViLmF0b21cIlxuSU5TVEFMTEFUSU9OX0xJTkVfUEFUVEVSTiA9IC9eSW5zdGFsbGluZyArKFteQF0rKUAoXFxTKykuK1xccysoXFxTKykkL1xuXG5tb2R1bGUuZXhwb3J0cyA9XG4gIHVwZGF0ZVBhY2thZ2VzOiAoaXNBdXRvVXBkYXRlID0gdHJ1ZSkgLT5cbiAgICBAcnVuQXBtVXBncmFkZSAobG9nKSA9PlxuICAgICAgZW50cmllcyA9IEBwYXJzZUxvZyhsb2cpXG4gICAgICBzdW1tYXJ5ID0gQGdlbmVyYXRlU3VtbWFyeShlbnRyaWVzLCBpc0F1dG9VcGRhdGUpXG5cbiAgICAgIHVwZGF0ZVdvcmRpbmcgPSBpZiBpc0F1dG9VcGRhdGUgaXMgdHJ1ZSB0aGVuIFwiQXV0by11cGRhdGluZ1wiIGVsc2UgXCJVcGRhdGluZ1wiXG4gICAgICBwYWNrYWdlV29yZGluZyA9aWYgZW50cmllcy5sZW5ndGggaXMgMSB0aGVuIFwicGFja2FnZVwiIGVsc2UgXCJwYWNrYWdlc1wiXG5cbiAgICAgIGlmIGVudHJpZXMubGVuZ3RoID4gMFxuICAgICAgICByZXF1aXJlKFwiLi9nYVwiKS5zZW5kRXZlbnQgXCJwYWNrYWdlLXVwZGF0ZXJcIiwgXCIje3VwZGF0ZVdvcmRpbmd9ICN7ZW50cmllcy5sZW5ndGh9ICN7cGFja2FnZVdvcmRpbmd9IChBdG9tIHYje2F0b20uYXBwVmVyc2lvbn0gI3thdG9tLmdldFJlbGVhc2VDaGFubmVsKCl9KVwiXG4gICAgICAgIGNvbnNvbGUubG9nKFwiI3t1cGRhdGVXb3JkaW5nfSAje2VudHJpZXMubGVuZ3RofSAje3BhY2thZ2VXb3JkaW5nfVwiKSBpZiBVdGlsLmdldENvbmZpZyhcImRlYnVnTW9kZVwiKVxuXG4gICAgICByZXR1cm4gdW5sZXNzIHN1bW1hcnlcbiAgICAgIEBub3RpZnlcbiAgICAgICAgdGl0bGU6IFwiQXRvbSBQYWNrYWdlIFVwZGF0ZXNcIlxuICAgICAgICBtZXNzYWdlOiBzdW1tYXJ5XG4gICAgICAgIHNlbmRlcjogQVRPTV9CVU5ETEVfSURFTlRJRklFUlxuICAgICAgICBhY3RpdmF0ZTogQVRPTV9CVU5ETEVfSURFTlRJRklFUlxuXG4gIHJ1bkFwbVVwZ3JhZGU6IChjYWxsYmFjaykgLT5cbiAgICBjb21tYW5kID0gYXRvbS5wYWNrYWdlcy5nZXRBcG1QYXRoKClcblxuICAgIGF2YWlsYWJsZVBhY2thZ2VzID0gYXRvbS5wYWNrYWdlcy5nZXRBdmFpbGFibGVQYWNrYWdlTmFtZXMoKVxuICAgIGluY2x1ZGVkUGFja2FnZXMgPSBhdG9tLmNvbmZpZy5nZXQoXCIje21ldGEubmFtZX0uaW5jbHVkZWRQYWNrYWdlc1wiKVxuICAgIGV4Y2x1ZGVkUGFja2FnZXMgPSBhdG9tLmNvbmZpZy5nZXQoXCIje21ldGEubmFtZX0uZXhjbHVkZWRQYWNrYWdlc1wiKVxuXG4gICAgYXJncyA9IFtcInVwZ3JhZGVcIl1cblxuICAgIGlmIGluY2x1ZGVkUGFja2FnZXMubGVuZ3RoID4gMFxuICAgICAgY29uc29sZS5sb2cgXCJQYWNrYWdlcyBpbmNsdWRlZCBpbiB1cGRhdGU6XCIgaWYgVXRpbC5nZXRDb25maWcoXCJkZWJ1Z01vZGVcIilcblxuICAgICAgZm9yIGluY2x1ZGVkUGFja2FnZSBpbiBpbmNsdWRlZFBhY2thZ2VzXG4gICAgICAgIGNvbnNvbGUubG9nIFwiLSAje2luY2x1ZGVkUGFja2FnZX1cIiBpZiBVdGlsLmdldENvbmZpZyhcImRlYnVnTW9kZVwiKVxuICAgICAgICBhcmdzLnB1c2ggaW5jbHVkZWRQYWNrYWdlXG5cbiAgICBlbHNlIGlmIGV4Y2x1ZGVkUGFja2FnZXMubGVuZ3RoID4gMFxuICAgICAgY29uc29sZS5sb2cgXCJQYWNrYWdlcyBleGNsdWRlZCBmcm9tIHVwZGF0ZTpcIiBpZiBVdGlsLmdldENvbmZpZyhcImRlYnVnTW9kZVwiKVxuXG4gICAgICBmb3IgZXhjbHVkZWRQYWNrYWdlIGluIGV4Y2x1ZGVkUGFja2FnZXNcbiAgICAgICAgaWYgZXhjbHVkZWRQYWNrYWdlIGluIGF2YWlsYWJsZVBhY2thZ2VzXG4gICAgICAgICAgY29uc29sZS5sb2cgXCItICN7ZXhjbHVkZWRQYWNrYWdlfVwiIGlmIFV0aWwuZ2V0Q29uZmlnKFwiZGVidWdNb2RlXCIpXG4gICAgICAgICAgaW5kZXggPSBhdmFpbGFibGVQYWNrYWdlcy5pbmRleE9mIGV4Y2x1ZGVkUGFja2FnZVxuICAgICAgICAgIGF2YWlsYWJsZVBhY2thZ2VzLnNwbGljZSBpbmRleCwgMSBpZiBpbmRleFxuXG4gICAgZWxzZVxuICAgICAgZm9yIGF2YWlsYWJsZVBhY2thZ2UgaW4gYXZhaWxhYmxlUGFja2FnZXNcbiAgICAgICAgYXJncy5wdXNoIGF2YWlsYWJsZVBhY2thZ2VcblxuICAgIGFyZ3MucHVzaCBcIi0tbm8tY29uZmlybVwiXG4gICAgYXJncy5wdXNoIFwiLS1uby1jb2xvclwiXG5cbiAgICBsb2cgPSBcIlwiXG5cbiAgICBzdGRvdXQgPSAoZGF0YSkgLT5cbiAgICAgIGxvZyArPSBkYXRhXG5cbiAgICBleGl0ID0gKGV4aXRDb2RlKSAtPlxuICAgICAgY2FsbGJhY2sobG9nKVxuXG4gICAgbmV3IEJ1ZmZlcmVkUHJvY2Vzcyh7Y29tbWFuZCwgYXJncywgc3Rkb3V0LCBleGl0fSlcblxuICAjIFBhcnNpbmcgdGhlIG91dHB1dCBvZiBhcG0gaXMgYSBkaXJ0eSB3YXksIGJ1dCB1c2luZyBhdG9tLXBhY2thZ2UtbWFuYWdlciBkaXJlY3RseSB2aWEgSmF2YVNjcmlwdFxuICAjIGlzIHByb2JhYmx5IG1vcmUgYnJpdHRsZSB0aGFuIHBhcnNpbmcgdGhlIG91dHB1dCBzaW5jZSBpdFwicyBhIHByaXZhdGUgcGFja2FnZS5cbiAgIyAvQXBwbGljYXRpb25zL0F0b20uYXBwL0NvbnRlbnRzL1Jlc291cmNlcy9hcHAvYXBtL25vZGVfbW9kdWxlcy9hdG9tLXBhY2thZ2UtbWFuYWdlclxuICBwYXJzZUxvZzogKGxvZykgLT5cbiAgICBsaW5lcyA9IGxvZy5zcGxpdChcIlxcblwiKVxuXG4gICAgZm9yIGxpbmUgaW4gbGluZXNcbiAgICAgIG1hdGNoZXMgPSBsaW5lLm1hdGNoKElOU1RBTExBVElPTl9MSU5FX1BBVFRFUk4pXG4gICAgICBjb250aW51ZSB1bmxlc3MgbWF0Y2hlcz9cbiAgICAgIFtfbWF0Y2gsIG5hbWUsIHZlcnNpb24sIHJlc3VsdF0gPSBtYXRjaGVzXG5cbiAgICAgIFwibmFtZVwiOiBuYW1lXG4gICAgICBcInZlcnNpb25cIjogdmVyc2lvblxuICAgICAgXCJpc0luc3RhbGxlZFwiOiByZXN1bHQgPT0gXCJcXHUyNzEzXCJcblxuICBnZW5lcmF0ZVN1bW1hcnk6IChlbnRyaWVzLCBpc0F1dG9VcGRhdGUgPSB0cnVlKSAtPlxuICAgIHN1Y2Nlc3NmdWxFbnRyaWVzID0gZW50cmllcy5maWx0ZXIgKGVudHJ5KSAtPlxuICAgICAgZW50cnkuaXNJbnN0YWxsZWRcbiAgICByZXR1cm4gbnVsbCB1bmxlc3Mgc3VjY2Vzc2Z1bEVudHJpZXMubGVuZ3RoID4gMFxuXG4gICAgbmFtZXMgPSBzdWNjZXNzZnVsRW50cmllcy5tYXAgKGVudHJ5KSAtPlxuICAgICAgZW50cnkubmFtZVxuXG4gICAgc3VtbWFyeSA9XG4gICAgICBpZiBzdWNjZXNzZnVsRW50cmllcy5sZW5ndGggPD0gYXRvbS5jb25maWcuZ2V0KFwiI3ttZXRhLm5hbWV9Lm1heGltdW1QYWNrYWdlRGV0YWlsXCIpXG4gICAgICAgIEBnZW5lcmF0ZUVudW1lcmF0aW9uRXhwcmVzc2lvbihuYW1lcylcbiAgICAgIGVsc2VcbiAgICAgICAgXCIje3N1Y2Nlc3NmdWxFbnRyaWVzLmxlbmd0aH0gcGFja2FnZXNcIlxuXG4gICAgc3VtbWFyeSArPSBpZiBzdWNjZXNzZnVsRW50cmllcy5sZW5ndGggPT0gMSB0aGVuIFwiIGhhc1wiIGVsc2UgXCIgaGF2ZVwiXG4gICAgc3VtbWFyeSArPSBcIiBiZWVuIHVwZGF0ZWRcIlxuICAgIHN1bW1hcnkgKz0gXCIgYXV0b21hdGljYWxseVwiIGlmIGlzQXV0b1VwZGF0ZVxuICAgIHN1bW1hcnkgKz0gXCIuXCJcbiAgICBzdW1tYXJ5XG5cbiAgZ2VuZXJhdGVFbnVtZXJhdGlvbkV4cHJlc3Npb246IChpdGVtcykgLT5cbiAgICBleHByZXNzaW9uID0gXCJcIlxuXG4gICAgZm9yIGl0ZW0sIGluZGV4IGluIGl0ZW1zXG4gICAgICBpZiBpbmRleCA+IDBcbiAgICAgICAgaWYgaW5kZXggKyAxIDwgaXRlbXMubGVuZ3RoXG4gICAgICAgICAgZXhwcmVzc2lvbiArPSBcIiwgXCJcbiAgICAgICAgZWxzZVxuICAgICAgICAgIGV4cHJlc3Npb24gKz0gXCIgYW5kIFwiXG5cbiAgICAgIGV4cHJlc3Npb24gKz0gaXRlbVxuXG4gICAgZXhwcmVzc2lvblxuXG4gIG5vdGlmeTogKG5vdGlmaWNhdGlvbikgLT5cbiAgICBpZiBhdG9tLmNvbmZpZy5nZXQoXCIje21ldGEubmFtZX0udXBkYXRlTm90aWZpY2F0aW9uXCIpXG4gICAgICByZXF1aXJlKFwiLi9nYVwiKS5zZW5kRXZlbnQgXCJwYWNrYWdlLXVwZGF0ZXJcIiwgXCJTaG93IE5vdGlmaWNhdGlvblwiXG4gICAgICBjb25zb2xlLmxvZyhcIlNob3cgTm90aWZpY2F0aW9uXCIpIGlmIFV0aWwuZ2V0Q29uZmlnKFwiZGVidWdNb2RlXCIpXG5cbiAgICAgIG5vdGlmeU9wdGlvbnMgPSB7XG4gICAgICAgIGRldGFpbDogbm90aWZpY2F0aW9uLm1lc3NhZ2VcbiAgICAgICAgZGlzbWlzc2FibGU6ICFhdG9tLmNvbmZpZy5nZXQoXCIje21ldGEubmFtZX0uZGlzbWlzc05vdGlmaWNhdGlvblwiKVxuICAgICAgICBidXR0b25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICB0ZXh0OiBcIlJlc3RhcnRcIlxuICAgICAgICAgICAgY2xhc3NOYW1lOiBcImljb24gaWNvbi1zeW5jXCJcbiAgICAgICAgICAgIG9uRGlkQ2xpY2s6IC0+XG4gICAgICAgICAgICAgIHJlcXVpcmUoXCIuL2dhXCIpLnNlbmRFdmVudCBcInBhY2thZ2UtdXBkYXRlclwiLCBcIlJlc3RhcnQgQXBwbGljYXRpb25cIlxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlJlc3RhcnQgQXBwbGljYXRpb25cIikgaWYgVXRpbC5nZXRDb25maWcoXCJkZWJ1Z01vZGVcIilcblxuICAgICAgICAgICAgICBhdG9tLnJlc3RhcnRBcHBsaWNhdGlvbigpXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9XG5cbiAgICAgIFV0aWwubm90aWZpY2F0aW9uKFwiKioje21ldGEubmFtZX0qKlwiLCBub3RpZnlPcHRpb25zKVxuIl19
