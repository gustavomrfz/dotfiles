(function() {
  module.exports = {
    selector: ['.source.puppet'],
    id: 'aligner-puppet',
    config: {
      '=>-alignment': {
        title: 'Padding for =>',
        description: 'Pad left or right of the character',
        type: 'string',
        "enum": ['left', 'right'],
        "default": 'left'
      },
      '=>-leftSpace': {
        title: 'Left space for =>',
        description: 'Add 1 whitespace to the left',
        type: 'boolean',
        "default": true
      },
      '=>-rightSpace': {
        title: 'Right space for =>',
        description: 'Add 1 whitespace to the right',
        type: 'boolean',
        "default": true
      },
      '=-alignment': {
        title: 'Padding for =',
        description: 'Pad left or right of the character',
        type: 'string',
        "enum": ['left', 'right'],
        "default": 'left'
      }
    },
    privateConfig: {
      '=-scope': 'assignment',
      '=-leftSpace': true,
      '=-rightSpace': true
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9hbGlnbmVyLXB1cHBldC9saWIvcHJvdmlkZXIuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQUEsTUFBTSxDQUFDLE9BQVAsR0FDRTtJQUFBLFFBQUEsRUFBVSxDQUFDLGdCQUFELENBQVY7SUFDQSxFQUFBLEVBQUksZ0JBREo7SUFFQSxNQUFBLEVBQ0U7TUFBQSxjQUFBLEVBQ0U7UUFBQSxLQUFBLEVBQU8sZ0JBQVA7UUFDQSxXQUFBLEVBQWEsb0NBRGI7UUFFQSxJQUFBLEVBQU0sUUFGTjtRQUdBLENBQUEsSUFBQSxDQUFBLEVBQU0sQ0FBQyxNQUFELEVBQVMsT0FBVCxDQUhOO1FBSUEsQ0FBQSxPQUFBLENBQUEsRUFBUyxNQUpUO09BREY7TUFNQSxjQUFBLEVBQ0U7UUFBQSxLQUFBLEVBQU8sbUJBQVA7UUFDQSxXQUFBLEVBQWEsOEJBRGI7UUFFQSxJQUFBLEVBQU0sU0FGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsSUFIVDtPQVBGO01BV0EsZUFBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLG9CQUFQO1FBQ0EsV0FBQSxFQUFhLCtCQURiO1FBRUEsSUFBQSxFQUFNLFNBRk47UUFHQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLElBSFQ7T0FaRjtNQWdCQSxhQUFBLEVBQ0U7UUFBQSxLQUFBLEVBQU8sZUFBUDtRQUNBLFdBQUEsRUFBYSxvQ0FEYjtRQUVBLElBQUEsRUFBTSxRQUZOO1FBR0EsQ0FBQSxJQUFBLENBQUEsRUFBTSxDQUFDLE1BQUQsRUFBUyxPQUFULENBSE47UUFJQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLE1BSlQ7T0FqQkY7S0FIRjtJQXlCQSxhQUFBLEVBQ0U7TUFBQSxTQUFBLEVBQVcsWUFBWDtNQUNBLGFBQUEsRUFBZSxJQURmO01BRUEsY0FBQSxFQUFnQixJQUZoQjtLQTFCRjs7QUFERiIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID1cbiAgc2VsZWN0b3I6IFsnLnNvdXJjZS5wdXBwZXQnXVxuICBpZDogJ2FsaWduZXItcHVwcGV0JyAjIHBhY2thZ2UgbmFtZVxuICBjb25maWc6XG4gICAgJz0+LWFsaWdubWVudCc6XG4gICAgICB0aXRsZTogJ1BhZGRpbmcgZm9yID0+J1xuICAgICAgZGVzY3JpcHRpb246ICdQYWQgbGVmdCBvciByaWdodCBvZiB0aGUgY2hhcmFjdGVyJ1xuICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgIGVudW06IFsnbGVmdCcsICdyaWdodCddXG4gICAgICBkZWZhdWx0OiAnbGVmdCdcbiAgICAnPT4tbGVmdFNwYWNlJzpcbiAgICAgIHRpdGxlOiAnTGVmdCBzcGFjZSBmb3IgPT4nXG4gICAgICBkZXNjcmlwdGlvbjogJ0FkZCAxIHdoaXRlc3BhY2UgdG8gdGhlIGxlZnQnXG4gICAgICB0eXBlOiAnYm9vbGVhbidcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICAnPT4tcmlnaHRTcGFjZSc6XG4gICAgICB0aXRsZTogJ1JpZ2h0IHNwYWNlIGZvciA9PidcbiAgICAgIGRlc2NyaXB0aW9uOiAnQWRkIDEgd2hpdGVzcGFjZSB0byB0aGUgcmlnaHQnXG4gICAgICB0eXBlOiAnYm9vbGVhbidcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICAnPS1hbGlnbm1lbnQnOlxuICAgICAgdGl0bGU6ICdQYWRkaW5nIGZvciA9J1xuICAgICAgZGVzY3JpcHRpb246ICdQYWQgbGVmdCBvciByaWdodCBvZiB0aGUgY2hhcmFjdGVyJ1xuICAgICAgdHlwZTogJ3N0cmluZydcbiAgICAgIGVudW06IFsnbGVmdCcsICdyaWdodCddXG4gICAgICBkZWZhdWx0OiAnbGVmdCdcbiAgcHJpdmF0ZUNvbmZpZzpcbiAgICAnPS1zY29wZSc6ICdhc3NpZ25tZW50J1xuICAgICc9LWxlZnRTcGFjZSc6IHRydWVcbiAgICAnPS1yaWdodFNwYWNlJzogdHJ1ZVxuIl19
