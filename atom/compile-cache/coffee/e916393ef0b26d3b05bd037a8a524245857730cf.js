(function() {
  module.exports = {
    config: {
      packageManager: {
        title: "Package Manager",
        description: "Pick your preferred package manager for installing",
        type: "string",
        "default": "yarn",
        "enum": ["apm", "pnpm", "yarn"],
        order: 0
      },
      showPrompt: {
        title: "Show Prompt",
        description: "Displays an prompt before installing Atom packages",
        type: "boolean",
        "default": false,
        order: 1
      },
      verboseMode: {
        title: "Verbose Mode",
        description: "Output progress to the console",
        type: "boolean",
        "default": false,
        order: 2
      },
      manageDependencies: {
        title: "Manage Dependencies",
        description: "When enabled, third-party dependencies will be installed automatically",
        type: "boolean",
        "default": true,
        order: 3
      }
    },
    subscriptions: null,
    activate: function() {
      var CompositeDisposable, satisfy;
      CompositeDisposable = require("atom").CompositeDisposable;
      satisfy = require("./dependencies").satisfy;
      this.subscriptions = new CompositeDisposable;
      this.subscriptions.add(atom.commands.add("atom-workspace", {
        "satisfy-dependencies:all": function() {
          return satisfy(true, true);
        }
      }));
      this.subscriptions.add(atom.commands.add("atom-workspace", {
        "satisfy-dependencies:atom-packages": function() {
          return satisfy(true, false);
        }
      }));
      return this.subscriptions.add(atom.commands.add("atom-workspace", {
        "satisfy-dependencies:node-packages": function() {
          return satisfy(false, true);
        }
      }));
    },
    deactivate: function() {
      var ref;
      if ((ref = this.subscriptions) != null) {
        ref.dispose();
      }
      return this.subscriptions = null;
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9zYXRpc2Z5LWRlcGVuZGVuY2llcy9saWIvbWFpbi5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBQSxNQUFNLENBQUMsT0FBUCxHQUNFO0lBQUEsTUFBQSxFQUNFO01BQUEsY0FBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLGlCQUFQO1FBQ0EsV0FBQSxFQUFhLG9EQURiO1FBRUEsSUFBQSxFQUFNLFFBRk47UUFHQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLE1BSFQ7UUFJQSxDQUFBLElBQUEsQ0FBQSxFQUFNLENBQ0osS0FESSxFQUVKLE1BRkksRUFHSixNQUhJLENBSk47UUFTQSxLQUFBLEVBQU8sQ0FUUDtPQURGO01BV0EsVUFBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLGFBQVA7UUFDQSxXQUFBLEVBQWEsb0RBRGI7UUFFQSxJQUFBLEVBQU0sU0FGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsS0FIVDtRQUlBLEtBQUEsRUFBTyxDQUpQO09BWkY7TUFpQkEsV0FBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLGNBQVA7UUFDQSxXQUFBLEVBQWEsZ0NBRGI7UUFFQSxJQUFBLEVBQU0sU0FGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsS0FIVDtRQUlBLEtBQUEsRUFBTyxDQUpQO09BbEJGO01BdUJBLGtCQUFBLEVBQ0U7UUFBQSxLQUFBLEVBQU8scUJBQVA7UUFDQSxXQUFBLEVBQWEsd0VBRGI7UUFFQSxJQUFBLEVBQU0sU0FGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsSUFIVDtRQUlBLEtBQUEsRUFBTyxDQUpQO09BeEJGO0tBREY7SUE4QkEsYUFBQSxFQUFlLElBOUJmO0lBZ0NBLFFBQUEsRUFBVSxTQUFBO0FBQ1IsVUFBQTtNQUFFLHNCQUF3QixPQUFBLENBQVEsTUFBUjtNQUN4QixVQUFZLE9BQUEsQ0FBUSxnQkFBUjtNQUdkLElBQUMsQ0FBQSxhQUFELEdBQWlCLElBQUk7TUFHckIsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0M7UUFBQSwwQkFBQSxFQUE0QixTQUFBO2lCQUFHLE9BQUEsQ0FBUSxJQUFSLEVBQWMsSUFBZDtRQUFILENBQTVCO09BQXBDLENBQW5CO01BQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0M7UUFBQSxvQ0FBQSxFQUFzQyxTQUFBO2lCQUFHLE9BQUEsQ0FBUSxJQUFSLEVBQWMsS0FBZDtRQUFILENBQXRDO09BQXBDLENBQW5CO2FBQ0EsSUFBQyxDQUFBLGFBQWEsQ0FBQyxHQUFmLENBQW1CLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBZCxDQUFrQixnQkFBbEIsRUFBb0M7UUFBQSxvQ0FBQSxFQUFzQyxTQUFBO2lCQUFHLE9BQUEsQ0FBUSxLQUFSLEVBQWUsSUFBZjtRQUFILENBQXRDO09BQXBDLENBQW5CO0lBVlEsQ0FoQ1Y7SUE0Q0EsVUFBQSxFQUFZLFNBQUE7QUFDVixVQUFBOztXQUFjLENBQUUsT0FBaEIsQ0FBQTs7YUFDQSxJQUFDLENBQUEsYUFBRCxHQUFpQjtJQUZQLENBNUNaOztBQURGIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPVxuICBjb25maWc6XG4gICAgcGFja2FnZU1hbmFnZXI6XG4gICAgICB0aXRsZTogXCJQYWNrYWdlIE1hbmFnZXJcIlxuICAgICAgZGVzY3JpcHRpb246IFwiUGljayB5b3VyIHByZWZlcnJlZCBwYWNrYWdlIG1hbmFnZXIgZm9yIGluc3RhbGxpbmdcIlxuICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgIGRlZmF1bHQ6IFwieWFyblwiLFxuICAgICAgZW51bTogW1xuICAgICAgICBcImFwbVwiLFxuICAgICAgICBcInBucG1cIixcbiAgICAgICAgXCJ5YXJuXCJcbiAgICAgIF0sXG4gICAgICBvcmRlcjogMFxuICAgIHNob3dQcm9tcHQ6XG4gICAgICB0aXRsZTogXCJTaG93IFByb21wdFwiXG4gICAgICBkZXNjcmlwdGlvbjogXCJEaXNwbGF5cyBhbiBwcm9tcHQgYmVmb3JlIGluc3RhbGxpbmcgQXRvbSBwYWNrYWdlc1wiXG4gICAgICB0eXBlOiBcImJvb2xlYW5cIlxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICAgIG9yZGVyOiAxXG4gICAgdmVyYm9zZU1vZGU6XG4gICAgICB0aXRsZTogXCJWZXJib3NlIE1vZGVcIlxuICAgICAgZGVzY3JpcHRpb246IFwiT3V0cHV0IHByb2dyZXNzIHRvIHRoZSBjb25zb2xlXCJcbiAgICAgIHR5cGU6IFwiYm9vbGVhblwiXG4gICAgICBkZWZhdWx0OiBmYWxzZVxuICAgICAgb3JkZXI6IDJcbiAgICBtYW5hZ2VEZXBlbmRlbmNpZXM6XG4gICAgICB0aXRsZTogXCJNYW5hZ2UgRGVwZW5kZW5jaWVzXCJcbiAgICAgIGRlc2NyaXB0aW9uOiBcIldoZW4gZW5hYmxlZCwgdGhpcmQtcGFydHkgZGVwZW5kZW5jaWVzIHdpbGwgYmUgaW5zdGFsbGVkIGF1dG9tYXRpY2FsbHlcIlxuICAgICAgdHlwZTogXCJib29sZWFuXCJcbiAgICAgIGRlZmF1bHQ6IHRydWVcbiAgICAgIG9yZGVyOiAzXG4gIHN1YnNjcmlwdGlvbnM6IG51bGxcblxuICBhY3RpdmF0ZTogLT5cbiAgICB7IENvbXBvc2l0ZURpc3Bvc2FibGUgfSA9IHJlcXVpcmUgXCJhdG9tXCJcbiAgICB7IHNhdGlzZnkgfSA9IHJlcXVpcmUgXCIuL2RlcGVuZGVuY2llc1wiXG5cbiAgICAjIEV2ZW50cyBzdWJzY3JpYmVkIHRvIGluIGF0b21cInMgc3lzdGVtIGNhbiBiZSBlYXNpbHkgY2xlYW5lZCB1cCB3aXRoIGEgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIEBzdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGVcblxuICAgICMgUmVnaXN0ZXIgY29tbWFuZCB0aGF0IHRvZ2dsZXMgdGhpcyB2aWV3XG4gICAgQHN1YnNjcmlwdGlvbnMuYWRkIGF0b20uY29tbWFuZHMuYWRkIFwiYXRvbS13b3Jrc3BhY2VcIiwgXCJzYXRpc2Z5LWRlcGVuZGVuY2llczphbGxcIjogLT4gc2F0aXNmeSh0cnVlLCB0cnVlKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCBcImF0b20td29ya3NwYWNlXCIsIFwic2F0aXNmeS1kZXBlbmRlbmNpZXM6YXRvbS1wYWNrYWdlc1wiOiAtPiBzYXRpc2Z5KHRydWUsIGZhbHNlKVxuICAgIEBzdWJzY3JpcHRpb25zLmFkZCBhdG9tLmNvbW1hbmRzLmFkZCBcImF0b20td29ya3NwYWNlXCIsIFwic2F0aXNmeS1kZXBlbmRlbmNpZXM6bm9kZS1wYWNrYWdlc1wiOiAtPiBzYXRpc2Z5KGZhbHNlLCB0cnVlKVxuXG4gIGRlYWN0aXZhdGU6IC0+XG4gICAgQHN1YnNjcmlwdGlvbnM/LmRpc3Bvc2UoKVxuICAgIEBzdWJzY3JpcHRpb25zID0gbnVsbFxuIl19
