(function() {
  var SublimePanesView;

  module.exports = SublimePanesView = (function() {
    function SublimePanesView(serializedState) {
      var message;
      this.element = document.createElement('div');
      this.element.classList.add('sublime-panes');
      message = document.createElement('div');
      message.textContent = "The SublimePanes package is Alive! It's ALIVE!";
      message.classList.add('message');
      this.element.appendChild(message);
    }

    SublimePanesView.prototype.serialize = function() {};

    SublimePanesView.prototype.destroy = function() {
      return this.element.remove();
    };

    SublimePanesView.prototype.getElement = function() {
      return this.element;
    };

    return SublimePanesView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9zdWJsaW1lLXBhbmVzL2xpYi9zdWJsaW1lLXBhbmVzLXZpZXcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsMEJBQUMsZUFBRDtBQUVYLFVBQUE7TUFBQSxJQUFDLENBQUEsT0FBRCxHQUFXLFFBQVEsQ0FBQyxhQUFULENBQXVCLEtBQXZCO01BQ1gsSUFBQyxDQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbkIsQ0FBdUIsZUFBdkI7TUFHQSxPQUFBLEdBQVUsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDVixPQUFPLENBQUMsV0FBUixHQUFzQjtNQUN0QixPQUFPLENBQUMsU0FBUyxDQUFDLEdBQWxCLENBQXNCLFNBQXRCO01BQ0EsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLE9BQXJCO0lBVFc7OytCQVliLFNBQUEsR0FBVyxTQUFBLEdBQUE7OytCQUdYLE9BQUEsR0FBUyxTQUFBO2FBQ1AsSUFBQyxDQUFBLE9BQU8sQ0FBQyxNQUFULENBQUE7SUFETzs7K0JBR1QsVUFBQSxHQUFZLFNBQUE7YUFDVixJQUFDLENBQUE7SUFEUzs7Ozs7QUFwQmQiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBTdWJsaW1lUGFuZXNWaWV3XG4gIGNvbnN0cnVjdG9yOiAoc2VyaWFsaXplZFN0YXRlKSAtPlxuICAgICMgQ3JlYXRlIHJvb3QgZWxlbWVudFxuICAgIEBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBAZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdzdWJsaW1lLXBhbmVzJylcblxuICAgICMgQ3JlYXRlIG1lc3NhZ2UgZWxlbWVudFxuICAgIG1lc3NhZ2UgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIG1lc3NhZ2UudGV4dENvbnRlbnQgPSBcIlRoZSBTdWJsaW1lUGFuZXMgcGFja2FnZSBpcyBBbGl2ZSEgSXQncyBBTElWRSFcIlxuICAgIG1lc3NhZ2UuY2xhc3NMaXN0LmFkZCgnbWVzc2FnZScpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQobWVzc2FnZSlcblxuICAjIFJldHVybnMgYW4gb2JqZWN0IHRoYXQgY2FuIGJlIHJldHJpZXZlZCB3aGVuIHBhY2thZ2UgaXMgYWN0aXZhdGVkXG4gIHNlcmlhbGl6ZTogLT5cblxuICAjIFRlYXIgZG93biBhbnkgc3RhdGUgYW5kIGRldGFjaFxuICBkZXN0cm95OiAtPlxuICAgIEBlbGVtZW50LnJlbW92ZSgpXG5cbiAgZ2V0RWxlbWVudDogLT5cbiAgICBAZWxlbWVudFxuIl19
