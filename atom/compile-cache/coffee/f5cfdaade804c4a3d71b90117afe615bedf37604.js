(function() {
  var MINIMUM_AUTO_UPDATE_BLOCK_DURATION_MINUTES, PackageUpdater, Util, WARMUP_WAIT, meta;

  meta = require("../package.json");

  Util = require("./util");

  PackageUpdater = null;

  WARMUP_WAIT = 10 * 1000;

  MINIMUM_AUTO_UPDATE_BLOCK_DURATION_MINUTES = 15;

  module.exports = {
    config: {
      includedPackages: {
        title: "Included Packages",
        description: "Comma-delimited list of packages to be included from automatic updates",
        type: "array",
        "default": [],
        order: 1
      },
      excludedPackages: {
        title: "Excluded Packages",
        description: "Comma-delimited list of packages to be excluded from automatic updates",
        type: "array",
        "default": [],
        order: 2
      },
      intervalMinutes: {
        title: "Update Interval",
        description: "Set the default update interval in minutes",
        type: "integer",
        minimum: MINIMUM_AUTO_UPDATE_BLOCK_DURATION_MINUTES,
        "default": 6 * 60,
        order: 3
      },
      updateNotification: {
        title: "Notify on Update",
        description: "Enable to show notifications when packages have been updated",
        type: "boolean",
        "default": true,
        order: 4
      },
      dismissNotification: {
        title: "Dismiss Notification",
        description: "Automatically dismiss the update notification after 5 seconds",
        type: "boolean",
        "default": true,
        order: 5
      },
      notificationStyle: {
        title: "Notification Style",
        description: "Specify a style for the notification popup (*Success=green*, *Info=blue*, *Warning=yellow*, and *Error=red*)",
        type: "string",
        "enum": ["Success", "Info", "Warning", "Error"],
        "default": "Success",
        order: 6
      },
      maximumPackageDetail: {
        title: "Maximum Package Detail",
        description: "Specify the maximum number of package names displayed in the notification (minimum is 3)",
        type: "number",
        "default": 5,
        minimum: 3,
        order: 7
      },
      debugMode: {
        title: "Debug Mode",
        description: "Enable to output details in your console",
        type: "boolean",
        "default": false,
        order: 8
      }
    },
    activate: function(state) {
      var commands;
      commands = {};
      commands[meta.name + ":update-now"] = (function(_this) {
        return function() {
          return _this.updatePackages(false);
        };
      })(this);
      this.commandSubscription = atom.commands.add("atom-workspace", commands);
      return setTimeout((function(_this) {
        return function() {
          return _this.enableAutoUpdate();
        };
      })(this), WARMUP_WAIT);
    },
    deactivate: function() {
      var ref;
      this.disableAutoUpdate();
      if ((ref = this.commandSubscription) != null) {
        ref.dispose();
      }
      return this.commandSubscription = null;
    },
    enableAutoUpdate: function() {
      this.updatePackagesIfAutoUpdateBlockIsExpired();
      this.autoUpdateCheck = setInterval((function(_this) {
        return function() {
          return _this.updatePackagesIfAutoUpdateBlockIsExpired();
        };
      })(this), this.getAutoUpdateCheckInterval());
      return this.configSubscription = atom.config.onDidChange(meta.name + ".intervalMinutes", (function(_this) {
        return function(arg) {
          var newValue, oldValue;
          newValue = arg.newValue, oldValue = arg.oldValue;
          if (Util.getConfig("debugMode")) {
            console.log("Changed update interval to " + newValue);
          }
          _this.disableAutoUpdate();
          return _this.enableAutoUpdate();
        };
      })(this));
    },
    disableAutoUpdate: function() {
      var ref;
      if ((ref = this.configSubscription) != null) {
        ref.dispose();
      }
      this.configSubscription = null;
      if (this.autoUpdateCheck) {
        clearInterval(this.autoUpdateCheck);
      }
      return this.autoUpdateCheck = null;
    },
    updatePackagesIfAutoUpdateBlockIsExpired: function() {
      var lastUpdateTime;
      lastUpdateTime = this.loadLastUpdateTime() || 0;
      if (Date.now() > lastUpdateTime + this.getAutoUpdateBlockDuration()) {
        return this.updatePackages();
      }
    },
    updatePackages: function(isAutoUpdate) {
      if (isAutoUpdate == null) {
        isAutoUpdate = true;
      }
      if (PackageUpdater == null) {
        PackageUpdater = require("./package-updater");
      }
      PackageUpdater.updatePackages(isAutoUpdate);
      return this.saveLastUpdateTime();
    },
    getAutoUpdateBlockDuration: function() {
      var minutes;
      minutes = atom.config.get(meta.name + ".intervalMinutes");
      if (isNaN(minutes) || minutes < MINIMUM_AUTO_UPDATE_BLOCK_DURATION_MINUTES) {
        minutes = MINIMUM_AUTO_UPDATE_BLOCK_DURATION_MINUTES;
      }
      return minutes * 60 * 1000;
    },
    getAutoUpdateCheckInterval: function() {
      return this.getAutoUpdateBlockDuration() / 15;
    },
    loadLastUpdateTime: function() {
      var lastUpdateTime;
      try {
        lastUpdateTime = localStorage.getItem(meta.name + ".lastUpdateTime");
        return parseInt(lastUpdateTime);
      } catch (error) {
        localStorage.setItem(meta.name + ".lastUpdateTime", Date.now());
        return null;
      }
    },
    saveLastUpdateTime: function() {
      return localStorage.setItem(meta.name + ".lastUpdateTime", Date.now().toString());
    }
  };

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9hdXRvLXVwZGF0ZS1wbHVzL2xpYi9tYWluLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUEsSUFBQSxHQUFPLE9BQUEsQ0FBUSxpQkFBUjs7RUFDUCxJQUFBLEdBQU8sT0FBQSxDQUFRLFFBQVI7O0VBQ1AsY0FBQSxHQUFpQjs7RUFFakIsV0FBQSxHQUFjLEVBQUEsR0FBSzs7RUFDbkIsMENBQUEsR0FBNkM7O0VBRTdDLE1BQU0sQ0FBQyxPQUFQLEdBQ0U7SUFBQSxNQUFBLEVBQ0U7TUFBQSxnQkFBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLG1CQUFQO1FBQ0EsV0FBQSxFQUFhLHdFQURiO1FBRUEsSUFBQSxFQUFNLE9BRk47UUFHQSxDQUFBLE9BQUEsQ0FBQSxFQUFTLEVBSFQ7UUFJQSxLQUFBLEVBQU8sQ0FKUDtPQURGO01BTUEsZ0JBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxtQkFBUDtRQUNBLFdBQUEsRUFBYSx3RUFEYjtRQUVBLElBQUEsRUFBTSxPQUZOO1FBR0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxFQUhUO1FBSUEsS0FBQSxFQUFPLENBSlA7T0FQRjtNQVlBLGVBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxpQkFBUDtRQUNBLFdBQUEsRUFBYSw0Q0FEYjtRQUVBLElBQUEsRUFBTSxTQUZOO1FBR0EsT0FBQSxFQUFTLDBDQUhUO1FBSUEsQ0FBQSxPQUFBLENBQUEsRUFBUyxDQUFBLEdBQUksRUFKYjtRQUtBLEtBQUEsRUFBTyxDQUxQO09BYkY7TUFtQkEsa0JBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxrQkFBUDtRQUNBLFdBQUEsRUFBYSw4REFEYjtRQUVBLElBQUEsRUFBTSxTQUZOO1FBR0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxJQUhUO1FBSUEsS0FBQSxFQUFPLENBSlA7T0FwQkY7TUF5QkEsbUJBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxzQkFBUDtRQUNBLFdBQUEsRUFBYSwrREFEYjtRQUVBLElBQUEsRUFBTSxTQUZOO1FBR0EsQ0FBQSxPQUFBLENBQUEsRUFBUyxJQUhUO1FBSUEsS0FBQSxFQUFPLENBSlA7T0ExQkY7TUErQkEsaUJBQUEsRUFDRTtRQUFBLEtBQUEsRUFBTyxvQkFBUDtRQUNBLFdBQUEsRUFBYSw4R0FEYjtRQUVBLElBQUEsRUFBTSxRQUZOO1FBR0EsQ0FBQSxJQUFBLENBQUEsRUFBTSxDQUNKLFNBREksRUFFSixNQUZJLEVBR0osU0FISSxFQUlKLE9BSkksQ0FITjtRQVNBLENBQUEsT0FBQSxDQUFBLEVBQVMsU0FUVDtRQVVBLEtBQUEsRUFBTyxDQVZQO09BaENGO01BMkNBLG9CQUFBLEVBQ0U7UUFBQSxLQUFBLEVBQU8sd0JBQVA7UUFDQSxXQUFBLEVBQWEsMEZBRGI7UUFFQSxJQUFBLEVBQU0sUUFGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsQ0FIVDtRQUlBLE9BQUEsRUFBUyxDQUpUO1FBS0EsS0FBQSxFQUFPLENBTFA7T0E1Q0Y7TUFrREEsU0FBQSxFQUNFO1FBQUEsS0FBQSxFQUFPLFlBQVA7UUFDQSxXQUFBLEVBQWEsMENBRGI7UUFFQSxJQUFBLEVBQU0sU0FGTjtRQUdBLENBQUEsT0FBQSxDQUFBLEVBQVMsS0FIVDtRQUlBLEtBQUEsRUFBTyxDQUpQO09BbkRGO0tBREY7SUEwREEsUUFBQSxFQUFVLFNBQUMsS0FBRDtBQUNSLFVBQUE7TUFBQSxRQUFBLEdBQVc7TUFDWCxRQUFTLENBQUcsSUFBSSxDQUFDLElBQU4sR0FBVyxhQUFiLENBQVQsR0FBc0MsQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUFHLEtBQUMsQ0FBQSxjQUFELENBQWdCLEtBQWhCO1FBQUg7TUFBQSxDQUFBLENBQUEsQ0FBQSxJQUFBO01BQ3RDLElBQUMsQ0FBQSxtQkFBRCxHQUF1QixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQWQsQ0FBa0IsZ0JBQWxCLEVBQW9DLFFBQXBDO2FBRXZCLFVBQUEsQ0FBVyxDQUFBLFNBQUEsS0FBQTtlQUFBLFNBQUE7aUJBQ1QsS0FBQyxDQUFBLGdCQUFELENBQUE7UUFEUztNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWCxFQUVFLFdBRkY7SUFMUSxDQTFEVjtJQW1FQSxVQUFBLEVBQVksU0FBQTtBQUNWLFVBQUE7TUFBQSxJQUFDLENBQUEsaUJBQUQsQ0FBQTs7V0FDb0IsQ0FBRSxPQUF0QixDQUFBOzthQUNBLElBQUMsQ0FBQSxtQkFBRCxHQUF1QjtJQUhiLENBbkVaO0lBd0VBLGdCQUFBLEVBQWtCLFNBQUE7TUFDaEIsSUFBQyxDQUFBLHdDQUFELENBQUE7TUFFQSxJQUFDLENBQUEsZUFBRCxHQUFtQixXQUFBLENBQVksQ0FBQSxTQUFBLEtBQUE7ZUFBQSxTQUFBO2lCQUM3QixLQUFDLENBQUEsd0NBQUQsQ0FBQTtRQUQ2QjtNQUFBLENBQUEsQ0FBQSxDQUFBLElBQUEsQ0FBWixFQUVqQixJQUFDLENBQUEsMEJBQUQsQ0FBQSxDQUZpQjthQUluQixJQUFDLENBQUEsa0JBQUQsR0FBc0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFaLENBQTJCLElBQUksQ0FBQyxJQUFOLEdBQVcsa0JBQXJDLEVBQXdELENBQUEsU0FBQSxLQUFBO2VBQUEsU0FBQyxHQUFEO0FBQzVFLGNBQUE7VUFEOEUseUJBQVU7VUFDeEYsSUFBd0QsSUFBSSxDQUFDLFNBQUwsQ0FBZSxXQUFmLENBQXhEO1lBQUEsT0FBTyxDQUFDLEdBQVIsQ0FBWSw2QkFBQSxHQUE4QixRQUExQyxFQUFBOztVQUNBLEtBQUMsQ0FBQSxpQkFBRCxDQUFBO2lCQUNBLEtBQUMsQ0FBQSxnQkFBRCxDQUFBO1FBSDRFO01BQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUF4RDtJQVBOLENBeEVsQjtJQW9GQSxpQkFBQSxFQUFtQixTQUFBO0FBQ2pCLFVBQUE7O1dBQW1CLENBQUUsT0FBckIsQ0FBQTs7TUFDQSxJQUFDLENBQUEsa0JBQUQsR0FBc0I7TUFFdEIsSUFBbUMsSUFBQyxDQUFBLGVBQXBDO1FBQUEsYUFBQSxDQUFjLElBQUMsQ0FBQSxlQUFmLEVBQUE7O2FBQ0EsSUFBQyxDQUFBLGVBQUQsR0FBbUI7SUFMRixDQXBGbkI7SUEyRkEsd0NBQUEsRUFBMEMsU0FBQTtBQUN4QyxVQUFBO01BQUEsY0FBQSxHQUFpQixJQUFDLENBQUEsa0JBQUQsQ0FBQSxDQUFBLElBQXlCO01BQzFDLElBQUcsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsY0FBQSxHQUFpQixJQUFDLENBQUEsMEJBQUQsQ0FBQSxDQUFqQztlQUNFLElBQUMsQ0FBQSxjQUFELENBQUEsRUFERjs7SUFGd0MsQ0EzRjFDO0lBZ0dBLGNBQUEsRUFBZ0IsU0FBQyxZQUFEOztRQUFDLGVBQWU7OztRQUM5QixpQkFBa0IsT0FBQSxDQUFRLG1CQUFSOztNQUNsQixjQUFjLENBQUMsY0FBZixDQUE4QixZQUE5QjthQUNBLElBQUMsQ0FBQSxrQkFBRCxDQUFBO0lBSGMsQ0FoR2hCO0lBcUdBLDBCQUFBLEVBQTRCLFNBQUE7QUFDMUIsVUFBQTtNQUFBLE9BQUEsR0FBVSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQVosQ0FBbUIsSUFBSSxDQUFDLElBQU4sR0FBVyxrQkFBN0I7TUFFVixJQUFHLEtBQUEsQ0FBTSxPQUFOLENBQUEsSUFBa0IsT0FBQSxHQUFVLDBDQUEvQjtRQUNFLE9BQUEsR0FBVSwyQ0FEWjs7YUFHQSxPQUFBLEdBQVUsRUFBVixHQUFlO0lBTlcsQ0FyRzVCO0lBNkdBLDBCQUFBLEVBQTRCLFNBQUE7YUFDMUIsSUFBQyxDQUFBLDBCQUFELENBQUEsQ0FBQSxHQUFnQztJQUROLENBN0c1QjtJQWtIQSxrQkFBQSxFQUFvQixTQUFBO0FBQ2xCLFVBQUE7QUFBQTtRQUNFLGNBQUEsR0FBaUIsWUFBWSxDQUFDLE9BQWIsQ0FBd0IsSUFBSSxDQUFDLElBQU4sR0FBVyxpQkFBbEM7ZUFDakIsUUFBQSxDQUFTLGNBQVQsRUFGRjtPQUFBLGFBQUE7UUFJRSxZQUFZLENBQUMsT0FBYixDQUF3QixJQUFJLENBQUMsSUFBTixHQUFXLGlCQUFsQyxFQUFvRCxJQUFJLENBQUMsR0FBTCxDQUFBLENBQXBEO2VBQ0EsS0FMRjs7SUFEa0IsQ0FsSHBCO0lBMEhBLGtCQUFBLEVBQW9CLFNBQUE7YUFDbEIsWUFBWSxDQUFDLE9BQWIsQ0FBd0IsSUFBSSxDQUFDLElBQU4sR0FBVyxpQkFBbEMsRUFBb0QsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFVLENBQUMsUUFBWCxDQUFBLENBQXBEO0lBRGtCLENBMUhwQjs7QUFSRiIsInNvdXJjZXNDb250ZW50IjpbIm1ldGEgPSByZXF1aXJlIFwiLi4vcGFja2FnZS5qc29uXCJcblV0aWwgPSByZXF1aXJlIFwiLi91dGlsXCJcblBhY2thZ2VVcGRhdGVyID0gbnVsbFxuXG5XQVJNVVBfV0FJVCA9IDEwICogMTAwMFxuTUlOSU1VTV9BVVRPX1VQREFURV9CTE9DS19EVVJBVElPTl9NSU5VVEVTID0gMTVcblxubW9kdWxlLmV4cG9ydHMgPVxuICBjb25maWc6XG4gICAgaW5jbHVkZWRQYWNrYWdlczpcbiAgICAgIHRpdGxlOiBcIkluY2x1ZGVkIFBhY2thZ2VzXCJcbiAgICAgIGRlc2NyaXB0aW9uOiBcIkNvbW1hLWRlbGltaXRlZCBsaXN0IG9mIHBhY2thZ2VzIHRvIGJlIGluY2x1ZGVkIGZyb20gYXV0b21hdGljIHVwZGF0ZXNcIlxuICAgICAgdHlwZTogXCJhcnJheVwiXG4gICAgICBkZWZhdWx0OiBbXVxuICAgICAgb3JkZXI6IDFcbiAgICBleGNsdWRlZFBhY2thZ2VzOlxuICAgICAgdGl0bGU6IFwiRXhjbHVkZWQgUGFja2FnZXNcIlxuICAgICAgZGVzY3JpcHRpb246IFwiQ29tbWEtZGVsaW1pdGVkIGxpc3Qgb2YgcGFja2FnZXMgdG8gYmUgZXhjbHVkZWQgZnJvbSBhdXRvbWF0aWMgdXBkYXRlc1wiXG4gICAgICB0eXBlOiBcImFycmF5XCJcbiAgICAgIGRlZmF1bHQ6IFtdXG4gICAgICBvcmRlcjogMlxuICAgIGludGVydmFsTWludXRlczpcbiAgICAgIHRpdGxlOiBcIlVwZGF0ZSBJbnRlcnZhbFwiXG4gICAgICBkZXNjcmlwdGlvbjogXCJTZXQgdGhlIGRlZmF1bHQgdXBkYXRlIGludGVydmFsIGluIG1pbnV0ZXNcIlxuICAgICAgdHlwZTogXCJpbnRlZ2VyXCJcbiAgICAgIG1pbmltdW06IE1JTklNVU1fQVVUT19VUERBVEVfQkxPQ0tfRFVSQVRJT05fTUlOVVRFU1xuICAgICAgZGVmYXVsdDogNiAqIDYwXG4gICAgICBvcmRlcjogM1xuICAgIHVwZGF0ZU5vdGlmaWNhdGlvbjpcbiAgICAgIHRpdGxlOiBcIk5vdGlmeSBvbiBVcGRhdGVcIlxuICAgICAgZGVzY3JpcHRpb246IFwiRW5hYmxlIHRvIHNob3cgbm90aWZpY2F0aW9ucyB3aGVuIHBhY2thZ2VzIGhhdmUgYmVlbiB1cGRhdGVkXCJcbiAgICAgIHR5cGU6IFwiYm9vbGVhblwiXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgICBvcmRlcjogNFxuICAgIGRpc21pc3NOb3RpZmljYXRpb246XG4gICAgICB0aXRsZTogXCJEaXNtaXNzIE5vdGlmaWNhdGlvblwiXG4gICAgICBkZXNjcmlwdGlvbjogXCJBdXRvbWF0aWNhbGx5IGRpc21pc3MgdGhlIHVwZGF0ZSBub3RpZmljYXRpb24gYWZ0ZXIgNSBzZWNvbmRzXCJcbiAgICAgIHR5cGU6IFwiYm9vbGVhblwiXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgICBvcmRlcjogNVxuICAgIG5vdGlmaWNhdGlvblN0eWxlOlxuICAgICAgdGl0bGU6IFwiTm90aWZpY2F0aW9uIFN0eWxlXCJcbiAgICAgIGRlc2NyaXB0aW9uOiBcIlNwZWNpZnkgYSBzdHlsZSBmb3IgdGhlIG5vdGlmaWNhdGlvbiBwb3B1cCAoKlN1Y2Nlc3M9Z3JlZW4qLCAqSW5mbz1ibHVlKiwgKldhcm5pbmc9eWVsbG93KiwgYW5kICpFcnJvcj1yZWQqKVwiXG4gICAgICB0eXBlOiBcInN0cmluZ1wiXG4gICAgICBlbnVtOiBbXG4gICAgICAgIFwiU3VjY2Vzc1wiXG4gICAgICAgIFwiSW5mb1wiXG4gICAgICAgIFwiV2FybmluZ1wiXG4gICAgICAgIFwiRXJyb3JcIlxuICAgICAgXVxuICAgICAgZGVmYXVsdDogXCJTdWNjZXNzXCJcbiAgICAgIG9yZGVyOiA2XG4gICAgbWF4aW11bVBhY2thZ2VEZXRhaWw6XG4gICAgICB0aXRsZTogXCJNYXhpbXVtIFBhY2thZ2UgRGV0YWlsXCJcbiAgICAgIGRlc2NyaXB0aW9uOiBcIlNwZWNpZnkgdGhlIG1heGltdW0gbnVtYmVyIG9mIHBhY2thZ2UgbmFtZXMgZGlzcGxheWVkIGluIHRoZSBub3RpZmljYXRpb24gKG1pbmltdW0gaXMgMylcIlxuICAgICAgdHlwZTogXCJudW1iZXJcIlxuICAgICAgZGVmYXVsdDogNVxuICAgICAgbWluaW11bTogM1xuICAgICAgb3JkZXI6IDdcbiAgICBkZWJ1Z01vZGU6XG4gICAgICB0aXRsZTogXCJEZWJ1ZyBNb2RlXCJcbiAgICAgIGRlc2NyaXB0aW9uOiBcIkVuYWJsZSB0byBvdXRwdXQgZGV0YWlscyBpbiB5b3VyIGNvbnNvbGVcIlxuICAgICAgdHlwZTogXCJib29sZWFuXCJcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgICBvcmRlcjogOFxuXG4gIGFjdGl2YXRlOiAoc3RhdGUpIC0+XG4gICAgY29tbWFuZHMgPSB7fVxuICAgIGNvbW1hbmRzW1wiI3ttZXRhLm5hbWV9OnVwZGF0ZS1ub3dcIl0gPSA9PiBAdXBkYXRlUGFja2FnZXMoZmFsc2UpXG4gICAgQGNvbW1hbmRTdWJzY3JpcHRpb24gPSBhdG9tLmNvbW1hbmRzLmFkZChcImF0b20td29ya3NwYWNlXCIsIGNvbW1hbmRzKVxuXG4gICAgc2V0VGltZW91dCA9PlxuICAgICAgQGVuYWJsZUF1dG9VcGRhdGUoKVxuICAgICwgV0FSTVVQX1dBSVRcblxuICBkZWFjdGl2YXRlOiAtPlxuICAgIEBkaXNhYmxlQXV0b1VwZGF0ZSgpXG4gICAgQGNvbW1hbmRTdWJzY3JpcHRpb24/LmRpc3Bvc2UoKVxuICAgIEBjb21tYW5kU3Vic2NyaXB0aW9uID0gbnVsbFxuXG4gIGVuYWJsZUF1dG9VcGRhdGU6IC0+XG4gICAgQHVwZGF0ZVBhY2thZ2VzSWZBdXRvVXBkYXRlQmxvY2tJc0V4cGlyZWQoKVxuXG4gICAgQGF1dG9VcGRhdGVDaGVjayA9IHNldEludGVydmFsID0+XG4gICAgICBAdXBkYXRlUGFja2FnZXNJZkF1dG9VcGRhdGVCbG9ja0lzRXhwaXJlZCgpXG4gICAgLCBAZ2V0QXV0b1VwZGF0ZUNoZWNrSW50ZXJ2YWwoKVxuXG4gICAgQGNvbmZpZ1N1YnNjcmlwdGlvbiA9IGF0b20uY29uZmlnLm9uRGlkQ2hhbmdlIFwiI3ttZXRhLm5hbWV9LmludGVydmFsTWludXRlc1wiLCAoe25ld1ZhbHVlLCBvbGRWYWx1ZX0pID0+XG4gICAgICBjb25zb2xlLmxvZyBcIkNoYW5nZWQgdXBkYXRlIGludGVydmFsIHRvICN7bmV3VmFsdWV9XCIgaWYgVXRpbC5nZXRDb25maWcoXCJkZWJ1Z01vZGVcIilcbiAgICAgIEBkaXNhYmxlQXV0b1VwZGF0ZSgpXG4gICAgICBAZW5hYmxlQXV0b1VwZGF0ZSgpXG5cbiAgZGlzYWJsZUF1dG9VcGRhdGU6IC0+XG4gICAgQGNvbmZpZ1N1YnNjcmlwdGlvbj8uZGlzcG9zZSgpXG4gICAgQGNvbmZpZ1N1YnNjcmlwdGlvbiA9IG51bGxcblxuICAgIGNsZWFySW50ZXJ2YWwoQGF1dG9VcGRhdGVDaGVjaykgaWYgQGF1dG9VcGRhdGVDaGVja1xuICAgIEBhdXRvVXBkYXRlQ2hlY2sgPSBudWxsXG5cbiAgdXBkYXRlUGFja2FnZXNJZkF1dG9VcGRhdGVCbG9ja0lzRXhwaXJlZDogLT5cbiAgICBsYXN0VXBkYXRlVGltZSA9IEBsb2FkTGFzdFVwZGF0ZVRpbWUoKSB8fCAwXG4gICAgaWYgRGF0ZS5ub3coKSA+IGxhc3RVcGRhdGVUaW1lICsgQGdldEF1dG9VcGRhdGVCbG9ja0R1cmF0aW9uKClcbiAgICAgIEB1cGRhdGVQYWNrYWdlcygpXG5cbiAgdXBkYXRlUGFja2FnZXM6IChpc0F1dG9VcGRhdGUgPSB0cnVlKSAtPlxuICAgIFBhY2thZ2VVcGRhdGVyID89IHJlcXVpcmUgXCIuL3BhY2thZ2UtdXBkYXRlclwiXG4gICAgUGFja2FnZVVwZGF0ZXIudXBkYXRlUGFja2FnZXMoaXNBdXRvVXBkYXRlKVxuICAgIEBzYXZlTGFzdFVwZGF0ZVRpbWUoKVxuXG4gIGdldEF1dG9VcGRhdGVCbG9ja0R1cmF0aW9uOiAtPlxuICAgIG1pbnV0ZXMgPSBhdG9tLmNvbmZpZy5nZXQoXCIje21ldGEubmFtZX0uaW50ZXJ2YWxNaW51dGVzXCIpXG5cbiAgICBpZiBpc05hTihtaW51dGVzKSB8fCBtaW51dGVzIDwgTUlOSU1VTV9BVVRPX1VQREFURV9CTE9DS19EVVJBVElPTl9NSU5VVEVTXG4gICAgICBtaW51dGVzID0gTUlOSU1VTV9BVVRPX1VQREFURV9CTE9DS19EVVJBVElPTl9NSU5VVEVTXG5cbiAgICBtaW51dGVzICogNjAgKiAxMDAwXG5cbiAgZ2V0QXV0b1VwZGF0ZUNoZWNrSW50ZXJ2YWw6IC0+XG4gICAgQGdldEF1dG9VcGRhdGVCbG9ja0R1cmF0aW9uKCkgLyAxNVxuXG4gICMgYXV0by11cGdyYWRlLXBhY2thZ2VzIHJ1bnMgb24gZWFjaCBBdG9tIGluc3RhbmNlLFxuICAjIHNvIHdlIG5lZWQgdG8gc2hhcmUgdGhlIGxhc3QgdXBkYXRlZCB0aW1lIHZpYSBhIGZpbGUgYmV0d2VlbiB0aGUgaW5zdGFuY2VzLlxuICBsb2FkTGFzdFVwZGF0ZVRpbWU6IC0+XG4gICAgdHJ5XG4gICAgICBsYXN0VXBkYXRlVGltZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiI3ttZXRhLm5hbWV9Lmxhc3RVcGRhdGVUaW1lXCIpXG4gICAgICBwYXJzZUludChsYXN0VXBkYXRlVGltZSlcbiAgICBjYXRjaFxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCIje21ldGEubmFtZX0ubGFzdFVwZGF0ZVRpbWVcIiwgRGF0ZS5ub3coKSlcbiAgICAgIG51bGxcblxuICBzYXZlTGFzdFVwZGF0ZVRpbWU6IC0+XG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCIje21ldGEubmFtZX0ubGFzdFVwZGF0ZVRpbWVcIiwgRGF0ZS5ub3coKS50b1N0cmluZygpKVxuIl19
