Object.defineProperty(exports, '__esModule', {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _reactForAtom = require('react-for-atom');

var _ServiceItem = require('./ServiceItem');

var _ServiceItem2 = _interopRequireDefault(_ServiceItem);

var _ServiceControls = require('./ServiceControls');

var _ServiceControls2 = _interopRequireDefault(_ServiceControls);

var _immutable = require('immutable');

'use babel';

exports['default'] = _reactForAtom.React.createClass({
  displayName: 'ServiceList',

  getInitialState: function getInitialState() {
    return {
      filters: []
    };
  },
  getDefaultProps: function getDefaultProps() {
    return {
      services: [],
      onFiltersChange: function onFiltersChange() {},
      onAction: function onAction(action, serviceNames) {}
    };
  },
  onFilterChange: function onFilterChange(serviceName) {
    var _this = this;

    return function (selected) {
      var newFilters = undefined;
      if (serviceName != "all") {
        if (selected) {
          newFilters = (0, _immutable.fromJS)(_this.state.filters).push(serviceName).toJS();
        } else {
          newFilters = _this.state.filters.filter(function (service) {
            return service != serviceName;
          });
        }
      } else {
        if (selected) {
          newFilters = _this.props.services.map(function (service) {
            return service.name;
          });
        } else {
          newFilters = [];
        }
      }
      _this.setState({ filters: newFilters }, function () {
        _this.props.onFiltersChange(newFilters);
      });
    };
  },
  render: function render() {
    var _this2 = this;

    return _reactForAtom.React.createElement(
      'div',
      { style: { padding: "15px", display: "flex", flexDirection: "column", alignSelf: "stretch", alignItems: "stretch", flex: "1" } },
      _reactForAtom.React.createElement(
        'div',
        { style: { display: "flex", flexDirection: "row", justifyContent: "flex-end", marginRight: "10px" } },
        _reactForAtom.React.createElement(_ServiceControls2['default'], {
          onFilterChange: this.onFilterChange("all"),
          selected: this.state.filters.length == this.props.services.length,
          onUpClick: function () {
            return _this2.props.onAction("up");
          },
          onBuildClick: function () {
            return _this2.props.onAction("build");
          },
          onRestartClick: function () {
            return _this2.props.onAction("restart");
          },
          onStopClick: function () {
            return _this2.props.onAction("stop");
          },
          onRmClick: function () {
            return _this2.props.onAction("rm");
          },
          onPSClick: function () {
            return _this2.props.onAction('ps');
          }
        })
      ),
      _reactForAtom.React.createElement(
        'div',
        { style: { marginTop: "10px", marginBottom: "10px", display: "flex", flexDirection: "column", overflowY: "scroll", flex: "1" } },
        _reactForAtom.React.createElement(
          'div',
          { style: {} },
          this.props.services.map(function (service, i) {
            return _reactForAtom.React.createElement(_ServiceItem2['default'], _extends({
              key: 'service' + i,
              onFilterChange: _this2.onFilterChange(service.name),
              selected: _this2.state.filters.find(function (filter) {
                return filter == service.name;
              }) !== undefined,
              onAction: function (action, serviceName) {
                return _this2.props.onAction(action, [serviceName]);
              }
            }, service));
          })
        )
      )
    );
  }
});
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvZG9ja2VyL2xpYi9jb21wb25lbnRzL1NlcnZpY2VMaXN0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OzRCQUVvQixnQkFBZ0I7OzJCQUNaLGVBQWU7Ozs7K0JBQ1gsbUJBQW1COzs7O3lCQUMxQixXQUFXOztBQUxoQyxXQUFXLENBQUE7O3FCQU9JLG9CQUFNLFdBQVcsQ0FBQzs7O0FBQy9CLGlCQUFlLEVBQUUsMkJBQVc7QUFDMUIsV0FBTztBQUNMLGFBQU8sRUFBRSxFQUFFO0tBQ1osQ0FBQztHQUNIO0FBQ0QsaUJBQWUsRUFBRSwyQkFBVztBQUMxQixXQUFPO0FBQ0wsY0FBUSxFQUFFLEVBQUU7QUFDWixxQkFBZSxFQUFFLDJCQUFXLEVBRTNCO0FBQ0QsY0FBUSxFQUFFLGtCQUFTLE1BQU0sRUFBRSxZQUFZLEVBQUUsRUFFeEM7S0FDRixDQUFDO0dBQ0g7QUFDRCxnQkFBYyxFQUFFLHdCQUFTLFdBQVcsRUFBRTs7O0FBQ3BDLFdBQU8sVUFBQyxRQUFRLEVBQUs7QUFDbkIsVUFBSSxVQUFVLFlBQUEsQ0FBQztBQUNmLFVBQUksV0FBVyxJQUFJLEtBQUssRUFBRTtBQUN4QixZQUFJLFFBQVEsRUFBRTtBQUNaLG9CQUFVLEdBQUcsdUJBQU8sTUFBSyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2xFLE1BQU07QUFDTCxvQkFBVSxHQUFHLE1BQUssS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxPQUFPO21CQUFJLE9BQU8sSUFBSSxXQUFXO1dBQUEsQ0FBQyxDQUFDO1NBQzNFO09BQ0YsTUFBTTtBQUNMLFlBQUksUUFBUSxFQUFFO0FBQ1osb0JBQVUsR0FBRyxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsT0FBTzttQkFBSSxPQUFPLENBQUMsSUFBSTtXQUFBLENBQUMsQ0FBQztTQUMvRCxNQUFNO0FBQ0wsb0JBQVUsR0FBRyxFQUFFLENBQUM7U0FDakI7T0FDRjtBQUNELFlBQUssUUFBUSxDQUFDLEVBQUMsT0FBTyxFQUFFLFVBQVUsRUFBQyxFQUFFLFlBQU07QUFBQyxjQUFLLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUE7T0FBQyxDQUFDLENBQUM7S0FDdEYsQ0FBQztHQUNIO0FBQ0QsUUFBTSxFQUFFLGtCQUFXOzs7QUFDakIsV0FDRTs7UUFBSyxLQUFLLEVBQUUsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBQyxBQUFDO01BQzlIOztVQUFLLEtBQUssRUFBRSxFQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxjQUFjLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUMsQUFBQztRQUNuRztBQUNFLHdCQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQUFBQztBQUMzQyxrQkFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEFBQUM7QUFDbEUsbUJBQVMsRUFBRTttQkFBTSxPQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1dBQUEsQUFBQztBQUMzQyxzQkFBWSxFQUFFO21CQUFNLE9BQUssS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7V0FBQSxBQUFDO0FBQ2pELHdCQUFjLEVBQUU7bUJBQU0sT0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztXQUFBLEFBQUM7QUFDckQscUJBQVcsRUFBRTttQkFBTSxPQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1dBQUEsQUFBQztBQUMvQyxtQkFBUyxFQUFFO21CQUFNLE9BQUssS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7V0FBQSxBQUFDO0FBQzNDLG1CQUFTLEVBQUU7bUJBQU0sT0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztXQUFBLEFBQUM7VUFDM0M7T0FDRTtNQUNOOztVQUFLLEtBQUssRUFBRSxFQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFDLEFBQUM7UUFDOUg7O1lBQUssS0FBSyxFQUFFLEVBQUUsQUFBQztVQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFDLE9BQU8sRUFBRSxDQUFDO21CQUNsQztBQUNFLGlCQUFHLGNBQVksQ0FBQyxBQUFHO0FBQ25CLDRCQUFjLEVBQUUsT0FBSyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxBQUFDO0FBQ2xELHNCQUFRLEVBQUUsT0FBSyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU07dUJBQUksTUFBTSxJQUFJLE9BQU8sQ0FBQyxJQUFJO2VBQUEsQ0FBQyxLQUFLLFNBQVMsQUFBQztBQUNsRixzQkFBUSxFQUFFLFVBQUMsTUFBTSxFQUFFLFdBQVc7dUJBQUssT0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2VBQUEsQUFBQztlQUMxRSxPQUFPLEVBQ1g7V0FDSCxDQUFDO1NBQ0U7T0FDRjtLQUNGLENBQ047R0FDSDtDQUNGLENBQUMiLCJmaWxlIjoiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9kb2NrZXIvbGliL2NvbXBvbmVudHMvU2VydmljZUxpc3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIGJhYmVsJ1xuXG5pbXBvcnQge1JlYWN0fSBmcm9tICdyZWFjdC1mb3ItYXRvbSc7XG5pbXBvcnQgU2VydmljZUl0ZW0gZnJvbSAnLi9TZXJ2aWNlSXRlbSc7XG5pbXBvcnQgU2VydmljZUNvbnRyb2xzIGZyb20gJy4vU2VydmljZUNvbnRyb2xzJztcbmltcG9ydCB7ZnJvbUpTfSBmcm9tICdpbW11dGFibGUnO1xuXG5leHBvcnQgZGVmYXVsdCBSZWFjdC5jcmVhdGVDbGFzcyh7XG4gIGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGZpbHRlcnM6IFtdXG4gICAgfTtcbiAgfSxcbiAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgc2VydmljZXM6IFtdLFxuICAgICAgb25GaWx0ZXJzQ2hhbmdlOiBmdW5jdGlvbigpIHtcblxuICAgICAgfSxcbiAgICAgIG9uQWN0aW9uOiBmdW5jdGlvbihhY3Rpb24sIHNlcnZpY2VOYW1lcykge1xuXG4gICAgICB9XG4gICAgfTtcbiAgfSxcbiAgb25GaWx0ZXJDaGFuZ2U6IGZ1bmN0aW9uKHNlcnZpY2VOYW1lKSB7XG4gICAgcmV0dXJuIChzZWxlY3RlZCkgPT4ge1xuICAgICAgbGV0IG5ld0ZpbHRlcnM7XG4gICAgICBpZiAoc2VydmljZU5hbWUgIT0gXCJhbGxcIikge1xuICAgICAgICBpZiAoc2VsZWN0ZWQpIHtcbiAgICAgICAgICBuZXdGaWx0ZXJzID0gZnJvbUpTKHRoaXMuc3RhdGUuZmlsdGVycykucHVzaChzZXJ2aWNlTmFtZSkudG9KUygpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIG5ld0ZpbHRlcnMgPSB0aGlzLnN0YXRlLmZpbHRlcnMuZmlsdGVyKHNlcnZpY2UgPT4gc2VydmljZSAhPSBzZXJ2aWNlTmFtZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChzZWxlY3RlZCkge1xuICAgICAgICAgIG5ld0ZpbHRlcnMgPSB0aGlzLnByb3BzLnNlcnZpY2VzLm1hcChzZXJ2aWNlID0+IHNlcnZpY2UubmFtZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbmV3RmlsdGVycyA9IFtdO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICB0aGlzLnNldFN0YXRlKHtmaWx0ZXJzOiBuZXdGaWx0ZXJzfSwgKCkgPT4ge3RoaXMucHJvcHMub25GaWx0ZXJzQ2hhbmdlKG5ld0ZpbHRlcnMpfSk7XG4gICAgfTtcbiAgfSxcbiAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBzdHlsZT17e3BhZGRpbmc6IFwiMTVweFwiLCBkaXNwbGF5OiBcImZsZXhcIiwgZmxleERpcmVjdGlvbjogXCJjb2x1bW5cIiwgYWxpZ25TZWxmOiBcInN0cmV0Y2hcIiwgYWxpZ25JdGVtczogXCJzdHJldGNoXCIsIGZsZXg6IFwiMVwifX0+XG4gICAgICAgIDxkaXYgc3R5bGU9e3tkaXNwbGF5OiBcImZsZXhcIiwgZmxleERpcmVjdGlvbjogXCJyb3dcIiwganVzdGlmeUNvbnRlbnQ6IFwiZmxleC1lbmRcIiwgbWFyZ2luUmlnaHQ6IFwiMTBweFwifX0+XG4gICAgICAgICAgPFNlcnZpY2VDb250cm9sc1xuICAgICAgICAgICAgb25GaWx0ZXJDaGFuZ2U9e3RoaXMub25GaWx0ZXJDaGFuZ2UoXCJhbGxcIil9XG4gICAgICAgICAgICBzZWxlY3RlZD17dGhpcy5zdGF0ZS5maWx0ZXJzLmxlbmd0aCA9PSB0aGlzLnByb3BzLnNlcnZpY2VzLmxlbmd0aH1cbiAgICAgICAgICAgIG9uVXBDbGljaz17KCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbihcInVwXCIpfVxuICAgICAgICAgICAgb25CdWlsZENsaWNrPXsoKSA9PiB0aGlzLnByb3BzLm9uQWN0aW9uKFwiYnVpbGRcIil9XG4gICAgICAgICAgICBvblJlc3RhcnRDbGljaz17KCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbihcInJlc3RhcnRcIil9XG4gICAgICAgICAgICBvblN0b3BDbGljaz17KCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbihcInN0b3BcIil9XG4gICAgICAgICAgICBvblJtQ2xpY2s9eygpID0+IHRoaXMucHJvcHMub25BY3Rpb24oXCJybVwiKX1cbiAgICAgICAgICAgIG9uUFNDbGljaz17KCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbigncHMnKX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17e21hcmdpblRvcDogXCIxMHB4XCIsIG1hcmdpbkJvdHRvbTogXCIxMHB4XCIsIGRpc3BsYXk6IFwiZmxleFwiLCBmbGV4RGlyZWN0aW9uOiBcImNvbHVtblwiLCBvdmVyZmxvd1k6IFwic2Nyb2xsXCIsIGZsZXg6IFwiMVwifX0+XG4gICAgICAgICAgPGRpdiBzdHlsZT17e319PlxuICAgICAgICAgICAge3RoaXMucHJvcHMuc2VydmljZXMubWFwKChzZXJ2aWNlLCBpKSA9PiAoXG4gICAgICAgICAgICAgIDxTZXJ2aWNlSXRlbVxuICAgICAgICAgICAgICAgIGtleT17YHNlcnZpY2Uke2l9YH1cbiAgICAgICAgICAgICAgICBvbkZpbHRlckNoYW5nZT17dGhpcy5vbkZpbHRlckNoYW5nZShzZXJ2aWNlLm5hbWUpfVxuICAgICAgICAgICAgICAgIHNlbGVjdGVkPXt0aGlzLnN0YXRlLmZpbHRlcnMuZmluZChmaWx0ZXIgPT4gZmlsdGVyID09IHNlcnZpY2UubmFtZSkgIT09IHVuZGVmaW5lZH1cbiAgICAgICAgICAgICAgICBvbkFjdGlvbj17KGFjdGlvbiwgc2VydmljZU5hbWUpID0+IHRoaXMucHJvcHMub25BY3Rpb24oYWN0aW9uLCBbc2VydmljZU5hbWVdKX1cbiAgICAgICAgICAgICAgICB7Li4uc2VydmljZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICkpfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn0pXG4iXX0=