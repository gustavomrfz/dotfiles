Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

'use babel';

function getConfig(file) {
  var fs = require('fs');
  var realFile = fs.realpathSync(file);
  delete require.cache[realFile];
  switch (require('path').extname(file)) {
    case '.json':
    case '.js':
      return require(realFile);

    case '.cson':
      return require('cson-parser').parse(fs.readFileSync(realFile));

    case '.yaml':
    case '.yml':
      return require('js-yaml').safeLoad(fs.readFileSync(realFile));
  }

  return {};
}

function createBuildConfig(build, name) {
  var conf = {
    name: 'Custom: ' + name,
    exec: build.cmd,
    env: build.env,
    args: build.args,
    cwd: build.cwd,
    sh: build.sh,
    errorMatch: build.errorMatch,
    functionMatch: build.functionMatch,
    warningMatch: build.warningMatch,
    atomCommandName: build.atomCommandName,
    keymap: build.keymap,
    killSignals: build.killSignals
  };

  if (typeof build.postBuild === 'function') {
    conf.postBuild = build.postBuild;
  }

  if (typeof build.preBuild === 'function') {
    conf.preBuild = build.preBuild;
  }

  return conf;
}

var CustomFile = (function (_EventEmitter) {
  _inherits(CustomFile, _EventEmitter);

  function CustomFile(cwd) {
    _classCallCheck(this, CustomFile);

    _get(Object.getPrototypeOf(CustomFile.prototype), 'constructor', this).call(this);
    this.cwd = cwd;
    this.fileWatchers = [];
  }

  _createClass(CustomFile, [{
    key: 'destructor',
    value: function destructor() {
      this.fileWatchers.forEach(function (fw) {
        return fw.close();
      });
    }
  }, {
    key: 'getNiceName',
    value: function getNiceName() {
      return 'Custom file';
    }
  }, {
    key: 'isEligible',
    value: function isEligible() {
      var _this = this;

      var os = require('os');
      var fs = require('fs');
      var path = require('path');
      this.files = [].concat.apply([], ['json', 'cson', 'yaml', 'yml', 'js'].map(function (ext) {
        return [path.join(_this.cwd, '.atom-build.' + ext), path.join(os.homedir(), '.atom-build.' + ext)];
      })).filter(fs.existsSync);
      return 0 < this.files.length;
    }
  }, {
    key: 'settings',
    value: function settings() {
      var _this2 = this;

      var fs = require('fs');
      this.fileWatchers.forEach(function (fw) {
        return fw.close();
      });
      // On Linux, closing a watcher triggers a new callback, which causes an infinite loop
      // fallback to `watchFile` here which polls instead.
      this.fileWatchers = this.files.map(function (file) {
        return (require('os').platform() === 'linux' ? fs.watchFile : fs.watch)(file, function () {
          return _this2.emit('refresh');
        });
      });

      var config = [];
      this.files.map(getConfig).forEach(function (build) {
        config.push.apply(config, [createBuildConfig(build, build.name || 'default')].concat(_toConsumableArray(Object.keys(build.targets || {}).map(function (name) {
          return createBuildConfig(build.targets[name], name);
        }))));
      });

      return config;
    }
  }]);

  return CustomFile;
})(_events2['default']);

exports['default'] = CustomFile;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvYnVpbGQvbGliL2F0b20tYnVpbGQuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztzQkFFeUIsUUFBUTs7OztBQUZqQyxXQUFXLENBQUM7O0FBSVosU0FBUyxTQUFTLENBQUMsSUFBSSxFQUFFO0FBQ3ZCLE1BQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6QixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3ZDLFNBQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvQixVQUFRLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0FBQ25DLFNBQUssT0FBTyxDQUFDO0FBQ2IsU0FBSyxLQUFLO0FBQ1IsYUFBTyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7O0FBQUEsQUFFM0IsU0FBSyxPQUFPO0FBQ1YsYUFBTyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs7QUFBQSxBQUVqRSxTQUFLLE9BQU8sQ0FBQztBQUNiLFNBQUssTUFBTTtBQUNULGFBQU8sT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7QUFBQSxHQUNqRTs7QUFFRCxTQUFPLEVBQUUsQ0FBQztDQUNYOztBQUVELFNBQVMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRTtBQUN0QyxNQUFNLElBQUksR0FBRztBQUNYLFFBQUksRUFBRSxVQUFVLEdBQUcsSUFBSTtBQUN2QixRQUFJLEVBQUUsS0FBSyxDQUFDLEdBQUc7QUFDZixPQUFHLEVBQUUsS0FBSyxDQUFDLEdBQUc7QUFDZCxRQUFJLEVBQUUsS0FBSyxDQUFDLElBQUk7QUFDaEIsT0FBRyxFQUFFLEtBQUssQ0FBQyxHQUFHO0FBQ2QsTUFBRSxFQUFFLEtBQUssQ0FBQyxFQUFFO0FBQ1osY0FBVSxFQUFFLEtBQUssQ0FBQyxVQUFVO0FBQzVCLGlCQUFhLEVBQUUsS0FBSyxDQUFDLGFBQWE7QUFDbEMsZ0JBQVksRUFBRSxLQUFLLENBQUMsWUFBWTtBQUNoQyxtQkFBZSxFQUFFLEtBQUssQ0FBQyxlQUFlO0FBQ3RDLFVBQU0sRUFBRSxLQUFLLENBQUMsTUFBTTtBQUNwQixlQUFXLEVBQUUsS0FBSyxDQUFDLFdBQVc7R0FDL0IsQ0FBQzs7QUFFRixNQUFJLE9BQU8sS0FBSyxDQUFDLFNBQVMsS0FBSyxVQUFVLEVBQUU7QUFDekMsUUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDO0dBQ2xDOztBQUVELE1BQUksT0FBTyxLQUFLLENBQUMsUUFBUSxLQUFLLFVBQVUsRUFBRTtBQUN4QyxRQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7R0FDaEM7O0FBRUQsU0FBTyxJQUFJLENBQUM7Q0FDYjs7SUFFb0IsVUFBVTtZQUFWLFVBQVU7O0FBQ2xCLFdBRFEsVUFBVSxDQUNqQixHQUFHLEVBQUU7MEJBREUsVUFBVTs7QUFFM0IsK0JBRmlCLFVBQVUsNkNBRW5CO0FBQ1IsUUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7QUFDZixRQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztHQUN4Qjs7ZUFMa0IsVUFBVTs7V0FPbkIsc0JBQUc7QUFDWCxVQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEVBQUU7ZUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFO09BQUEsQ0FBQyxDQUFDO0tBQzdDOzs7V0FFVSx1QkFBRztBQUNaLGFBQU8sYUFBYSxDQUFDO0tBQ3RCOzs7V0FFUyxzQkFBRzs7O0FBQ1gsVUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pCLFVBQU0sRUFBRSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN6QixVQUFNLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDN0IsVUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFFLENBQUMsR0FBRyxDQUFDLFVBQUEsR0FBRztlQUFJLENBQ2xGLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBSyxHQUFHLG1CQUFpQixHQUFHLENBQUcsRUFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLG1CQUFpQixHQUFHLENBQUcsQ0FDOUM7T0FBQSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQzFCLGFBQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0tBQzlCOzs7V0FFTyxvQkFBRzs7O0FBQ1QsVUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3pCLFVBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsRUFBRTtlQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUU7T0FBQSxDQUFDLENBQUM7OztBQUc1QyxVQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtlQUNyQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsS0FBSyxPQUFPLEdBQUcsRUFBRSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFBLENBQUUsSUFBSSxFQUFFO2lCQUFNLE9BQUssSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUFBLENBQUM7T0FBQSxDQUNuRyxDQUFDOztBQUVGLFVBQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQztBQUNsQixVQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLLEVBQUk7QUFDekMsY0FBTSxDQUFDLElBQUksTUFBQSxDQUFYLE1BQU0sR0FDSixpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUksSUFBSSxTQUFTLENBQUMsNEJBQzlDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO2lCQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDO1NBQUEsQ0FBQyxHQUM5RixDQUFDO09BQ0gsQ0FBQyxDQUFDOztBQUVILGFBQU8sTUFBTSxDQUFDO0tBQ2Y7OztTQTVDa0IsVUFBVTs7O3FCQUFWLFVBQVUiLCJmaWxlIjoiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9idWlsZC9saWIvYXRvbS1idWlsZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2UgYmFiZWwnO1xuXG5pbXBvcnQgRXZlbnRFbWl0dGVyIGZyb20gJ2V2ZW50cyc7XG5cbmZ1bmN0aW9uIGdldENvbmZpZyhmaWxlKSB7XG4gIGNvbnN0IGZzID0gcmVxdWlyZSgnZnMnKTtcbiAgY29uc3QgcmVhbEZpbGUgPSBmcy5yZWFscGF0aFN5bmMoZmlsZSk7XG4gIGRlbGV0ZSByZXF1aXJlLmNhY2hlW3JlYWxGaWxlXTtcbiAgc3dpdGNoIChyZXF1aXJlKCdwYXRoJykuZXh0bmFtZShmaWxlKSkge1xuICAgIGNhc2UgJy5qc29uJzpcbiAgICBjYXNlICcuanMnOlxuICAgICAgcmV0dXJuIHJlcXVpcmUocmVhbEZpbGUpO1xuXG4gICAgY2FzZSAnLmNzb24nOlxuICAgICAgcmV0dXJuIHJlcXVpcmUoJ2Nzb24tcGFyc2VyJykucGFyc2UoZnMucmVhZEZpbGVTeW5jKHJlYWxGaWxlKSk7XG5cbiAgICBjYXNlICcueWFtbCc6XG4gICAgY2FzZSAnLnltbCc6XG4gICAgICByZXR1cm4gcmVxdWlyZSgnanMteWFtbCcpLnNhZmVMb2FkKGZzLnJlYWRGaWxlU3luYyhyZWFsRmlsZSkpO1xuICB9XG5cbiAgcmV0dXJuIHt9O1xufVxuXG5mdW5jdGlvbiBjcmVhdGVCdWlsZENvbmZpZyhidWlsZCwgbmFtZSkge1xuICBjb25zdCBjb25mID0ge1xuICAgIG5hbWU6ICdDdXN0b206ICcgKyBuYW1lLFxuICAgIGV4ZWM6IGJ1aWxkLmNtZCxcbiAgICBlbnY6IGJ1aWxkLmVudixcbiAgICBhcmdzOiBidWlsZC5hcmdzLFxuICAgIGN3ZDogYnVpbGQuY3dkLFxuICAgIHNoOiBidWlsZC5zaCxcbiAgICBlcnJvck1hdGNoOiBidWlsZC5lcnJvck1hdGNoLFxuICAgIGZ1bmN0aW9uTWF0Y2g6IGJ1aWxkLmZ1bmN0aW9uTWF0Y2gsXG4gICAgd2FybmluZ01hdGNoOiBidWlsZC53YXJuaW5nTWF0Y2gsXG4gICAgYXRvbUNvbW1hbmROYW1lOiBidWlsZC5hdG9tQ29tbWFuZE5hbWUsXG4gICAga2V5bWFwOiBidWlsZC5rZXltYXAsXG4gICAga2lsbFNpZ25hbHM6IGJ1aWxkLmtpbGxTaWduYWxzXG4gIH07XG5cbiAgaWYgKHR5cGVvZiBidWlsZC5wb3N0QnVpbGQgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb25mLnBvc3RCdWlsZCA9IGJ1aWxkLnBvc3RCdWlsZDtcbiAgfVxuXG4gIGlmICh0eXBlb2YgYnVpbGQucHJlQnVpbGQgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb25mLnByZUJ1aWxkID0gYnVpbGQucHJlQnVpbGQ7XG4gIH1cblxuICByZXR1cm4gY29uZjtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ3VzdG9tRmlsZSBleHRlbmRzIEV2ZW50RW1pdHRlciB7XG4gIGNvbnN0cnVjdG9yKGN3ZCkge1xuICAgIHN1cGVyKCk7XG4gICAgdGhpcy5jd2QgPSBjd2Q7XG4gICAgdGhpcy5maWxlV2F0Y2hlcnMgPSBbXTtcbiAgfVxuXG4gIGRlc3RydWN0b3IoKSB7XG4gICAgdGhpcy5maWxlV2F0Y2hlcnMuZm9yRWFjaChmdyA9PiBmdy5jbG9zZSgpKTtcbiAgfVxuXG4gIGdldE5pY2VOYW1lKCkge1xuICAgIHJldHVybiAnQ3VzdG9tIGZpbGUnO1xuICB9XG5cbiAgaXNFbGlnaWJsZSgpIHtcbiAgICBjb25zdCBvcyA9IHJlcXVpcmUoJ29zJyk7XG4gICAgY29uc3QgZnMgPSByZXF1aXJlKCdmcycpO1xuICAgIGNvbnN0IHBhdGggPSByZXF1aXJlKCdwYXRoJyk7XG4gICAgdGhpcy5maWxlcyA9IFtdLmNvbmNhdC5hcHBseShbXSwgWyAnanNvbicsICdjc29uJywgJ3lhbWwnLCAneW1sJywgJ2pzJyBdLm1hcChleHQgPT4gW1xuICAgICAgcGF0aC5qb2luKHRoaXMuY3dkLCBgLmF0b20tYnVpbGQuJHtleHR9YCksXG4gICAgICBwYXRoLmpvaW4ob3MuaG9tZWRpcigpLCBgLmF0b20tYnVpbGQuJHtleHR9YClcbiAgICBdKSkuZmlsdGVyKGZzLmV4aXN0c1N5bmMpO1xuICAgIHJldHVybiAwIDwgdGhpcy5maWxlcy5sZW5ndGg7XG4gIH1cblxuICBzZXR0aW5ncygpIHtcbiAgICBjb25zdCBmcyA9IHJlcXVpcmUoJ2ZzJyk7XG4gICAgdGhpcy5maWxlV2F0Y2hlcnMuZm9yRWFjaChmdyA9PiBmdy5jbG9zZSgpKTtcbiAgICAvLyBPbiBMaW51eCwgY2xvc2luZyBhIHdhdGNoZXIgdHJpZ2dlcnMgYSBuZXcgY2FsbGJhY2ssIHdoaWNoIGNhdXNlcyBhbiBpbmZpbml0ZSBsb29wXG4gICAgLy8gZmFsbGJhY2sgdG8gYHdhdGNoRmlsZWAgaGVyZSB3aGljaCBwb2xscyBpbnN0ZWFkLlxuICAgIHRoaXMuZmlsZVdhdGNoZXJzID0gdGhpcy5maWxlcy5tYXAoZmlsZSA9PlxuICAgICAgKHJlcXVpcmUoJ29zJykucGxhdGZvcm0oKSA9PT0gJ2xpbnV4JyA/IGZzLndhdGNoRmlsZSA6IGZzLndhdGNoKShmaWxlLCAoKSA9PiB0aGlzLmVtaXQoJ3JlZnJlc2gnKSlcbiAgICApO1xuXG4gICAgY29uc3QgY29uZmlnID0gW107XG4gICAgdGhpcy5maWxlcy5tYXAoZ2V0Q29uZmlnKS5mb3JFYWNoKGJ1aWxkID0+IHtcbiAgICAgIGNvbmZpZy5wdXNoKFxuICAgICAgICBjcmVhdGVCdWlsZENvbmZpZyhidWlsZCwgYnVpbGQubmFtZSB8fCAnZGVmYXVsdCcpLFxuICAgICAgICAuLi5PYmplY3Qua2V5cyhidWlsZC50YXJnZXRzIHx8IHt9KS5tYXAobmFtZSA9PiBjcmVhdGVCdWlsZENvbmZpZyhidWlsZC50YXJnZXRzW25hbWVdLCBuYW1lKSlcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gY29uZmlnO1xuICB9XG59XG4iXX0=