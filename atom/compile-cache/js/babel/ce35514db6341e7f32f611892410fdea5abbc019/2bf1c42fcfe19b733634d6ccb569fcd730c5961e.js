Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _redux = require('redux');

var _reducers = require('./reducers');

var _reducers2 = _interopRequireDefault(_reducers);

'use babel';

var store = (0, _redux.createStore)(_reducers2['default']);
exports['default'] = store;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvZG9ja2VyL2xpYi9yZWR1eC9zdG9yZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7cUJBRTRCLE9BQU87O3dCQUNuQixZQUFZOzs7O0FBSDVCLFdBQVcsQ0FBQTs7QUFJWCxJQUFJLEtBQUssR0FBRyw4Q0FBZ0IsQ0FBQztxQkFDZCxLQUFLIiwiZmlsZSI6Ii9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvZG9ja2VyL2xpYi9yZWR1eC9zdG9yZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2UgYmFiZWwnXG5cbmltcG9ydCB7IGNyZWF0ZVN0b3JlIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IGFwcCBmcm9tICcuL3JlZHVjZXJzJztcbmxldCBzdG9yZSA9IGNyZWF0ZVN0b3JlKGFwcCk7XG5leHBvcnQgZGVmYXVsdCBzdG9yZTtcbiJdfQ==