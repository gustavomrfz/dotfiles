Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x3, _x4, _x5) { var _again = true; _function: while (_again) { var object = _x3, property = _x4, receiver = _x5; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x3 = parent; _x4 = property; _x5 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

'use babel';

var TargetManager = (function (_EventEmitter) {
  _inherits(TargetManager, _EventEmitter);

  function TargetManager() {
    var _this = this;

    _classCallCheck(this, TargetManager);

    _get(Object.getPrototypeOf(TargetManager.prototype), 'constructor', this).call(this);

    var projectPaths = atom.project.getPaths();

    this.pathTargets = projectPaths.map(function (path) {
      return _this._defaultPathTarget(path);
    });

    atom.project.onDidChangePaths(function (newProjectPaths) {
      var addedPaths = newProjectPaths.filter(function (el) {
        return projectPaths.indexOf(el) === -1;
      });
      var removedPaths = projectPaths.filter(function (el) {
        return newProjectPaths.indexOf(el) === -1;
      });
      addedPaths.forEach(function (path) {
        return _this.pathTargets.push(_this._defaultPathTarget(path));
      });
      _this.pathTargets = _this.pathTargets.filter(function (pt) {
        return -1 === removedPaths.indexOf(pt.path);
      });
      _this.refreshTargets(addedPaths);
      projectPaths = newProjectPaths;
    });

    atom.commands.add('atom-workspace', 'build:refresh-targets', function () {
      return _this.refreshTargets();
    });
    atom.commands.add('atom-workspace', 'build:select-active-target', function () {
      return _this.selectActiveTarget();
    });
  }

  _createClass(TargetManager, [{
    key: 'setBusyProvider',
    value: function setBusyProvider(busyProvider) {
      this.busyProvider = busyProvider;
    }
  }, {
    key: '_defaultPathTarget',
    value: function _defaultPathTarget(path) {
      var CompositeDisposable = require('atom').CompositeDisposable;
      return {
        path: path,
        loading: false,
        targets: [],
        instancedTools: [],
        activeTarget: null,
        tools: [],
        subscriptions: new CompositeDisposable()
      };
    }
  }, {
    key: 'destroy',
    value: function destroy() {
      this.pathTargets.forEach(function (pathTarget) {
        return pathTarget.tools.map(function (tool) {
          tool.removeAllListeners && tool.removeAllListeners('refresh');
          tool.destructor && tool.destructor();
        });
      });
    }
  }, {
    key: 'setTools',
    value: function setTools(tools) {
      this.tools = tools || [];
    }
  }, {
    key: 'refreshTargets',
    value: function refreshTargets(refreshPaths) {
      var _this2 = this;

      refreshPaths = refreshPaths || atom.project.getPaths();

      this.busyProvider && this.busyProvider.add('Refreshing targets for ' + refreshPaths.join(','));
      var pathPromises = refreshPaths.map(function (path) {
        var pathTarget = _this2.pathTargets.find(function (pt) {
          return pt.path === path;
        });
        pathTarget.loading = true;

        pathTarget.instancedTools = pathTarget.instancedTools.map(function (t) {
          return t.removeAllListeners && t.removeAllListeners('refresh');
        }).filter(function () {
          return false;
        }); // Just empty the array

        var settingsPromise = _this2.tools.map(function (Tool) {
          return new Tool(path);
        }).filter(function (tool) {
          return tool.isEligible();
        }).map(function (tool) {
          pathTarget.instancedTools.push(tool);
          require('./google-analytics').sendEvent('build', 'tool eligible', tool.getNiceName());

          tool.on && tool.on('refresh', _this2.refreshTargets.bind(_this2, [path]));
          return Promise.resolve().then(function () {
            return tool.settings();
          })['catch'](function (err) {
            if (err instanceof SyntaxError) {
              atom.notifications.addError('Invalid build file.', {
                detail: 'You have a syntax error in your build file: ' + err.message,
                dismissable: true
              });
            } else {
              var toolName = tool.getNiceName();
              atom.notifications.addError('Ooops. Something went wrong' + (toolName ? ' in the ' + toolName + ' build provider' : '') + '.', {
                detail: err.message,
                stack: err.stack,
                dismissable: true
              });
            }
          });
        });

        var CompositeDisposable = require('atom').CompositeDisposable;
        return Promise.all(settingsPromise).then(function (settings) {
          settings = require('./utils').uniquifySettings([].concat.apply([], settings).filter(Boolean).map(function (setting) {
            return require('./utils').getDefaultSettings(path, setting);
          }));

          if (null === pathTarget.activeTarget || !settings.find(function (s) {
            return s.name === pathTarget.activeTarget;
          })) {
            /* Active target has been removed or not set. Set it to the highest prio target */
            pathTarget.activeTarget = settings[0] ? settings[0].name : undefined;
          }

          // CompositeDisposable cannot be reused, so we must create a new instance on every refresh
          pathTarget.subscriptions.dispose();
          pathTarget.subscriptions = new CompositeDisposable();

          settings.forEach(function (setting, index) {
            if (setting.keymap && !setting.atomCommandName) {
              setting.atomCommandName = 'build:trigger:' + setting.name;
            }

            if (setting.atomCommandName) {
              pathTarget.subscriptions.add(atom.commands.add('atom-workspace', setting.atomCommandName, function (atomCommandName) {
                return _this2.emit('trigger', atomCommandName);
              }));
            }

            if (setting.keymap) {
              require('./google-analytics').sendEvent('keymap', 'registered', setting.keymap);
              var keymapSpec = { 'atom-workspace, atom-text-editor': {} };
              keymapSpec['atom-workspace, atom-text-editor'][setting.keymap] = setting.atomCommandName;
              pathTarget.subscriptions.add(atom.keymaps.add(setting.name, keymapSpec));
            }
          });

          pathTarget.targets = settings;
          pathTarget.loading = false;

          return pathTarget;
        })['catch'](function (err) {
          atom.notifications.addError('Ooops. Something went wrong.', {
            detail: err.message,
            stack: err.stack,
            dismissable: true
          });
        });
      });

      return Promise.all(pathPromises).then(function (pathTargets) {
        _this2.fillTargets(require('./utils').activePath(), false);
        _this2.emit('refresh-complete');
        _this2.busyProvider && _this2.busyProvider.remove('Refreshing targets for ' + refreshPaths.join(','));

        if (pathTargets.length === 0) {
          return;
        }

        if (atom.config.get('build.notificationOnRefresh')) {
          var rows = refreshPaths.map(function (path) {
            var pathTarget = _this2.pathTargets.find(function (pt) {
              return pt.path === path;
            });
            if (!pathTarget) {
              return 'Targets ' + path + ' no longer exists. Is build deactivated?';
            }
            return pathTarget.targets.length + ' targets at: ' + path;
          });
          atom.notifications.addInfo('Build targets parsed.', {
            detail: rows.join('\n')
          });
        }
      })['catch'](function (err) {
        atom.notifications.addError('Ooops. Something went wrong.', {
          detail: err.message,
          stack: err.stack,
          dismissable: true
        });
      });
    }
  }, {
    key: 'fillTargets',
    value: function fillTargets(path) {
      var _this3 = this;

      var refreshOnEmpty = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

      if (!this.targetsView) {
        return;
      }

      var activeTarget = this.getActiveTarget(path);
      activeTarget && this.targetsView.setActiveTarget(activeTarget.name);

      this.getTargets(path, refreshOnEmpty).then(function (targets) {
        return targets.map(function (t) {
          return t.name;
        });
      }).then(function (targetNames) {
        return _this3.targetsView && _this3.targetsView.setItems(targetNames);
      });
    }
  }, {
    key: 'selectActiveTarget',
    value: function selectActiveTarget() {
      var _this4 = this;

      if (atom.config.get('build.refreshOnShowTargetList')) {
        this.refreshTargets();
      }

      var path = require('./utils').activePath();
      if (!path) {
        atom.notifications.addWarning('Unable to build.', {
          detail: 'Open file is not part of any open project in Atom'
        });
        return;
      }

      var TargetsView = require('./targets-view');
      this.targetsView = new TargetsView();

      if (this.isLoading(path)) {
        this.targetsView.setLoading('Loading project build targets…');
      } else {
        this.fillTargets(path);
      }

      this.targetsView.awaitSelection().then(function (newTarget) {
        _this4.setActiveTarget(path, newTarget);

        _this4.targetsView = null;
      })['catch'](function (err) {
        _this4.targetsView.setError(err.message);
        _this4.targetsView = null;
      });
    }
  }, {
    key: 'getTargets',
    value: function getTargets(path) {
      var refreshOnEmpty = arguments.length <= 1 || arguments[1] === undefined ? true : arguments[1];

      var pathTarget = this.pathTargets.find(function (pt) {
        return pt.path === path;
      });
      if (!pathTarget) {
        return Promise.resolve([]);
      }

      if (refreshOnEmpty && pathTarget.targets.length === 0) {
        return this.refreshTargets([pathTarget.path]).then(function () {
          return pathTarget.targets;
        });
      }
      return Promise.resolve(pathTarget.targets);
    }
  }, {
    key: 'getActiveTarget',
    value: function getActiveTarget(path) {
      var pathTarget = this.pathTargets.find(function (pt) {
        return pt.path === path;
      });
      if (!pathTarget) {
        return null;
      }
      return pathTarget.targets.find(function (target) {
        return target.name === pathTarget.activeTarget;
      });
    }
  }, {
    key: 'setActiveTarget',
    value: function setActiveTarget(path, targetName) {
      this.pathTargets.find(function (pt) {
        return pt.path === path;
      }).activeTarget = targetName;
      this.emit('new-active-target', path, this.getActiveTarget(path));
    }
  }, {
    key: 'isLoading',
    value: function isLoading(path) {
      return this.pathTargets.find(function (pt) {
        return pt.path === path;
      }).loading;
    }
  }]);

  return TargetManager;
})(_events2['default']);

exports['default'] = TargetManager;
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvYnVpbGQvbGliL3RhcmdldC1tYW5hZ2VyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O3NCQUV5QixRQUFROzs7O0FBRmpDLFdBQVcsQ0FBQzs7SUFJTixhQUFhO1lBQWIsYUFBYTs7QUFDTixXQURQLGFBQWEsR0FDSDs7OzBCQURWLGFBQWE7O0FBRWYsK0JBRkUsYUFBYSw2Q0FFUDs7QUFFUixRQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDOztBQUUzQyxRQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO2FBQUksTUFBSyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7S0FBQSxDQUFDLENBQUM7O0FBRTNFLFFBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsVUFBQSxlQUFlLEVBQUk7QUFDL0MsVUFBTSxVQUFVLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFBLEVBQUU7ZUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztPQUFBLENBQUMsQ0FBQztBQUNqRixVQUFNLFlBQVksR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUEsRUFBRTtlQUFJLGVBQWUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO09BQUEsQ0FBQyxDQUFDO0FBQ25GLGdCQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtlQUFJLE1BQUssV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFLLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO09BQUEsQ0FBQyxDQUFDO0FBQ2pGLFlBQUssV0FBVyxHQUFHLE1BQUssV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLEVBQUU7ZUFBSSxDQUFDLENBQUMsS0FBSyxZQUFZLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7T0FBQSxDQUFDLENBQUM7QUFDdkYsWUFBSyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDaEMsa0JBQVksR0FBRyxlQUFlLENBQUM7S0FDaEMsQ0FBQyxDQUFDOztBQUVILFFBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHVCQUF1QixFQUFFO2FBQU0sTUFBSyxjQUFjLEVBQUU7S0FBQSxDQUFDLENBQUM7QUFDMUYsUUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsNEJBQTRCLEVBQUU7YUFBTSxNQUFLLGtCQUFrQixFQUFFO0tBQUEsQ0FBQyxDQUFDO0dBQ3BHOztlQW5CRyxhQUFhOztXQXFCRix5QkFBQyxZQUFZLEVBQUU7QUFDNUIsVUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7S0FDbEM7OztXQUVpQiw0QkFBQyxJQUFJLEVBQUU7QUFDdkIsVUFBTSxtQkFBbUIsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsbUJBQW1CLENBQUM7QUFDaEUsYUFBTztBQUNMLFlBQUksRUFBRSxJQUFJO0FBQ1YsZUFBTyxFQUFFLEtBQUs7QUFDZCxlQUFPLEVBQUUsRUFBRTtBQUNYLHNCQUFjLEVBQUUsRUFBRTtBQUNsQixvQkFBWSxFQUFFLElBQUk7QUFDbEIsYUFBSyxFQUFFLEVBQUU7QUFDVCxxQkFBYSxFQUFFLElBQUksbUJBQW1CLEVBQUU7T0FDekMsQ0FBQztLQUNIOzs7V0FFTSxtQkFBRztBQUNSLFVBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsVUFBVTtlQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxFQUFJO0FBQ2xFLGNBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDOUQsY0FBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDdEMsQ0FBQztPQUFBLENBQUMsQ0FBQztLQUNMOzs7V0FFTyxrQkFBQyxLQUFLLEVBQUU7QUFDZCxVQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7S0FDMUI7OztXQUVhLHdCQUFDLFlBQVksRUFBRTs7O0FBQzNCLGtCQUFZLEdBQUcsWUFBWSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7O0FBRXZELFVBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLDZCQUEyQixZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFHLENBQUM7QUFDL0YsVUFBTSxZQUFZLEdBQUcsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBSztBQUM5QyxZQUFNLFVBQVUsR0FBRyxPQUFLLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFO2lCQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssSUFBSTtTQUFBLENBQUMsQ0FBQztBQUNqRSxrQkFBVSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRTFCLGtCQUFVLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQ2xELEdBQUcsQ0FBQyxVQUFBLENBQUM7aUJBQUksQ0FBQyxDQUFDLGtCQUFrQixJQUFJLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUM7U0FBQSxDQUFDLENBQ2pFLE1BQU0sQ0FBQztpQkFBTSxLQUFLO1NBQUEsQ0FBQyxDQUFDOztBQUV2QixZQUFNLGVBQWUsR0FBRyxPQUFLLEtBQUssQ0FDL0IsR0FBRyxDQUFDLFVBQUEsSUFBSTtpQkFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7U0FBQSxDQUFDLENBQzNCLE1BQU0sQ0FBQyxVQUFBLElBQUk7aUJBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtTQUFBLENBQUMsQ0FDakMsR0FBRyxDQUFDLFVBQUEsSUFBSSxFQUFJO0FBQ1gsb0JBQVUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3JDLGlCQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQzs7QUFFdEYsY0FBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxPQUFLLGNBQWMsQ0FBQyxJQUFJLFNBQU8sQ0FBRSxJQUFJLENBQUUsQ0FBQyxDQUFDLENBQUM7QUFDeEUsaUJBQU8sT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUNyQixJQUFJLENBQUM7bUJBQU0sSUFBSSxDQUFDLFFBQVEsRUFBRTtXQUFBLENBQUMsU0FDdEIsQ0FBQyxVQUFBLEdBQUcsRUFBSTtBQUNaLGdCQUFJLEdBQUcsWUFBWSxXQUFXLEVBQUU7QUFDOUIsa0JBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLHFCQUFxQixFQUFFO0FBQ2pELHNCQUFNLEVBQUUsOENBQThDLEdBQUcsR0FBRyxDQUFDLE9BQU87QUFDcEUsMkJBQVcsRUFBRSxJQUFJO2VBQ2xCLENBQUMsQ0FBQzthQUNKLE1BQU07QUFDTCxrQkFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0FBQ3BDLGtCQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyw2QkFBNkIsSUFBSSxRQUFRLEdBQUcsVUFBVSxHQUFHLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyxFQUFFLENBQUEsQUFBQyxHQUFHLEdBQUcsRUFBRTtBQUM3SCxzQkFBTSxFQUFFLEdBQUcsQ0FBQyxPQUFPO0FBQ25CLHFCQUFLLEVBQUUsR0FBRyxDQUFDLEtBQUs7QUFDaEIsMkJBQVcsRUFBRSxJQUFJO2VBQ2xCLENBQUMsQ0FBQzthQUNKO1dBQ0YsQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDOztBQUVMLFlBQU0sbUJBQW1CLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLG1CQUFtQixDQUFDO0FBQ2hFLGVBQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRLEVBQUs7QUFDckQsa0JBQVEsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxDQUN6RSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQ2YsR0FBRyxDQUFDLFVBQUEsT0FBTzttQkFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQztXQUFBLENBQUMsQ0FBQyxDQUFDOztBQUV6RSxjQUFJLElBQUksS0FBSyxVQUFVLENBQUMsWUFBWSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUM7bUJBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsWUFBWTtXQUFBLENBQUMsRUFBRTs7QUFFL0Ysc0JBQVUsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDO1dBQ3RFOzs7QUFHRCxvQkFBVSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUNuQyxvQkFBVSxDQUFDLGFBQWEsR0FBRyxJQUFJLG1CQUFtQixFQUFFLENBQUM7O0FBRXJELGtCQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUssRUFBSztBQUNuQyxnQkFBSSxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsRUFBRTtBQUM5QyxxQkFBTyxDQUFDLGVBQWUsc0JBQW9CLE9BQU8sQ0FBQyxJQUFJLEFBQUUsQ0FBQzthQUMzRDs7QUFFRCxnQkFBSSxPQUFPLENBQUMsZUFBZSxFQUFFO0FBQzNCLHdCQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsZUFBZSxFQUFFLFVBQUEsZUFBZTt1QkFBSSxPQUFLLElBQUksQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDO2VBQUEsQ0FBQyxDQUFDLENBQUM7YUFDdEo7O0FBRUQsZ0JBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtBQUNsQixxQkFBTyxDQUFDLG9CQUFvQixDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ2hGLGtCQUFNLFVBQVUsR0FBRyxFQUFFLGtDQUFrQyxFQUFFLEVBQUUsRUFBRSxDQUFDO0FBQzlELHdCQUFVLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQztBQUN6Rix3QkFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO2FBQzFFO1dBQ0YsQ0FBQyxDQUFDOztBQUVILG9CQUFVLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQztBQUM5QixvQkFBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7O0FBRTNCLGlCQUFPLFVBQVUsQ0FBQztTQUNuQixDQUFDLFNBQU0sQ0FBQyxVQUFBLEdBQUcsRUFBSTtBQUNkLGNBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDhCQUE4QixFQUFFO0FBQzFELGtCQUFNLEVBQUUsR0FBRyxDQUFDLE9BQU87QUFDbkIsaUJBQUssRUFBRSxHQUFHLENBQUMsS0FBSztBQUNoQix1QkFBVyxFQUFFLElBQUk7V0FDbEIsQ0FBQyxDQUFDO1NBQ0osQ0FBQyxDQUFDO09BQ0osQ0FBQyxDQUFDOztBQUVILGFBQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxXQUFXLEVBQUk7QUFDbkQsZUFBSyxXQUFXLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ3pELGVBQUssSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7QUFDOUIsZUFBSyxZQUFZLElBQUksT0FBSyxZQUFZLENBQUMsTUFBTSw2QkFBMkIsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBRyxDQUFDOztBQUVsRyxZQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO0FBQzVCLGlCQUFPO1NBQ1I7O0FBRUQsWUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxFQUFFO0FBQ2xELGNBQU0sSUFBSSxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLEVBQUk7QUFDcEMsZ0JBQU0sVUFBVSxHQUFHLE9BQUssV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLEVBQUU7cUJBQUksRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJO2FBQUEsQ0FBQyxDQUFDO0FBQ2pFLGdCQUFJLENBQUMsVUFBVSxFQUFFO0FBQ2Ysa0NBQWtCLElBQUksOENBQTJDO2FBQ2xFO0FBQ0QsbUJBQVUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxNQUFNLHFCQUFnQixJQUFJLENBQUc7V0FDM0QsQ0FBQyxDQUFDO0FBQ0gsY0FBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLEVBQUU7QUFDbEQsa0JBQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztXQUN4QixDQUFDLENBQUM7U0FDSjtPQUNGLENBQUMsU0FBTSxDQUFDLFVBQUEsR0FBRyxFQUFJO0FBQ2QsWUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsOEJBQThCLEVBQUU7QUFDMUQsZ0JBQU0sRUFBRSxHQUFHLENBQUMsT0FBTztBQUNuQixlQUFLLEVBQUUsR0FBRyxDQUFDLEtBQUs7QUFDaEIscUJBQVcsRUFBRSxJQUFJO1NBQ2xCLENBQUMsQ0FBQztPQUNKLENBQUMsQ0FBQztLQUNKOzs7V0FFVSxxQkFBQyxJQUFJLEVBQXlCOzs7VUFBdkIsY0FBYyx5REFBRyxJQUFJOztBQUNyQyxVQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtBQUNyQixlQUFPO09BQ1I7O0FBRUQsVUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNoRCxrQkFBWSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7QUFFcEUsVUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQ2xDLElBQUksQ0FBQyxVQUFBLE9BQU87ZUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQztpQkFBSSxDQUFDLENBQUMsSUFBSTtTQUFBLENBQUM7T0FBQSxDQUFDLENBQ3pDLElBQUksQ0FBQyxVQUFBLFdBQVc7ZUFBSSxPQUFLLFdBQVcsSUFBSSxPQUFLLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO09BQUEsQ0FBQyxDQUFDO0tBQ3BGOzs7V0FFaUIsOEJBQUc7OztBQUNuQixVQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLEVBQUU7QUFDcEQsWUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO09BQ3ZCOztBQUVELFVBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztBQUM3QyxVQUFJLENBQUMsSUFBSSxFQUFFO0FBQ1QsWUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUU7QUFDaEQsZ0JBQU0sRUFBRSxtREFBbUQ7U0FDNUQsQ0FBQyxDQUFDO0FBQ0gsZUFBTztPQUNSOztBQUVELFVBQU0sV0FBVyxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0FBQzlDLFVBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQzs7QUFFckMsVUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO0FBQ3hCLFlBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdDQUFxQyxDQUFDLENBQUM7T0FDcEUsTUFBTTtBQUNMLFlBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7T0FDeEI7O0FBRUQsVUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxTQUFTLEVBQUk7QUFDbEQsZUFBSyxlQUFlLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDOztBQUV0QyxlQUFLLFdBQVcsR0FBRyxJQUFJLENBQUM7T0FDekIsQ0FBQyxTQUFNLENBQUMsVUFBQyxHQUFHLEVBQUs7QUFDaEIsZUFBSyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUN2QyxlQUFLLFdBQVcsR0FBRyxJQUFJLENBQUM7T0FDekIsQ0FBQyxDQUFDO0tBQ0o7OztXQUVTLG9CQUFDLElBQUksRUFBeUI7VUFBdkIsY0FBYyx5REFBRyxJQUFJOztBQUNwQyxVQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLEVBQUU7ZUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLElBQUk7T0FBQSxDQUFDLENBQUM7QUFDakUsVUFBSSxDQUFDLFVBQVUsRUFBRTtBQUNmLGVBQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztPQUM1Qjs7QUFFRCxVQUFJLGNBQWMsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7QUFDckQsZUFBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUUsVUFBVSxDQUFDLElBQUksQ0FBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2lCQUFNLFVBQVUsQ0FBQyxPQUFPO1NBQUEsQ0FBQyxDQUFDO09BQ2hGO0FBQ0QsYUFBTyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUM1Qzs7O1dBRWMseUJBQUMsSUFBSSxFQUFFO0FBQ3BCLFVBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsRUFBRTtlQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssSUFBSTtPQUFBLENBQUMsQ0FBQztBQUNqRSxVQUFJLENBQUMsVUFBVSxFQUFFO0FBQ2YsZUFBTyxJQUFJLENBQUM7T0FDYjtBQUNELGFBQU8sVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO2VBQUksTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsWUFBWTtPQUFBLENBQUMsQ0FBQztLQUNuRjs7O1dBRWMseUJBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRTtBQUNoQyxVQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLEVBQUU7ZUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLElBQUk7T0FBQSxDQUFDLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQztBQUN4RSxVQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDbEU7OztXQUVRLG1CQUFDLElBQUksRUFBRTtBQUNkLGFBQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFO2VBQUksRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJO09BQUEsQ0FBQyxDQUFDLE9BQU8sQ0FBQztLQUM5RDs7O1NBM09HLGFBQWE7OztxQkE4T0osYUFBYSIsImZpbGUiOiIvaG9tZS9ndXN0YXZvLy5hdG9tL3BhY2thZ2VzL2J1aWxkL2xpYi90YXJnZXQtbWFuYWdlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2UgYmFiZWwnO1xuXG5pbXBvcnQgRXZlbnRFbWl0dGVyIGZyb20gJ2V2ZW50cyc7XG5cbmNsYXNzIFRhcmdldE1hbmFnZXIgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuXG4gICAgbGV0IHByb2plY3RQYXRocyA9IGF0b20ucHJvamVjdC5nZXRQYXRocygpO1xuXG4gICAgdGhpcy5wYXRoVGFyZ2V0cyA9IHByb2plY3RQYXRocy5tYXAocGF0aCA9PiB0aGlzLl9kZWZhdWx0UGF0aFRhcmdldChwYXRoKSk7XG5cbiAgICBhdG9tLnByb2plY3Qub25EaWRDaGFuZ2VQYXRocyhuZXdQcm9qZWN0UGF0aHMgPT4ge1xuICAgICAgY29uc3QgYWRkZWRQYXRocyA9IG5ld1Byb2plY3RQYXRocy5maWx0ZXIoZWwgPT4gcHJvamVjdFBhdGhzLmluZGV4T2YoZWwpID09PSAtMSk7XG4gICAgICBjb25zdCByZW1vdmVkUGF0aHMgPSBwcm9qZWN0UGF0aHMuZmlsdGVyKGVsID0+IG5ld1Byb2plY3RQYXRocy5pbmRleE9mKGVsKSA9PT0gLTEpO1xuICAgICAgYWRkZWRQYXRocy5mb3JFYWNoKHBhdGggPT4gdGhpcy5wYXRoVGFyZ2V0cy5wdXNoKHRoaXMuX2RlZmF1bHRQYXRoVGFyZ2V0KHBhdGgpKSk7XG4gICAgICB0aGlzLnBhdGhUYXJnZXRzID0gdGhpcy5wYXRoVGFyZ2V0cy5maWx0ZXIocHQgPT4gLTEgPT09IHJlbW92ZWRQYXRocy5pbmRleE9mKHB0LnBhdGgpKTtcbiAgICAgIHRoaXMucmVmcmVzaFRhcmdldHMoYWRkZWRQYXRocyk7XG4gICAgICBwcm9qZWN0UGF0aHMgPSBuZXdQcm9qZWN0UGF0aHM7XG4gICAgfSk7XG5cbiAgICBhdG9tLmNvbW1hbmRzLmFkZCgnYXRvbS13b3Jrc3BhY2UnLCAnYnVpbGQ6cmVmcmVzaC10YXJnZXRzJywgKCkgPT4gdGhpcy5yZWZyZXNoVGFyZ2V0cygpKTtcbiAgICBhdG9tLmNvbW1hbmRzLmFkZCgnYXRvbS13b3Jrc3BhY2UnLCAnYnVpbGQ6c2VsZWN0LWFjdGl2ZS10YXJnZXQnLCAoKSA9PiB0aGlzLnNlbGVjdEFjdGl2ZVRhcmdldCgpKTtcbiAgfVxuXG4gIHNldEJ1c3lQcm92aWRlcihidXN5UHJvdmlkZXIpIHtcbiAgICB0aGlzLmJ1c3lQcm92aWRlciA9IGJ1c3lQcm92aWRlcjtcbiAgfVxuXG4gIF9kZWZhdWx0UGF0aFRhcmdldChwYXRoKSB7XG4gICAgY29uc3QgQ29tcG9zaXRlRGlzcG9zYWJsZSA9IHJlcXVpcmUoJ2F0b20nKS5Db21wb3NpdGVEaXNwb3NhYmxlO1xuICAgIHJldHVybiB7XG4gICAgICBwYXRoOiBwYXRoLFxuICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICB0YXJnZXRzOiBbXSxcbiAgICAgIGluc3RhbmNlZFRvb2xzOiBbXSxcbiAgICAgIGFjdGl2ZVRhcmdldDogbnVsbCxcbiAgICAgIHRvb2xzOiBbXSxcbiAgICAgIHN1YnNjcmlwdGlvbnM6IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlKClcbiAgICB9O1xuICB9XG5cbiAgZGVzdHJveSgpIHtcbiAgICB0aGlzLnBhdGhUYXJnZXRzLmZvckVhY2gocGF0aFRhcmdldCA9PiBwYXRoVGFyZ2V0LnRvb2xzLm1hcCh0b29sID0+IHtcbiAgICAgIHRvb2wucmVtb3ZlQWxsTGlzdGVuZXJzICYmIHRvb2wucmVtb3ZlQWxsTGlzdGVuZXJzKCdyZWZyZXNoJyk7XG4gICAgICB0b29sLmRlc3RydWN0b3IgJiYgdG9vbC5kZXN0cnVjdG9yKCk7XG4gICAgfSkpO1xuICB9XG5cbiAgc2V0VG9vbHModG9vbHMpIHtcbiAgICB0aGlzLnRvb2xzID0gdG9vbHMgfHwgW107XG4gIH1cblxuICByZWZyZXNoVGFyZ2V0cyhyZWZyZXNoUGF0aHMpIHtcbiAgICByZWZyZXNoUGF0aHMgPSByZWZyZXNoUGF0aHMgfHwgYXRvbS5wcm9qZWN0LmdldFBhdGhzKCk7XG5cbiAgICB0aGlzLmJ1c3lQcm92aWRlciAmJiB0aGlzLmJ1c3lQcm92aWRlci5hZGQoYFJlZnJlc2hpbmcgdGFyZ2V0cyBmb3IgJHtyZWZyZXNoUGF0aHMuam9pbignLCcpfWApO1xuICAgIGNvbnN0IHBhdGhQcm9taXNlcyA9IHJlZnJlc2hQYXRocy5tYXAoKHBhdGgpID0+IHtcbiAgICAgIGNvbnN0IHBhdGhUYXJnZXQgPSB0aGlzLnBhdGhUYXJnZXRzLmZpbmQocHQgPT4gcHQucGF0aCA9PT0gcGF0aCk7XG4gICAgICBwYXRoVGFyZ2V0LmxvYWRpbmcgPSB0cnVlO1xuXG4gICAgICBwYXRoVGFyZ2V0Lmluc3RhbmNlZFRvb2xzID0gcGF0aFRhcmdldC5pbnN0YW5jZWRUb29sc1xuICAgICAgICAubWFwKHQgPT4gdC5yZW1vdmVBbGxMaXN0ZW5lcnMgJiYgdC5yZW1vdmVBbGxMaXN0ZW5lcnMoJ3JlZnJlc2gnKSlcbiAgICAgICAgLmZpbHRlcigoKSA9PiBmYWxzZSk7IC8vIEp1c3QgZW1wdHkgdGhlIGFycmF5XG5cbiAgICAgIGNvbnN0IHNldHRpbmdzUHJvbWlzZSA9IHRoaXMudG9vbHNcbiAgICAgICAgLm1hcChUb29sID0+IG5ldyBUb29sKHBhdGgpKVxuICAgICAgICAuZmlsdGVyKHRvb2wgPT4gdG9vbC5pc0VsaWdpYmxlKCkpXG4gICAgICAgIC5tYXAodG9vbCA9PiB7XG4gICAgICAgICAgcGF0aFRhcmdldC5pbnN0YW5jZWRUb29scy5wdXNoKHRvb2wpO1xuICAgICAgICAgIHJlcXVpcmUoJy4vZ29vZ2xlLWFuYWx5dGljcycpLnNlbmRFdmVudCgnYnVpbGQnLCAndG9vbCBlbGlnaWJsZScsIHRvb2wuZ2V0TmljZU5hbWUoKSk7XG5cbiAgICAgICAgICB0b29sLm9uICYmIHRvb2wub24oJ3JlZnJlc2gnLCB0aGlzLnJlZnJlc2hUYXJnZXRzLmJpbmQodGhpcywgWyBwYXRoIF0pKTtcbiAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKClcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHRvb2wuc2V0dGluZ3MoKSlcbiAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge1xuICAgICAgICAgICAgICBpZiAoZXJyIGluc3RhbmNlb2YgU3ludGF4RXJyb3IpIHtcbiAgICAgICAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoJ0ludmFsaWQgYnVpbGQgZmlsZS4nLCB7XG4gICAgICAgICAgICAgICAgICBkZXRhaWw6ICdZb3UgaGF2ZSBhIHN5bnRheCBlcnJvciBpbiB5b3VyIGJ1aWxkIGZpbGU6ICcgKyBlcnIubWVzc2FnZSxcbiAgICAgICAgICAgICAgICAgIGRpc21pc3NhYmxlOiB0cnVlXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgdG9vbE5hbWUgPSB0b29sLmdldE5pY2VOYW1lKCk7XG4gICAgICAgICAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZEVycm9yKCdPb29wcy4gU29tZXRoaW5nIHdlbnQgd3JvbmcnICsgKHRvb2xOYW1lID8gJyBpbiB0aGUgJyArIHRvb2xOYW1lICsgJyBidWlsZCBwcm92aWRlcicgOiAnJykgKyAnLicsIHtcbiAgICAgICAgICAgICAgICAgIGRldGFpbDogZXJyLm1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgICBzdGFjazogZXJyLnN0YWNrLFxuICAgICAgICAgICAgICAgICAgZGlzbWlzc2FibGU6IHRydWVcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICBjb25zdCBDb21wb3NpdGVEaXNwb3NhYmxlID0gcmVxdWlyZSgnYXRvbScpLkNvbXBvc2l0ZURpc3Bvc2FibGU7XG4gICAgICByZXR1cm4gUHJvbWlzZS5hbGwoc2V0dGluZ3NQcm9taXNlKS50aGVuKChzZXR0aW5ncykgPT4ge1xuICAgICAgICBzZXR0aW5ncyA9IHJlcXVpcmUoJy4vdXRpbHMnKS51bmlxdWlmeVNldHRpbmdzKFtdLmNvbmNhdC5hcHBseShbXSwgc2V0dGluZ3MpXG4gICAgICAgICAgLmZpbHRlcihCb29sZWFuKVxuICAgICAgICAgIC5tYXAoc2V0dGluZyA9PiByZXF1aXJlKCcuL3V0aWxzJykuZ2V0RGVmYXVsdFNldHRpbmdzKHBhdGgsIHNldHRpbmcpKSk7XG5cbiAgICAgICAgaWYgKG51bGwgPT09IHBhdGhUYXJnZXQuYWN0aXZlVGFyZ2V0IHx8ICFzZXR0aW5ncy5maW5kKHMgPT4gcy5uYW1lID09PSBwYXRoVGFyZ2V0LmFjdGl2ZVRhcmdldCkpIHtcbiAgICAgICAgICAvKiBBY3RpdmUgdGFyZ2V0IGhhcyBiZWVuIHJlbW92ZWQgb3Igbm90IHNldC4gU2V0IGl0IHRvIHRoZSBoaWdoZXN0IHByaW8gdGFyZ2V0ICovXG4gICAgICAgICAgcGF0aFRhcmdldC5hY3RpdmVUYXJnZXQgPSBzZXR0aW5nc1swXSA/IHNldHRpbmdzWzBdLm5hbWUgOiB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDb21wb3NpdGVEaXNwb3NhYmxlIGNhbm5vdCBiZSByZXVzZWQsIHNvIHdlIG11c3QgY3JlYXRlIGEgbmV3IGluc3RhbmNlIG9uIGV2ZXJ5IHJlZnJlc2hcbiAgICAgICAgcGF0aFRhcmdldC5zdWJzY3JpcHRpb25zLmRpc3Bvc2UoKTtcbiAgICAgICAgcGF0aFRhcmdldC5zdWJzY3JpcHRpb25zID0gbmV3IENvbXBvc2l0ZURpc3Bvc2FibGUoKTtcblxuICAgICAgICBzZXR0aW5ncy5mb3JFYWNoKChzZXR0aW5nLCBpbmRleCkgPT4ge1xuICAgICAgICAgIGlmIChzZXR0aW5nLmtleW1hcCAmJiAhc2V0dGluZy5hdG9tQ29tbWFuZE5hbWUpIHtcbiAgICAgICAgICAgIHNldHRpbmcuYXRvbUNvbW1hbmROYW1lID0gYGJ1aWxkOnRyaWdnZXI6JHtzZXR0aW5nLm5hbWV9YDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoc2V0dGluZy5hdG9tQ29tbWFuZE5hbWUpIHtcbiAgICAgICAgICAgIHBhdGhUYXJnZXQuc3Vic2NyaXB0aW9ucy5hZGQoYXRvbS5jb21tYW5kcy5hZGQoJ2F0b20td29ya3NwYWNlJywgc2V0dGluZy5hdG9tQ29tbWFuZE5hbWUsIGF0b21Db21tYW5kTmFtZSA9PiB0aGlzLmVtaXQoJ3RyaWdnZXInLCBhdG9tQ29tbWFuZE5hbWUpKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHNldHRpbmcua2V5bWFwKSB7XG4gICAgICAgICAgICByZXF1aXJlKCcuL2dvb2dsZS1hbmFseXRpY3MnKS5zZW5kRXZlbnQoJ2tleW1hcCcsICdyZWdpc3RlcmVkJywgc2V0dGluZy5rZXltYXApO1xuICAgICAgICAgICAgY29uc3Qga2V5bWFwU3BlYyA9IHsgJ2F0b20td29ya3NwYWNlLCBhdG9tLXRleHQtZWRpdG9yJzoge30gfTtcbiAgICAgICAgICAgIGtleW1hcFNwZWNbJ2F0b20td29ya3NwYWNlLCBhdG9tLXRleHQtZWRpdG9yJ11bc2V0dGluZy5rZXltYXBdID0gc2V0dGluZy5hdG9tQ29tbWFuZE5hbWU7XG4gICAgICAgICAgICBwYXRoVGFyZ2V0LnN1YnNjcmlwdGlvbnMuYWRkKGF0b20ua2V5bWFwcy5hZGQoc2V0dGluZy5uYW1lLCBrZXltYXBTcGVjKSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBwYXRoVGFyZ2V0LnRhcmdldHMgPSBzZXR0aW5ncztcbiAgICAgICAgcGF0aFRhcmdldC5sb2FkaW5nID0gZmFsc2U7XG5cbiAgICAgICAgcmV0dXJuIHBhdGhUYXJnZXQ7XG4gICAgICB9KS5jYXRjaChlcnIgPT4ge1xuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoJ09vb3BzLiBTb21ldGhpbmcgd2VudCB3cm9uZy4nLCB7XG4gICAgICAgICAgZGV0YWlsOiBlcnIubWVzc2FnZSxcbiAgICAgICAgICBzdGFjazogZXJyLnN0YWNrLFxuICAgICAgICAgIGRpc21pc3NhYmxlOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gUHJvbWlzZS5hbGwocGF0aFByb21pc2VzKS50aGVuKHBhdGhUYXJnZXRzID0+IHtcbiAgICAgIHRoaXMuZmlsbFRhcmdldHMocmVxdWlyZSgnLi91dGlscycpLmFjdGl2ZVBhdGgoKSwgZmFsc2UpO1xuICAgICAgdGhpcy5lbWl0KCdyZWZyZXNoLWNvbXBsZXRlJyk7XG4gICAgICB0aGlzLmJ1c3lQcm92aWRlciAmJiB0aGlzLmJ1c3lQcm92aWRlci5yZW1vdmUoYFJlZnJlc2hpbmcgdGFyZ2V0cyBmb3IgJHtyZWZyZXNoUGF0aHMuam9pbignLCcpfWApO1xuXG4gICAgICBpZiAocGF0aFRhcmdldHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKGF0b20uY29uZmlnLmdldCgnYnVpbGQubm90aWZpY2F0aW9uT25SZWZyZXNoJykpIHtcbiAgICAgICAgY29uc3Qgcm93cyA9IHJlZnJlc2hQYXRocy5tYXAocGF0aCA9PiB7XG4gICAgICAgICAgY29uc3QgcGF0aFRhcmdldCA9IHRoaXMucGF0aFRhcmdldHMuZmluZChwdCA9PiBwdC5wYXRoID09PSBwYXRoKTtcbiAgICAgICAgICBpZiAoIXBhdGhUYXJnZXQpIHtcbiAgICAgICAgICAgIHJldHVybiBgVGFyZ2V0cyAke3BhdGh9IG5vIGxvbmdlciBleGlzdHMuIElzIGJ1aWxkIGRlYWN0aXZhdGVkP2A7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBgJHtwYXRoVGFyZ2V0LnRhcmdldHMubGVuZ3RofSB0YXJnZXRzIGF0OiAke3BhdGh9YDtcbiAgICAgICAgfSk7XG4gICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRJbmZvKCdCdWlsZCB0YXJnZXRzIHBhcnNlZC4nLCB7XG4gICAgICAgICAgZGV0YWlsOiByb3dzLmpvaW4oJ1xcbicpXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pLmNhdGNoKGVyciA9PiB7XG4gICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoJ09vb3BzLiBTb21ldGhpbmcgd2VudCB3cm9uZy4nLCB7XG4gICAgICAgIGRldGFpbDogZXJyLm1lc3NhZ2UsXG4gICAgICAgIHN0YWNrOiBlcnIuc3RhY2ssXG4gICAgICAgIGRpc21pc3NhYmxlOiB0cnVlXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGZpbGxUYXJnZXRzKHBhdGgsIHJlZnJlc2hPbkVtcHR5ID0gdHJ1ZSkge1xuICAgIGlmICghdGhpcy50YXJnZXRzVmlldykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGFjdGl2ZVRhcmdldCA9IHRoaXMuZ2V0QWN0aXZlVGFyZ2V0KHBhdGgpO1xuICAgIGFjdGl2ZVRhcmdldCAmJiB0aGlzLnRhcmdldHNWaWV3LnNldEFjdGl2ZVRhcmdldChhY3RpdmVUYXJnZXQubmFtZSk7XG5cbiAgICB0aGlzLmdldFRhcmdldHMocGF0aCwgcmVmcmVzaE9uRW1wdHkpXG4gICAgICAudGhlbih0YXJnZXRzID0+IHRhcmdldHMubWFwKHQgPT4gdC5uYW1lKSlcbiAgICAgIC50aGVuKHRhcmdldE5hbWVzID0+IHRoaXMudGFyZ2V0c1ZpZXcgJiYgdGhpcy50YXJnZXRzVmlldy5zZXRJdGVtcyh0YXJnZXROYW1lcykpO1xuICB9XG5cbiAgc2VsZWN0QWN0aXZlVGFyZ2V0KCkge1xuICAgIGlmIChhdG9tLmNvbmZpZy5nZXQoJ2J1aWxkLnJlZnJlc2hPblNob3dUYXJnZXRMaXN0JykpIHtcbiAgICAgIHRoaXMucmVmcmVzaFRhcmdldHMoKTtcbiAgICB9XG5cbiAgICBjb25zdCBwYXRoID0gcmVxdWlyZSgnLi91dGlscycpLmFjdGl2ZVBhdGgoKTtcbiAgICBpZiAoIXBhdGgpIHtcbiAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRXYXJuaW5nKCdVbmFibGUgdG8gYnVpbGQuJywge1xuICAgICAgICBkZXRhaWw6ICdPcGVuIGZpbGUgaXMgbm90IHBhcnQgb2YgYW55IG9wZW4gcHJvamVjdCBpbiBBdG9tJ1xuICAgICAgfSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgVGFyZ2V0c1ZpZXcgPSByZXF1aXJlKCcuL3RhcmdldHMtdmlldycpO1xuICAgIHRoaXMudGFyZ2V0c1ZpZXcgPSBuZXcgVGFyZ2V0c1ZpZXcoKTtcblxuICAgIGlmICh0aGlzLmlzTG9hZGluZyhwYXRoKSkge1xuICAgICAgdGhpcy50YXJnZXRzVmlldy5zZXRMb2FkaW5nKCdMb2FkaW5nIHByb2plY3QgYnVpbGQgdGFyZ2V0c1xcdTIwMjYnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5maWxsVGFyZ2V0cyhwYXRoKTtcbiAgICB9XG5cbiAgICB0aGlzLnRhcmdldHNWaWV3LmF3YWl0U2VsZWN0aW9uKCkudGhlbihuZXdUYXJnZXQgPT4ge1xuICAgICAgdGhpcy5zZXRBY3RpdmVUYXJnZXQocGF0aCwgbmV3VGFyZ2V0KTtcblxuICAgICAgdGhpcy50YXJnZXRzVmlldyA9IG51bGw7XG4gICAgfSkuY2F0Y2goKGVycikgPT4ge1xuICAgICAgdGhpcy50YXJnZXRzVmlldy5zZXRFcnJvcihlcnIubWVzc2FnZSk7XG4gICAgICB0aGlzLnRhcmdldHNWaWV3ID0gbnVsbDtcbiAgICB9KTtcbiAgfVxuXG4gIGdldFRhcmdldHMocGF0aCwgcmVmcmVzaE9uRW1wdHkgPSB0cnVlKSB7XG4gICAgY29uc3QgcGF0aFRhcmdldCA9IHRoaXMucGF0aFRhcmdldHMuZmluZChwdCA9PiBwdC5wYXRoID09PSBwYXRoKTtcbiAgICBpZiAoIXBhdGhUYXJnZXQpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoW10pO1xuICAgIH1cblxuICAgIGlmIChyZWZyZXNoT25FbXB0eSAmJiBwYXRoVGFyZ2V0LnRhcmdldHMubGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gdGhpcy5yZWZyZXNoVGFyZ2V0cyhbIHBhdGhUYXJnZXQucGF0aCBdKS50aGVuKCgpID0+IHBhdGhUYXJnZXQudGFyZ2V0cyk7XG4gICAgfVxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUocGF0aFRhcmdldC50YXJnZXRzKTtcbiAgfVxuXG4gIGdldEFjdGl2ZVRhcmdldChwYXRoKSB7XG4gICAgY29uc3QgcGF0aFRhcmdldCA9IHRoaXMucGF0aFRhcmdldHMuZmluZChwdCA9PiBwdC5wYXRoID09PSBwYXRoKTtcbiAgICBpZiAoIXBhdGhUYXJnZXQpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gcGF0aFRhcmdldC50YXJnZXRzLmZpbmQodGFyZ2V0ID0+IHRhcmdldC5uYW1lID09PSBwYXRoVGFyZ2V0LmFjdGl2ZVRhcmdldCk7XG4gIH1cblxuICBzZXRBY3RpdmVUYXJnZXQocGF0aCwgdGFyZ2V0TmFtZSkge1xuICAgIHRoaXMucGF0aFRhcmdldHMuZmluZChwdCA9PiBwdC5wYXRoID09PSBwYXRoKS5hY3RpdmVUYXJnZXQgPSB0YXJnZXROYW1lO1xuICAgIHRoaXMuZW1pdCgnbmV3LWFjdGl2ZS10YXJnZXQnLCBwYXRoLCB0aGlzLmdldEFjdGl2ZVRhcmdldChwYXRoKSk7XG4gIH1cblxuICBpc0xvYWRpbmcocGF0aCkge1xuICAgIHJldHVybiB0aGlzLnBhdGhUYXJnZXRzLmZpbmQocHQgPT4gcHQucGF0aCA9PT0gcGF0aCkubG9hZGluZztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUYXJnZXRNYW5hZ2VyO1xuIl19