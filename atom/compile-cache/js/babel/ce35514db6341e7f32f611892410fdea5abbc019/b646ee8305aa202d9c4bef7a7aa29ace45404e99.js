Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _reactForAtom = require('react-for-atom');

var _ServiceControls = require('./ServiceControls');

var _ServiceControls2 = _interopRequireDefault(_ServiceControls);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

'use babel';

exports['default'] = _reactForAtom.React.createClass({
  displayName: 'ServiceItem',

  getDefaultProps: function getDefaultProps() {
    return {
      name: "error",
      up: "unknown",
      onFilterChange: function onFilterChange() {},
      onAction: function onAction(action, serviceName) {}
    };
  },
  render: function render() {
    var _this = this;

    return _reactForAtom.React.createElement(
      'div',
      { style: { display: "flex", flexDirection: "row", marginTop: "5px", marginBottom: "5px", justifyContent: "space-between" } },
      _reactForAtom.React.createElement(
        'span',
        { className: (0, _classnames2['default'])("service-name", {
            "up": this.props.up == "up",
            "down": this.props.up == "down"
          }) },
        this.props.name
      ),
      _reactForAtom.React.createElement(_ServiceControls2['default'], {
        onFilterChange: this.props.onFilterChange,
        selected: this.props.selected,
        onUpClick: function () {
          return _this.props.onAction("up", _this.props.name);
        },
        onBuildClick: function () {
          return _this.props.onAction("build", _this.props.name);
        },
        onRestartClick: function () {
          return _this.props.onAction("restart", _this.props.name);
        },
        onStopClick: function () {
          return _this.props.onAction("stop", _this.props.name);
        },
        onRmClick: function () {
          return _this.props.onAction("rm", _this.props.name);
        },
        onPushClick: this.props.tag ? function () {
          return _this.props.onAction('push', _this.props.name);
        } : undefined
      })
    );
  }
});
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvZG9ja2VyL2xpYi9jb21wb25lbnRzL1NlcnZpY2VJdGVtLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs0QkFFb0IsZ0JBQWdCOzsrQkFDUixtQkFBbUI7Ozs7MEJBQ3hCLFlBQVk7Ozs7QUFKbkMsV0FBVyxDQUFBOztxQkFNSSxvQkFBTSxXQUFXLENBQUM7OztBQUMvQixpQkFBZSxFQUFFLDJCQUFXO0FBQzFCLFdBQU87QUFDTCxVQUFJLEVBQUUsT0FBTztBQUNiLFFBQUUsRUFBRSxTQUFTO0FBQ2Isb0JBQWMsRUFBRSwwQkFBVyxFQUUxQjtBQUNELGNBQVEsRUFBRSxrQkFBUyxNQUFNLEVBQUUsV0FBVyxFQUFFLEVBRXZDO0tBQ0YsQ0FBQztHQUNIO0FBQ0QsUUFBTSxFQUFFLGtCQUFXOzs7QUFDakIsV0FDRTs7UUFBSyxLQUFLLEVBQUUsRUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxlQUFlLEVBQUMsQUFBQztNQUMxSDs7VUFBTSxTQUFTLEVBQUUsNkJBQVcsY0FBYyxFQUFFO0FBQzFDLGdCQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksSUFBSTtBQUMzQixrQkFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLE1BQU07V0FDaEMsQ0FBQyxBQUFDO1FBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJO09BQVE7TUFDNUI7QUFDRSxzQkFBYyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxBQUFDO0FBQzFDLGdCQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEFBQUM7QUFDOUIsaUJBQVMsRUFBRTtpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEFBQUM7QUFDNUQsb0JBQVksRUFBRTtpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEFBQUM7QUFDbEUsc0JBQWMsRUFBRTtpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEFBQUM7QUFDdEUsbUJBQVcsRUFBRTtpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEFBQUM7QUFDaEUsaUJBQVMsRUFBRTtpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEFBQUM7QUFDNUQsbUJBQVcsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRztpQkFBTSxNQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLE1BQUssS0FBSyxDQUFDLElBQUksQ0FBQztTQUFBLEdBQUcsU0FBUyxBQUFDO1FBQzdGO0tBQ0UsQ0FDTjtHQUNIO0NBQ0YsQ0FBQyIsImZpbGUiOiIvaG9tZS9ndXN0YXZvLy5hdG9tL3BhY2thZ2VzL2RvY2tlci9saWIvY29tcG9uZW50cy9TZXJ2aWNlSXRlbS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2UgYmFiZWwnXG5cbmltcG9ydCB7UmVhY3R9IGZyb20gJ3JlYWN0LWZvci1hdG9tJztcbmltcG9ydCBTZXJ2aWNlQ29udHJvbHMgZnJvbSAnLi9TZXJ2aWNlQ29udHJvbHMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5cbmV4cG9ydCBkZWZhdWx0IFJlYWN0LmNyZWF0ZUNsYXNzKHtcbiAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmFtZTogXCJlcnJvclwiLFxuICAgICAgdXA6IFwidW5rbm93blwiLFxuICAgICAgb25GaWx0ZXJDaGFuZ2U6IGZ1bmN0aW9uKCkge1xuXG4gICAgICB9LFxuICAgICAgb25BY3Rpb246IGZ1bmN0aW9uKGFjdGlvbiwgc2VydmljZU5hbWUpIHtcblxuICAgICAgfVxuICAgIH07XG4gIH0sXG4gIHJlbmRlcjogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgc3R5bGU9e3tkaXNwbGF5OiBcImZsZXhcIiwgZmxleERpcmVjdGlvbjogXCJyb3dcIiwgbWFyZ2luVG9wOiBcIjVweFwiLCBtYXJnaW5Cb3R0b206IFwiNXB4XCIsIGp1c3RpZnlDb250ZW50OiBcInNwYWNlLWJldHdlZW5cIn19PlxuICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoXCJzZXJ2aWNlLW5hbWVcIiwge1xuICAgICAgICAgIFwidXBcIjogdGhpcy5wcm9wcy51cCA9PSBcInVwXCIsXG4gICAgICAgICAgXCJkb3duXCI6IHRoaXMucHJvcHMudXAgPT0gXCJkb3duXCJcbiAgICAgICAgfSl9Pnt0aGlzLnByb3BzLm5hbWV9PC9zcGFuPlxuICAgICAgICA8U2VydmljZUNvbnRyb2xzXG4gICAgICAgICAgb25GaWx0ZXJDaGFuZ2U9e3RoaXMucHJvcHMub25GaWx0ZXJDaGFuZ2V9XG4gICAgICAgICAgc2VsZWN0ZWQ9e3RoaXMucHJvcHMuc2VsZWN0ZWR9XG4gICAgICAgICAgb25VcENsaWNrPXsoKSA9PiB0aGlzLnByb3BzLm9uQWN0aW9uKFwidXBcIiwgdGhpcy5wcm9wcy5uYW1lKX1cbiAgICAgICAgICBvbkJ1aWxkQ2xpY2s9eygpID0+IHRoaXMucHJvcHMub25BY3Rpb24oXCJidWlsZFwiLCB0aGlzLnByb3BzLm5hbWUpfVxuICAgICAgICAgIG9uUmVzdGFydENsaWNrPXsoKSA9PiB0aGlzLnByb3BzLm9uQWN0aW9uKFwicmVzdGFydFwiLCB0aGlzLnByb3BzLm5hbWUpfVxuICAgICAgICAgIG9uU3RvcENsaWNrPXsoKSA9PiB0aGlzLnByb3BzLm9uQWN0aW9uKFwic3RvcFwiLCB0aGlzLnByb3BzLm5hbWUpfVxuICAgICAgICAgIG9uUm1DbGljaz17KCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbihcInJtXCIsIHRoaXMucHJvcHMubmFtZSl9XG4gICAgICAgICAgb25QdXNoQ2xpY2s9e3RoaXMucHJvcHMudGFnID8gKCkgPT4gdGhpcy5wcm9wcy5vbkFjdGlvbigncHVzaCcsIHRoaXMucHJvcHMubmFtZSkgOiB1bmRlZmluZWR9XG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59KTtcbiJdfQ==