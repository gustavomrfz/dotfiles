Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

//  weak

var _atom = require('atom');

var _jsYaml = require('js-yaml');

var _jsYaml2 = _interopRequireDefault(_jsYaml);

var _reactForAtom = require('react-for-atom');

var _componentsComposePanel = require('./components/ComposePanel');

var _componentsComposePanel2 = _interopRequireDefault(_componentsComposePanel);

var _child_process = require('child_process');

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _reduxStore = require('./redux/store');

var _reduxStore2 = _interopRequireDefault(_reduxStore);

var _reduxActionsLog = require('./redux/actions/log');

var _reduxActionsServices = require('./redux/actions/services');

var _componentsRemoteInfosPrompt = require('./components/RemoteInfosPrompt');

var _componentsRemoteInfosPrompt2 = _interopRequireDefault(_componentsRemoteInfosPrompt);

var _immutable = require('immutable');

'use babel';exports['default'] = {

  dockerView: null,
  bottomPanel: null,
  subscriptions: null,
  config: {
    supressNotifications: {
      title: 'Supress notifications',
      description: 'This supresses "verbose" notifications when commands are successfully executed',
      type: 'boolean',
      'default': false
    }
  },

  activate: function activate(state) {
    var _this = this;

    this.dockerView = document.createElement('div');
    this.dockerView.classList.add("docker");
    this.bottomPanel = atom.workspace.addBottomPanel({
      item: this.dockerView,
      visible: false
    });
    this.modalView = document.createElement('div');
    this.modalView.classList.add("docker");
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.modalView,
      visible: false
    });

    _reactForAtom.ReactDOM.render(_reactForAtom.React.createElement(
      'div',
      null,
      'Select a compose file with docker:select-compose-file'
    ), this.dockerView);
    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new _atom.CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'docker:toggle': function dockerToggle() {
        return _this.toggle();
      },
      'docker:select-compose-file': function dockerSelectComposeFile() {
        return _this.selectComposeFile().then(function () {});
      },
      'docker:add-compose-file': function dockerAddComposeFile() {
        return _this.selectComposeFile(true).then(function () {});
      }
    }));
  },

  deactivate: function deactivate() {
    this.bottomPanel.destroy();
    this.modalPanel.destroy();
    this.subscriptions.dispose();
  },

  serialize: function serialize() {
    return {};
  },
  pushSuccessVerboseNotification: function pushSuccessVerboseNotification() {
    var _atom$notifications;

    if (atom.config.get('docker.supressNotifications')) {
      return;
    }
    (_atom$notifications = atom.notifications).addSuccess.apply(_atom$notifications, arguments);
  },
  selectComposeFile: function selectComposeFile(adding) {
    var _this2 = this;

    return new Promise(function (resolve, reject) {
      var grammarName = atom.workspace.getActiveTextEditor().getGrammar().name;
      if (grammarName != "YAML") {
        atom.notifications.addWarning("Selected file is not a docker-compose file");
      } else {
        (function () {
          var composeFile = atom.workspace.getActivePaneItem().buffer.file;
          var composeFilePath = composeFile.getPath();
          console.log('selected compose file : ' + composeFilePath);
          composeFile.read().then(function (content) {
            try {
              var yamlContent = _jsYaml2['default'].safeLoad(content);
              console.log(yamlContent);
              var services = undefined;
              var version = yamlContent.version;
              if (version == '2') {
                services = Object.keys(yamlContent.services).map(function (key) {
                  return {
                    name: key,
                    tag: yamlContent.services[key].image && yamlContent.services[key].build ? yamlContent.services[key].image : undefined
                  };
                });
              } else {
                services = Object.keys(yamlContent).map(function (key) {
                  return { name: key };
                });
              }
              _reduxStore2['default'].dispatch(adding ? (0, _reduxActionsServices.createComposeFileAddedAction)(composeFilePath, services, version) : (0, _reduxActionsServices.createComposeFileSelectedAction)(composeFilePath, services, version));
              _this2.renderServiceList();
              _this2.execPS();
              if (_this2.bottomPanel.isVisible() == false) _this2.bottomPanel.show();
              resolve();
            } catch (e) {
              console.log(e);
              atom.notifications.addError("Impossible to select compose file", {
                detail: e.toString()
              });
              resolve(e);
            }
          });
        })();
      }
    });
  },
  getCommandArgs: function getCommandArgs(filePaths, action, serviceNames) {
    return [].concat(_toConsumableArray((0, _immutable.fromJS)(filePaths).map(function (filePath) {
      return ['-f', filePath];
    }).reduce(function (reduction, value) {
      return reduction.concat(value);
    }, (0, _immutable.fromJS)([])).toJS()), [action, action == "up" ? "-d" : "", action == "rm" ? "-f" : ""], _toConsumableArray(serviceNames)).filter(function (arg) {
      return arg != "";
    });
  },
  execComposeCommand: function execComposeCommand(action, serviceNames, withExec) {
    var _this3 = this;

    withExec = withExec || false;
    serviceNames = serviceNames || [];
    var filePaths = _reduxStore2['default'].getState().compose.map(function (conf) {
      return conf.filePath;
    });
    return new Promise(function (resolve, reject) {
      if (withExec) {
        (0, _child_process.exec)('docker-compose '.concat(_this3.getCommandArgs(filePaths, action, serviceNames).join(' ')), {
          cwd: _path2['default'].dirname(filePaths[0])
        }, function (error, stdout, stderr) {
          if (error) reject(stderr);else resolve(stdout);
        });
      } else {
        var child = (0, _child_process.spawn)('docker-compose', _this3.getCommandArgs(filePaths, action, serviceNames), { cwd: _path2['default'].dirname(filePaths[0]) });
        var dataHandler = function dataHandler(data) {
          var str = data.toString();
          _reduxStore2['default'].dispatch((0, _reduxActionsLog.createLogReceivedAction)(str));
          if (str.indexOf('exited with code') != -1) _this3.execPS();
        };
        child.stdout.on('data', dataHandler);
        child.stderr.on('data', dataHandler);
        child.on('exit', function (code) {
          if (code == 0) resolve();else reject();
        });
      }
    });
  },
  execPS: function execPS() {
    var _this4 = this;

    this.execComposeCommand('ps', [], true).then(function (stdout) {
      return _this4.handlePSOutput(stdout);
    })['catch'](function () {});
  },
  handlePSOutput: function handlePSOutput(output) {
    var lines = output.split('\n').slice(2);
    var services = (0, _immutable.fromJS)(_reduxStore2['default'].getState().compose).map(function (config) {
      return config.get('services');
    }).reduce(function (reduction, value) {
      return reduction.concat(value);
    }, (0, _immutable.fromJS)([])).toJS();
    var refreshedServices = services.map(function (service) {
      var line = lines.find(function (line) {
        var splittedLine = line.split(' ');
        if (splittedLine.length > 0) {
          return splittedLine[0].indexOf(service.name) != -1;
        } else {
          return false;
        }
      });
      return {
        name: service.name,
        up: line ? line.indexOf(' Up ') != -1 ? 'up' : 'down' : 'down'
      };
    });
    console.log(refreshedServices);
    _reduxStore2['default'].dispatch((0, _reduxActionsServices.createServiceStateChangedAction)(refreshedServices));
    this.renderServiceList();
  },
  pushImage: function pushImage(tag, remoteTag) {
    (0, _child_process.exec)('docker tag ' + tag + ' ' + remoteTag, {}, function (err, stdout, stderr) {
      if (err) {
        atom.notifications.addError('Impossible to tag ' + tag + ' with ' + remoteTag, { dismissable: true, detail: stderr });
        return;
      } else {
        atom.notifications.addSuccess('Tagged ' + tag + ' with ' + remoteTag + ' successfully, pushing ...');
        (0, _child_process.exec)('docker push ' + remoteTag, {}, function (error, pushStdout, pushStderr) {
          if (error) {
            atom.notifications.addError('Impossible to push ' + remoteTag, { dismissable: true, detail: pushStderr });
          } else {
            atom.notifications.addSuccess('Pushed ' + remoteTag + ' successfully');
          }
        });
      }
    });
  },
  onPush: function onPush(serviceNames) {
    var _this5 = this;

    var tag = (0, _immutable.fromJS)(_reduxStore2['default'].getState().compose).map(function (conf) {
      return conf.services;
    }).reduce(function (reduction, value) {
      return reduction.concat(value);
    }, (0, _immutable.fromJS)([])).find(function (service) {
      return service.get('name') == serviceNames[0];
    }).get('tag');
    var prompt = _reactForAtom.ReactDOM.render(_reactForAtom.React.createElement(_componentsRemoteInfosPrompt2['default'], null), this.modalView);
    this.modalView.onkeydown = function (e) {
      var ctrlDown = e.ctrlKey || e.metaKey;
      if (e.which == 27) {
        // esc
        _this5.modalPanel.hide();
      } else if (e.which == 13) {
        //enter
        if (prompt.text.value.length > 0) {
          _this5.modalPanel.hide();
          var newTag = prompt.text.value;
          atom.notifications.addSuccess(tag + ' => ' + newTag);
          _this5.pushImage(tag, newTag);
        }
      }
    };
    this.modalPanel.show();
    prompt.text.focus();
  },
  onComposeAction: function onComposeAction(action, serviceNames) {
    var _this6 = this;

    _reduxStore2['default'].dispatch((0, _reduxActionsLog.createLogReceivedAction)('[Atom] ' + action + '...'));
    if (action == "push") {
      this.onPush(serviceNames);
    } else {
      this.execComposeCommand(action, serviceNames, action == "ps").then(function (stdout) {
        _this6.pushSuccessVerboseNotification(action + ' ' + (serviceNames && serviceNames.length > 0 ? serviceNames.join(' ') : ""), {});
        if (action == "ps") {
          _this6.handlePSOutput(stdout);
        }
        if (action == "up" || action == "restart") {
          _this6.composePanel.composeLogs.serviceLaunched();
        }
        if (action == "up" || action == "restart" || action == "stop") {
          _this6.execPS();
        }
      })['catch'](function (stderr) {
        atom.notifications.addError(action + ' ' + (serviceNames && serviceNames.length > 0 ? serviceNames.join(' ') : ""), {
          dismissable: false,
          detail: stderr
        });
      });
    }
  },
  renderServiceList: function renderServiceList() {
    this.composePanel = _reactForAtom.ReactDOM.render(_reactForAtom.React.createElement(_componentsComposePanel2['default'], {
      onAction: this.onComposeAction.bind(this),
      composeFilePaths: _reduxStore2['default'].getState().compose.map(function (conf) {
        return conf.filePath;
      })
    }), this.dockerView);
  },
  toggle: function toggle() {
    return this.bottomPanel.isVisible() ? this.bottomPanel.hide() : this.bottomPanel.show();
  }

};
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvZG9ja2VyL2xpYi9kb2NrZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztvQkFJb0MsTUFBTTs7c0JBQ3pCLFNBQVM7Ozs7NEJBQ0ksZ0JBQWdCOztzQ0FDckIsMkJBQTJCOzs7OzZCQUMxQixlQUFlOztvQkFDeEIsTUFBTTs7OzswQkFDTCxlQUFlOzs7OytCQUNLLHFCQUFxQjs7b0NBQ2tELDBCQUEwQjs7MkNBQ3pHLGdDQUFnQzs7Ozt5QkFDekMsV0FBVzs7QUFkaEMsV0FBVyxDQUFBLHFCQWdCSTs7QUFFYixZQUFVLEVBQUUsSUFBSTtBQUNoQixhQUFXLEVBQUUsSUFBSTtBQUNqQixlQUFhLEVBQUUsSUFBSTtBQUNuQixRQUFNLEVBQUU7QUFDTix3QkFBb0IsRUFBRTtBQUNwQixXQUFLLEVBQUUsdUJBQXVCO0FBQzlCLGlCQUFXLEVBQUUsZ0ZBQWdGO0FBQzdGLFVBQUksRUFBRSxTQUFTO0FBQ2YsaUJBQVMsS0FBSztLQUNmO0dBQ0Y7O0FBRUQsVUFBUSxFQUFBLGtCQUFDLEtBQUssRUFBRTs7O0FBQ2QsUUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ2hELFFBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUN4QyxRQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDO0FBQy9DLFVBQUksRUFBRSxJQUFJLENBQUMsVUFBVTtBQUNyQixhQUFPLEVBQUUsS0FBSztLQUNmLENBQUMsQ0FBQztBQUNILFFBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUMvQyxRQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDdkMsUUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztBQUM3QyxVQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7QUFDcEIsYUFBTyxFQUFFLEtBQUs7S0FDZixDQUFDLENBQUM7O0FBRUgsMkJBQVMsTUFBTSxDQUFDOzs7O0tBQWdFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOztBQUVuRyxRQUFJLENBQUMsYUFBYSxHQUFHLCtCQUF5QixDQUFDOzs7QUFHL0MsUUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUU7QUFDekQscUJBQWUsRUFBRTtlQUFNLE1BQUssTUFBTSxFQUFFO09BQUE7QUFDcEMsa0NBQTRCLEVBQUU7ZUFBTSxNQUFLLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVcsRUFFNUUsQ0FBQztPQUFBO0FBQ0YsK0JBQXlCLEVBQUU7ZUFBTSxNQUFLLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFXLEVBRTdFLENBQUM7T0FBQTtLQUNILENBQUMsQ0FBQyxDQUFDO0dBQ0w7O0FBRUQsWUFBVSxFQUFBLHNCQUFHO0FBQ1gsUUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMzQixRQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzFCLFFBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7R0FDOUI7O0FBRUQsV0FBUyxFQUFBLHFCQUFHO0FBQ1YsV0FBTyxFQUNOLENBQUM7R0FDSDtBQUNELGdDQUE4QixFQUFBLDBDQUFVOzs7QUFDdEMsUUFBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxFQUFFO0FBQ2pELGFBQU87S0FDUjtBQUNELDJCQUFBLElBQUksQ0FBQyxhQUFhLEVBQUMsVUFBVSxNQUFBLGdDQUFTLENBQUM7R0FDeEM7QUFDRCxtQkFBaUIsRUFBQSwyQkFBQyxNQUFNLEVBQUU7OztBQUN4QixXQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU0sRUFBSztBQUN0QyxVQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDO0FBQ3pFLFVBQUksV0FBVyxJQUFJLE1BQU0sRUFBRTtBQUN6QixZQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDO09BQzdFLE1BQU07O0FBQ0wsY0FBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDakUsY0FBSSxlQUFlLEdBQUcsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzVDLGlCQUFPLENBQUMsR0FBRyw4QkFBNEIsZUFBZSxDQUFHLENBQUM7QUFDMUQscUJBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFPLEVBQUs7QUFDbkMsZ0JBQUk7QUFDRixrQkFBSSxXQUFXLEdBQUcsb0JBQUssUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQ3pDLHFCQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pCLGtCQUFJLFFBQVEsWUFBQSxDQUFDO0FBQ2Isa0JBQUksT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7QUFDbEMsa0JBQUksT0FBTyxJQUFJLEdBQUcsRUFBRTtBQUNsQix3QkFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUcsRUFBSztBQUFDLHlCQUFPO0FBQ2hFLHdCQUFJLEVBQUUsR0FBRztBQUNULHVCQUFHLEVBQUUsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsU0FBUzttQkFDdEgsQ0FBQTtpQkFBQyxDQUFDLENBQUM7ZUFDTCxNQUFNO0FBQ0wsd0JBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUcsRUFBSztBQUFDLHlCQUFPLEVBQUMsSUFBSSxFQUFFLEdBQUcsRUFBQyxDQUFBO2lCQUFDLENBQUMsQ0FBQztlQUN4RTtBQUNELHNDQUFNLFFBQVEsQ0FDWixNQUFNLEdBRUosd0RBQTZCLGVBQWUsRUFBRSxRQUFRLEVBQUUsT0FBTyxDQUFDLEdBRWhFLDJEQUFnQyxlQUFlLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUN0RSxDQUFDO0FBQ0YscUJBQUssaUJBQWlCLEVBQUUsQ0FBQztBQUN6QixxQkFBSyxNQUFNLEVBQUUsQ0FBQztBQUNkLGtCQUFJLE9BQUssV0FBVyxDQUFDLFNBQVMsRUFBRSxJQUFJLEtBQUssRUFDdkMsT0FBSyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDMUIscUJBQU8sRUFBRSxDQUFDO2FBQ1gsQ0FBQyxPQUFPLENBQUMsRUFBRTtBQUNWLHFCQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2Ysa0JBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLG1DQUFtQyxFQUFFO0FBQy9ELHNCQUFNLEVBQUUsQ0FBQyxDQUFDLFFBQVEsRUFBRTtlQUNyQixDQUFDLENBQUM7QUFDSCxxQkFBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ1o7V0FDRixDQUFDLENBQUM7O09BQ0o7S0FDRixDQUFDLENBQUM7R0FDSjtBQUNELGdCQUFjLEVBQUUsd0JBQVMsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUU7QUFDeEQsV0FBTyw2QkFDRix1QkFBTyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQSxRQUFRO2FBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDO0tBQUEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFDLFNBQVMsRUFBRSxLQUFLO2FBQUssU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7S0FBQSxFQUFFLHVCQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQy9ILE1BQU0sRUFDTixNQUFNLElBQUksSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFLEVBQzFCLE1BQU0sSUFBSSxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUUsc0JBQ3ZCLFlBQVksR0FDZixNQUFNLENBQUMsVUFBQyxHQUFHO2FBQUssR0FBRyxJQUFJLEVBQUU7S0FBQSxDQUFDLENBQUM7R0FDOUI7QUFDRCxvQkFBa0IsRUFBRSw0QkFBUyxNQUFNLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRTs7O0FBQzNELFlBQVEsR0FBRyxRQUFRLElBQUksS0FBSyxDQUFDO0FBQzdCLGdCQUFZLEdBQUcsWUFBWSxJQUFJLEVBQUUsQ0FBQztBQUNsQyxRQUFJLFNBQVMsR0FBRyx3QkFBTSxRQUFRLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTthQUFJLElBQUksQ0FBQyxRQUFRO0tBQUEsQ0FBQyxDQUFDO0FBQ3BFLFdBQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFLO0FBQ3RDLFVBQUksUUFBUSxFQUFFO0FBQ1osaUNBQUssaUJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQUssY0FBYyxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7QUFDN0YsYUFBRyxFQUFFLGtCQUFLLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDaEMsRUFBRSxVQUFTLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO0FBQ2pDLGNBQUksS0FBSyxFQUNQLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUVmLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNuQixDQUFDLENBQUM7T0FDSixNQUFNO0FBQ0wsWUFBSSxLQUFLLEdBQUcsMEJBQ1YsZ0JBQWdCLEVBQUUsT0FBSyxjQUFjLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUMsRUFDdEUsRUFBQyxHQUFHLEVBQUUsa0JBQUssT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQ2xDLENBQUM7QUFDRixZQUFJLFdBQVcsR0FBRyxTQUFkLFdBQVcsQ0FBSSxJQUFJLEVBQUs7QUFDMUIsY0FBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0FBQzFCLGtDQUFNLFFBQVEsQ0FBQyw4Q0FBd0IsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUM3QyxjQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUMsRUFDdkMsT0FBSyxNQUFNLEVBQUUsQ0FBQztTQUNqQixDQUFDO0FBQ0YsYUFBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0FBQ3JDLGFBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsQ0FBQztBQUNyQyxhQUFLLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFDLElBQUksRUFBSztBQUN6QixjQUFJLElBQUksSUFBSSxDQUFDLEVBQ2IsT0FBTyxFQUFFLENBQUMsS0FFVixNQUFNLEVBQUUsQ0FBQztTQUNWLENBQUMsQ0FBQztPQUNKO0tBQ0YsQ0FBQyxDQUFDO0dBQ0o7QUFDRCxRQUFNLEVBQUUsa0JBQVc7OztBQUNqQixRQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FDcEMsSUFBSSxDQUFDLFVBQUMsTUFBTTthQUFLLE9BQUssY0FBYyxDQUFDLE1BQU0sQ0FBQztLQUFBLENBQUMsU0FDeEMsQ0FBQyxZQUFNLEVBQUUsQ0FBQyxDQUFDO0dBQ3BCO0FBQ0QsZ0JBQWMsRUFBRSx3QkFBUyxNQUFNLEVBQUU7QUFDL0IsUUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDeEMsUUFBSSxRQUFRLEdBQUcsdUJBQU8sd0JBQU0sUUFBUSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQzlCLEdBQUcsQ0FBQyxVQUFBLE1BQU07YUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQztLQUFBLENBQUMsQ0FDckMsTUFBTSxDQUFDLFVBQUMsU0FBUyxFQUFFLEtBQUs7YUFBSyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztLQUFBLEVBQUUsdUJBQU8sRUFBRSxDQUFDLENBQUMsQ0FDakUsSUFBSSxFQUFFLENBQUM7QUFDeEIsUUFBSSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUNsQyxVQUFBLE9BQU8sRUFBSTtBQUNULFVBQUksSUFBSSxHQUNOLEtBQUssQ0FDRixJQUFJLENBQUMsVUFBQSxJQUFJLEVBQUk7QUFDWixZQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ25DLFlBQUksWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7QUFDM0IsaUJBQU8sWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDcEQsTUFBTTtBQUNMLGlCQUFPLEtBQUssQ0FBQztTQUNkO09BQ0YsQ0FBQyxDQUFDO0FBQ1AsYUFBTztBQUNMLFlBQUksRUFBRSxPQUFPLENBQUMsSUFBSTtBQUNsQixVQUFFLEVBQUUsSUFBSSxHQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxHQUFHLE1BQU0sR0FBSSxNQUFNO09BQ2pFLENBQUM7S0FDSCxDQUNGLENBQUM7QUFDRixXQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7QUFDL0IsNEJBQU0sUUFBUSxDQUFDLDJEQUFnQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7QUFDbkUsUUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7R0FDMUI7QUFDRCxXQUFTLEVBQUUsbUJBQVMsR0FBRyxFQUFFLFNBQVMsRUFBRTtBQUNsQyw2Q0FBbUIsR0FBRyxTQUFJLFNBQVMsRUFBSSxFQUFFLEVBQUUsVUFBQyxHQUFHLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBSztBQUNsRSxVQUFJLEdBQUcsRUFBRTtBQUNQLFlBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSx3QkFBc0IsR0FBRyxjQUFTLFNBQVMsRUFBSSxFQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7QUFDL0csZUFBTztPQUNSLE1BQU07QUFDTCxZQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsYUFBVyxHQUFHLGNBQVMsU0FBUyxnQ0FBNkIsQ0FBQztBQUMzRixrREFBb0IsU0FBUyxFQUFJLEVBQUUsRUFBRSxVQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFLO0FBQ3RFLGNBQUksS0FBSyxFQUFFO0FBQ1QsZ0JBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSx5QkFBdUIsU0FBUyxFQUFJLEVBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztXQUN6RyxNQUFNO0FBQ0wsZ0JBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxhQUFXLFNBQVMsbUJBQWdCLENBQUM7V0FDbkU7U0FDRixDQUFDLENBQUM7T0FDSjtLQUNGLENBQUMsQ0FBQztHQUNKO0FBQ0QsUUFBTSxFQUFFLGdCQUFTLFlBQVksRUFBRTs7O0FBQzdCLFFBQUksR0FBRyxHQUFHLHVCQUFPLHdCQUFNLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUMvQixHQUFHLENBQUMsVUFBQSxJQUFJO2FBQUksSUFBSSxDQUFDLFFBQVE7S0FBQSxDQUFDLENBQzFCLE1BQU0sQ0FBQyxVQUFDLFNBQVMsRUFBRSxLQUFLO2FBQUssU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7S0FBQSxFQUFFLHVCQUFPLEVBQUUsQ0FBQyxDQUFDLENBQ2pFLElBQUksQ0FBQyxVQUFBLE9BQU87YUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUM7S0FBQSxDQUFDLENBQ3ZELEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUN0QixRQUFJLE1BQU0sR0FBRyx1QkFBUyxNQUFNLENBQUMsaUZBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ3BFLFFBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLFVBQUMsQ0FBQyxFQUFLO0FBQ2hDLFVBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQztBQUN0QyxVQUFJLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRSxFQUFFOztBQUNqQixlQUFLLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztPQUN4QixNQUFNLElBQUksQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLEVBQUU7O0FBQ3hCLFlBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUNoQyxpQkFBSyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDdkIsY0FBSSxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7QUFDL0IsY0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUksR0FBRyxZQUFPLE1BQU0sQ0FBRyxDQUFDO0FBQ3JELGlCQUFLLFNBQVMsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDN0I7T0FDRjtLQUNGLENBQUM7QUFDRixRQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFBO0FBQ3RCLFVBQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7R0FDckI7QUFDRCxpQkFBZSxFQUFFLHlCQUFTLE1BQU0sRUFBRSxZQUFZLEVBQUU7OztBQUM5Qyw0QkFBTSxRQUFRLENBQUMsMERBQWtDLE1BQU0sU0FBTSxDQUFDLENBQUM7QUFDL0QsUUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO0FBQ3BCLFVBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7S0FDM0IsTUFBTTtBQUNMLFVBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FDNUQsSUFBSSxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ2hCLGVBQUssOEJBQThCLENBQUksTUFBTSxVQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQSxFQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQzlILFlBQUksTUFBTSxJQUFJLElBQUksRUFBRTtBQUNsQixpQkFBSyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDN0I7QUFDRCxZQUFJLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxJQUFJLFNBQVMsRUFBRTtBQUN6QyxpQkFBSyxZQUFZLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQ2pEO0FBQ0QsWUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sSUFBSSxTQUFTLElBQUksTUFBTSxJQUFJLE1BQU0sRUFBRTtBQUM3RCxpQkFBSyxNQUFNLEVBQUUsQ0FBQztTQUNmO09BQ0YsQ0FBQyxTQUNJLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDakIsWUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUksTUFBTSxVQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQSxFQUFJO0FBQ2hILHFCQUFXLEVBQUUsS0FBSztBQUNsQixnQkFBTSxFQUFFLE1BQU07U0FDZixDQUFDLENBQUM7T0FDSixDQUFDLENBQUM7S0FDSjtHQUNGO0FBQ0QsbUJBQWlCLEVBQUUsNkJBQVc7QUFDNUIsUUFBSSxDQUFDLFlBQVksR0FBRyx1QkFBUyxNQUFNLENBQ2pDO0FBQ0UsY0FBUSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxBQUFDO0FBQzFDLHNCQUFnQixFQUFFLHdCQUFNLFFBQVEsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO2VBQUksSUFBSSxDQUFDLFFBQVE7T0FBQSxDQUFDLEFBQUM7TUFDdEUsRUFDRixJQUFJLENBQUMsVUFBVSxDQUNoQixDQUFDO0dBQ0g7QUFDRCxRQUFNLEVBQUEsa0JBQUc7QUFDUCxXQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLEdBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEdBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQ3ZCO0dBQ0g7O0NBRUYiLCJmaWxlIjoiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9kb2NrZXIvbGliL2RvY2tlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2UgYmFiZWwnXG4vLyBAZmxvdyB3ZWFrXG5cblxuaW1wb3J0IHsgQ29tcG9zaXRlRGlzcG9zYWJsZSB9IGZyb20gJ2F0b20nO1xuaW1wb3J0IHlhbWwgZnJvbSAnanMteWFtbCc7XG5pbXBvcnQge1JlYWN0LCBSZWFjdERPTX0gZnJvbSAncmVhY3QtZm9yLWF0b20nO1xuaW1wb3J0IENvbXBvc2VQYW5lbCBmcm9tICcuL2NvbXBvbmVudHMvQ29tcG9zZVBhbmVsJztcbmltcG9ydCB7ZXhlYywgc3Bhd259IGZyb20gJ2NoaWxkX3Byb2Nlc3MnO1xuaW1wb3J0IHBhdGggZnJvbSAncGF0aCc7XG5pbXBvcnQgc3RvcmUgZnJvbSAnLi9yZWR1eC9zdG9yZSc7XG5pbXBvcnQge2NyZWF0ZUxvZ1JlY2VpdmVkQWN0aW9ufSBmcm9tICcuL3JlZHV4L2FjdGlvbnMvbG9nJztcbmltcG9ydCB7Y3JlYXRlQ29tcG9zZUZpbGVTZWxlY3RlZEFjdGlvbiwgY3JlYXRlQ29tcG9zZUZpbGVBZGRlZEFjdGlvbiwgY3JlYXRlU2VydmljZVN0YXRlQ2hhbmdlZEFjdGlvbn0gZnJvbSAnLi9yZWR1eC9hY3Rpb25zL3NlcnZpY2VzJztcbmltcG9ydCBSZW1vdGVJbmZvc1Byb21wdCBmcm9tICcuL2NvbXBvbmVudHMvUmVtb3RlSW5mb3NQcm9tcHQnO1xuaW1wb3J0IHtmcm9tSlN9IGZyb20gJ2ltbXV0YWJsZSc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcblxuICBkb2NrZXJWaWV3OiBudWxsLFxuICBib3R0b21QYW5lbDogbnVsbCxcbiAgc3Vic2NyaXB0aW9uczogbnVsbCxcbiAgY29uZmlnOiB7XG4gICAgc3VwcmVzc05vdGlmaWNhdGlvbnM6IHtcbiAgICAgIHRpdGxlOiAnU3VwcmVzcyBub3RpZmljYXRpb25zJyxcbiAgICAgIGRlc2NyaXB0aW9uOiAnVGhpcyBzdXByZXNzZXMgXCJ2ZXJib3NlXCIgbm90aWZpY2F0aW9ucyB3aGVuIGNvbW1hbmRzIGFyZSBzdWNjZXNzZnVsbHkgZXhlY3V0ZWQnLFxuICAgICAgdHlwZTogJ2Jvb2xlYW4nLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9XG4gIH0sXG5cbiAgYWN0aXZhdGUoc3RhdGUpIHtcbiAgICB0aGlzLmRvY2tlclZpZXcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICB0aGlzLmRvY2tlclZpZXcuY2xhc3NMaXN0LmFkZChcImRvY2tlclwiKTtcbiAgICB0aGlzLmJvdHRvbVBhbmVsID0gYXRvbS53b3Jrc3BhY2UuYWRkQm90dG9tUGFuZWwoe1xuICAgICAgaXRlbTogdGhpcy5kb2NrZXJWaWV3LFxuICAgICAgdmlzaWJsZTogZmFsc2VcbiAgICB9KTtcbiAgICB0aGlzLm1vZGFsVmlldyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgIHRoaXMubW9kYWxWaWV3LmNsYXNzTGlzdC5hZGQoXCJkb2NrZXJcIik7XG4gICAgdGhpcy5tb2RhbFBhbmVsID0gYXRvbS53b3Jrc3BhY2UuYWRkTW9kYWxQYW5lbCh7XG4gICAgICBpdGVtOiB0aGlzLm1vZGFsVmlldyxcbiAgICAgIHZpc2libGU6IGZhbHNlXG4gICAgfSk7XG5cbiAgICBSZWFjdERPTS5yZW5kZXIoPGRpdj5TZWxlY3QgYSBjb21wb3NlIGZpbGUgd2l0aCBkb2NrZXI6c2VsZWN0LWNvbXBvc2UtZmlsZTwvZGl2PiwgdGhpcy5kb2NrZXJWaWV3KTtcbiAgICAvLyBFdmVudHMgc3Vic2NyaWJlZCB0byBpbiBhdG9tJ3Mgc3lzdGVtIGNhbiBiZSBlYXNpbHkgY2xlYW5lZCB1cCB3aXRoIGEgQ29tcG9zaXRlRGlzcG9zYWJsZVxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucyA9IG5ldyBDb21wb3NpdGVEaXNwb3NhYmxlKCk7XG5cbiAgICAvLyBSZWdpc3RlciBjb21tYW5kIHRoYXQgdG9nZ2xlcyB0aGlzIHZpZXdcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMuYWRkKGF0b20uY29tbWFuZHMuYWRkKCdhdG9tLXdvcmtzcGFjZScsIHtcbiAgICAgICdkb2NrZXI6dG9nZ2xlJzogKCkgPT4gdGhpcy50b2dnbGUoKSxcbiAgICAgICdkb2NrZXI6c2VsZWN0LWNvbXBvc2UtZmlsZSc6ICgpID0+IHRoaXMuc2VsZWN0Q29tcG9zZUZpbGUoKS50aGVuKGZ1bmN0aW9uKCkge1xuXG4gICAgICB9KSxcbiAgICAgICdkb2NrZXI6YWRkLWNvbXBvc2UtZmlsZSc6ICgpID0+IHRoaXMuc2VsZWN0Q29tcG9zZUZpbGUodHJ1ZSkudGhlbihmdW5jdGlvbigpIHtcblxuICAgICAgfSksXG4gICAgfSkpO1xuICB9LFxuXG4gIGRlYWN0aXZhdGUoKSB7XG4gICAgdGhpcy5ib3R0b21QYW5lbC5kZXN0cm95KCk7XG4gICAgdGhpcy5tb2RhbFBhbmVsLmRlc3Ryb3koKTtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZGlzcG9zZSgpO1xuICB9LFxuXG4gIHNlcmlhbGl6ZSgpIHtcbiAgICByZXR1cm4ge1xuICAgIH07XG4gIH0sXG4gIHB1c2hTdWNjZXNzVmVyYm9zZU5vdGlmaWNhdGlvbiguLi5hcmdzKSB7XG4gICAgaWYoYXRvbS5jb25maWcuZ2V0KCdkb2NrZXIuc3VwcmVzc05vdGlmaWNhdGlvbnMnKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkU3VjY2VzcyguLi5hcmdzKTtcbiAgfSxcbiAgc2VsZWN0Q29tcG9zZUZpbGUoYWRkaW5nKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBncmFtbWFyTmFtZSA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKS5nZXRHcmFtbWFyKCkubmFtZTtcbiAgICAgIGlmIChncmFtbWFyTmFtZSAhPSBcIllBTUxcIikge1xuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkV2FybmluZyhcIlNlbGVjdGVkIGZpbGUgaXMgbm90IGEgZG9ja2VyLWNvbXBvc2UgZmlsZVwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxldCBjb21wb3NlRmlsZSA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVBhbmVJdGVtKCkuYnVmZmVyLmZpbGU7XG4gICAgICAgIGxldCBjb21wb3NlRmlsZVBhdGggPSBjb21wb3NlRmlsZS5nZXRQYXRoKCk7XG4gICAgICAgIGNvbnNvbGUubG9nKGBzZWxlY3RlZCBjb21wb3NlIGZpbGUgOiAke2NvbXBvc2VGaWxlUGF0aH1gKTtcbiAgICAgICAgY29tcG9zZUZpbGUucmVhZCgpLnRoZW4oKGNvbnRlbnQpID0+IHtcbiAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIHlhbWxDb250ZW50ID0geWFtbC5zYWZlTG9hZChjb250ZW50KTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHlhbWxDb250ZW50KTtcbiAgICAgICAgICAgIGxldCBzZXJ2aWNlcztcbiAgICAgICAgICAgIGxldCB2ZXJzaW9uID0geWFtbENvbnRlbnQudmVyc2lvbjtcbiAgICAgICAgICAgIGlmICh2ZXJzaW9uID09ICcyJykge1xuICAgICAgICAgICAgICBzZXJ2aWNlcyA9IE9iamVjdC5rZXlzKHlhbWxDb250ZW50LnNlcnZpY2VzKS5tYXAoKGtleSkgPT4ge3JldHVybiB7XG4gICAgICAgICAgICAgICAgbmFtZToga2V5LFxuICAgICAgICAgICAgICAgIHRhZzogeWFtbENvbnRlbnQuc2VydmljZXNba2V5XS5pbWFnZSAmJiB5YW1sQ29udGVudC5zZXJ2aWNlc1trZXldLmJ1aWxkID8geWFtbENvbnRlbnQuc2VydmljZXNba2V5XS5pbWFnZSA6IHVuZGVmaW5lZFxuICAgICAgICAgICAgICB9fSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBzZXJ2aWNlcyA9IE9iamVjdC5rZXlzKHlhbWxDb250ZW50KS5tYXAoKGtleSkgPT4ge3JldHVybiB7bmFtZToga2V5fX0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3RvcmUuZGlzcGF0Y2goXG4gICAgICAgICAgICAgIGFkZGluZ1xuICAgICAgICAgICAgICA/XG4gICAgICAgICAgICAgICAgY3JlYXRlQ29tcG9zZUZpbGVBZGRlZEFjdGlvbihjb21wb3NlRmlsZVBhdGgsIHNlcnZpY2VzLCB2ZXJzaW9uKVxuICAgICAgICAgICAgICA6XG4gICAgICAgICAgICAgICAgY3JlYXRlQ29tcG9zZUZpbGVTZWxlY3RlZEFjdGlvbihjb21wb3NlRmlsZVBhdGgsIHNlcnZpY2VzLCB2ZXJzaW9uKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIHRoaXMucmVuZGVyU2VydmljZUxpc3QoKTtcbiAgICAgICAgICAgIHRoaXMuZXhlY1BTKCk7XG4gICAgICAgICAgICBpZiAodGhpcy5ib3R0b21QYW5lbC5pc1Zpc2libGUoKSA9PSBmYWxzZSlcbiAgICAgICAgICAgICAgdGhpcy5ib3R0b21QYW5lbC5zaG93KCk7XG4gICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoXCJJbXBvc3NpYmxlIHRvIHNlbGVjdCBjb21wb3NlIGZpbGVcIiwge1xuICAgICAgICAgICAgICBkZXRhaWw6IGUudG9TdHJpbmcoKVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXNvbHZlKGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0sXG4gIGdldENvbW1hbmRBcmdzOiBmdW5jdGlvbihmaWxlUGF0aHMsIGFjdGlvbiwgc2VydmljZU5hbWVzKSB7XG4gICAgcmV0dXJuIFtcbiAgICAgIC4uLmZyb21KUyhmaWxlUGF0aHMpLm1hcChmaWxlUGF0aCA9PiBbJy1mJywgZmlsZVBhdGhdKS5yZWR1Y2UoKHJlZHVjdGlvbiwgdmFsdWUpID0+IHJlZHVjdGlvbi5jb25jYXQodmFsdWUpLCBmcm9tSlMoW10pKS50b0pTKCksXG4gICAgICBhY3Rpb24sXG4gICAgICBhY3Rpb24gPT0gXCJ1cFwiID8gXCItZFwiIDogXCJcIixcbiAgICAgIGFjdGlvbiA9PSBcInJtXCIgPyBcIi1mXCIgOiBcIlwiLFxuICAgICAgLi4uc2VydmljZU5hbWVzXG4gICAgXS5maWx0ZXIoKGFyZykgPT4gYXJnICE9IFwiXCIpO1xuICB9LFxuICBleGVjQ29tcG9zZUNvbW1hbmQ6IGZ1bmN0aW9uKGFjdGlvbiwgc2VydmljZU5hbWVzLCB3aXRoRXhlYykge1xuICAgIHdpdGhFeGVjID0gd2l0aEV4ZWMgfHzCoGZhbHNlO1xuICAgIHNlcnZpY2VOYW1lcyA9IHNlcnZpY2VOYW1lcyB8fCBbXTtcbiAgICBsZXQgZmlsZVBhdGhzID0gc3RvcmUuZ2V0U3RhdGUoKS5jb21wb3NlLm1hcChjb25mID0+IGNvbmYuZmlsZVBhdGgpO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBpZiAod2l0aEV4ZWMpIHtcbiAgICAgICAgZXhlYygnZG9ja2VyLWNvbXBvc2UgJy5jb25jYXQodGhpcy5nZXRDb21tYW5kQXJncyhmaWxlUGF0aHMsIGFjdGlvbiwgc2VydmljZU5hbWVzKS5qb2luKCcgJykpLCB7XG4gICAgICAgICAgY3dkOiBwYXRoLmRpcm5hbWUoZmlsZVBhdGhzWzBdKVxuICAgICAgICB9LCBmdW5jdGlvbihlcnJvciwgc3Rkb3V0LCBzdGRlcnIpIHtcbiAgICAgICAgICBpZiAoZXJyb3IpXG4gICAgICAgICAgICByZWplY3Qoc3RkZXJyKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICByZXNvbHZlKHN0ZG91dCk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGNoaWxkID0gc3Bhd24oXG4gICAgICAgICAgJ2RvY2tlci1jb21wb3NlJywgdGhpcy5nZXRDb21tYW5kQXJncyhmaWxlUGF0aHMsIGFjdGlvbiwgc2VydmljZU5hbWVzKSxcbiAgICAgICAgICB7Y3dkOiBwYXRoLmRpcm5hbWUoZmlsZVBhdGhzWzBdKX1cbiAgICAgICAgKTtcbiAgICAgICAgbGV0IGRhdGFIYW5kbGVyID0gKGRhdGEpID0+IHtcbiAgICAgICAgICBsZXQgc3RyID0gZGF0YS50b1N0cmluZygpO1xuICAgICAgICAgIHN0b3JlLmRpc3BhdGNoKGNyZWF0ZUxvZ1JlY2VpdmVkQWN0aW9uKHN0cikpO1xuICAgICAgICAgIGlmIChzdHIuaW5kZXhPZignZXhpdGVkIHdpdGggY29kZScpICE9IC0xKVxuICAgICAgICAgICAgdGhpcy5leGVjUFMoKTtcbiAgICAgICAgfTtcbiAgICAgICAgY2hpbGQuc3Rkb3V0Lm9uKCdkYXRhJywgZGF0YUhhbmRsZXIpO1xuICAgICAgICBjaGlsZC5zdGRlcnIub24oJ2RhdGEnLCBkYXRhSGFuZGxlcik7XG4gICAgICAgIGNoaWxkLm9uKCdleGl0JywgKGNvZGUpID0+IHtcbiAgICAgICAgICBpZiAoY29kZSA9PSAwKVxuICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgcmVqZWN0KCk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxuICBleGVjUFM6IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuZXhlY0NvbXBvc2VDb21tYW5kKCdwcycsIFtdLCB0cnVlKVxuICAgICAgLnRoZW4oKHN0ZG91dCkgPT4gdGhpcy5oYW5kbGVQU091dHB1dChzdGRvdXQpKVxuICAgICAgLmNhdGNoKCgpID0+IHt9KTtcbiAgfSxcbiAgaGFuZGxlUFNPdXRwdXQ6IGZ1bmN0aW9uKG91dHB1dCkge1xuICAgIGxldCBsaW5lcyA9IG91dHB1dC5zcGxpdCgnXFxuJykuc2xpY2UoMik7XG4gICAgbGV0IHNlcnZpY2VzID0gZnJvbUpTKHN0b3JlLmdldFN0YXRlKCkuY29tcG9zZSlcbiAgICAgICAgICAgICAgICAgICAgLm1hcChjb25maWcgPT4gY29uZmlnLmdldCgnc2VydmljZXMnKSlcbiAgICAgICAgICAgICAgICAgICAgLnJlZHVjZSgocmVkdWN0aW9uLCB2YWx1ZSkgPT4gcmVkdWN0aW9uLmNvbmNhdCh2YWx1ZSksIGZyb21KUyhbXSkpXG4gICAgICAgICAgICAgICAgICAgIC50b0pTKCk7XG4gICAgbGV0IHJlZnJlc2hlZFNlcnZpY2VzID0gc2VydmljZXMubWFwKFxuICAgICAgc2VydmljZSA9PiB7XG4gICAgICAgIGxldCBsaW5lID1cbiAgICAgICAgICBsaW5lc1xuICAgICAgICAgICAgLmZpbmQobGluZSA9PiB7XG4gICAgICAgICAgICAgIGxldCBzcGxpdHRlZExpbmUgPSBsaW5lLnNwbGl0KCcgJyk7XG4gICAgICAgICAgICAgIGlmIChzcGxpdHRlZExpbmUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBzcGxpdHRlZExpbmVbMF0uaW5kZXhPZihzZXJ2aWNlLm5hbWUpICE9IC0xO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbmFtZTogc2VydmljZS5uYW1lLFxuICAgICAgICAgIHVwOiBsaW5lID8gKGxpbmUuaW5kZXhPZignIFVwICcpICE9IC0xID8gJ3VwJyA6ICdkb3duJykgOiAnZG93bidcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICApO1xuICAgIGNvbnNvbGUubG9nKHJlZnJlc2hlZFNlcnZpY2VzKTtcbiAgICBzdG9yZS5kaXNwYXRjaChjcmVhdGVTZXJ2aWNlU3RhdGVDaGFuZ2VkQWN0aW9uKHJlZnJlc2hlZFNlcnZpY2VzKSk7XG4gICAgdGhpcy5yZW5kZXJTZXJ2aWNlTGlzdCgpO1xuICB9LFxuICBwdXNoSW1hZ2U6IGZ1bmN0aW9uKHRhZywgcmVtb3RlVGFnKSB7XG4gICAgZXhlYyhgZG9ja2VyIHRhZyAke3RhZ30gJHtyZW1vdGVUYWd9YCwge30sIChlcnIsIHN0ZG91dCwgc3RkZXJyKSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihgSW1wb3NzaWJsZSB0byB0YWcgJHt0YWd9IHdpdGggJHtyZW1vdGVUYWd9YCwge2Rpc21pc3NhYmxlOiB0cnVlLCBkZXRhaWw6IHN0ZGVycn0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkU3VjY2VzcyhgVGFnZ2VkICR7dGFnfSB3aXRoICR7cmVtb3RlVGFnfSBzdWNjZXNzZnVsbHksIHB1c2hpbmcgLi4uYCk7XG4gICAgICAgIGV4ZWMoYGRvY2tlciBwdXNoICR7cmVtb3RlVGFnfWAsIHt9LCAoZXJyb3IsIHB1c2hTdGRvdXQsIHB1c2hTdGRlcnIpID0+IHtcbiAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihgSW1wb3NzaWJsZSB0byBwdXNoICR7cmVtb3RlVGFnfWAsIHtkaXNtaXNzYWJsZTogdHJ1ZSwgZGV0YWlsOiBwdXNoU3RkZXJyfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRTdWNjZXNzKGBQdXNoZWQgJHtyZW1vdGVUYWd9IHN1Y2Nlc3NmdWxseWApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0sXG4gIG9uUHVzaDogZnVuY3Rpb24oc2VydmljZU5hbWVzKSB7XG4gICAgbGV0IHRhZyA9IGZyb21KUyhzdG9yZS5nZXRTdGF0ZSgpLmNvbXBvc2UpXG4gICAgICAgICAgICAgIC5tYXAoY29uZiA9PiBjb25mLnNlcnZpY2VzKVxuICAgICAgICAgICAgICAucmVkdWNlKChyZWR1Y3Rpb24sIHZhbHVlKSA9PiByZWR1Y3Rpb24uY29uY2F0KHZhbHVlKSwgZnJvbUpTKFtdKSlcbiAgICAgICAgICAgICAgLmZpbmQoc2VydmljZSA9PiBzZXJ2aWNlLmdldCgnbmFtZScpID09IHNlcnZpY2VOYW1lc1swXSlcbiAgICAgICAgICAgICAgLmdldCgndGFnJyk7XG4gICAgbGV0IHByb21wdCA9IFJlYWN0RE9NLnJlbmRlcig8UmVtb3RlSW5mb3NQcm9tcHQgLz4sIHRoaXMubW9kYWxWaWV3KTtcbiAgICB0aGlzLm1vZGFsVmlldy5vbmtleWRvd24gPSAoZSkgPT4ge1xuICAgICAgdmFyIGN0cmxEb3duID0gZS5jdHJsS2V5IHx8IGUubWV0YUtleTtcbiAgICAgIGlmIChlLndoaWNoID09IDI3KSB7IC8vIGVzY1xuICAgICAgICB0aGlzLm1vZGFsUGFuZWwuaGlkZSgpO1xuICAgICAgfSBlbHNlIGlmIChlLndoaWNoID09IDEzKSB7IC8vZW50ZXJcbiAgICAgICAgaWYgKHByb21wdC50ZXh0LnZhbHVlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICB0aGlzLm1vZGFsUGFuZWwuaGlkZSgpO1xuICAgICAgICAgIGxldCBuZXdUYWcgPSBwcm9tcHQudGV4dC52YWx1ZTtcbiAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkU3VjY2VzcyhgJHt0YWd9ID0+ICR7bmV3VGFnfWApO1xuICAgICAgICAgIHRoaXMucHVzaEltYWdlKHRhZywgbmV3VGFnKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gICAgdGhpcy5tb2RhbFBhbmVsLnNob3coKVxuICAgIHByb21wdC50ZXh0LmZvY3VzKCk7XG4gIH0sXG4gIG9uQ29tcG9zZUFjdGlvbjogZnVuY3Rpb24oYWN0aW9uLCBzZXJ2aWNlTmFtZXMpIHtcbiAgICBzdG9yZS5kaXNwYXRjaChjcmVhdGVMb2dSZWNlaXZlZEFjdGlvbihgW0F0b21dICR7YWN0aW9ufS4uLmApKTtcbiAgICBpZiAoYWN0aW9uID09IFwicHVzaFwiKSB7XG4gICAgICB0aGlzLm9uUHVzaChzZXJ2aWNlTmFtZXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmV4ZWNDb21wb3NlQ29tbWFuZChhY3Rpb24sIHNlcnZpY2VOYW1lcywgYWN0aW9uID09IFwicHNcIilcbiAgICAgIC50aGVuKChzdGRvdXQpID0+IHtcbiAgICAgICAgdGhpcy5wdXNoU3VjY2Vzc1ZlcmJvc2VOb3RpZmljYXRpb24oYCR7YWN0aW9ufSAke3NlcnZpY2VOYW1lcyAmJiBzZXJ2aWNlTmFtZXMubGVuZ3RoID4gMCA/IHNlcnZpY2VOYW1lcy5qb2luKCcgJykgOiBcIlwifWAsIHt9KTtcbiAgICAgICAgaWYgKGFjdGlvbiA9PSBcInBzXCIpIHtcbiAgICAgICAgICB0aGlzLmhhbmRsZVBTT3V0cHV0KHN0ZG91dCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGFjdGlvbiA9PSBcInVwXCIgfHwgYWN0aW9uID09IFwicmVzdGFydFwiKSB7XG4gICAgICAgICAgdGhpcy5jb21wb3NlUGFuZWwuY29tcG9zZUxvZ3Muc2VydmljZUxhdW5jaGVkKCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGFjdGlvbiA9PSBcInVwXCIgfHwgYWN0aW9uID09IFwicmVzdGFydFwiIHx8IGFjdGlvbiA9PSBcInN0b3BcIikge1xuICAgICAgICAgIHRoaXMuZXhlY1BTKCk7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAuY2F0Y2goKHN0ZGVycikgPT4ge1xuICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoYCR7YWN0aW9ufSAke3NlcnZpY2VOYW1lcyAmJiBzZXJ2aWNlTmFtZXMubGVuZ3RoID4gMCA/IHNlcnZpY2VOYW1lcy5qb2luKCcgJykgOiBcIlwifWAsIHtcbiAgICAgICAgICBkaXNtaXNzYWJsZTogZmFsc2UsXG4gICAgICAgICAgZGV0YWlsOiBzdGRlcnJcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sXG4gIHJlbmRlclNlcnZpY2VMaXN0OiBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmNvbXBvc2VQYW5lbCA9IFJlYWN0RE9NLnJlbmRlcihcbiAgICAgIDxDb21wb3NlUGFuZWxcbiAgICAgICAgb25BY3Rpb249e3RoaXMub25Db21wb3NlQWN0aW9uLmJpbmQodGhpcyl9XG4gICAgICAgIGNvbXBvc2VGaWxlUGF0aHM9e3N0b3JlLmdldFN0YXRlKCkuY29tcG9zZS5tYXAoY29uZiA9PiBjb25mLmZpbGVQYXRoKX1cbiAgICAgIC8+LFxuICAgICAgdGhpcy5kb2NrZXJWaWV3XG4gICAgKTtcbiAgfSxcbiAgdG9nZ2xlKCkge1xuICAgIHJldHVybiAoXG4gICAgICB0aGlzLmJvdHRvbVBhbmVsLmlzVmlzaWJsZSgpID9cbiAgICAgIHRoaXMuYm90dG9tUGFuZWwuaGlkZSgpIDpcbiAgICAgIHRoaXMuYm90dG9tUGFuZWwuc2hvdygpXG4gICAgKTtcbiAgfVxuXG59O1xuIl19