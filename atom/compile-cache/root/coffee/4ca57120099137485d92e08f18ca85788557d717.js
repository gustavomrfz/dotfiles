(function() {
  describe("Puppet grammar", function() {
    var grammar;
    grammar = null;
    beforeEach(function() {
      waitsForPromise(function() {
        return atom.packages.activatePackage("language-puppet");
      });
      return runs(function() {
        return grammar = atom.grammars.grammarForScopeName("source.puppet");
      });
    });
    it("parses the grammar", function() {
      expect(grammar).toBeTruthy();
      return expect(grammar.scopeName).toBe("source.puppet");
    });
    describe("separators", function() {
      it("tokenizes attribute separator", function() {
        var tokens;
        tokens = grammar.tokenizeLine('ensure => present').tokens;
        return expect(tokens[1]).toEqual({
          value: '=>',
          scopes: ['source.puppet', 'punctuation.separator.key-value.puppet']
        });
      });
      return it("tokenizes attribute separator with string values", function() {
        var tokens;
        tokens = grammar.tokenizeLine('ensure => "present"').tokens;
        return expect(tokens[1]).toEqual({
          value: '=>',
          scopes: ['source.puppet', 'punctuation.separator.key-value.puppet']
        });
      });
    });
    return describe("blocks", function() {
      it("tokenizes single quoted node", function() {
        var tokens;
        tokens = grammar.tokenizeLine("node 'hostname' {").tokens;
        return expect(tokens[0]).toEqual({
          value: 'node',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'storage.type.puppet']
        });
      });
      it("tokenizes double quoted node", function() {
        var tokens;
        tokens = grammar.tokenizeLine('node "hostname" {').tokens;
        return expect(tokens[0]).toEqual({
          value: 'node',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'storage.type.puppet']
        });
      });
      it("tokenizes non-default class parameters", function() {
        var tokens;
        tokens = grammar.tokenizeLine('class "classname" ($myvar) {').tokens;
        expect(tokens[5]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.no-default.untyped.puppet', 'variable.other.puppet', 'punctuation.definition.variable.puppet']
        });
        return expect(tokens[6]).toEqual({
          value: 'myvar',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.no-default.untyped.puppet', 'variable.other.puppet']
        });
      });
      it("tokenizes default class parameters", function() {
        var tokens;
        tokens = grammar.tokenizeLine('class "classname" ($myvar = "myval") {').tokens;
        expect(tokens[5]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.default.untyped.puppet', 'variable.other.puppet', 'punctuation.definition.variable.puppet']
        });
        return expect(tokens[6]).toEqual({
          value: 'myvar',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.default.untyped.puppet', 'variable.other.puppet']
        });
      });
      it("tokenizes non-default class parameter types", function() {
        var tokens;
        tokens = grammar.tokenizeLine('class "classname" (String $myvar) {').tokens;
        return expect(tokens[5]).toEqual({
          value: 'String',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.no-default.typed.puppet', 'storage.type.puppet']
        });
      });
      it("tokenizes default class parameter types", function() {
        var tokens;
        tokens = grammar.tokenizeLine('class "classname" (String $myvar = "myval") {').tokens;
        return expect(tokens[5]).toEqual({
          value: 'String',
          scopes: ['source.puppet', 'meta.definition.class.puppet', 'meta.function.argument.default.typed.puppet', 'storage.type.puppet']
        });
      });
      it("tokenizes include as an include function", function() {
        var tokens;
        tokens = grammar.tokenizeLine("contain foo").tokens;
        return expect(tokens[0]).toEqual({
          value: 'contain',
          scopes: ['source.puppet', 'meta.include.puppet', 'keyword.control.import.include.puppet']
        });
      });
      it("tokenizes contain as an include function", function() {
        var tokens;
        tokens = grammar.tokenizeLine('include foo').tokens;
        return expect(tokens[0]).toEqual({
          value: 'include',
          scopes: ['source.puppet', 'meta.include.puppet', 'keyword.control.import.include.puppet']
        });
      });
      it("tokenizes resource type and string title", function() {
        var tokens;
        tokens = grammar.tokenizeLine("package {'foo':}").tokens;
        expect(tokens[0]).toEqual({
          value: 'package',
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'storage.type.puppet']
        });
        return expect(tokens[2]).toEqual({
          value: "'foo'",
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'entity.name.section.puppet']
        });
      });
      it("tokenizes resource type and variable title", function() {
        var tokens;
        tokens = grammar.tokenizeLine("package {$foo:}").tokens;
        expect(tokens[0]).toEqual({
          value: 'package',
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'storage.type.puppet']
        });
        return expect(tokens[2]).toEqual({
          value: '$foo',
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'entity.name.section.puppet']
        });
      });
      it("tokenizes require classname as an include", function() {
        var tokens;
        tokens = grammar.tokenizeLine("require ::foo").tokens;
        return expect(tokens[0]).toEqual({
          value: 'require',
          scopes: ['source.puppet', 'meta.include.puppet', 'keyword.control.import.include.puppet']
        });
      });
      it("tokenizes require => variable as a parameter", function() {
        var tokens;
        tokens = grammar.tokenizeLine("require => Class['foo']").tokens;
        return expect(tokens[0]).toEqual({
          value: 'require ',
          scopes: ['source.puppet', 'constant.other.key.puppet']
        });
      });
      it("tokenizes regular variables", function() {
        var tokens;
        tokens = grammar.tokenizeLine('$foo').tokens;
        expect(tokens[0]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet', 'punctuation.definition.variable.puppet']
        });
        expect(tokens[1]).toEqual({
          value: 'foo',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet']
        });
        tokens = grammar.tokenizeLine('$_foo').tokens;
        expect(tokens[0]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet', 'punctuation.definition.variable.puppet']
        });
        expect(tokens[1]).toEqual({
          value: '_foo',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet']
        });
        tokens = grammar.tokenizeLine('$_foo_').tokens;
        expect(tokens[0]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet', 'punctuation.definition.variable.puppet']
        });
        expect(tokens[1]).toEqual({
          value: '_foo_',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet']
        });
        tokens = grammar.tokenizeLine('$::foo').tokens;
        expect(tokens[0]).toEqual({
          value: '$',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet', 'punctuation.definition.variable.puppet']
        });
        return expect(tokens[1]).toEqual({
          value: '::foo',
          scopes: ['source.puppet', 'variable.other.readwrite.global.puppet']
        });
      });
      return it("tokenizes resource types correctly", function() {
        var tokens;
        tokens = grammar.tokenizeLine("file {'/var/tmp':}").tokens;
        expect(tokens[0]).toEqual({
          value: 'file',
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'storage.type.puppet']
        });
        tokens = grammar.tokenizeLine("package {'foo':}").tokens;
        return expect(tokens[0]).toEqual({
          value: 'package',
          scopes: ['source.puppet', 'meta.definition.resource.puppet', 'storage.type.puppet']
        });
      });
    });
  });

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9sYW5ndWFnZS1wdXBwZXQvc3BlYy9wdXBwZXQtc3BlYy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBQSxRQUFBLENBQVMsZ0JBQVQsRUFBMkIsU0FBQTtBQUN6QixRQUFBO0lBQUEsT0FBQSxHQUFVO0lBRVYsVUFBQSxDQUFXLFNBQUE7TUFDVCxlQUFBLENBQWdCLFNBQUE7ZUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsaUJBQTlCO01BRGMsQ0FBaEI7YUFHQSxJQUFBLENBQUssU0FBQTtlQUNILE9BQUEsR0FBVSxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFkLENBQWtDLGVBQWxDO01BRFAsQ0FBTDtJQUpTLENBQVg7SUFPQSxFQUFBLENBQUcsb0JBQUgsRUFBeUIsU0FBQTtNQUN2QixNQUFBLENBQU8sT0FBUCxDQUFlLENBQUMsVUFBaEIsQ0FBQTthQUNBLE1BQUEsQ0FBTyxPQUFPLENBQUMsU0FBZixDQUF5QixDQUFDLElBQTFCLENBQStCLGVBQS9CO0lBRnVCLENBQXpCO0lBSUEsUUFBQSxDQUFTLFlBQVQsRUFBdUIsU0FBQTtNQUNyQixFQUFBLENBQUcsK0JBQUgsRUFBb0MsU0FBQTtBQUNsQyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixtQkFBckI7ZUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLElBQVA7VUFBYSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHdDQUFsQixDQUFyQjtTQUExQjtNQUZrQyxDQUFwQzthQUlBLEVBQUEsQ0FBRyxrREFBSCxFQUF1RCxTQUFBO0FBQ3JELFlBQUE7UUFBQyxTQUFVLE9BQU8sQ0FBQyxZQUFSLENBQXFCLHFCQUFyQjtlQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sSUFBUDtVQUFhLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLENBQXJCO1NBQTFCO01BRnFELENBQXZEO0lBTHFCLENBQXZCO1dBU0EsUUFBQSxDQUFTLFFBQVQsRUFBbUIsU0FBQTtNQUNqQixFQUFBLENBQUcsOEJBQUgsRUFBbUMsU0FBQTtBQUNqQyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixtQkFBckI7ZUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLE1BQVA7VUFBZSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLDhCQUFsQixFQUFrRCxxQkFBbEQsQ0FBdkI7U0FBMUI7TUFGaUMsQ0FBbkM7TUFJQSxFQUFBLENBQUcsOEJBQUgsRUFBbUMsU0FBQTtBQUNqQyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixtQkFBckI7ZUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLE1BQVA7VUFBZSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLDhCQUFsQixFQUFrRCxxQkFBbEQsQ0FBdkI7U0FBMUI7TUFGaUMsQ0FBbkM7TUFJQSxFQUFBLENBQUcsd0NBQUgsRUFBNkMsU0FBQTtBQUMzQyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQiw4QkFBckI7UUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLEdBQVA7VUFBWSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLDhCQUFsQixFQUFrRCxrREFBbEQsRUFBc0csdUJBQXRHLEVBQStILHdDQUEvSCxDQUFwQjtTQUExQjtlQUNBLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sT0FBUDtVQUFnQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLDhCQUFsQixFQUFrRCxrREFBbEQsRUFBc0csdUJBQXRHLENBQXhCO1NBQTFCO01BSDJDLENBQTdDO01BS0EsRUFBQSxDQUFHLG9DQUFILEVBQXlDLFNBQUE7QUFDdkMsWUFBQTtRQUFDLFNBQVUsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsd0NBQXJCO1FBQ1gsTUFBQSxDQUFPLE1BQU8sQ0FBQSxDQUFBLENBQWQsQ0FBaUIsQ0FBQyxPQUFsQixDQUEwQjtVQUFBLEtBQUEsRUFBTyxHQUFQO1VBQVksTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQiw4QkFBbEIsRUFBa0QsK0NBQWxELEVBQW1HLHVCQUFuRyxFQUE0SCx3Q0FBNUgsQ0FBcEI7U0FBMUI7ZUFDQSxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLE9BQVA7VUFBZ0IsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQiw4QkFBbEIsRUFBa0QsK0NBQWxELEVBQW1HLHVCQUFuRyxDQUF4QjtTQUExQjtNQUh1QyxDQUF6QztNQUtBLEVBQUEsQ0FBRyw2Q0FBSCxFQUFrRCxTQUFBO0FBQ2hELFlBQUE7UUFBQyxTQUFVLE9BQU8sQ0FBQyxZQUFSLENBQXFCLHFDQUFyQjtlQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sUUFBUDtVQUFpQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLDhCQUFsQixFQUFrRCxnREFBbEQsRUFBb0cscUJBQXBHLENBQXpCO1NBQTFCO01BRmdELENBQWxEO01BSUEsRUFBQSxDQUFHLHlDQUFILEVBQThDLFNBQUE7QUFDNUMsWUFBQTtRQUFDLFNBQVUsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsK0NBQXJCO2VBQ1gsTUFBQSxDQUFPLE1BQU8sQ0FBQSxDQUFBLENBQWQsQ0FBaUIsQ0FBQyxPQUFsQixDQUEwQjtVQUFBLEtBQUEsRUFBTyxRQUFQO1VBQWlCLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0IsOEJBQWxCLEVBQWtELDZDQUFsRCxFQUFpRyxxQkFBakcsQ0FBekI7U0FBMUI7TUFGNEMsQ0FBOUM7TUFJQSxFQUFBLENBQUcsMENBQUgsRUFBK0MsU0FBQTtBQUM3QyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixhQUFyQjtlQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sU0FBUDtVQUFrQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHFCQUFsQixFQUF5Qyx1Q0FBekMsQ0FBMUI7U0FBMUI7TUFGNkMsQ0FBL0M7TUFJQSxFQUFBLENBQUcsMENBQUgsRUFBK0MsU0FBQTtBQUM3QyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixhQUFyQjtlQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sU0FBUDtVQUFrQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHFCQUFsQixFQUF5Qyx1Q0FBekMsQ0FBMUI7U0FBMUI7TUFGNkMsQ0FBL0M7TUFJQSxFQUFBLENBQUcsMENBQUgsRUFBK0MsU0FBQTtBQUM3QyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixrQkFBckI7UUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLFNBQVA7VUFBa0IsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQixpQ0FBbEIsRUFBcUQscUJBQXJELENBQTFCO1NBQTFCO2VBQ0EsTUFBQSxDQUFPLE1BQU8sQ0FBQSxDQUFBLENBQWQsQ0FBaUIsQ0FBQyxPQUFsQixDQUEwQjtVQUFBLEtBQUEsRUFBTyxPQUFQO1VBQWdCLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0IsaUNBQWxCLEVBQXFELDRCQUFyRCxDQUF4QjtTQUExQjtNQUg2QyxDQUEvQztNQUtBLEVBQUEsQ0FBRyw0Q0FBSCxFQUFpRCxTQUFBO0FBQy9DLFlBQUE7UUFBQyxTQUFVLE9BQU8sQ0FBQyxZQUFSLENBQXFCLGlCQUFyQjtRQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sU0FBUDtVQUFrQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLGlDQUFsQixFQUFxRCxxQkFBckQsQ0FBMUI7U0FBMUI7ZUFDQSxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLE1BQVA7VUFBZSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLGlDQUFsQixFQUFxRCw0QkFBckQsQ0FBdkI7U0FBMUI7TUFIK0MsQ0FBakQ7TUFLQSxFQUFBLENBQUcsMkNBQUgsRUFBZ0QsU0FBQTtBQUM5QyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixlQUFyQjtlQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sU0FBUDtVQUFrQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHFCQUFsQixFQUF5Qyx1Q0FBekMsQ0FBMUI7U0FBMUI7TUFGOEMsQ0FBaEQ7TUFJQSxFQUFBLENBQUcsOENBQUgsRUFBbUQsU0FBQTtBQUNqRCxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQix5QkFBckI7ZUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLFVBQVA7VUFBbUIsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQiwyQkFBbEIsQ0FBM0I7U0FBMUI7TUFGaUQsQ0FBbkQ7TUFJQSxFQUFBLENBQUcsNkJBQUgsRUFBa0MsU0FBQTtBQUNoQyxZQUFBO1FBQUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixNQUFyQjtRQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sR0FBUDtVQUFZLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLEVBQTRELHdDQUE1RCxDQUFwQjtTQUExQjtRQUNBLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sS0FBUDtVQUFjLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLENBQXRCO1NBQTFCO1FBRUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixPQUFyQjtRQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sR0FBUDtVQUFZLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLEVBQTRELHdDQUE1RCxDQUFwQjtTQUExQjtRQUNBLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sTUFBUDtVQUFlLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLENBQXZCO1NBQTFCO1FBRUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixRQUFyQjtRQUNYLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sR0FBUDtVQUFZLE1BQUEsRUFBUSxDQUFDLGVBQUQsRUFBa0Isd0NBQWxCLEVBQTRELHdDQUE1RCxDQUFwQjtTQUExQjtRQUNBLE1BQUEsQ0FBTyxNQUFPLENBQUEsQ0FBQSxDQUFkLENBQWlCLENBQUMsT0FBbEIsQ0FBMEI7VUFBQSxLQUFBLEVBQU8sT0FBUDtVQUFnQixNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHdDQUFsQixDQUF4QjtTQUExQjtRQUVDLFNBQVUsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsUUFBckI7UUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLEdBQVA7VUFBWSxNQUFBLEVBQVEsQ0FBQyxlQUFELEVBQWtCLHdDQUFsQixFQUE0RCx3Q0FBNUQsQ0FBcEI7U0FBMUI7ZUFDQSxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLE9BQVA7VUFBZ0IsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQix3Q0FBbEIsQ0FBeEI7U0FBMUI7TUFmZ0MsQ0FBbEM7YUFpQkEsRUFBQSxDQUFHLG9DQUFILEVBQXlDLFNBQUE7QUFDdkMsWUFBQTtRQUFDLFNBQVUsT0FBTyxDQUFDLFlBQVIsQ0FBcUIsb0JBQXJCO1FBQ1gsTUFBQSxDQUFPLE1BQU8sQ0FBQSxDQUFBLENBQWQsQ0FBaUIsQ0FBQyxPQUFsQixDQUEwQjtVQUFBLEtBQUEsRUFBTyxNQUFQO1VBQWUsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQixpQ0FBbEIsRUFBcUQscUJBQXJELENBQXZCO1NBQTFCO1FBRUMsU0FBVSxPQUFPLENBQUMsWUFBUixDQUFxQixrQkFBckI7ZUFDWCxNQUFBLENBQU8sTUFBTyxDQUFBLENBQUEsQ0FBZCxDQUFpQixDQUFDLE9BQWxCLENBQTBCO1VBQUEsS0FBQSxFQUFPLFNBQVA7VUFBa0IsTUFBQSxFQUFRLENBQUMsZUFBRCxFQUFrQixpQ0FBbEIsRUFBcUQscUJBQXJELENBQTFCO1NBQTFCO01BTHVDLENBQXpDO0lBdEVpQixDQUFuQjtFQXZCeUIsQ0FBM0I7QUFBQSIsInNvdXJjZXNDb250ZW50IjpbImRlc2NyaWJlIFwiUHVwcGV0IGdyYW1tYXJcIiwgLT5cbiAgZ3JhbW1hciA9IG51bGxcblxuICBiZWZvcmVFYWNoIC0+XG4gICAgd2FpdHNGb3JQcm9taXNlIC0+XG4gICAgICBhdG9tLnBhY2thZ2VzLmFjdGl2YXRlUGFja2FnZShcImxhbmd1YWdlLXB1cHBldFwiKVxuXG4gICAgcnVucyAtPlxuICAgICAgZ3JhbW1hciA9IGF0b20uZ3JhbW1hcnMuZ3JhbW1hckZvclNjb3BlTmFtZShcInNvdXJjZS5wdXBwZXRcIilcblxuICBpdCBcInBhcnNlcyB0aGUgZ3JhbW1hclwiLCAtPlxuICAgIGV4cGVjdChncmFtbWFyKS50b0JlVHJ1dGh5KClcbiAgICBleHBlY3QoZ3JhbW1hci5zY29wZU5hbWUpLnRvQmUgXCJzb3VyY2UucHVwcGV0XCJcblxuICBkZXNjcmliZSBcInNlcGFyYXRvcnNcIiwgLT5cbiAgICBpdCBcInRva2VuaXplcyBhdHRyaWJ1dGUgc2VwYXJhdG9yXCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKCdlbnN1cmUgPT4gcHJlc2VudCcpXG4gICAgICBleHBlY3QodG9rZW5zWzFdKS50b0VxdWFsIHZhbHVlOiAnPT4nLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdwdW5jdHVhdGlvbi5zZXBhcmF0b3Iua2V5LXZhbHVlLnB1cHBldCddXG5cbiAgICBpdCBcInRva2VuaXplcyBhdHRyaWJ1dGUgc2VwYXJhdG9yIHdpdGggc3RyaW5nIHZhbHVlc1wiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnZW5zdXJlID0+IFwicHJlc2VudFwiJylcbiAgICAgIGV4cGVjdCh0b2tlbnNbMV0pLnRvRXF1YWwgdmFsdWU6ICc9PicsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ3B1bmN0dWF0aW9uLnNlcGFyYXRvci5rZXktdmFsdWUucHVwcGV0J11cblxuICBkZXNjcmliZSBcImJsb2Nrc1wiLCAtPlxuICAgIGl0IFwidG9rZW5pemVzIHNpbmdsZSBxdW90ZWQgbm9kZVwiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZShcIm5vZGUgJ2hvc3RuYW1lJyB7XCIpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAnbm9kZScsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5jbGFzcy5wdXBwZXQnLCAnc3RvcmFnZS50eXBlLnB1cHBldCddXG5cbiAgICBpdCBcInRva2VuaXplcyBkb3VibGUgcXVvdGVkIG5vZGVcIiwgLT5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoJ25vZGUgXCJob3N0bmFtZVwiIHsnKVxuICAgICAgZXhwZWN0KHRva2Vuc1swXSkudG9FcXVhbCB2YWx1ZTogJ25vZGUnLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmRlZmluaXRpb24uY2xhc3MucHVwcGV0JywgJ3N0b3JhZ2UudHlwZS5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgbm9uLWRlZmF1bHQgY2xhc3MgcGFyYW1ldGVyc1wiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnY2xhc3MgXCJjbGFzc25hbWVcIiAoJG15dmFyKSB7JylcbiAgICAgIGV4cGVjdCh0b2tlbnNbNV0pLnRvRXF1YWwgdmFsdWU6ICckJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAnbWV0YS5kZWZpbml0aW9uLmNsYXNzLnB1cHBldCcsICdtZXRhLmZ1bmN0aW9uLmFyZ3VtZW50Lm5vLWRlZmF1bHQudW50eXBlZC5wdXBwZXQnLCAndmFyaWFibGUub3RoZXIucHVwcGV0JywgJ3B1bmN0dWF0aW9uLmRlZmluaXRpb24udmFyaWFibGUucHVwcGV0J11cbiAgICAgIGV4cGVjdCh0b2tlbnNbNl0pLnRvRXF1YWwgdmFsdWU6ICdteXZhcicsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5jbGFzcy5wdXBwZXQnLCAnbWV0YS5mdW5jdGlvbi5hcmd1bWVudC5uby1kZWZhdWx0LnVudHlwZWQucHVwcGV0JywgJ3ZhcmlhYmxlLm90aGVyLnB1cHBldCddXG5cbiAgICBpdCBcInRva2VuaXplcyBkZWZhdWx0IGNsYXNzIHBhcmFtZXRlcnNcIiwgLT5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoJ2NsYXNzIFwiY2xhc3NuYW1lXCIgKCRteXZhciA9IFwibXl2YWxcIikgeycpXG4gICAgICBleHBlY3QodG9rZW5zWzVdKS50b0VxdWFsIHZhbHVlOiAnJCcsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5jbGFzcy5wdXBwZXQnLCAnbWV0YS5mdW5jdGlvbi5hcmd1bWVudC5kZWZhdWx0LnVudHlwZWQucHVwcGV0JywgJ3ZhcmlhYmxlLm90aGVyLnB1cHBldCcsICdwdW5jdHVhdGlvbi5kZWZpbml0aW9uLnZhcmlhYmxlLnB1cHBldCddXG4gICAgICBleHBlY3QodG9rZW5zWzZdKS50b0VxdWFsIHZhbHVlOiAnbXl2YXInLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmRlZmluaXRpb24uY2xhc3MucHVwcGV0JywgJ21ldGEuZnVuY3Rpb24uYXJndW1lbnQuZGVmYXVsdC51bnR5cGVkLnB1cHBldCcsICd2YXJpYWJsZS5vdGhlci5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgbm9uLWRlZmF1bHQgY2xhc3MgcGFyYW1ldGVyIHR5cGVzXCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKCdjbGFzcyBcImNsYXNzbmFtZVwiIChTdHJpbmcgJG15dmFyKSB7JylcbiAgICAgIGV4cGVjdCh0b2tlbnNbNV0pLnRvRXF1YWwgdmFsdWU6ICdTdHJpbmcnLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmRlZmluaXRpb24uY2xhc3MucHVwcGV0JywgJ21ldGEuZnVuY3Rpb24uYXJndW1lbnQubm8tZGVmYXVsdC50eXBlZC5wdXBwZXQnLCAnc3RvcmFnZS50eXBlLnB1cHBldCddXG5cbiAgICBpdCBcInRva2VuaXplcyBkZWZhdWx0IGNsYXNzIHBhcmFtZXRlciB0eXBlc1wiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnY2xhc3MgXCJjbGFzc25hbWVcIiAoU3RyaW5nICRteXZhciA9IFwibXl2YWxcIikgeycpXG4gICAgICBleHBlY3QodG9rZW5zWzVdKS50b0VxdWFsIHZhbHVlOiAnU3RyaW5nJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAnbWV0YS5kZWZpbml0aW9uLmNsYXNzLnB1cHBldCcsICdtZXRhLmZ1bmN0aW9uLmFyZ3VtZW50LmRlZmF1bHQudHlwZWQucHVwcGV0JywgJ3N0b3JhZ2UudHlwZS5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgaW5jbHVkZSBhcyBhbiBpbmNsdWRlIGZ1bmN0aW9uXCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKFwiY29udGFpbiBmb29cIilcbiAgICAgIGV4cGVjdCh0b2tlbnNbMF0pLnRvRXF1YWwgdmFsdWU6ICdjb250YWluJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAnbWV0YS5pbmNsdWRlLnB1cHBldCcsICdrZXl3b3JkLmNvbnRyb2wuaW1wb3J0LmluY2x1ZGUucHVwcGV0J11cblxuICAgIGl0IFwidG9rZW5pemVzIGNvbnRhaW4gYXMgYW4gaW5jbHVkZSBmdW5jdGlvblwiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnaW5jbHVkZSBmb28nKVxuICAgICAgZXhwZWN0KHRva2Vuc1swXSkudG9FcXVhbCB2YWx1ZTogJ2luY2x1ZGUnLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmluY2x1ZGUucHVwcGV0JywgJ2tleXdvcmQuY29udHJvbC5pbXBvcnQuaW5jbHVkZS5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgcmVzb3VyY2UgdHlwZSBhbmQgc3RyaW5nIHRpdGxlXCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKFwicGFja2FnZSB7J2Zvbyc6fVwiKVxuICAgICAgZXhwZWN0KHRva2Vuc1swXSkudG9FcXVhbCB2YWx1ZTogJ3BhY2thZ2UnLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmRlZmluaXRpb24ucmVzb3VyY2UucHVwcGV0JywgJ3N0b3JhZ2UudHlwZS5wdXBwZXQnXVxuICAgICAgZXhwZWN0KHRva2Vuc1syXSkudG9FcXVhbCB2YWx1ZTogXCInZm9vJ1wiLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICdtZXRhLmRlZmluaXRpb24ucmVzb3VyY2UucHVwcGV0JywgJ2VudGl0eS5uYW1lLnNlY3Rpb24ucHVwcGV0J11cblxuICAgIGl0IFwidG9rZW5pemVzIHJlc291cmNlIHR5cGUgYW5kIHZhcmlhYmxlIHRpdGxlXCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKFwicGFja2FnZSB7JGZvbzp9XCIpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAncGFja2FnZScsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5yZXNvdXJjZS5wdXBwZXQnLCAnc3RvcmFnZS50eXBlLnB1cHBldCddXG4gICAgICBleHBlY3QodG9rZW5zWzJdKS50b0VxdWFsIHZhbHVlOiAnJGZvbycsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5yZXNvdXJjZS5wdXBwZXQnLCAnZW50aXR5Lm5hbWUuc2VjdGlvbi5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgcmVxdWlyZSBjbGFzc25hbWUgYXMgYW4gaW5jbHVkZVwiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZShcInJlcXVpcmUgOjpmb29cIilcbiAgICAgIGV4cGVjdCh0b2tlbnNbMF0pLnRvRXF1YWwgdmFsdWU6ICdyZXF1aXJlJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAnbWV0YS5pbmNsdWRlLnB1cHBldCcsICdrZXl3b3JkLmNvbnRyb2wuaW1wb3J0LmluY2x1ZGUucHVwcGV0J11cblxuICAgIGl0IFwidG9rZW5pemVzIHJlcXVpcmUgPT4gdmFyaWFibGUgYXMgYSBwYXJhbWV0ZXJcIiwgLT5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoXCJyZXF1aXJlID0+IENsYXNzWydmb28nXVwiKVxuICAgICAgZXhwZWN0KHRva2Vuc1swXSkudG9FcXVhbCB2YWx1ZTogJ3JlcXVpcmUgJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAnY29uc3RhbnQub3RoZXIua2V5LnB1cHBldCddXG5cbiAgICBpdCBcInRva2VuaXplcyByZWd1bGFyIHZhcmlhYmxlc1wiLCAtPlxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnJGZvbycpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAnJCcsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ3ZhcmlhYmxlLm90aGVyLnJlYWR3cml0ZS5nbG9iYWwucHVwcGV0JywgJ3B1bmN0dWF0aW9uLmRlZmluaXRpb24udmFyaWFibGUucHVwcGV0J11cbiAgICAgIGV4cGVjdCh0b2tlbnNbMV0pLnRvRXF1YWwgdmFsdWU6ICdmb28nLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICd2YXJpYWJsZS5vdGhlci5yZWFkd3JpdGUuZ2xvYmFsLnB1cHBldCddXG5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoJyRfZm9vJylcbiAgICAgIGV4cGVjdCh0b2tlbnNbMF0pLnRvRXF1YWwgdmFsdWU6ICckJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAndmFyaWFibGUub3RoZXIucmVhZHdyaXRlLmdsb2JhbC5wdXBwZXQnLCAncHVuY3R1YXRpb24uZGVmaW5pdGlvbi52YXJpYWJsZS5wdXBwZXQnXVxuICAgICAgZXhwZWN0KHRva2Vuc1sxXSkudG9FcXVhbCB2YWx1ZTogJ19mb28nLCBzY29wZXM6IFsnc291cmNlLnB1cHBldCcsICd2YXJpYWJsZS5vdGhlci5yZWFkd3JpdGUuZ2xvYmFsLnB1cHBldCddXG5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoJyRfZm9vXycpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAnJCcsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ3ZhcmlhYmxlLm90aGVyLnJlYWR3cml0ZS5nbG9iYWwucHVwcGV0JywgJ3B1bmN0dWF0aW9uLmRlZmluaXRpb24udmFyaWFibGUucHVwcGV0J11cbiAgICAgIGV4cGVjdCh0b2tlbnNbMV0pLnRvRXF1YWwgdmFsdWU6ICdfZm9vXycsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ3ZhcmlhYmxlLm90aGVyLnJlYWR3cml0ZS5nbG9iYWwucHVwcGV0J11cblxuICAgICAge3Rva2Vuc30gPSBncmFtbWFyLnRva2VuaXplTGluZSgnJDo6Zm9vJylcbiAgICAgIGV4cGVjdCh0b2tlbnNbMF0pLnRvRXF1YWwgdmFsdWU6ICckJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAndmFyaWFibGUub3RoZXIucmVhZHdyaXRlLmdsb2JhbC5wdXBwZXQnLCAncHVuY3R1YXRpb24uZGVmaW5pdGlvbi52YXJpYWJsZS5wdXBwZXQnXVxuICAgICAgZXhwZWN0KHRva2Vuc1sxXSkudG9FcXVhbCB2YWx1ZTogJzo6Zm9vJywgc2NvcGVzOiBbJ3NvdXJjZS5wdXBwZXQnLCAndmFyaWFibGUub3RoZXIucmVhZHdyaXRlLmdsb2JhbC5wdXBwZXQnXVxuXG4gICAgaXQgXCJ0b2tlbml6ZXMgcmVzb3VyY2UgdHlwZXMgY29ycmVjdGx5XCIsIC0+XG4gICAgICB7dG9rZW5zfSA9IGdyYW1tYXIudG9rZW5pemVMaW5lKFwiZmlsZSB7Jy92YXIvdG1wJzp9XCIpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAnZmlsZScsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5yZXNvdXJjZS5wdXBwZXQnLCAnc3RvcmFnZS50eXBlLnB1cHBldCddXG5cbiAgICAgIHt0b2tlbnN9ID0gZ3JhbW1hci50b2tlbml6ZUxpbmUoXCJwYWNrYWdlIHsnZm9vJzp9XCIpXG4gICAgICBleHBlY3QodG9rZW5zWzBdKS50b0VxdWFsIHZhbHVlOiAncGFja2FnZScsIHNjb3BlczogWydzb3VyY2UucHVwcGV0JywgJ21ldGEuZGVmaW5pdGlvbi5yZXNvdXJjZS5wdXBwZXQnLCAnc3RvcmFnZS50eXBlLnB1cHBldCddXG4iXX0=
