(function() {
  var RubyBlockView;

  module.exports = RubyBlockView = (function() {
    function RubyBlockView(serializeState) {
      this.element = document.createElement('div');
      this.element.classList.add('ruby-block');
    }

    RubyBlockView.prototype.destroy = function() {
      return this.element.remove();
    };

    RubyBlockView.prototype.getElement = function() {
      return this.element;
    };

    RubyBlockView.prototype.updateMessage = function(rowNumber) {
      var message, row;
      row = atom.workspace.getActiveTextEditor().lineTextForBufferRow(rowNumber);
      if (this.element.hasChildNodes()) {
        this.element.removeChild(this.element.firstChild);
      }
      message = document.createElement('div');
      message.textContent = "Line: " + (rowNumber + 1) + " " + row;
      message.classList.add('message');
      return this.element.appendChild(message);
    };

    return RubyBlockView;

  })();

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL2hvbWUvZ3VzdGF2by8uYXRvbS9wYWNrYWdlcy9ydWJ5LWJsb2NrL2xpYi9ydWJ5LWJsb2NrLXZpZXcuY29mZmVlIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQUEsTUFBQTs7RUFBQSxNQUFNLENBQUMsT0FBUCxHQUNNO0lBQ1MsdUJBQUMsY0FBRDtNQUVYLElBQUMsQ0FBQSxPQUFELEdBQVcsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDWCxJQUFDLENBQUEsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFuQixDQUF1QixZQUF2QjtJQUhXOzs0QkFNYixPQUFBLEdBQVMsU0FBQTthQUNQLElBQUMsQ0FBQSxPQUFPLENBQUMsTUFBVCxDQUFBO0lBRE87OzRCQUdULFVBQUEsR0FBWSxTQUFBO2FBQ1YsSUFBQyxDQUFBO0lBRFM7OzRCQUdaLGFBQUEsR0FBZSxTQUFDLFNBQUQ7QUFDYixVQUFBO01BQUEsR0FBQSxHQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQWYsQ0FBQSxDQUFvQyxDQUFDLG9CQUFyQyxDQUEwRCxTQUExRDtNQUVOLElBQTZDLElBQUMsQ0FBQSxPQUFPLENBQUMsYUFBVCxDQUFBLENBQTdDO1FBQUEsSUFBQyxDQUFBLE9BQU8sQ0FBQyxXQUFULENBQXFCLElBQUMsQ0FBQSxPQUFPLENBQUMsVUFBOUIsRUFBQTs7TUFFQSxPQUFBLEdBQVUsUUFBUSxDQUFDLGFBQVQsQ0FBdUIsS0FBdkI7TUFDVixPQUFPLENBQUMsV0FBUixHQUFzQixRQUFBLEdBQVEsQ0FBQyxTQUFBLEdBQVUsQ0FBWCxDQUFSLEdBQXFCLEdBQXJCLEdBQXdCO01BQzlDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBbEIsQ0FBc0IsU0FBdEI7YUFDQSxJQUFDLENBQUEsT0FBTyxDQUFDLFdBQVQsQ0FBcUIsT0FBckI7SUFSYTs7Ozs7QUFkakIiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9XG5jbGFzcyBSdWJ5QmxvY2tWaWV3XG4gIGNvbnN0cnVjdG9yOiAoc2VyaWFsaXplU3RhdGUpIC0+XG4gICAgIyBDcmVhdGUgcm9vdCBlbGVtZW50XG4gICAgQGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgIEBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3J1YnktYmxvY2snKVxuXG4gICMgVGVhciBkb3duIGFueSBzdGF0ZSBhbmQgZGV0YWNoXG4gIGRlc3Ryb3k6IC0+XG4gICAgQGVsZW1lbnQucmVtb3ZlKClcblxuICBnZXRFbGVtZW50OiAtPlxuICAgIEBlbGVtZW50XG5cbiAgdXBkYXRlTWVzc2FnZTogKHJvd051bWJlciktPlxuICAgIHJvdyA9IGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVRleHRFZGl0b3IoKS5saW5lVGV4dEZvckJ1ZmZlclJvdyhyb3dOdW1iZXIpXG5cbiAgICBAZWxlbWVudC5yZW1vdmVDaGlsZChAZWxlbWVudC5maXJzdENoaWxkKSBpZiBAZWxlbWVudC5oYXNDaGlsZE5vZGVzKClcbiAgICAjIENyZWF0ZSBtZXNzYWdlIGVsZW1lbnRcbiAgICBtZXNzYWdlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICBtZXNzYWdlLnRleHRDb250ZW50ID0gXCJMaW5lOiAje3Jvd051bWJlcisxfSAje3Jvd31cIlxuICAgIG1lc3NhZ2UuY2xhc3NMaXN0LmFkZCgnbWVzc2FnZScpXG4gICAgQGVsZW1lbnQuYXBwZW5kQ2hpbGQobWVzc2FnZSlcbiJdfQ==
