Object.defineProperty(exports, '__esModule', {
  value: true
});

// eslint-disable-next-line import/no-extraneous-dependencies, import/extensions

var _atom = require('atom');

// Some internal variables
'use babel';var subscriptions = undefined;
var executablePath = undefined;
var errorLevel = undefined;
var allSettings = undefined;

exports['default'] = {
  // Activate linter
  activate: function activate() {
    var helpers = require('atom-linter');

    subscriptions = new _atom.CompositeDisposable();

    subscriptions.add(atom.config.observe('linter-puppet-lint', function (settings) {
      allSettings = settings;
      executablePath = settings.executablePath;
      errorLevel = settings.errorLevel;
    }));

    require('atom-package-deps').install('linter-puppet-lint');

    // Check if puppet-lint has support for the %{column} placeholder
    helpers.exec(executablePath, ['--help']).then(function (output) {
      var regexColumn = /%{column}/;

      if (regexColumn.exec(output) === null) {
        atom.config.set('linter-puppet-lint.oldVersion', true);
        atom.notifications.addError('You are using an old version of puppet-lint!', {
          detail: 'Please upgrade your version of puppet-lint.\n' + 'Check the README for further information.'
        });
      } else {
        atom.config.set('linter-puppet-lint.oldVersion', false);
      }
    });
  },

  deactivate: function deactivate() {
    subscriptions.dispose();
  },

  provideLinter: function provideLinter() {
    var helpers = require('atom-linter');

    return {
      name: 'Puppet-Lint',
      grammarScopes: ['source.puppet'],
      scope: 'file',
      lintsOnChange: false,
      lint: function lint(activeEditor) {
        // Check again if puppet-lint is too old to support column information
        if (atom.config.get('linter-puppet-lint.oldVersion') === true) {
          atom.notifications.addError('You are using an old version of puppet-lint!', {
            detail: 'Please upgrade your version of puppet-lint.\n' + 'Check the README for further information.'
          });
          return [];
        }

        // Setup const vars for later use
        var path = require('path');
        var file = activeEditor.getPath();
        var cwd = path.dirname(file);
        // With the custom format the puppet-int ouput looks like this:
        // error mongodb::service not in autoload module layout 3 7
        var regexLine = /^(warning|error)\s(.*)\s(\d+)\s(\d+)$/;
        var args = ['--log-format', '%{kind} %{message} %{line} %{column}', '--error-level', errorLevel];

        var optionsMap = require('./flags.js');

        // Add the flags to the command options
        Object.keys(allSettings).forEach(function (flag) {
          if (Object.hasOwnProperty.call(optionsMap, flag) && allSettings[flag] === true) {
            args.push(optionsMap[flag]);
          }
        });

        // Add the file to be checked to the arguments
        args.push(file);

        return helpers.exec(executablePath, args, { cwd: cwd, ignoreExitCode: true }).then(function (output) {
          // If puppet-lint errors to stdout then redirect the message to stderr so it is caught
          if (/puppet-lint:/.exec(output)) {
            throw output;
          }
          var toReturn = [];

          // Check for proper warnings and errors from stdout
          output.split(/\r?\n/).forEach(function (line) {
            var matches = regexLine.exec(line);
            if (matches != null) {
              var errLine = Number.parseInt(matches[3], 10) - 1;
              var errCol = Number.parseInt(matches[4], 10) - 1;

              toReturn.push({
                severity: matches[1],
                excerpt: matches[2],
                location: {
                  file: file,
                  position: helpers.generateRange(activeEditor, errLine, errCol)
                }
              });
            }
          });
          return toReturn;
        });
      }
    };
  }
};
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvbGludGVyLXB1cHBldC1saW50L2xpYi9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztvQkFHb0MsTUFBTTs7O0FBSDFDLFdBQVcsQ0FBQyxBQU1aLElBQUksYUFBYSxZQUFBLENBQUM7QUFDbEIsSUFBSSxjQUFjLFlBQUEsQ0FBQztBQUNuQixJQUFJLFVBQVUsWUFBQSxDQUFDO0FBQ2YsSUFBSSxXQUFXLFlBQUEsQ0FBQzs7cUJBRUQ7O0FBRWIsVUFBUSxFQUFBLG9CQUFHO0FBQ1QsUUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDOztBQUV2QyxpQkFBYSxHQUFHLCtCQUF5QixDQUFDOztBQUUxQyxpQkFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxVQUFDLFFBQVEsRUFBSztBQUN4RSxpQkFBVyxHQUFHLFFBQVEsQ0FBQztBQUN2QixvQkFBYyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUM7QUFDekMsZ0JBQVUsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO0tBQ2xDLENBQUMsQ0FBQyxDQUFDOztBQUVKLFdBQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOzs7QUFHM0QsV0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUN4RCxVQUFNLFdBQVcsR0FBRyxXQUFXLENBQUM7O0FBRWhDLFVBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7QUFDckMsWUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDdkQsWUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQ3pCLDhDQUE4QyxFQUM5QztBQUNFLGdCQUFNLEVBQUUsK0NBQStDLEdBQ3JELDJDQUEyQztTQUM5QyxDQUNGLENBQUM7T0FDSCxNQUFNO0FBQ0wsWUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsS0FBSyxDQUFDLENBQUM7T0FDekQ7S0FDRixDQUFDLENBQUM7R0FDSjs7QUFFRCxZQUFVLEVBQUEsc0JBQUc7QUFDWCxpQkFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO0dBQ3pCOztBQUVELGVBQWEsRUFBQSx5QkFBRztBQUNkLFFBQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQzs7QUFFdkMsV0FBTztBQUNMLFVBQUksRUFBRSxhQUFhO0FBQ25CLG1CQUFhLEVBQUUsQ0FBQyxlQUFlLENBQUM7QUFDaEMsV0FBSyxFQUFFLE1BQU07QUFDYixtQkFBYSxFQUFFLEtBQUs7QUFDcEIsVUFBSSxFQUFFLGNBQUMsWUFBWSxFQUFLOztBQUV0QixZQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLEtBQUssSUFBSSxFQUFFO0FBQzdELGNBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUN6Qiw4Q0FBOEMsRUFDOUM7QUFDRSxrQkFBTSxFQUFFLCtDQUErQyxHQUNyRCwyQ0FBMkM7V0FDOUMsQ0FDRixDQUFDO0FBQ0YsaUJBQU8sRUFBRSxDQUFDO1NBQ1g7OztBQUdELFlBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM3QixZQUFNLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDcEMsWUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzs7O0FBRy9CLFlBQU0sU0FBUyxHQUFHLHVDQUF1QyxDQUFDO0FBQzFELFlBQU0sSUFBSSxHQUFHLENBQUMsY0FBYyxFQUFFLHNDQUFzQyxFQUFFLGVBQWUsRUFBRSxVQUFVLENBQUMsQ0FBQzs7QUFFbkcsWUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDOzs7QUFHekMsY0FBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDekMsY0FBSSxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRTtBQUM5RSxnQkFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztXQUM3QjtTQUNGLENBQUMsQ0FBQzs7O0FBR0gsWUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7QUFFaEIsZUFBTyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUgsR0FBRyxFQUFFLGNBQWMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU0sRUFBSzs7QUFFeEYsY0FBSSxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO0FBQy9CLGtCQUFNLE1BQU0sQ0FBQztXQUNkO0FBQ0QsY0FBTSxRQUFRLEdBQUcsRUFBRSxDQUFDOzs7QUFHcEIsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ3RDLGdCQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3JDLGdCQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7QUFDbkIsa0JBQU0sT0FBTyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNwRCxrQkFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDOztBQUVuRCxzQkFBUSxDQUFDLElBQUksQ0FBQztBQUNaLHdCQUFRLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUNwQix1QkFBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDbkIsd0JBQVEsRUFBRTtBQUNSLHNCQUFJLEVBQUosSUFBSTtBQUNKLDBCQUFRLEVBQUUsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQztpQkFDL0Q7ZUFDRixDQUFDLENBQUM7YUFDSjtXQUNGLENBQUMsQ0FBQztBQUNILGlCQUFPLFFBQVEsQ0FBQztTQUNqQixDQUFDLENBQUM7T0FDSjtLQUNGLENBQUM7R0FDSDtDQUNGIiwiZmlsZSI6Ii9ob21lL2d1c3Rhdm8vLmF0b20vcGFja2FnZXMvbGludGVyLXB1cHBldC1saW50L2xpYi9tYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBiYWJlbCc7XG5cbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBpbXBvcnQvbm8tZXh0cmFuZW91cy1kZXBlbmRlbmNpZXMsIGltcG9ydC9leHRlbnNpb25zXG5pbXBvcnQgeyBDb21wb3NpdGVEaXNwb3NhYmxlIH0gZnJvbSAnYXRvbSc7XG5cbi8vIFNvbWUgaW50ZXJuYWwgdmFyaWFibGVzXG5sZXQgc3Vic2NyaXB0aW9ucztcbmxldCBleGVjdXRhYmxlUGF0aDtcbmxldCBlcnJvckxldmVsO1xubGV0IGFsbFNldHRpbmdzO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIC8vIEFjdGl2YXRlIGxpbnRlclxuICBhY3RpdmF0ZSgpIHtcbiAgICBjb25zdCBoZWxwZXJzID0gcmVxdWlyZSgnYXRvbS1saW50ZXInKTtcblxuICAgIHN1YnNjcmlwdGlvbnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZSgpO1xuXG4gICAgc3Vic2NyaXB0aW9ucy5hZGQoYXRvbS5jb25maWcub2JzZXJ2ZSgnbGludGVyLXB1cHBldC1saW50JywgKHNldHRpbmdzKSA9PiB7XG4gICAgICBhbGxTZXR0aW5ncyA9IHNldHRpbmdzO1xuICAgICAgZXhlY3V0YWJsZVBhdGggPSBzZXR0aW5ncy5leGVjdXRhYmxlUGF0aDtcbiAgICAgIGVycm9yTGV2ZWwgPSBzZXR0aW5ncy5lcnJvckxldmVsO1xuICAgIH0pKTtcblxuICAgIHJlcXVpcmUoJ2F0b20tcGFja2FnZS1kZXBzJykuaW5zdGFsbCgnbGludGVyLXB1cHBldC1saW50Jyk7XG5cbiAgICAvLyBDaGVjayBpZiBwdXBwZXQtbGludCBoYXMgc3VwcG9ydCBmb3IgdGhlICV7Y29sdW1ufSBwbGFjZWhvbGRlclxuICAgIGhlbHBlcnMuZXhlYyhleGVjdXRhYmxlUGF0aCwgWyctLWhlbHAnXSkudGhlbigob3V0cHV0KSA9PiB7XG4gICAgICBjb25zdCByZWdleENvbHVtbiA9IC8le2NvbHVtbn0vO1xuXG4gICAgICBpZiAocmVnZXhDb2x1bW4uZXhlYyhvdXRwdXQpID09PSBudWxsKSB7XG4gICAgICAgIGF0b20uY29uZmlnLnNldCgnbGludGVyLXB1cHBldC1saW50Lm9sZFZlcnNpb24nLCB0cnVlKTtcbiAgICAgICAgYXRvbS5ub3RpZmljYXRpb25zLmFkZEVycm9yKFxuICAgICAgICAgICdZb3UgYXJlIHVzaW5nIGFuIG9sZCB2ZXJzaW9uIG9mIHB1cHBldC1saW50IScsXG4gICAgICAgICAge1xuICAgICAgICAgICAgZGV0YWlsOiAnUGxlYXNlIHVwZ3JhZGUgeW91ciB2ZXJzaW9uIG9mIHB1cHBldC1saW50LlxcbicgK1xuICAgICAgICAgICAgICAnQ2hlY2sgdGhlIFJFQURNRSBmb3IgZnVydGhlciBpbmZvcm1hdGlvbi4nLFxuICAgICAgICAgIH0sXG4gICAgICAgICk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBhdG9tLmNvbmZpZy5zZXQoJ2xpbnRlci1wdXBwZXQtbGludC5vbGRWZXJzaW9uJywgZmFsc2UpO1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxuXG4gIGRlYWN0aXZhdGUoKSB7XG4gICAgc3Vic2NyaXB0aW9ucy5kaXNwb3NlKCk7XG4gIH0sXG5cbiAgcHJvdmlkZUxpbnRlcigpIHtcbiAgICBjb25zdCBoZWxwZXJzID0gcmVxdWlyZSgnYXRvbS1saW50ZXInKTtcblxuICAgIHJldHVybiB7XG4gICAgICBuYW1lOiAnUHVwcGV0LUxpbnQnLFxuICAgICAgZ3JhbW1hclNjb3BlczogWydzb3VyY2UucHVwcGV0J10sXG4gICAgICBzY29wZTogJ2ZpbGUnLFxuICAgICAgbGludHNPbkNoYW5nZTogZmFsc2UsXG4gICAgICBsaW50OiAoYWN0aXZlRWRpdG9yKSA9PiB7XG4gICAgICAgIC8vIENoZWNrIGFnYWluIGlmIHB1cHBldC1saW50IGlzIHRvbyBvbGQgdG8gc3VwcG9ydCBjb2x1bW4gaW5mb3JtYXRpb25cbiAgICAgICAgaWYgKGF0b20uY29uZmlnLmdldCgnbGludGVyLXB1cHBldC1saW50Lm9sZFZlcnNpb24nKSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcihcbiAgICAgICAgICAgICdZb3UgYXJlIHVzaW5nIGFuIG9sZCB2ZXJzaW9uIG9mIHB1cHBldC1saW50IScsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGRldGFpbDogJ1BsZWFzZSB1cGdyYWRlIHlvdXIgdmVyc2lvbiBvZiBwdXBwZXQtbGludC5cXG4nICtcbiAgICAgICAgICAgICAgICAnQ2hlY2sgdGhlIFJFQURNRSBmb3IgZnVydGhlciBpbmZvcm1hdGlvbi4nLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiBbXTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFNldHVwIGNvbnN0IHZhcnMgZm9yIGxhdGVyIHVzZVxuICAgICAgICBjb25zdCBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xuICAgICAgICBjb25zdCBmaWxlID0gYWN0aXZlRWRpdG9yLmdldFBhdGgoKTtcbiAgICAgICAgY29uc3QgY3dkID0gcGF0aC5kaXJuYW1lKGZpbGUpO1xuICAgICAgICAvLyBXaXRoIHRoZSBjdXN0b20gZm9ybWF0IHRoZSBwdXBwZXQtaW50IG91cHV0IGxvb2tzIGxpa2UgdGhpczpcbiAgICAgICAgLy8gZXJyb3IgbW9uZ29kYjo6c2VydmljZSBub3QgaW4gYXV0b2xvYWQgbW9kdWxlIGxheW91dCAzIDdcbiAgICAgICAgY29uc3QgcmVnZXhMaW5lID0gL14od2FybmluZ3xlcnJvcilcXHMoLiopXFxzKFxcZCspXFxzKFxcZCspJC87XG4gICAgICAgIGNvbnN0IGFyZ3MgPSBbJy0tbG9nLWZvcm1hdCcsICcle2tpbmR9ICV7bWVzc2FnZX0gJXtsaW5lfSAle2NvbHVtbn0nLCAnLS1lcnJvci1sZXZlbCcsIGVycm9yTGV2ZWxdO1xuXG4gICAgICAgIGNvbnN0IG9wdGlvbnNNYXAgPSByZXF1aXJlKCcuL2ZsYWdzLmpzJyk7XG5cbiAgICAgICAgLy8gQWRkIHRoZSBmbGFncyB0byB0aGUgY29tbWFuZCBvcHRpb25zXG4gICAgICAgIE9iamVjdC5rZXlzKGFsbFNldHRpbmdzKS5mb3JFYWNoKChmbGFnKSA9PiB7XG4gICAgICAgICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9wdGlvbnNNYXAsIGZsYWcpICYmIGFsbFNldHRpbmdzW2ZsYWddID09PSB0cnVlKSB7XG4gICAgICAgICAgICBhcmdzLnB1c2gob3B0aW9uc01hcFtmbGFnXSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBBZGQgdGhlIGZpbGUgdG8gYmUgY2hlY2tlZCB0byB0aGUgYXJndW1lbnRzXG4gICAgICAgIGFyZ3MucHVzaChmaWxlKTtcblxuICAgICAgICByZXR1cm4gaGVscGVycy5leGVjKGV4ZWN1dGFibGVQYXRoLCBhcmdzLCB7IGN3ZCwgaWdub3JlRXhpdENvZGU6IHRydWUgfSkudGhlbigob3V0cHV0KSA9PiB7XG4gICAgICAgICAgLy8gSWYgcHVwcGV0LWxpbnQgZXJyb3JzIHRvIHN0ZG91dCB0aGVuIHJlZGlyZWN0IHRoZSBtZXNzYWdlIHRvIHN0ZGVyciBzbyBpdCBpcyBjYXVnaHRcbiAgICAgICAgICBpZiAoL3B1cHBldC1saW50Oi8uZXhlYyhvdXRwdXQpKSB7XG4gICAgICAgICAgICB0aHJvdyBvdXRwdXQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IHRvUmV0dXJuID0gW107XG5cbiAgICAgICAgICAvLyBDaGVjayBmb3IgcHJvcGVyIHdhcm5pbmdzIGFuZCBlcnJvcnMgZnJvbSBzdGRvdXRcbiAgICAgICAgICBvdXRwdXQuc3BsaXQoL1xccj9cXG4vKS5mb3JFYWNoKChsaW5lKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBtYXRjaGVzID0gcmVnZXhMaW5lLmV4ZWMobGluZSk7XG4gICAgICAgICAgICBpZiAobWF0Y2hlcyAhPSBudWxsKSB7XG4gICAgICAgICAgICAgIGNvbnN0IGVyckxpbmUgPSBOdW1iZXIucGFyc2VJbnQobWF0Y2hlc1szXSwgMTApIC0gMTtcbiAgICAgICAgICAgICAgY29uc3QgZXJyQ29sID0gTnVtYmVyLnBhcnNlSW50KG1hdGNoZXNbNF0sIDEwKSAtIDE7XG5cbiAgICAgICAgICAgICAgdG9SZXR1cm4ucHVzaCh7XG4gICAgICAgICAgICAgICAgc2V2ZXJpdHk6IG1hdGNoZXNbMV0sXG4gICAgICAgICAgICAgICAgZXhjZXJwdDogbWF0Y2hlc1syXSxcbiAgICAgICAgICAgICAgICBsb2NhdGlvbjoge1xuICAgICAgICAgICAgICAgICAgZmlsZSxcbiAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBoZWxwZXJzLmdlbmVyYXRlUmFuZ2UoYWN0aXZlRWRpdG9yLCBlcnJMaW5lLCBlcnJDb2wpLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiB0b1JldHVybjtcbiAgICAgICAgfSk7XG4gICAgICB9LFxuICAgIH07XG4gIH0sXG59O1xuIl19