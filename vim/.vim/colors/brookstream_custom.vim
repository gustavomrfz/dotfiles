" Vim color file - brookstream_custom
" Generated by http://bytefluent.com/vivify 2018-05-15
set background=dark
if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = "brookstream_custom"

"hi WildMenu -- no settings --
"hi SignColumn -- no settings --
"hi TabLineSel -- no settings --
"hi CTagsMember -- no settings --
"hi CTagsGlobalConstant -- no settings --
hi Normal guifg=#d5d5d5 guibg=#000000 guisp=#000000 gui=NONE ctermfg=188 ctermbg=NONE cterm=NONE
"hi CTagsImport -- no settings --
"hi Search -- no settings --
"hi CTagsGlobalVariable -- no settings --
"hi SpellRare -- no settings --
"hi EnumerationValue -- no settings --
"hi Float -- no settings --
"hi CursorLine -- no settings --
"hi Union -- no settings --
"hi TabLineFill -- no settings --
"hi VisualNOS -- no settings --
"hi CursorColumn -- no settings --
"hi FoldColumn -- no settings --
"hi EnumerationName -- no settings --
"hi SpellCap -- no settings --
"hi VertSplit -- no settings --
"hi SpellLocal -- no settings --
"hi DefinedName -- no settings --
"hi MatchParen -- no settings --
"hi LocalVariable -- no settings --
"hi SpellBad -- no settings --
"hi CTagsClass -- no settings --
"hi TabLine -- no settings --
"hi clear -- no settings --
"hi htmlitalic -- no settings --
"hi htmlboldunderlineitalic -- no settings --
"hi htmlbolditalic -- no settings --
"hi htmlunderlineitalic -- no settings --
"hi htmlbold -- no settings --
"hi htmlboldunderline -- no settings --
"hi htmlunderline -- no settings --
"hi cursorime -- no settings --
"hi def -- no settings --
"hi semicolon -- no settings --
"hi spelllocale -- no settings --
"hi cssattributeselector -- no settings --
"hi default -- no settings --
"hi pythonspaceerror -- no settings --
hi IncSearch guifg=#d3e6e6 guibg=#000000 guisp=#000000 gui=NONE ctermfg=152 ctermbg=NONE cterm=NONE
hi SpecialComment guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi Typedef guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Title guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Folded guifg=#0000a2 guibg=NONE guisp=NONE gui=NONE ctermfg=19 ctermbg=NONE cterm=NONE
hi PreCondit guifg=#8370ff guibg=NONE guisp=NONE gui=NONE ctermfg=12 ctermbg=NONE cterm=NONE
hi Include guifg=#8370ff guibg=NONE guisp=NONE gui=NONE ctermfg=12 ctermbg=NONE cterm=NONE
hi StatusLineNC guifg=#1a1a1a guibg=#bbbbbb guisp=#bbbbbb gui=NONE ctermfg=234 ctermbg=250 cterm=NONE
hi NonText guifg=#4444ff guibg=NONE guisp=NONE gui=NONE ctermfg=63 ctermbg=NONE cterm=NONE
hi DiffText guifg=#d50000 guibg=#080808 guisp=#080808 gui=NONE ctermfg=160 ctermbg=232 cterm=NONE
hi ErrorMsg guifg=#ffffff guibg=#880000 guisp=#880000 gui=NONE ctermfg=15 ctermbg=88 cterm=NONE
hi Ignore guifg=#5e5e5e guibg=NONE guisp=NONE gui=NONE ctermfg=59 ctermbg=NONE cterm=NONE
hi Debug guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi PMenuSbar guifg=NONE guibg=#050505 guisp=#050505 gui=NONE ctermfg=NONE ctermbg=232 cterm=NONE
hi Identifier guifg=#00f7ff guibg=NONE guisp=NONE gui=NONE ctermfg=14 ctermbg=NONE cterm=NONE
hi SpecialChar guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi Conditional guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi StorageClass guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Todo guifg=#fff200 guibg=#aa0006 guisp=#aa0006 gui=NONE ctermfg=11 ctermbg=124 cterm=NONE
hi Special guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi LineNr guifg=#5095ce guibg=#050505 guisp=#050505 gui=NONE ctermfg=74 ctermbg=232 cterm=NONE
hi StatusLine guifg=#ffffff guibg=#2f4f4f guisp=#2f4f4f gui=NONE ctermfg=15 ctermbg=66 cterm=NONE
hi Label guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi PMenuSel guifg=#ffffff guibg=#2f4f4f guisp=#2f4f4f gui=NONE ctermfg=15 ctermbg=66 cterm=NONE
hi Delimiter guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi Statement guifg=#00ffff guibg=NONE guisp=NONE gui=NONE ctermfg=14 ctermbg=NONE cterm=NONE
hi Comment guifg=#838383 guibg=NONE guisp=NONE gui=NONE ctermfg=8 ctermbg=NONE cterm=NONE
hi Character guifg=#00c4c4 guibg=NONE guisp=NONE gui=NONE ctermfg=4 ctermbg=NONE cterm=NONE
hi Number guifg=#00c4c4 guibg=NONE guisp=NONE gui=NONE ctermfg=4 ctermbg=NONE cterm=NONE
hi Boolean guifg=#afe7af guibg=NONE guisp=NONE gui=NONE ctermfg=151 ctermbg=NONE cterm=NONE
hi Operator guifg=#00bfff guibg=NONE guisp=NONE gui=NONE ctermfg=39 ctermbg=NONE cterm=NONE
hi Question guifg=#ffff00 guibg=NONE guisp=NONE gui=NONE ctermfg=11 ctermbg=NONE cterm=NONE
hi WarningMsg guifg=#ffff00 guibg=NONE guisp=NONE gui=NONE ctermfg=11 ctermbg=NONE cterm=NONE
hi DiffDelete guifg=#5e5e5e guibg=#080808 guisp=#080808 gui=NONE ctermfg=59 ctermbg=232 cterm=NONE
hi ModeMsg guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Define guifg=#8370ff guibg=NONE guisp=NONE gui=NONE ctermfg=12 ctermbg=NONE cterm=NONE
hi Function guifg=#1e8fff guibg=NONE guisp=NONE gui=NONE ctermfg=33 ctermbg=NONE cterm=NONE
hi PreProc guifg=#8370ff guibg=NONE guisp=NONE gui=NONE ctermfg=12 ctermbg=NONE cterm=NONE
hi Visual guifg=#1a1a1a guibg=#bbbbbb guisp=#bbbbbb gui=NONE ctermfg=234 ctermbg=250 cterm=NONE
hi MoreMsg guifg=#44ff44 guibg=NONE guisp=NONE gui=NONE ctermfg=83 ctermbg=NONE cterm=NONE
hi Exception guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Keyword guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Type guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi DiffChange guifg=#ffffff guibg=#080808 guisp=#080808 gui=NONE ctermfg=15 ctermbg=232 cterm=NONE
hi Cursor guifg=#1a1a1a guibg=#44ff44 guisp=#44ff44 gui=NONE ctermfg=234 ctermbg=83 cterm=NONE
hi Error guifg=#d50000 guibg=#000000 guisp=#000000 gui=NONE ctermfg=160 ctermbg=NONE cterm=NONE
hi PMenu guifg=#1a1a1a guibg=#bbbbbb guisp=#bbbbbb gui=NONE ctermfg=234 ctermbg=250 cterm=NONE
hi SpecialKey guifg=#4444ff guibg=NONE guisp=NONE gui=NONE ctermfg=63 ctermbg=NONE cterm=NONE
hi Constant guifg=#00c4c4 guibg=NONE guisp=NONE gui=NONE ctermfg=4 ctermbg=NONE cterm=NONE
hi Tag guifg=#8ad2ff guibg=NONE guisp=NONE gui=NONE ctermfg=117 ctermbg=NONE cterm=NONE
hi String guifg=#5095ce guibg=NONE guisp=NONE gui=NONE ctermfg=74 ctermbg=NONE cterm=NONE
hi PMenuThumb guifg=NONE guibg=#4682b4 guisp=#4682b4 gui=NONE ctermfg=NONE ctermbg=67 cterm=NONE
hi Repeat guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Directory guifg=#44ffff guibg=NONE guisp=NONE gui=NONE ctermfg=87 ctermbg=NONE cterm=NONE
hi Structure guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi Macro guifg=#8370ff guibg=NONE guisp=NONE gui=NONE ctermfg=12 ctermbg=NONE cterm=NONE
hi Underlined guifg=#4444ff guibg=NONE guisp=NONE gui=NONE ctermfg=63 ctermbg=NONE cterm=NONE
hi DiffAdd guifg=#ffff00 guibg=#080808 guisp=#080808 gui=NONE ctermfg=11 ctermbg=232 cterm=NONE
hi cursorim guifg=NONE guibg=#fff8dc guisp=#fff8dc gui=NONE ctermfg=NONE ctermbg=230 cterm=NONE
hi mbenormal guifg=#e9d7c3 guibg=#2e2e3f guisp=#2e2e3f gui=NONE ctermfg=187 ctermbg=237 cterm=NONE
hi perlspecialstring guifg=#d890ea guibg=#404040 guisp=#404040 gui=NONE ctermfg=176 ctermbg=238 cterm=NONE
hi doxygenspecial guifg=#ffd191 guibg=NONE guisp=NONE gui=NONE ctermfg=222 ctermbg=NONE cterm=NONE
hi mbechanged guifg=#ffffff guibg=#2e2e3f guisp=#2e2e3f gui=NONE ctermfg=15 ctermbg=237 cterm=NONE
hi mbevisiblechanged guifg=#ffffff guibg=#4e4e8f guisp=#4e4e8f gui=NONE ctermfg=15 ctermbg=60 cterm=NONE
hi doxygenparam guifg=#ffd191 guibg=NONE guisp=NONE gui=NONE ctermfg=222 ctermbg=NONE cterm=NONE
hi doxygensmallspecial guifg=#ffd191 guibg=NONE guisp=NONE gui=NONE ctermfg=222 ctermbg=NONE cterm=NONE
hi doxygenprev guifg=#ffd191 guibg=NONE guisp=NONE gui=NONE ctermfg=222 ctermbg=NONE cterm=NONE
hi perlspecialmatch guifg=#d890ea guibg=#404040 guisp=#404040 gui=NONE ctermfg=176 ctermbg=238 cterm=NONE
hi cformat guifg=#d890ea guibg=#404040 guisp=#404040 gui=NONE ctermfg=176 ctermbg=238 cterm=NONE
hi lcursor guifg=#404040 guibg=#00afff guisp=#00afff gui=NONE ctermfg=238 ctermbg=39 cterm=NONE
hi doxygenspecialmultilinedesc guifg=#c76d0c guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi taglisttagname guifg=#8a96ff guibg=NONE guisp=NONE gui=NONE ctermfg=105 ctermbg=NONE cterm=NONE
hi doxygenbrief guifg=#ffad61 guibg=NONE guisp=NONE gui=NONE ctermfg=215 ctermbg=NONE cterm=NONE
hi mbevisiblenormal guifg=#e9e9e7 guibg=#4e4e8f guisp=#4e4e8f gui=NONE ctermfg=255 ctermbg=60 cterm=NONE
hi user2 guifg=#ffff8c guibg=#45637f guisp=#45637f gui=NONE ctermfg=228 ctermbg=66 cterm=NONE
hi user1 guifg=#ffffff guibg=#00008b guisp=#00008b gui=NONE ctermfg=15 ctermbg=18 cterm=NONE
hi doxygenspecialonelinedesc guifg=#c76d0c guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi doxygencomment guifg=#c78e25 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi cspecialcharacter guifg=#d890ea guibg=#404040 guisp=#404040 gui=NONE ctermfg=176 ctermbg=238 cterm=NONE
hi pythonimport guifg=#00aa00 guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi pythonexception guifg=#ff0000 guibg=NONE guisp=NONE gui=NONE ctermfg=196 ctermbg=NONE cterm=NONE
hi pythonbuiltinfunction guifg=#00aa00 guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi pythonoperator guifg=#92a0bc guibg=NONE guisp=NONE gui=NONE ctermfg=103 ctermbg=NONE cterm=NONE
hi pythonexclass guifg=#00aa00 guibg=NONE guisp=NONE gui=NONE ctermfg=34 ctermbg=NONE cterm=NONE
hi underline guifg=#afafff guibg=NONE guisp=NONE gui=NONE ctermfg=147 ctermbg=NONE cterm=NONE
hi taglistcomment guifg=#ffffff guibg=#878686 guisp=#878686 gui=NONE ctermfg=15 ctermbg=102 cterm=NONE
hi taglisttitle guifg=#f8fdff guibg=#000000 guisp=#000000 gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi user4 guifg=#3ae6ad guibg=#45637f guisp=#45637f gui=NONE ctermfg=79 ctermbg=66 cterm=NONE
hi user5 guifg=#fffdf8 guibg=#02df70 guisp=#02df70 gui=NONE ctermfg=230 ctermbg=41 cterm=NONE
hi user3 guifg=#1a1a1a guibg=#45637f guisp=#45637f gui=NONE ctermfg=234 ctermbg=66 cterm=NONE
hi taglistfilename guifg=#fff8fd guibg=#868787 guisp=#868787 gui=NONE ctermfg=15 ctermbg=102 cterm=NONE
hi taglisttagscope guifg=#ffffff guibg=#878686 guisp=#878686 gui=NONE ctermfg=15 ctermbg=102 cterm=NONE
hi match guifg=#0000ff guibg=#ffff00 guisp=#ffff00 gui=NONE ctermfg=21 ctermbg=11 cterm=NONE
hi subtitle guifg=#ffffff guibg=#8fbb63 guisp=#8fbb63 gui=NONE ctermfg=15 ctermbg=107 cterm=NONE
hi javaparen2 guifg=#95ffa8 guibg=NONE guisp=NONE gui=NONE ctermfg=121 ctermbg=NONE cterm=NONE
hi javaparen1 guifg=#ff76bd guibg=NONE guisp=NONE gui=NONE ctermfg=212 ctermbg=NONE cterm=NONE
hi javabraces guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi javaparen guifg=#75ffa5 guibg=NONE guisp=NONE gui=NONE ctermfg=121 ctermbg=NONE cterm=NONE
hi javaexternal guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi javafuncdef guifg=#ff3295 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi javalangobject guifg=#7fff9d guibg=NONE guisp=NONE gui=NONE ctermfg=121 ctermbg=NONE cterm=NONE
hi javascopedecl guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi tags guifg=#fff8ff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi char guifg=#0000ca guibg=#b0b0b0 guisp=#b0b0b0 gui=NONE ctermfg=4 ctermbg=145 cterm=NONE
hi function guifg=#09ff19 guibg=#f0f2f0 guisp=#f0f2f0 gui=NONE ctermfg=10 ctermbg=194 cterm=NONE
hi titled guifg=#ffffff guibg=#332211 guisp=#332211 gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi spellerrors guifg=#fff8fd guibg=#faf7ff guisp=#faf7ff gui=NONE ctermfg=15 ctermbg=15 cterm=NONE
hi constant guifg=#ff45e0 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi htmlarg guifg=#fff6fe guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi icursor guifg=#fff8fd guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi cssuiprop guifg=#fff6fe guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phprelation guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phpoperator guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phparraypair guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phpunknownselector guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi javascriptoperator guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi cssbraces guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phppropertyselector guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phpvarselector guifg=#fff4f6 guibg=NONE guisp=NONE gui=NONE ctermfg=224 ctermbg=NONE cterm=NONE
hi htmltagname guifg=#fcfeff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi htmltitle guifg=#aed7ff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi phpsemicolon guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi javascriptbraces guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi htmlspecialtagname guifg=#56ba56 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi htmlendtag guifg=#00ccff guibg=NONE guisp=NONE gui=NONE ctermfg=45 ctermbg=NONE cterm=NONE
hi phpassignbyref guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi htmltag guifg=#00ccff guibg=NONE guisp=NONE gui=NONE ctermfg=45 ctermbg=NONE cterm=NONE
hi csspseudoclassid guifg=#fffffe guibg=NONE guisp=NONE gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi phpfunctions guifg=#fff6fe guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phppropertyselectorinstring guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi phpregiondelimiter guifg=#bae1ff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi cssidentifier guifg=#ff3de8 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi phpparent guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi cssselectorop guifg=#fffffe guibg=NONE guisp=NONE gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi csstagname guifg=#ff3de8 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi phpmemberselector guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi method guifg=#f8fffd guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi htmllink guifg=#d3ffbd guibg=#000000 guisp=#000000 gui=NONE ctermfg=193 ctermbg=NONE cterm=NONE
hi pythonbuiltin guifg=#79939d guibg=NONE guisp=NONE gui=NONE ctermfg=109 ctermbg=NONE cterm=NONE
hi phpstringdouble guifg=#fcfeff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi javascriptstrings guifg=#fcfeff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi htmlstring guifg=#fcfeff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi phpstringsingle guifg=#fcfeff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi pythonstatement guifg=#3bff0a guibg=NONE guisp=NONE gui=NONE ctermfg=82 ctermbg=NONE cterm=NONE
hi menu guifg=#ffffff guibg=#b061ff guisp=#b061ff gui=NONE ctermfg=15 ctermbg=135 cterm=NONE
hi comments guifg=#a6ffb0 guibg=NONE guisp=NONE gui=NONE ctermfg=157 ctermbg=NONE cterm=NONE
hi pythoncomment guifg=#75ff93 guibg=NONE guisp=NONE gui=NONE ctermfg=121 ctermbg=NONE cterm=NONE
hi pythonprecondit guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi pythonrawstring guifg=#69ff7d guibg=NONE guisp=NONE gui=NONE ctermfg=84 ctermbg=NONE cterm=NONE
hi pythonconditional guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi pythonrepeat guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi constants guifg=#ff3399 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi attribute guifg=#bddfff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi statement guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi statementu guifg=#ffb1ca guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi keyword guifg=#ffb1ca guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi comment guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi condtional guifg=#ff0000 guibg=#000000 guisp=#000000 gui=NONE ctermfg=196 ctermbg=NONE cterm=NONE
hi tagname guifg=#b0ceff guibg=#5a4e66 guisp=#5a4e66 gui=NONE ctermfg=153 ctermbg=241 cterm=NONE
hi mydiffsubname guifg=#ceffa6 guibg=NONE guisp=NONE gui=NONE ctermfg=193 ctermbg=NONE cterm=NONE
hi mydiffcommline guifg=#a6ceff guibg=#5e658b guisp=#5e658b gui=NONE ctermfg=153 ctermbg=60 cterm=NONE
hi mailqu guifg=#ffffff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi mydiffnew guifg=#d0a6ff guibg=NONE guisp=NONE gui=NONE ctermfg=183 ctermbg=NONE cterm=NONE
hi mydiffremoved guifg=#d0a6ff guibg=NONE guisp=NONE gui=NONE ctermfg=183 ctermbg=NONE cterm=NONE
hi mydiffnormal guifg=#ffffff guibg=#a5d2ff guisp=#a5d2ff gui=NONE ctermfg=15 ctermbg=153 cterm=NONE
hi charachter guifg=#ffa6ce guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi done guifg=#a6ceff guibg=#be85a0 guisp=#be85a0 gui=NONE ctermfg=153 ctermbg=138 cterm=NONE
hi perlpod guifg=#f6e1ff guibg=NONE guisp=NONE gui=NONE ctermfg=225 ctermbg=NONE cterm=NONE
hi mailq guifg=#ffffff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi scrollbar guifg=#00bfff guibg=#000040 guisp=#000040 gui=NONE ctermfg=39 ctermbg=17 cterm=NONE
hi cdefine guifg=#fffdf8 guibg=NONE guisp=NONE gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi mysemis guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi cinclude guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi mydots guifg=#fdfff8 guibg=NONE guisp=NONE gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi myassignments guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi comma guifg=#ececff guibg=NONE guisp=NONE gui=NONE ctermfg=189 ctermbg=NONE cterm=NONE
hi warningmsgildmenu guifg=#ffffff guibg=#ffa5f5 guisp=#ffa5f5 gui=NONE ctermfg=15 ctermbg=219 cterm=NONE
hi paren guifg=#ececff guibg=NONE guisp=NONE gui=NONE ctermfg=189 ctermbg=NONE cterm=NONE
hi cterm guifg=#ffcfda guibg=#000000 guisp=#000000 gui=NONE ctermfg=224 ctermbg=NONE cterm=NONE
hi gui guifg=#ffcfda guibg=#000000 guisp=#000000 gui=NONE ctermfg=224 ctermbg=NONE cterm=NONE
hi namespace guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi tablinefillsel guifg=#f8fffd guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi stringdelimiter guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubyregexp guifg=#ffb1d8 guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi string guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi normal guifg=#fff8fd guibg=#042012 guisp=#042012 gui=NONE ctermfg=15 ctermbg=22 cterm=NONE
hi rubyinstancevariable guifg=#76ffbd guibg=NONE guisp=NONE gui=NONE ctermfg=122 ctermbg=NONE cterm=NONE
hi rubyclass guifg=#a6ceff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi identifier guifg=#8aff8a guibg=NONE guisp=NONE gui=NONE ctermfg=120 ctermbg=NONE cterm=NONE
hi rubyregexpdelimiter guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubyregexpspecial guifg=#c4c4ff guibg=NONE guisp=NONE gui=NONE ctermfg=189 ctermbg=NONE cterm=NONE
hi rubypredefinedidentifier guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi directory guifg=#ff4fbc guibg=NONE guisp=NONE gui=NONE ctermfg=205 ctermbg=NONE cterm=NONE
hi rubysymbol guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubycontrol guifg=#47ff78 guibg=NONE guisp=NONE gui=NONE ctermfg=84 ctermbg=NONE cterm=NONE
hi rubyidentifier guifg=#6bffd5 guibg=NONE guisp=NONE gui=NONE ctermfg=86 ctermbg=NONE cterm=NONE
hi defined guifg=#e0ffff guibg=NONE guisp=NONE gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi rcursor guifg=#ffffff guibg=#bdffa5 guisp=#bdffa5 gui=NONE ctermfg=15 ctermbg=193 cterm=NONE
hi ncursor guifg=#ffffff guibg=#a5d2ff guisp=#a5d2ff gui=NONE ctermfg=15 ctermbg=153 cterm=NONE
hi rubyconstant guifg=#ceffa6 guibg=NONE guisp=NONE gui=NONE ctermfg=193 ctermbg=NONE cterm=NONE
hi rubylocalvariableormethod guifg=#76ffbd guibg=NONE guisp=NONE gui=NONE ctermfg=122 ctermbg=NONE cterm=NONE
hi xmltagname guifg=#ff25d3 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi rubyblockparameter guifg=#a6ceff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi xmlendtag guifg=#ff25d3 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi rubypseudovariable guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi xmltag guifg=#ff25d3 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi rubystringdelimiter guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubyinterpolation guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubypredefinedconstant guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubypredefinedvariable guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubyexception guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi repeat guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi type guifg=#ff38a5 guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi rubyaccess guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi sql_statement guifg=#ffb1db guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi cics_statement guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi builtin guifg=#ceffc5 guibg=NONE guisp=NONE gui=NONE ctermfg=194 ctermbg=NONE cterm=NONE
hi browsedirectory guifg=#ffa6ce guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi tmesupport guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi special guifg=#ff7df2 guibg=NONE guisp=NONE gui=NONE ctermfg=213 ctermbg=NONE cterm=NONE
hi incsearch guifg=#ffffff guibg=#0066cc guisp=#0066cc gui=NONE ctermfg=15 ctermbg=26 cterm=NONE
hi htmlh1 guifg=#ffffff guibg=#332211 guisp=#332211 gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi htmlh3 guifg=#e0e0e0 guibg=#332211 guisp=#332211 gui=NONE ctermfg=254 ctermbg=52 cterm=NONE
hi htmlh2 guifg=#f4f4f4 guibg=#332211 guisp=#332211 gui=NONE ctermfg=255 ctermbg=52 cterm=NONE
hi htmlh5 guifg=#b8b8b8 guibg=#332211 guisp=#332211 gui=NONE ctermfg=250 ctermbg=52 cterm=NONE
hi htmlh4 guifg=#cccccc guibg=#332211 guisp=#332211 gui=NONE ctermfg=252 ctermbg=52 cterm=NONE
hi htmlh6 guifg=#a4a4a4 guibg=#332211 guisp=#332211 gui=NONE ctermfg=248 ctermbg=52 cterm=NONE
hi cursor guifg=#ffffff guibg=#cc4455 guisp=#cc4455 gui=NONE ctermfg=15 ctermbg=167 cterm=NONE
hi sourceline guifg=#7fff8c guibg=#323835 guisp=#323835 gui=NONE ctermfg=120 ctermbg=237 cterm=NONE
hi javaexceptions guifg=#e59eff guibg=NONE guisp=NONE gui=NONE ctermfg=183 ctermbg=NONE cterm=NONE
hi bufexploreractbuf guifg=#ffffff guibg=#042012 guisp=#042012 gui=NONE ctermfg=15 ctermbg=22 cterm=NONE
hi bufexplorertogglesplit guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi mytaglisttagscope guifg=#6cff6c guibg=#282828 guisp=#282828 gui=NONE ctermfg=83 ctermbg=235 cterm=NONE
hi level14c guifg=#ffb1ff guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi bufexplorertitle guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi level8c guifg=#ffb0f7 guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi mytaglistcomment guifg=#6cff6c guibg=#282828 guisp=#282828 gui=NONE ctermfg=83 ctermbg=235 cterm=NONE
hi bufexplorersorttype guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi bufexplorermapping guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi bufexploreropenin guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi level11c guifg=#ffb0f7 guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi bufexplorerbufnbr guifg=#ffffff guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi level7c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi level16c guifg=#ff9ec5 guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi javaclassdecl guifg=#ff1d83 guibg=NONE guisp=NONE gui=NONE ctermfg=198 ctermbg=NONE cterm=NONE
hi javatypedef guifg=#9e27ff guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi level6c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi mytaglistfilename guifg=#ff0988 guibg=#400623 guisp=#400623 gui=NONE ctermfg=198 ctermbg=52 cterm=NONE
hi level1c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi level15c guifg=#ff9df5 guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi bufexplorerlockedbuf guifg=#ffffff guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi bufexplorermodbuf guifg=#ffffff guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi level9c guifg=#ffb7ff guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi bufexplorerhelp guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi javadebug guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi javadoccomment guifg=#fff6cd guibg=NONE guisp=NONE gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi level5c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi bufexplorertoggleopen guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi bufexplorersortby guifg=NONE guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=NONE ctermbg=236 cterm=NONE
hi level10c guifg=#fff8fd guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi bufexplorercurbuf guifg=#fff8fd guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi level4c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi bufexplorerhidbuf guifg=#ffffff guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi bufexplorerunlbuf guifg=#ffffff guibg=#2c302f guisp=#2c302f gui=NONE ctermfg=15 ctermbg=236 cterm=NONE
hi mytaglisttagname guifg=#7fff8c guibg=#282828 guisp=#282828 gui=NONE ctermfg=120 ctermbg=235 cterm=NONE
hi level12c guifg=#ffb7ff guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi bufexplorerxxxbuf guifg=#7fffbf guibg=#147042 guisp=#147042 gui=NONE ctermfg=121 ctermbg=2 cterm=NONE
hi level3c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi mytaglisttitle guifg=#7fff8c guibg=#282828 guisp=#282828 gui=NONE ctermfg=120 ctermbg=235 cterm=NONE
hi level13c guifg=#ffb1ff guibg=NONE guisp=NONE gui=NONE ctermfg=219 ctermbg=NONE cterm=NONE
hi level2c guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi bufexploreraltbuf guifg=#7fffbf guibg=#147042 guisp=#147042 gui=NONE ctermfg=121 ctermbg=2 cterm=NONE
hi javastring guifg=#e96cff guibg=NONE guisp=NONE gui=NONE ctermfg=171 ctermbg=NONE cterm=NONE
hi javarepeat guifg=#ffbae2 guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi rubyfloat guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubyinteger guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi rubysharpbang guifg=#fffdf8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi rubydocumentation guifg=#fff8fd guibg=#a9a7a9 guisp=#a9a7a9 gui=NONE ctermfg=15 ctermbg=248 cterm=NONE
hi lisplist guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi debugstop guifg=#fff8fd guibg=#eebf91 guisp=#eebf91 gui=NONE ctermfg=15 ctermbg=180 cterm=NONE
hi debugbreak guifg=#fff8fd guibg=#89898b guisp=#89898b gui=NONE ctermfg=15 ctermbg=245 cterm=NONE
hi vimerror guifg=#fff8ff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi foldecolumn guifg=#ffffff guibg=#200412 guisp=#200412 gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi cppstltype guifg=#8aff8a guibg=NONE guisp=NONE gui=NONE ctermfg=120 ctermbg=NONE cterm=NONE
hi vimfold guifg=#ffffff guibg=#220816 guisp=#220816 gui=NONE ctermfg=15 ctermbg=53 cterm=NONE
hi underlined guifg=#bae1ff guibg=NONE guisp=NONE gui=NONE ctermfg=153 ctermbg=NONE cterm=NONE
hi number guifg=#ff52c0 guibg=NONE guisp=NONE gui=NONE ctermfg=206 ctermbg=NONE cterm=NONE
hi perlsharpbang guifg=#ffc4e2 guibg=#505050 guisp=#505050 gui=NONE ctermfg=218 ctermbg=239 cterm=NONE
hi diffchanged guifg=#fdfff8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi diffoldline guifg=#ff8809 guibg=#000000 guisp=#000000 gui=NONE ctermfg=208 ctermbg=NONE cterm=NONE
hi doxygenstart guifg=#fffdf8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi perlstatement guifg=#ffc4e2 guibg=NONE guisp=NONE gui=NONE ctermfg=218 ctermbg=NONE cterm=NONE
hi doxygenstartl guifg=#fffdf8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi diffnewfile guifg=#ff8809 guibg=#000000 guisp=#000000 gui=NONE ctermfg=208 ctermbg=NONE cterm=NONE
hi doxygencommentl guifg=#ffffff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi vimcommenttitle guifg=#fffdf8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi diffadded guifg=#fff8ff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi doxygenparamname guifg=#f8fffd guibg=#000000 guisp=#000000 gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi diffoldfile guifg=#ff8809 guibg=#000000 guisp=#000000 gui=NONE ctermfg=208 ctermbg=NONE cterm=NONE
hi doxygenbriefl guifg=#ffffff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi helphypertextjump guifg=#fff8ff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi doxygenbriefline guifg=#ffffff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi diffline guifg=#fffdf8 guibg=#000000 guisp=#000000 gui=NONE ctermfg=230 ctermbg=NONE cterm=NONE
hi doxygenparamdirection guifg=#ff0988 guibg=#000000 guisp=#000000 gui=NONE ctermfg=198 ctermbg=NONE cterm=NONE
hi diffremoved guifg=#fdf8ff guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi perlvarplain guifg=#caff92 guibg=#34041c guisp=#34041c gui=NONE ctermfg=192 ctermbg=52 cterm=NONE
hi perlstatementstorage guifg=#fff8fd guibg=#000000 guisp=#000000 gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi perlvarplain2 guifg=#e6ff92 guibg=#34041c guisp=#34041c gui=NONE ctermfg=192 ctermbg=52 cterm=NONE
hi doxygenargumentword guifg=#f8fffd guibg=#000000 guisp=#000000 gui=NONE ctermfg=195 ctermbg=NONE cterm=NONE
hi htm guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi js guifg=#ba75ff guibg=NONE guisp=NONE gui=NONE ctermfg=135 ctermbg=NONE cterm=NONE
hi fortrantype guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi fortranlabelnumber guifg=#ffffff guibg=NONE guisp=NONE gui=NONE ctermfg=15 ctermbg=NONE cterm=NONE
hi fortranunitheader guifg=#22e2ff guibg=NONE guisp=NONE gui=NONE ctermfg=45 ctermbg=NONE cterm=NONE
hi perlfunctionname guifg=#fff8fd guibg=#34041c guisp=#34041c gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi perlstatementinclude guifg=#ffc4e2 guibg=#3f3e40 guisp=#3f3e40 gui=NONE ctermfg=218 ctermbg=238 cterm=NONE
hi perlcontrol guifg=#ffc4e2 guibg=#400623 guisp=#400623 gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perllabel guifg=#ffc4e2 guibg=#400623 guisp=#400623 gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perlmatchstartend guifg=#ffc4e2 guibg=#423a3f guisp=#423a3f gui=NONE ctermfg=218 ctermbg=238 cterm=NONE
hi perlrepeat guifg=#ffc4f5 guibg=#34041c guisp=#34041c gui=NONE ctermfg=225 ctermbg=52 cterm=NONE
hi perlshellcommand guifg=NONE guibg=#423a3f guisp=#423a3f gui=NONE ctermfg=NONE ctermbg=238 cterm=NONE
hi perlstatementfiledesc guifg=#ffc4c4 guibg=#34041c guisp=#34041c gui=NONE ctermfg=224 ctermbg=52 cterm=NONE
hi perlstatementsub guifg=#ffc4e2 guibg=#34041c guisp=#34041c gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perloperator guifg=#ffc4e2 guibg=#400623 guisp=#400623 gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perlvarsimplemembername guifg=#fff5ff guibg=#34041c guisp=#34041c gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi perlnumber guifg=#ffd6c3 guibg=#34041c guisp=#34041c gui=NONE ctermfg=223 ctermbg=52 cterm=NONE
hi perlvarnotinmatches guifg=#ffffff guibg=#34041c guisp=#34041c gui=NONE ctermfg=15 ctermbg=52 cterm=NONE
hi perlqq guifg=#ff0988 guibg=#393336 guisp=#393336 gui=NONE ctermfg=198 ctermbg=237 cterm=NONE
hi perlstatementcontrol guifg=#ff7ebf guibg=#34041c guisp=#34041c gui=NONE ctermfg=212 ctermbg=52 cterm=NONE
hi perlstatementhash guifg=#ffc4e2 guibg=#400623 guisp=#400623 gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perlvarsimplemember guifg=#ffc4e2 guibg=#34041c guisp=#34041c gui=NONE ctermfg=218 ctermbg=52 cterm=NONE
hi perlidentifier guifg=#e2ffc4 guibg=NONE guisp=NONE gui=NONE ctermfg=193 ctermbg=NONE cterm=NONE
hi perlstringstartend guifg=#f56dff guibg=#35061e guisp=#35061e gui=NONE ctermfg=207 ctermbg=53 cterm=NONE
hi perlspecialbeom guifg=#ff0988 guibg=#400623 guisp=#400623 gui=NONE ctermfg=198 ctermbg=52 cterm=NONE
hi perlstatementnew guifg=#ffc4e2 guibg=#423a3f guisp=#423a3f gui=NONE ctermfg=218 ctermbg=238 cterm=NONE
hi perlpackagedecl guifg=#ffd6c3 guibg=#400623 guisp=#400623 gui=NONE ctermfg=223 ctermbg=52 cterm=NONE
hi jinjafilter guifg=#ff0088 guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=198 ctermbg=230 cterm=NONE
hi pythondoctest2 guifg=#46ab7d guibg=NONE guisp=NONE gui=NONE ctermfg=72 ctermbg=NONE cterm=NONE
hi jinjaraw guifg=#c4c4c4 guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=251 ctermbg=230 cterm=NONE
hi htmltagn guifg=#56ba56 guibg=NONE guisp=NONE gui=NONE ctermfg=71 ctermbg=NONE cterm=NONE
hi jinjaoperator guifg=#ffffff guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=15 ctermbg=230 cterm=NONE
hi jinjavarblock guifg=#ff0009 guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=196 ctermbg=230 cterm=NONE
hi jinjaattribute guifg=#f78400 guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=208 ctermbg=230 cterm=NONE
hi pythondoctest guifg=#3c795d guibg=NONE guisp=NONE gui=NONE ctermfg=66 ctermbg=NONE cterm=NONE
hi jinjastring guifg=#0095ec guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=39 ctermbg=230 cterm=NONE
hi pythonfunction guifg=#ff0000 guibg=NONE guisp=NONE gui=NONE ctermfg=196 ctermbg=NONE cterm=NONE
hi jinjacomment guifg=#00a200 guibg=#002300 guisp=#002300 gui=NONE ctermfg=34 ctermbg=22 cterm=NONE
hi jinjanumber guifg=#d90a4f guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=161 ctermbg=230 cterm=NONE
hi pythoncoding guifg=#ff0088 guibg=NONE guisp=NONE gui=NONE ctermfg=198 ctermbg=NONE cterm=NONE
hi jinjatagblock guifg=#ff0009 guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=196 ctermbg=230 cterm=NONE
hi jinjastatement guifg=#ff680a guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=202 ctermbg=230 cterm=NONE
hi pythonbuiltinfunc guifg=#327cbc guibg=NONE guisp=NONE gui=NONE ctermfg=4 ctermbg=NONE cterm=NONE
hi pythonrun guifg=#ff0088 guibg=NONE guisp=NONE gui=NONE ctermfg=198 ctermbg=NONE cterm=NONE
hi pythonclass guifg=#ff0088 guibg=NONE guisp=NONE gui=NONE ctermfg=198 ctermbg=NONE cterm=NONE
hi pythonbuiltinobj guifg=#327cbc guibg=NONE guisp=NONE gui=NONE ctermfg=4 ctermbg=NONE cterm=NONE
hi jinjaspecial guifg=#0091ff guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=33 ctermbg=230 cterm=NONE
hi jinjavariable guifg=#a5e73c guibg=#fbf4c7 guisp=#fbf4c7 gui=NONE ctermfg=149 ctermbg=230 cterm=NONE
hi cream_showmarkshl guifg=#1a1a1a guibg=#aacc77 guisp=#aacc77 gui=NONE ctermfg=234 ctermbg=150 cterm=NONE
hi badword guifg=#ff9999 guibg=#003333 guisp=#003333 gui=NONE ctermfg=210 ctermbg=23 cterm=NONE
hi cif0 guifg=#d8d8d8 guibg=NONE guisp=NONE gui=NONE ctermfg=188 ctermbg=NONE cterm=NONE
hi djangostatement guifg=#007900 guibg=#ddffaa guisp=#ddffaa gui=NONE ctermfg=2 ctermbg=193 cterm=NONE
hi doctrans guifg=#ffffff guibg=#ffffff guisp=#ffffff gui=NONE ctermfg=15 ctermbg=15 cterm=NONE
hi helpnote guifg=#1a1a1a guibg=#ffd700 guisp=#ffd700 gui=NONE ctermfg=234 ctermbg=220 cterm=NONE
hi doccode guifg=#00c400 guibg=NONE guisp=NONE gui=NONE ctermfg=40 ctermbg=NONE cterm=NONE
hi docspecial guifg=#4876ff guibg=NONE guisp=NONE gui=NONE ctermfg=69 ctermbg=NONE cterm=NONE
hi htmlstatement guifg=#c96d9b guibg=NONE guisp=NONE gui=NONE ctermfg=175 ctermbg=NONE cterm=NONE
hi debug guifg=#ffffff guibg=#006400 guisp=#006400 gui=NONE ctermfg=15 ctermbg=22 cterm=NONE
hi warningmsg guifg=#ffffff guibg=#00008b guisp=#00008b gui=NONE ctermfg=15 ctermbg=18 cterm=NONE
hi ifdefifout guifg=#c3c3c3 guibg=NONE guisp=NONE gui=NONE ctermfg=7 ctermbg=NONE cterm=NONE
