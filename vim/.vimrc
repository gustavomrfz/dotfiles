filetype off
execute pathogen#infect()
filetype plugin indent on
colorscheme darkbone_custom
syntax enable

set encoding=utf8
set wrap     " Always wrap long lines
set linebreak
set colorcolumn=+1        " highlight column after 'textwidth'
"highlight ColorColumn ctermbg=grey guibg=red
set textwidth=78
set paste
set autoindent        " Indent at the beginning of last line
set ruler
set showcmd
set incsearch
set number
set tabstop=4 softtabstop=0 expandtab shiftwidth=2 smarttab " iTab with 2 spaces
set list    " Show tabs
set cmdheight=2         " Height in lines of insert mode window
set statusline+=%#warningmsg#     " Syntastic settings
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Nerdtree
" autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
set guifont=DroidSansMono\ Nerd\ Font\ 11
map <C-n> :NERDTreeToggle<CR>

" Synastic settings
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Bash Support
let g:BASH_AuthorName   = 'Gustavo Moreno'
let g:BASH_Email        = 'gustavo@laenredadera.net'
let g:BASH_Company      = ''
call pathogen#helptags()

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256


